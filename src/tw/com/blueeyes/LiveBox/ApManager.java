package tw.com.blueeyes.LiveBox;

import java.lang.reflect.Method;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.util.Log;


public class ApManager {
	// check whether wifi hotspot on or off
	public static boolean isApOn(Context context) {
		WifiManager wifimanager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
		try {
//			Method method = wifimanager.getClass().getDeclaredMethod("isWifiApEnabled");
//			method.setAccessible(true);
//			return (Boolean) method.invoke(wifimanager);
			
			Method method = wifimanager.getClass().getMethod("isWifiApEnabled");
	        return (Boolean) method.invoke(wifimanager);
		} catch (Throwable ignored) {
		}
		return false;
	}

	// toggle wifi hotspot on or off
	public static void configApState(Context context) {
		try {
			// if WiFi is on, turn it off
			WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

	        Boolean state = isApOn(context);

	        WifiConfiguration apConfig = getApConfiguration(context);

	        Method method = wifi.getClass().getMethod(
	                "setWifiApEnabled", WifiConfiguration.class, Boolean.TYPE);

	        boolean enabled = !state;

	        if (enabled) {
	            wifi.setWifiEnabled(!enabled);
	            method.invoke(wifi, apConfig, enabled);
	            Log.v("ap" , "AP ON");
	        } else {
	            method.invoke(wifi, apConfig, enabled);
	            wifi.setWifiEnabled(enabled);
	            Log.v("ap" , "AP OFF");
	        }
		} catch (Exception e) {
			Log.v("hotspot" , e.toString());
			e.printStackTrace();
		}
	}
	
	private static WifiConfiguration getApConfiguration(Context context) {
		SharedPreferences Camera_data = PreferenceManager.getDefaultSharedPreferences(context);
        WifiConfiguration apConfig = new WifiConfiguration();
        //配置热点的名称
        apConfig.SSID = "Livebox_" + Camera_data.getString("hotspot", "001");
        apConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        apConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//        apConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
//        apConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        //配置热点的密码
//        apConfig.preSharedKey = "yourPassword";
        return apConfig;
    }
}