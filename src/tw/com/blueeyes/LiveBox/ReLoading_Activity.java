package tw.com.blueeyes.LiveBox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.io.IOUtils;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.usb.UsbManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

public class ReLoading_Activity extends Activity {
	int count = 6;
	TextView text;
	SharedPreferences Camera_data;
	int type;
	final int eth0 = 0x01;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		if (Build.VERSION.RELEASE.equals("4.4.2")) {
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		} 
		setContentView(R.layout.activity_reloading);
		Bundle bundle = this.getIntent().getExtras();
		type = bundle.getInt("type");

		Camera_data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor editor = Camera_data.edit();
		editor.putInt("activity", 0x1c);
		editor.putInt("show", 1);
		editor.commit();

		text = (TextView) findViewById(R.id.loading_tv);

		if (Camera_data.getBoolean("ap_reon", true) && (Camera_data.getInt("net_type", eth0) == eth0)) {
			if (ApManager.isApOn(ReLoading_Activity.this)) {
				ApManager.configApState(ReLoading_Activity.this);
			}
		}

		new Thread() {
			public void run() {
				try {
					while (count != 0) {
						Thread.sleep(1000);
						if (type == 0) {
							Message msg = new Message();
							msg.arg1 = count;
							msg.arg2 = 0;
							handler.sendMessage(msg);
							count--;
						} else {
							Message msg = new Message();
							msg.arg1 = count;
							msg.arg2 = 1;
							handler.sendMessage(msg);
							count--;
						}

					}
					Intent intent = new Intent();
					if (Camera_data.getBoolean("OnDemand", false)) {
						intent.setClass(ReLoading_Activity.this, Ondemand_LiveBox_Activity.class);
					} else {
						intent.setClass(ReLoading_Activity.this, LiveBox_Activity.class);
					}
					startActivity(intent);
					finish();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();

	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg2) {
			case 0x00:
				text.setText("Setting changed from net, wait " + msg.arg1 + " secs.");
				break;
			case 0x01:
				text.setText("Setting changed from USB, wait " + msg.arg1 + " secs.");
				break;
			}

		}
	};

}
