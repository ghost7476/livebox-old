package tw.com.blueeyes.LiveBox;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Surface;

public class Decode_h264 {
	private MediaCodec mediaCodec;
	byte h264[] = null;
	String TAG = "h264";
	boolean getdata = false;
	int size;
	Surface surface = null;
	// BufferedOutputStream outputStream;
	long gettime = 0;
	List<byte[]> bytes = new ArrayList<byte[]>();
	long startTime = 0;
	long endTime = 0;
	long totTime = 0;
	long videotime = 0;
	int count = 0;
	boolean start = true;
	boolean flag = false;
	boolean inputfinish = false;
	byte[] videodata;
	byte[] tem;
	int width = 0;
	int height = 0;
	final char[] hexArray = "0123456789ABCDEF".toCharArray();
	MediaFormat format;
	int count_frame = 0;

	@SuppressLint("NewApi")
	public Decode_h264(Surface sur) {
		this.surface = sur;
		String key_mime = "video/avc";
		mediaCodec = MediaCodec.createDecoderByType(key_mime);
		format = MediaFormat.createVideoFormat(key_mime, 1920, 1080);
		try {
			mediaCodec.configure(format, surface, null, 0);
		} catch (Exception e) {
			Log.v("error", e.toString());
			e.getStackTrace();
		}

		mediaCodec.start();
		// show.start();
	}

	public void setformat(int width, int height) {
		try {
			clsoe();
		} catch(Exception e) {
			mediaCodec = null;
			e.printStackTrace();
		}	
		this.width = width;
		this.height = height;
		String key_mime = "video/avc";
		mediaCodec = MediaCodec.createDecoderByType(key_mime);
		format = MediaFormat.createVideoFormat(key_mime, width, height);
		try {
			mediaCodec.configure(format, surface, null, 0);
		} catch (Exception e) {
			Log.v("error", e.toString());
			e.getStackTrace();
		}

		mediaCodec.start();
	}

	public void get_h264(final byte buffer[], final long video_time) {
		videotime = video_time;
		decode(buffer, videotime);
	}

	public void flush() {
		mediaCodec.flush();
	}

	public byte[] streamCopy(List<byte[]> srcArrays) {
		byte[] destAray = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			for (byte[] srcArray : srcArrays) {
				bos.write(srcArray);
			}
			bos.flush();
			destAray = bos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bos.close();
			} catch (IOException e) {
			}
		}
		return destAray;
	}

	private void decode(final byte video[], final long videotime) {
		try {
			ByteBuffer[] inputBuffers = mediaCodec.getInputBuffers();
			ByteBuffer[] outputBuffers = mediaCodec.getOutputBuffers();
			MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
			
			
			int inIndex = mediaCodec.dequeueInputBuffer(-1);
			if (inIndex >= 0) {
				ByteBuffer buffer = inputBuffers[inIndex];
				buffer.clear();
				buffer.put(video);
				mediaCodec.queueInputBuffer(inIndex, 0, video.length, 0, 0);
			}
//			inputfinish = true;
//			 Log.v(TAG, "input finish");
			int outIndex = mediaCodec.dequeueOutputBuffer(info, videotime);

			switch (outIndex) {
			case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
				Log.d(TAG, "INFO_OUTPUT_BUFFERS_CHANGED");
				outputBuffers = mediaCodec.getOutputBuffers();
				break;
			case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
				Log.d(TAG, "New format " + mediaCodec.getOutputFormat());
				break;
			case MediaCodec.INFO_TRY_AGAIN_LATER:
				Log.d(TAG, "dequeueOutputBuffer timed out!");
				// Thread.sleep(30);
				break;
			default:
				// ByteBuffer outbuffer = outputBuffers[outIndex];
				mediaCodec.releaseOutputBuffer(outIndex, true);
				count_frame++;
				break;
			}
			// endTime = System.currentTimeMillis();
			// totTime = endTime - startTime;
			// Log.v(TAG, "decode time2 " + totTime);

		} catch (Exception e) {
			// mediaCodec.flush();
			MyLog.e(TAG, "video decode got exception");
			Log.e(TAG, e.toString());
			setformat(width, height);
		}
//		Log.v(TAG, "decode end");
	}

	/*
	 * Thread decode = new Thread() { public void run() { ByteBuffer[]
	 * inputBuffers = mediaCodec.getInputBuffers();
	 * 
	 * while (true) { try { if (flag) { videodata = tem; flag = false; //
	 * startTime = System.currentTimeMillis(); // Log.v(TAG, "start decode");
	 * int inIndex = mediaCodec.dequeueInputBuffer(videotime); if (inIndex >= 0)
	 * { ByteBuffer buffer = inputBuffers[inIndex]; buffer.clear();
	 * buffer.put(videodata); mediaCodec.queueInputBuffer(inIndex, 0,
	 * videodata.length, 0, 0); inputfinish = true; }
	 * 
	 * // Log.v(TAG, "input finish"); // endTime = System.currentTimeMillis();
	 * // totTime = endTime - startTime; // Log.v(TAG, "decode time2 " +
	 * totTime); } else { Thread.sleep(5); // Log.v(TAG ,"decode sleep"); if
	 * (!LiveBox_Activity.run) { break; } } } catch (Exception e) { Log.v(TAG,
	 * "video deocode 掛了"); Log.v(TAG, e.toString()); } } } };
	 */

//	Thread show = new Thread() {
//		public void run() {
//			MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
//			while (true) {
//				try {
//					if (inputfinish) {
//						count = 0;
//						ByteBuffer[] outputBuffers = mediaCodec.getOutputBuffers();
//						inputfinish = false;
//						int outIndex = mediaCodec.dequeueOutputBuffer(info, videotime);
//
//						switch (outIndex) {
//						case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
//							Log.d(TAG, "INFO_OUTPUT_BUFFERS_CHANGED");
//							outputBuffers = mediaCodec.getOutputBuffers();
//							break;
//						case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
//							Log.d(TAG, "New format " + mediaCodec.getOutputFormat());
//							break;
//						case MediaCodec.INFO_TRY_AGAIN_LATER:
//							Log.d(TAG, "dequeueOutputBuffer timed out!");
//							// Thread.sleep(30);
//							break;
//						default:
//							// ByteBuffer outbuffer = outputBuffers[outIndex];
//							mediaCodec.releaseOutputBuffer(outIndex, true);
//							// Log.v(TAG, "finish decode");
//							count_frame++;
//							break;
//						}
//						// Log.v(TAG, "show finish");
//					} else {
//						// Log.v(TAG, "wait data");
//						Thread.sleep(2);
//
//						if (!LiveBox_Activity.run) {
//							break;
//						}
//					}
//				} catch (Exception e) {
//					MyLog.e(TAG, "video decode got exception");
//					mediaCodec.flush();
//					Log.v(TAG, "video 掛了");
//					Log.v(TAG, e.toString());
//				}
//			}
//
//			clsoe();
//		}
//	};

	
	public void clsoe() {
			mediaCodec.stop();
			mediaCodec.release();

	}
}
