package tw.com.blueeyes.LiveBox;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootBroadcaseReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {

			Intent bootintent = new Intent(context, LiveBox_Activity.class);
			bootintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(bootintent);

			System.exit(0);
		}
	}

}
