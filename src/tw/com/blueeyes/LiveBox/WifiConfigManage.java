package tw.com.blueeyes.LiveBox;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.DhcpInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class WifiConfigManage {

	public  WifiManager wifi_service;
	public  WifiInfo wifiInfo;

	public  boolean SSIDEXIST = true;
	public  String SSID;
	public  String wifipassword;
	public  String ipaddress;
	public  String gateway;
	public  String wifidns1;
	public  String wifidns2;
	public  String wifi_type;
	public  int networkPrefixLength;
	final  int no_pwd = 0x00;
	final  int WEP = 0xa0;
	final  int WPA2 = 0xb0;
	final  int WPA = 0xc0;

	public WifiConfigManage(WifiManager wifi_service, WifiInfo wifiInfo) {
		this.wifi_service = wifi_service;
		this.wifiInfo = wifiInfo;
	}
	
	
	
	/* Open Wifi and Set Wifi */
	public void openWifi(String wifi_type, String SSID, String Password, int Type, String ipaddress,
			int networkPrefixLength, String gateway, String wifidns1, String wifidns2) {

		WifiConfiguration wificonfig = null;
		try {
			wificonfig = createWiFi(SSID, Password, Type);
		} catch (Exception ex) {
			Log.e("Network INIT", "createWiFi", ex);
		}

		if (wificonfig == null) {
			Log.e("Network INIT", "wificonfig==null");
		}

		// 做到哪 blueeyes2 沒問題 測我手機WIFI有問題
		try {
			// if (wifi_type.equals("STATIC")) {
			setIpAssignment("STATIC", wificonfig);
			setIpAddress(ipaddress, networkPrefixLength, wificonfig); // 24
																		// = //
																		// networkPrefixLength
			setGateway(gateway, wificonfig);
			setDNS(wifidns1, wifidns2, wificonfig);
			Log.i("Sian Network", "靜態_ip設置成功！");
			// }

		} catch (Exception e) {
			// if (wifi_type.equals("STATIC")) {
			Log.e("Sian Network", "靜態_ip設置失敗！", e);
			// }

			// Toast.makeText(this,"WiFi 設置失敗 "+ SSID,Toast.LENGTH_LONG).show();
		}

		int wcgID = wifi_service.addNetwork(wificonfig);
		wifi_service.updateNetwork(wificonfig); // apply the setting
		wifi_service.saveConfiguration();
		wifi_service.enableNetwork(wcgID, true);

		try {
			Thread.sleep(3500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/* Create a Wifi SSID&Password */
	public  WifiConfiguration createWiFi(String SSID, String Password, int Type) {

		WifiConfiguration config = new WifiConfiguration();
		config.allowedAuthAlgorithms.clear();
		config.allowedGroupCiphers.clear();
		config.allowedKeyManagement.clear();
		config.allowedPairwiseCiphers.clear();
		config.allowedProtocols.clear();
		config.SSID = "\"" + SSID + "\"";

		WifiConfiguration tempConfig = IsExsits(SSID); // 此方法是在檢察WIFI內是否有相同的SSID，不判斷每次都會新增一個SSID
		if (tempConfig != null) {
			wifi_service.removeNetwork(tempConfig.networkId);
			// return config;
		}

		Log.i("Network", "Type Password=" + Type + "," + Password);

		Password = Password.trim();

		if (Type == no_pwd) // WIFICIPHER_NOPASS
		{
			// config.wepKeys[0] = ""; //空值表示不需要密碼
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);// WPA
																			// //
																			// used
			config.wepTxKeyIndex = 0;
		} else if (Type == WEP) // WIFICIPHER_WEP
		{
			config.hiddenSSID = true; // 隱藏SSID
			if (Password.length() >= 10) { // 必須大寫
				config.wepKeys[0] = Password.toUpperCase();

			} else { // 5個ASCII
				config.wepKeys[0] = "\"" + Password + "\"";
			}
			config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);// Shared
																						// Key
																						// authentication
																						// (requires
																						// static
																						// WEP
																						// keys)
			// 配置密碼類型
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);// AES
																				// Constant
																				// Value:
																				// 3
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);// Temporal
																				// Key
																				// Value:2
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);// WEP
																				// (40-bit
																				// key)
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);// WEP
																					// (104-bit
																					// key)
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);// WPA
																			// is
																			// not
																			// used
			config.wepTxKeyIndex = 0;
		} else if (Type == WPA2) // WIFICIPHER_WEP2
		{
			config.preSharedKey = "\"" + Password + "\"";
			config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
			config.allowedProtocols.set(WifiConfiguration.Protocol.RSN); // For WPA2
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
			config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
			config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
			config.status = WifiConfiguration.Status.ENABLED;
		} else if (Type == WPA) // WIFICIPHER_WPA
		{
			config.preSharedKey = "\"" + Password + "\"";// 使用WPA密碼
			config.hiddenSSID = true; // 隱藏SSID
			config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);// Open
																					// System
																					// authentication
																					// (required
																					// for
																					// WPA/WPA2)
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);// Temporal
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);// EAP
			config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);// Temporal
																						// Key
																						// Value:1
			// config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);//WPA
			/* 替換上面那句，否則當WIFI需要輸入密碼時，無法加入網路 */
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);// AES
																				// Constant
																				// Value:
																				// 3
			config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);// AES
																						// Constant
																						// Value:
																						// 2
			config.status = WifiConfiguration.Status.ENABLED;
		}

		return config;

	}

	/* 判斷SSID */
	public WifiConfiguration IsExsits(String SSID) {
		try {
			List<WifiConfiguration> existingConfigs = wifi_service.getConfiguredNetworks();

			for (WifiConfiguration existingConfig : existingConfigs) {
				// Log.d("Network",existingConfig.SSID +" =? "+SSID);
				if (existingConfig.SSID.equals("\"" + SSID + "\"")) {
					return existingConfig;
				}
			}
		} catch (NullPointerException ex) {
			return null;
		}

		return null;
	}

	/** 弄懂這些 **/
	public static void setIpAssignment(String assign, WifiConfiguration wifiConf) throws SecurityException,
			IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
		setEnumField(wifiConf, assign, "ipAssignment");
	}

	public static void setIpAddress(String addr, int prefixLength, WifiConfiguration wifiConf)
			throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
			NoSuchMethodException, ClassNotFoundException, InstantiationException, InvocationTargetException {
		Object linkProperties = getField(wifiConf, "linkProperties");
		if (linkProperties == null)
			return;
		Class laClass = Class.forName("android.net.LinkAddress");
		Constructor laConstructor = laClass.getConstructor(new Class[] { InetAddress.class, int.class });

		addr = addr.trim();
		if (!(addr.length() < 7 || addr.length() > 15)) {
			try {
				InetAddress ADDR = InetAddress.getByName(addr);
				Object linkAddress = laConstructor.newInstance(ADDR, prefixLength);
				ArrayList mLinkAddresses = (ArrayList) getDeclaredField(linkProperties, "mLinkAddresses");
				mLinkAddresses.clear();

				mLinkAddresses.add(linkAddress);

			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void setGateway(String gateway, WifiConfiguration wifiConf) throws SecurityException,
			IllegalArgumentException, NoSuchFieldException, IllegalAccessException, ClassNotFoundException,
			NoSuchMethodException, InstantiationException, InvocationTargetException {
		Object linkProperties = getField(wifiConf, "linkProperties");
		if (linkProperties == null)
			return;
		Class routeInfoClass = Class.forName("android.net.RouteInfo");
		Constructor routeInfoConstructor = routeInfoClass.getConstructor(new Class[] { InetAddress.class });

		gateway = gateway.trim();
		if (!(gateway.length() < 7 || gateway.length() > 15)) {
			try {
				InetAddress Gateway = InetAddress.getByName(gateway);
				Object routeInfo = routeInfoConstructor.newInstance(Gateway);
				ArrayList mRoutes = (ArrayList) getDeclaredField(linkProperties, "mRoutes");
				mRoutes.clear();
				if (!gateway.equals(""))
					mRoutes.add(routeInfo);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void setDNS(String dns1, String dns2, WifiConfiguration wifiConf) throws SecurityException,
			IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
		Object linkProperties = getField(wifiConf, "linkProperties");
		if (linkProperties == null)
			return;

		ArrayList<InetAddress> mDnses = (ArrayList<InetAddress>) getDeclaredField(linkProperties, "mDnses");
		mDnses.clear(); // or add a new dns address , here I just want to
						// replace DNS1

		try {
			dns1 = dns1.trim();
			dns2 = dns2.trim();
			if (dns1.length() < 7 || dns1.length() > 15) {
				InetAddress DNS1 = InetAddress.getByName("8.8.8.8"); // google
				mDnses.add(DNS1);
			} else {
				InetAddress DNS1 = InetAddress.getByName(dns1);
				mDnses.add(DNS1);
			}

			if (dns2.length() < 7 || dns2.length() > 15) {
				InetAddress DNS2 = InetAddress.getByName("8.8.4.4"); // google
				mDnses.add(DNS2);
			} else {
				InetAddress DNS2 = InetAddress.getByName(dns2);
				mDnses.add(DNS2);
			}

		} catch (Exception e) {
			Log.e("Sian", "靜態IP設置  dns1&dns2");
		}

	}

	public static Object getField(Object obj, String name) throws SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		Field f = obj.getClass().getField(name);
		Object out = f.get(obj);
		return out;
	}

	public static Object getDeclaredField(Object obj, String name) throws SecurityException, NoSuchFieldException,
			IllegalArgumentException, IllegalAccessException {
		Field f = obj.getClass().getDeclaredField(name);
		f.setAccessible(true);
		Object out = f.get(obj);
		return out;
	}

	public static void setEnumField(Object obj, String value, String name) throws SecurityException,
			NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Field f = obj.getClass().getField(name);
		f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));
	}
}
