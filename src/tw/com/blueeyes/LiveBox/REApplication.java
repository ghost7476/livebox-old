package tw.com.blueeyes.LiveBox;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class REApplication extends Application {//implements  Thread.UncaughtExceptionHandler {   
    PendingIntent restartIntent;  
  
    @Override  
    public void onCreate() {  
        super.onCreate();  
  
        Intent intent = new Intent();  
        intent.setClassName("tw.com.blueeyes.LiveBox", "tw.com.blueeyes.LiveBox.Loading_Activity");  
        restartIntent = PendingIntent.getActivity(getApplicationContext(), 0,  
                intent, Intent.FLAG_ACTIVITY_NEW_TASK);  
        Thread.setDefaultUncaughtExceptionHandler(restartHandler);
    }  
  
    public UncaughtExceptionHandler restartHandler = new UncaughtExceptionHandler() {  
        public void uncaughtException(Thread thread, Throwable ex) {  
        	Log.e("crash" , ex.toString());
        	ex.printStackTrace();
        	System.gc();
            AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);  
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10000, restartIntent); 
            new ActivityContrl().finishProgram(); 
        }  
    };  
	
    
    public class ActivityContrl {  
    	         private List<Activity> activityList = new ArrayList<Activity>();  
    	       
    	         public void remove(Activity activity) {  
    	             activityList.remove(activity);  
    	         }  
    	       
    	         public void add(Activity activity) {  
    	             activityList.add(activity);  
    	         }  
    	       
    	        public void finishProgram() {  
    	             for (Activity activity : activityList) {  
    	                 if (null != activity) {  
    	                     activity.finish();  
    	                 }  
    	             }  
    	             android.os.Process.killProcess(android.os.Process.myPid());  
    	         }  
    	     }
}  
