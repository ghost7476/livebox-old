package tw.com.blueeyes.LiveBox;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.io.IOUtils;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.usb.UsbManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

public class Loading_Activity extends Activity {
	SharedPreferences Camera_data;
	private static String enkey;
	private static String dekey;
	private static String dekey_pro;
	final int wifi = 0x00;
	final int eth0 = 0x01;
	TextView loading;
	private String keypath = Environment.getExternalStorageDirectory().getPath()
			+ "/Android/data/tw.com.blueeyes.LiveBox/";
	private String live_keyname = ".LiveBox";
	private static final String privatekey = "39U07Y4TCi1gJQgXjyROAy3v9+4sbaRJn+PDUqC/3PO0Fq5hHQO8dXCN/t5jK/t0rIiJ4ZocSqg2s4aGjI9dj4PxZPNIQAhBQQq/qrE6IMW46wB+L9/ZzfoPYO6qtl+53eoa6TE2MCtJo4TOQMjFfIA3jS00aGnC7jJjshwf6C8y+WOBQojSd/ZwvyhyU52brFJvR+oAQnkgpmfQBnWCzee0RuShnPI5UcR+ViGUyM5n3U9myesdsAN4yW9LGPeQBaovrsypvbpql02HluA2Wcu2I/SINYapRxVwMK+e/IDZx2Z1Dx8aaVuK8GIxiLR+5XrzmK2r0SMgiCsDWFSPg0QYaz+i3JwRj+RXnwk/wZ4Bu8+ZlaoSUw1Y3giGahHEUNpRWIWAHznOVztV5c/geYwor9RohW+sDOVezvJbbSWYX5oV48ajSTKvzrhS90bxgtV8Du0EiK2raoF7AWm9bdNteMqsc/ds4/yAtwrkTrA8NT36meVQ2I3nww70Cfu9CEIbMBZo";
	private static final String privatekey_pro = "63801Y62GxWeqhNR9sJZDIW92EBa8MM4mut8zBWpKDq3WIttepjoAdFzSG5n1rZ2vDyDCfrnP4hxhlvwM9fm01BzNb8JN+jH8mSf8B6LlCS0NVbMtDyqMBopKBQoIplmHZb6p3OIxP53hrLBifd2+YDtEG6Y0RZG/Oc2HZotEoWCtCbxazyeU0LUVpP7llNXQg6UWMF+DVvFWVHfi+B=";
	private static String key = "";
	private static String keypro = "";
//	WifiManager wifi_service;
	private File key_path = new File(keypath);
	private File Live_Key = new File(keypath + live_keyname);
	String usbpath_old = "/mnt/usb_storage/USB_DISK0/udisk0/";
	String usbpath = "/mnt/usb_storage/USB_DISK0/";
	File usbfilePath_old = new File(usbpath_old);
	File usbfilePath = new File(usbpath);
	private File USB_Key = new File(usbpath + ".LiveBox");
	private File licdatakeyPath = new File(usbpath + "licdata_livebox.key");
	private File licdatakeyPath_pro = new File(usbpath + "licdata_livebox+.key");
	final int realtime_close = 0x0a;
	final int realtime_general = 0x0b;
	final int realtime_smooth = 0x0c;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		if (Build.VERSION.RELEASE.equals("4.4.2")) {
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		} 
		setContentView(R.layout.activity_loading);
		setWallpaper();
		Camera_data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor editor = Camera_data.edit();
		editor.putInt("show", 1);
		editor.commit();

//		try {
//				Process proc = Runtime.getRuntime().exec("su");
//				DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
//				opt.writeBytes("ifconfig eth0 up\n");
//				opt.writeBytes("ifconfig eth0 " + Camera_data.getString("eth0", "192.168.1.51") + " netmask "
//						+ Camera_data.getString("mask", "255.255.255.0") + "\n");
//				opt.writeBytes("route add default gw " + Camera_data.getString("geteway", "192.168.1.1")
//						+ " dev eth0\n");
//				opt.writeBytes("exit\n");
//				opt.flush();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			Log.v("test", e.toString());
//		}
//		wifi_service = (WifiManager) getSystemService(WIFI_SERVICE);
		loading = (TextView) findViewById(R.id.loading_tv);
		loading.setText("Loading...");

		if (!key_path.exists()) {
			key_path.mkdirs();
		}

		if(usbfilePath.exists()) {
			if(usbfilePath_old.exists()) {
				if(usbfilePath != usbfilePath_old) {
					usbfilePath = usbfilePath_old;
					usbpath = usbpath_old;
					USB_Key = new File(usbpath + ".LiveBox");
					licdatakeyPath = new File(usbpath + "licdata_livebox.key");
					licdatakeyPath_pro = new File(usbpath + "licdata_livebox+.key");
				}
			}
		}
		
		
		check_key();

//		editor = Camera_data.edit();
//		editor.putBoolean("OnDemand", true);
//		editor.commit();
//		Intent intent = new Intent();
//		intent.setClass(Loading_Activity.this, Ondemand_LiveBox_Activity.class);
//		startActivity(intent);
//		finish();
	}

	public void check_key() {		
		if (licdatakeyPath.exists() || licdatakeyPath_pro.exists()) {
			if (licdatakeyPath.exists()) {
				FileInputStream fin2 = null;
				try {
					fin2 = new FileInputStream(licdatakeyPath);
					boolean blueeyes = compareBlueeyes_default(fin2); // 比對
																		// //
																		// (KEY=licdata.key)?
					Toast.makeText(Loading_Activity.this, "Now is default version!!!", Toast.LENGTH_SHORT).show();
					if (blueeyes) {
						// Message msg = new Message();
						// msg.what = 0x0a;
						// handler.sendMessage(msg);
						Intent intent = new Intent();
						if (Camera_data.getBoolean("OnDemand", false)) {
							intent.setClass(Loading_Activity.this, Ondemand_LiveBox_Activity.class);
						} else {
							intent.setClass(Loading_Activity.this, LiveBox_Activity.class);
						}
						startActivity(intent);
						finish();
					} else {
						Message msg = new Message();
						msg.what = 0x0b;
						handler.sendMessage(msg);
					}
					// 如果等於就編碼後存入KEY
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					if (fin2 != null) {
						try {
							fin2.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			} else if (licdatakeyPath_pro.exists()) {
				FileInputStream fin2 = null;
				try {
					fin2 = new FileInputStream(licdatakeyPath_pro);
					boolean blueeyes = compareBlueeyes_pro(fin2); // 比對
																	// //
																	// (KEY=licdata.key)?
					Toast.makeText(Loading_Activity.this, "Now is pro version!!!", Toast.LENGTH_SHORT).show();
					if (blueeyes) {
						// Message msg = new Message();
						// msg.what = 0x0a;
						// handler.sendMessage(msg);
						Intent intent = new Intent();
						if (Camera_data.getBoolean("OnDemand", false)) {
							intent.setClass(Loading_Activity.this, Ondemand_LiveBox_Activity.class);
						} else {
							intent.setClass(Loading_Activity.this, LiveBox_Activity.class);
						}
						startActivity(intent);
						finish();
					} else {
						Message msg = new Message();
						msg.what = 0x0b;
						handler.sendMessage(msg);
					}
					// 如果等於就編碼後存入KEY
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					if (fin2 != null) {
						try {
							fin2.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} else if (Live_Key.exists()) {
			check.start();
		} else if (USB_Key.exists()) {
			try {
				FileInputStream fis = new FileInputStream(USB_Key);
				byte[] bytes = IOUtils.toByteArray(fis);
				FileOutputStream fos = new FileOutputStream(Live_Key);
				fos.write(bytes);
				fos.flush();
				fos.close();
				check.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			registerUsbStateReceiver();
			Message msg = new Message();
			msg.what = 0x0c;
			handler.sendMessage(msg);
//			loading.setText("Please Import License KEY");
		}
	}

	Thread check = new Thread() {
		public void run() {
			key = key();
			keypro = key_pro();
			FileInputStream blueeyesfile = null;
			InputStreamReader isr = null;
			String str = null;
			try {
				blueeyesfile = new FileInputStream(Live_Key);
				isr = new InputStreamReader(blueeyesfile);
				str = IOUtils.toString(isr);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (isr != null || blueeyesfile != null) {
					try {
						isr.close();
						blueeyesfile.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			Log.v("key", "key " + key);
			Log.v("key", "keypro " + keypro);
			Log.v("key", "livekey " + str);
			dekey = selfEncode(key, key);
			dekey_pro = selfEncode(keypro, keypro);
			Log.v("key", "dekey " + dekey);
			Log.v("key", "dekey_pro " + dekey_pro);
			// dekey = selfDecode(key, str); // blueeyeskey 編碼過後的KEY 解碼後判斷
			int dlen = 0, dlen_pro = 0;

			if (dekey != null) {
				dlen = dekey.length();
			}

			if (dekey_pro != null) {
				dlen_pro = dekey.length();
			}
			int keylen = str.length();
			Log.v("test", "dlen " + dlen + " keylen " + keylen);
			if (str.equals(dekey)) {
				SharedPreferences.Editor editor = Camera_data.edit();
				editor.putBoolean("OnDemand", false);
				editor.commit();
				Intent intent = new Intent();
				intent.setClass(Loading_Activity.this, LiveBox_Activity.class);
				startActivity(intent);
				finish();
			} else if (str.equals(dekey_pro)) {
				SharedPreferences.Editor editor = Camera_data.edit();
				editor.putBoolean("OnDemand", true);
				editor.commit();
				Intent intent = new Intent();
				intent.setClass(Loading_Activity.this, Ondemand_LiveBox_Activity.class);
				startActivity(intent);
				finish();
			} else {
				Live_Key.delete();
				Message msg = new Message();
				msg.what = 0x0b;
				handler.sendMessage(msg);
				registerUsbStateReceiver();
			}
		}
	};

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0x0a:
				loading.setText("Key OK");
				break;
			case 0x0b:
				loading.setText("Key not correct, please check your key");
				break;
			case 0x0c:
				loading.setText("Please Import License KEY");
				break;
			}
		}
	};

//	private BroadcastReceiver wifi_receiver = new BroadcastReceiver() {
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
//
//			switch (state) {
//			case WifiManager.WIFI_STATE_DISABLED:
//				Log.v("wifi", "it is disabled");
//				break;
//			case WifiManager.WIFI_STATE_ENABLED:
//				Log.v("wifi", "it is enabled");
//				unregisterReceiver(wifi_receiver);
//				check_key();
//				break;
//			case WifiManager.WIFI_STATE_DISABLING:
//				Log.v("wifi", "it is switching off");
//				break;
//			case WifiManager.WIFI_STATE_ENABLING:
//				Log.v("wifi", "wifi is getting enabled");
//				break;
//			default:
//				Log.v("wifi", "not working properly");
//				unregisterReceiver(wifi_receiver);
//				check_key();
//				break;
//			}
//		}
//	};

	// 監控USB連接狀態
	private BroadcastReceiver mUsbStateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
				loading.setText("Check License KEY");
				new Thread() {
					public void run() {
						try {
							Thread.sleep(2000);
							
							if(usbfilePath.exists()) {
								if(usbfilePath_old.exists()) {
									if(usbfilePath != usbfilePath_old) {
										usbfilePath = usbfilePath_old;
										usbpath = usbpath_old;
										USB_Key = new File(usbpath + ".LiveBox");
										licdatakeyPath = new File(usbpath + "licdata_livebox.key");
										licdatakeyPath_pro = new File(usbpath + "licdata_livebox+.key");
									}
								}
							}
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						check_key();
					}
				}.start();
			}
		}
	};

	// 判斷兩文件是否一樣
	private boolean compareBlueeyes(String privatekey, FileInputStream fin2) {
		boolean blueeyes = false;

		InputStreamReader isr = new InputStreamReader(fin2);
		try {
			int len1 = privatekey.length(); // 內部 privatekey
			int len2 = fin2.available(); // 外部KEY
			Log.v("key", "len1,len2=" + len1 + "," + len2);

			if (len1 == len2) {
				String str = IOUtils.toString(isr);

				if ((str).equals(privatekey)) // 匯入KEY
				{
					// 刪除licdata.key
					// licdatakeyPath.delete();

					FileOutputStream fout = new FileOutputStream(Live_Key);
					// Log.v("key" ,"key " + key);
					key = key();
					enkey = selfEncode(key, key);
					// Log.v("key" , "key " + enkey);
					byte[] contentInBytes = enkey.getBytes();

					fout.write(contentInBytes);
					fout.flush();
					fout.close();
					isr.close();
					fin2.close();

					return blueeyes = true;
				} else {
					return blueeyes = false;
				}
			} else {
				return blueeyes = false;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return blueeyes;
	}

	// 判斷兩文件是否一樣
	private boolean compareBlueeyes_default(FileInputStream fin2) {
		boolean blueeyes = false;
		InputStreamReader isr = new InputStreamReader(fin2);
		try {
			int len1 = privatekey.length(); // 內部 privatekey
			int len2 = fin2.available(); // 外部KEY
			Log.v("key", "len1,len2=" + len1 + "," + len2);

			if (len1 == len2) {
				String str = IOUtils.toString(isr);

				if ((str).equals(privatekey)) // 匯入KEY
				{
					// 刪除licdata.key
					// licdatakeyPath.delete();

					FileOutputStream fout = new FileOutputStream(Live_Key);
					// Log.v("key" ,"key " + key);
					key = key();
					enkey = selfEncode(key, key);
					 Log.v("key" , "default key " + enkey);
					SharedPreferences.Editor editor = Camera_data.edit();
					editor.putBoolean("OnDemand", false);
					editor.commit();
					byte[] contentInBytes = enkey.getBytes();

					fout.write(contentInBytes);
					fout.flush();
					fout.close();
					isr.close();
					fin2.close();

					return blueeyes = true;
				} else {
					return blueeyes = false;
				}
			} else {
				return blueeyes = false;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return blueeyes;
	}

	// 判斷兩文件是否一樣
	private boolean compareBlueeyes_pro(FileInputStream fin2) {
		boolean blueeyes = false;

		InputStreamReader isr = new InputStreamReader(fin2);
		try {
			int len1 = privatekey_pro.length(); // 內部 privatekey
			int len2 = fin2.available(); // 外部KEY
			Log.v("key", "len1,len2=" + len1 + "," + len2);

			if (len1 == len2) {
				String str = IOUtils.toString(isr);

				if ((str).equals(privatekey_pro)) // 匯入KEY
				{
					// 刪除licdata.key
					// licdatakeyPath.delete();

					FileOutputStream fout = new FileOutputStream(Live_Key);
					// Log.v("key" ,"key " + key);
					keypro = key_pro();
					enkey = selfEncode(keypro, keypro);
					SharedPreferences.Editor editor = Camera_data.edit();
					editor.putBoolean("OnDemand", true);
					editor.commit();
					 Log.v("key" , "pro key " + enkey);
					byte[] contentInBytes = enkey.getBytes();

					fout.write(contentInBytes);
					fout.flush();
					fout.close();
					isr.close();
					fin2.close();

					return blueeyes = true;
				} else {
					return blueeyes = false;
				}
			} else {
				return blueeyes = false;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return blueeyes;
	}

	private void registerUsbStateReceiver() {
		// Log.i(TAG, "Register usb Receiver");
		try {
			IntentFilter filter = new IntentFilter();
			filter.addAction("android.hardware.usb.action.USB_STATE");
			filter.addAction("android.hardware.action.USB_DISCONNECTED");
			filter.addAction("android.hardware.action.USB_CONNECTED");
			filter.addAction("android.intent.action.UMS_CONNECTED");
			filter.addAction("android.intent.action.UMS_DISCONNECTED");
			filter.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED);
			filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
			filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
			filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
			registerReceiver(mUsbStateReceiver, filter);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void unRegisterUsbStateReceiver() {
		// Log.i(TAG, "UnRegister usb Receiver");
		try {
			if (mUsbStateReceiver != null) {
				unregisterReceiver(mUsbStateReceiver);
			}
			mUsbStateReceiver = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String selfEncode(String key, String value) {
		SecretKeySpec spec = new SecretKeySpec(selfKey(key).getBytes(), "AES");
		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, spec);
			return Base64.encodeToString(cipher.doFinal(value.getBytes()), android.util.Base64.NO_WRAP);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String selfDecode(String key, String value) {
		SecretKeySpec spec = new SecretKeySpec(selfKey(key).getBytes(), "AES");
		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, spec);
			return new String(cipher.doFinal(Base64.decode(value, android.util.Base64.NO_WRAP)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String loadFileAsString(String filePath) throws java.io.IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
		}
		reader.close();
		return fileData.toString();
	}

	/*
	 * Get the STB MacAddress
	 */
	public String getMacAddress() {
		try {
			return loadFileAsString("/sys/class/net/eth0/address").toLowerCase().substring(0, 17);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String key() {
		// WifiInfo wifiInfo = wifi_service.getConnectionInfo();
		// String wifimacdb = wifiInfo.getMacAddress();
		// String wifi_mac = wifimacdb.replace(":", "");
		// key = selfKey("live" + wifi_mac.toLowerCase());

		if (getMacAddress() != null) {
			String mac = getMacAddress().replace(":", "");
			key = selfKey("live" + mac.toLowerCase());
			return key;
		} else {
			return null;
		}
	}

	public String key_pro() {
		String key_tem;
		if (getMacAddress() != null) {
			String mac = getMacAddress().replace(":", "");
			Log.v("test" , mac);
			key_tem = selfKey("lpro" + mac.toLowerCase());
			return key_tem;
		} else {
			return null;
		}
	}

	// key加解密
	public static String selfKey(String key) { // key.length() must be 16 . key
												// // 為'blue'+MAC
		int length = key.length();
		if (length <= 16) {
			for (int i = length; i < 16; ++i)
				key += i % 10;
			return key;
		} else if (length <= 24) {
			for (int i = length; i < 24; ++i)
				key += i % 10;
			return key;
		} else if (length <= 32) {
			for (int i = length; i < 32; ++i)
				key += i % 10;
			return key;
		}
		return key.substring(0, 16);
	}

	public void setWallpaper() {
		try {
			WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
			wallpaperManager.setResource(R.drawable.livebox_wallpaper);
		} catch (Exception ex) {
			Log.e("桌面", "桌面設置" + ex.toString());
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unRegisterUsbStateReceiver();
	}
}
