package tw.com.blueeyes.LiveBox;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.usb.UsbManager;
import android.media.AudioManager;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Ondemand_LiveBox_Activity extends Activity {
	ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
	// int camera = 0;
	// final int camera1 = 0xa1;
	// final int camera2 = 0xa2;
	// final int camera3 = 0xa3;
	// final int camera4 = 0xa4;
	// private MediaCodec mediaCodec;
	static Handler Change_iCam;
	static boolean debug_frame = true;
	SharedPreferences Camera_data;
	SurfaceView sv, sv1;
	Ondemand_Getdata get_camera;
	GetCamera_Data getdata_thread;
	// GetCamera4_Data getdata4_thread;
	public static SurfaceHolder surfaceHolder_h264;
	int Camera_Type = 0;
	int Camera_v1 = 0x00;
	int Camera_v2 = 0x01;
	int Camera_v3 = 0x02;
	int SES_class = 0x03;
	int SES_group = 0x04;
	final int camera1 = 0xa1;
	final int camera2 = 0xa2;
	final int camera3 = 0xa3;
	final int camera4 = 0xa4;

	View notice_view;
	TextView dialog_content;
	Button dialog_ok;
	AlertDialog.Builder NAD;
	AlertDialog N_alert;
	LinearLayout banner;
	LinearLayout debug;
	TextView time, frame, count_time;
	TextView title;
	Button disabletext;
	ImageButton last, next, system, vo_up, vo_down, circle_icon;
	ImageView count_amin;
	AnimationDrawable animationDrawable;
	// Animation am;
	AudioManager audioManager;
	Audio_PCM pcm;
	Audio_decode g711;
	Decode_h264 de_h264;
	// Decode_mp4 de_mp4;
	static Boolean run = true;
	static ImageView image;
	ImageView disconnect;
	final int wifi = 0x00;
	final int eth0 = 0x01;
	static int width = 1920;
	static int height = 1080;
	boolean checkroot = true;
	boolean call = false;
	boolean check_key = true;
	IServiceControl isend;
	final int OK = 0x00;
	final int Fail = 0x01;
	final int cam_type = 0x03;
	final int volume = 0x04;
	final int loop = 0x05;
	String show_type;
	ImageButton logo;
	Bitmap logo_a;
	Bitmap logo_b;
	String usbpath_old = "/mnt/usb_storage/USB_DISK0/udisk0/";
	String usbpath = "/mnt/usb_storage/USB_DISK0/";
	File usbfilePath_old = new File(usbpath_old);
	File usbfilePath = new File(usbpath);

	String apkName = "LiveBox.apk";
	String apkPath;
	int now_play = 0;
	boolean change_ok = true;
	int status[] = new int[20];
	boolean circle_run = false;
	Bitmap circle[] = new Bitmap[10];
	Bitmap circle_start;
	Cirlce_channel circle_t;
	boolean send_loop = false;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 全畫面
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// 標題隱藏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (Build.VERSION.RELEASE.equals("4.4.2")) {
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		} 
		setContentView(R.layout.activity_main_ondemand);

		// key = key();

		MyLog.i("LiveBox", "APP start");
		run = true;
		checkroot = checkroot();
		Log.v("root", checkroot + "");

		// camera param use sharedoreferences
		Camera_data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		// 取得音量控制器
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		sv = (SurfaceView) findViewById(R.id.surfaceView);
		surfaceHolder_h264 = sv.getHolder();

		banner = (LinearLayout) findViewById(R.id.banner);
		// debug = (LinearLayout) findViewById(R.id.debug);
		last = (ImageButton) findViewById(R.id.last);
		next = (ImageButton) findViewById(R.id.next);
		system = (ImageButton) findViewById(R.id.system);
		vo_up = (ImageButton) findViewById(R.id.vo_up);
		vo_down = (ImageButton) findViewById(R.id.vo_down);
		time = (TextView) findViewById(R.id.tv_time);
		title = (TextView) findViewById(R.id.title);
		frame = (TextView) findViewById(R.id.tv_frame);
		image = (ImageView) findViewById(R.id.imageview);
		disconnect = (ImageView) findViewById(R.id.disconnect);
		disabletext = (Button) findViewById(R.id.disable);
		logo = (ImageButton) findViewById(R.id.logo);
		count_time = (TextView) findViewById(R.id.count_time);
		count_amin = (ImageView) findViewById(R.id.count_time_bk);
		circle_icon = (ImageButton) findViewById(R.id.circle);
		if (Camera_data.getBoolean("loop_on", false)) {
			circle_icon.setImageResource(R.drawable.circulate_open);
		} else {
			circle_icon.setImageResource(R.drawable.circulate_close);
		}

		Resources res = getResources();
		circle_start = BitmapFactory.decodeResource(res, R.drawable.count_0);
		circle[0] = BitmapFactory.decodeResource(res, R.drawable.count_1);
		circle[1] = BitmapFactory.decodeResource(res, R.drawable.count_2);
		circle[2] = BitmapFactory.decodeResource(res, R.drawable.count_3);
		circle[3] = BitmapFactory.decodeResource(res, R.drawable.count_4);
		circle[4] = BitmapFactory.decodeResource(res, R.drawable.count_5);
		circle[5] = BitmapFactory.decodeResource(res, R.drawable.count_6);
		circle[6] = BitmapFactory.decodeResource(res, R.drawable.count_7);
		circle[7] = BitmapFactory.decodeResource(res, R.drawable.count_8);
		circle[8] = BitmapFactory.decodeResource(res, R.drawable.count_9);
		circle[9] = BitmapFactory.decodeResource(res, R.drawable.count_10);
		// Resources res = getResources();
		// logo_a = BitmapFactory.decodeResource(res, R.drawable.logo_a);
		// logo_a = getTransparentBitmap(logo_a, 60);
		// logo_b = BitmapFactory.decodeResource(res, R.drawable.logo_b);
		// logo_b = getTransparentBitmap(logo_b, 60);
		if (getLocaleLanguage().equalsIgnoreCase("zh-TW")) {
			logo.setBackgroundResource(R.drawable.animation_blueeyes);
		} else {
			logo.setBackgroundResource(R.drawable.animation_livebox);
		}
		animationDrawable = (AnimationDrawable) logo.getBackground();
		animationDrawable.start();

		// am = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f,
		// Animation.RELATIVE_TO_SELF, 0.5f);
		// am.setDuration(3000);
		// am.setRepeatCount(-1);
		// circle_icon.setAnimation(am);

		new Thread() {
			public void run() {
				if (Camera_data.getInt("net_type", eth0) == wifi) {
					check_wifi();
					MyLog.i("LiveBox", "Connect Type: Wifi");
				} else {
					open_eth0();
					MyLog.i("LiveBox", "Connect Type: Ethernet");
				}
			}
		}.start();

		Log.v("show", "" + Camera_data.getInt("show", 1));

		SharedPreferences.Editor editor = Camera_data.edit();
		editor.putInt("activity", 0x1a);
		editor.apply();
		editor.commit();

		if (usbfilePath.exists()) {
			if (usbfilePath_old.exists()) {
				if (usbfilePath != usbfilePath_old) {
					usbfilePath = usbfilePath_old;
				}
			}
		}

		apkPath = usbfilePath.getPath() + "/" + apkName;
		File livebox_file = new File(apkPath);

		if (livebox_file.exists()) {
			checkVNupdatePG();
		}

		bindService(new Intent(Ondemand_LiveBox_Activity.this, Remote_Service.class), conn, Context.BIND_AUTO_CREATE);

		if (Camera_data.getBoolean("video_title", true)) {
			title.setVisibility(View.VISIBLE);
		} else {
			title.setVisibility(View.GONE);
		}

		if (Camera_data.getBoolean("video_time", true)) {
			time.setVisibility(View.VISIBLE);
			frame.setVisibility(View.VISIBLE);
			// time.setTextColor(Color.parseColor("#FFFFFF"));
		} else {
			time.setVisibility(View.GONE);
			frame.setVisibility(View.GONE);
		}

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			surfaceHolder_h264.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		load_data();

		surfaceHolder_h264.addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				// MyLog.v("surface", "h264 change");
			}

			@SuppressLint("NewApi")
			@Override
			public void surfaceCreated(SurfaceHolder arg0) {
				if (run) {
					Log.v("surface", "surface start");
					pcm = new Audio_PCM(Ondemand_LiveBox_Activity.this);
					g711 = new Audio_decode();
					de_h264 = new Decode_h264(arg0.getSurface());
					camera_thread();
					circle_run = Camera_data.getBoolean("loop_on", false);
					if (circle_run) {
						circle_t = new Cirlce_channel();
						circle_t.start();
						// am.startNow();
					} else {
						if (count_amin.getVisibility() == View.VISIBLE) {
							count_amin.setVisibility(View.GONE);
							count_time.setText("");
							// am.cancel();
						}
					}
				}
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				// TODO Auto-generated method stub
				// MyLog.v("surface", "h264 destory");
				// thread_interupt();
			}
		});

		sv.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (banner.getVisibility() == View.VISIBLE) {
					banner.setVisibility(View.GONE);
				} else {
					banner.setVisibility(View.VISIBLE);
				}
			}
		});

		image.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (banner.getVisibility() == View.VISIBLE) {
					banner.setVisibility(View.GONE);
				} else {
					banner.setVisibility(View.VISIBLE);
				}
			}
		});

		disabletext.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (banner.getVisibility() == View.VISIBLE) {
					banner.setVisibility(View.GONE);
				} else {
					banner.setVisibility(View.VISIBLE);
				}
			}
		});

		vo_up.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
			}
		});

		vo_down.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
			}
		});

		circle_icon.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Message msg_cicrcle = new Message();
				msg_cicrcle.arg1 = 0x2f;
				handler.sendMessage(msg_cicrcle);
			}
		});

		last.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				circle_run = false;
				Message msg_cicrcle = new Message();
				msg_cicrcle.arg1 = 0x2e;
				handler.sendMessage(msg_cicrcle);

				if (change_ok) {
					if (disabletext.getVisibility() == View.GONE) {
						disabletext.setText("");
						disabletext.setVisibility(View.VISIBLE);
						disconnect.setVisibility(View.GONE);
						image.setVisibility(View.GONE);
					} else {
						disabletext.setText("");
						disconnect.setVisibility(View.GONE);
						image.setVisibility(View.GONE);
					}

					change_ok = false;
					if (getdata_thread != null) {
						if (get_camera != null) {
							get_camera.run = false;
							if (getdata_thread != null) {
								getdata_thread.interrupt();
								getdata_thread = null;
							}
						}
						// try {
						// Thread.sleep(500);
						// get_camera.clearbuffer();
						// } catch (InterruptedException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						Log.v("test", "now_play" +now_play);
						if (now_play == 0) {
							now_play = list.size() - 1;
						} else {
							now_play--;
						}
						Log.v("test", "now_play" +now_play);
						
						for (; now_play > 0; now_play--) {
							if ((Boolean) list.get(now_play).get("active")) {
								change_pref((Integer) list.get(now_play).get("id"));
								break;
							}
						}

						if (now_play == 0) {
							if (!(Boolean) list.get(0).get("active")) {
								now_play = list.size() - 1;
								for (; now_play > 0; now_play--) {
									if ((Boolean) list.get(now_play).get("active")) {
										change_pref((Integer) list.get(now_play).get("id"));
										break;
									}
								}
							} else {
								change_pref((Integer) list.get(now_play).get("id"));
							}
						}
					}
				}
			}
		});

		next.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				circle_run = false;
				Message msg_cicrcle = new Message();
				msg_cicrcle.arg1 = 0x2e;
				handler.sendMessage(msg_cicrcle);

				if (change_ok) {
					if (disabletext.getVisibility() == View.GONE) {
						disabletext.setText("");
						disabletext.setVisibility(View.VISIBLE);
						disconnect.setVisibility(View.GONE);
						image.setVisibility(View.GONE);
					} else {
						disabletext.setText("");
						disconnect.setVisibility(View.GONE);
						image.setVisibility(View.GONE);
					}

					change_ok = false;
					if (getdata_thread != null) {
						if (get_camera != null) {
							get_camera.run = false;
							if (getdata_thread != null) {
								getdata_thread.interrupt();
								getdata_thread = null;
							}
						}

						// try {
						// Thread.sleep(500);
						// get_camera.clearbuffer();
						// } catch (InterruptedException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }

						if (now_play == list.size() - 1 || now_play == list.size()) {
							now_play = 0;
						} else {
							now_play++;
						}

						for (; now_play < list.size(); now_play++) {
							if ((Boolean) list.get(now_play).get("active")) {
								change_pref((Integer) list.get(now_play).get("id"));
								break;
							}
						}

						if (now_play == list.size() - 1 || now_play == list.size()) {
							if (!(Boolean) list.get(list.size() - 1).get("active")) {
								now_play = 0;
								for (; now_play < list.size(); now_play++) {
									if ((Boolean) list.get(now_play).get("active")) {
										change_pref((Integer) list.get(now_play).get("id"));
										break;
									}
								}
							}
						}
					}
				}
			}
		});

		system.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// notshowall();

				call = true;
				Intent intent = new Intent();
				if (Camera_data.getBoolean("OnDemand", false)) {
					intent.setClass(Ondemand_LiveBox_Activity.this, Ondemand_Setting_Activity.class);
				} else {
					intent.setClass(Ondemand_LiveBox_Activity.this, Setting_Activity.class);
				}

				startActivity(intent);
				finish();
				onDestroy();
				System.exit(0);
			}
		});

		logo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// notshowall();
				call = true;
				stopService(new Intent(Ondemand_LiveBox_Activity.this, Remote_Service.class));
				Intent intent = new Intent();
				if (Build.VERSION.RELEASE.equals("4.4.2")) {
					intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
				} else {
					ComponentName comp = new ComponentName("com.android.settings",
							"com.android.settings.ScreenSettings");
					intent.setComponent(comp);
				}
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
				startActivity(intent);
				finish();
				onDestroy();
				System.exit(0);
			}
		});

		Change_iCam = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.arg1) {
				case 0x01:
					int cmd_num = Integer.parseInt((String) msg.obj);
					if (cmd_num >= 0 && cmd_num <= 19) {
						circle_run = false;
						Message msg_cicrcle = new Message();
						msg_cicrcle.arg1 = 0x2e;
						handler.sendMessage(msg_cicrcle);
						remote_chane(cmd_num);
						sendicam();
						returncmd(cam_type);
					} else if (((String) msg.obj).equalsIgnoreCase("1004")) {			
						sendicam();
						returncmd(cam_type);
					} else if (((String) msg.obj).equalsIgnoreCase("1005")) {
						int tem_mod = audioManager.getRingerMode();
						if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
							audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
						} else {
							audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
						}
//						Log.v("test" , "sligent " + audioManager.RINGER_MODE_SILENT + " " +audioManager.getRingerMode());
						returncmd(volume);
					} else if (((String) msg.obj).equalsIgnoreCase("1006")) {
						send_loop = true;
						circle_icon.performClick();
					} else if (((String) msg.obj).equalsIgnoreCase("1007")) {
						returncmd(loop);
					} else {
						returncmd(Fail);
					}
					break;
				case 0x02:
					if (Camera_data.getInt("net_type", eth0) == wifi) {
						check_wifi();
					} else {
						open_eth0();
					}
					break;
				case 0x03:
					Log.v("test", " intet 0");
					call = true;
					Intent intent = new Intent();
					intent.setClass(Ondemand_LiveBox_Activity.this, ReLoading_Activity.class);
					Bundle bundle = new Bundle();
					bundle.putInt("type", 0);
					intent.putExtras(bundle);
					startActivity(intent);
					finish();
					onDestroy();
					System.exit(0);
					break;
				case 0x04:
					Log.v("test", " intet 1");
					call = true;
					Intent intent1 = new Intent();
					intent1.setClass(Ondemand_LiveBox_Activity.this, ReLoading_Activity.class);
					Bundle bundle1 = new Bundle();
					bundle1.putInt("type", 1);
					intent1.putExtras(bundle1);
					startActivity(intent1);
					finish();
					onDestroy();
					System.exit(0);
					break;
				case 0x05:
					call = true;
					Intent intent2 = new Intent();
					intent2.setClass(Ondemand_LiveBox_Activity.this, Loading_Activity.class);
					startActivity(intent2);
					finish();
					onDestroy();
					System.exit(0);
					break;
				}
			}
		};
		// logochange.start();
	}

	public void load_data() {
		HashMap<String, Object> item = new HashMap<String, Object>();
		item.put("id", 1);
		item.put("active", Camera_data.getBoolean("Camera1", false));
		if (Camera_data.getBoolean("Camera1", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 2);
		item.put("active", Camera_data.getBoolean("Camera2", false));
		if (Camera_data.getBoolean("Camera2", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 3);
		item.put("active", Camera_data.getBoolean("Camera3", false));
		if (Camera_data.getBoolean("Camera3", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 4);
		item.put("active", Camera_data.getBoolean("Camera4", false));
		if (Camera_data.getBoolean("Camera4", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 5);
		item.put("active", Camera_data.getBoolean("Camera5", false));
		if (Camera_data.getBoolean("Camera5", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 6);
		item.put("active", Camera_data.getBoolean("Camera6", false));
		if (Camera_data.getBoolean("Camera6", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 7);
		item.put("active", Camera_data.getBoolean("Camera7", false));
		if (Camera_data.getBoolean("Camera7", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 8);
		item.put("active", Camera_data.getBoolean("Camera8", false));
		if (Camera_data.getBoolean("Camera8", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 9);
		item.put("active", Camera_data.getBoolean("Camera9", false));
		if (Camera_data.getBoolean("Camera9", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 10);
		item.put("active", Camera_data.getBoolean("Camera10", false));
		if (Camera_data.getBoolean("Camera10", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 11);
		item.put("active", Camera_data.getBoolean("Camera11", false));
		if (Camera_data.getBoolean("Camera11", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 12);
		item.put("active", Camera_data.getBoolean("Camera12", false));
		if (Camera_data.getBoolean("Camera12", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 13);
		item.put("active", Camera_data.getBoolean("Camera13", false));
		if (Camera_data.getBoolean("Camera13", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 14);
		item.put("active", Camera_data.getBoolean("Camera14", false));
		if (Camera_data.getBoolean("Camera14", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 15);
		item.put("active", Camera_data.getBoolean("Camera15", false));
		if (Camera_data.getBoolean("Camera15", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 16);
		item.put("active", Camera_data.getBoolean("Camera16", false));
		if (Camera_data.getBoolean("Camera16", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 17);
		item.put("active", Camera_data.getBoolean("Camera17", false));
		if (Camera_data.getBoolean("Camera17", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 18);
		item.put("active", Camera_data.getBoolean("Camera18", false));
		if (Camera_data.getBoolean("Camera18", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 19);
		item.put("active", Camera_data.getBoolean("Camera19", false));
		if (Camera_data.getBoolean("Camera19", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);

		item = new HashMap<String, Object>();
		item.put("id", 20);
		item.put("active", Camera_data.getBoolean("Camera20", false));
		if (Camera_data.getBoolean("Camera20", false)) {
			item.put("ative_num", 1);
		} else {
			item.put("ative_num", 0);
		}
		list.add(item);
	}

	public void sendicam() {
		show_type = "ONDMAND_LIVEBOX_";
		for (int i = 0; i < list.size(); i++) {
			status[i] = (Integer) list.get(i).get("ative_num");
			if (now_play == i) {
				status[i] = 2;
			}
			show_type = show_type + status[i] + ":";
		}

		if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
			show_type = show_type + 0;
		} else {
			show_type = show_type + 1;
		}
	}

	private void remote_chane(int cmd) {
		if (change_ok) {
			if (disabletext.getVisibility() == View.GONE) {
				disabletext.setText("");
				disabletext.setVisibility(View.VISIBLE);
				disconnect.setVisibility(View.GONE);
				image.setVisibility(View.GONE);
			} else {
				disabletext.setText("");
				disconnect.setVisibility(View.GONE);
				image.setVisibility(View.GONE);
			}
			change_ok = false;
			now_play = cmd;
			if (getdata_thread != null) {
				if (get_camera != null) {
					get_camera.run = false;
					if (getdata_thread != null) {
						getdata_thread.interrupt();
						getdata_thread = null;
					}
				}

				// try {
				// Thread.sleep(500);
				// get_camera.clearbuffer();
				// } catch (InterruptedException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				change_pref((Integer) list.get(now_play).get("id"));
			}
		}
	}

	void camera_thread() {
		for (now_play = 0; now_play < list.size(); now_play++) {
			if ((Boolean) list.get(now_play).get("active")) {
				new_object((Integer) list.get(now_play).get("id"));
				break;
			}
		}
	}

	private void new_object(int id) {
		Log.v("test", "id" + id);
		get_camera = null;
		get_camera = new Ondemand_Getdata(Ondemand_LiveBox_Activity.this, id, disabletext, disconnect, image, frame,
				time, g711, pcm, de_h264, Camera_data.getString("Camera" + id + "_URL", "192.168.1.171"),
				Camera_data.getInt("Camera" + id + "_Port", 80), Camera_data.getString("Camera" + id + "_Account",
						"root"), Camera_data.getString("Camera" + id + "_Pwd", "27507522"), id, Camera_data.getInt(
						"Camera" + id + "_type", Camera_v3), Camera_data.getInt("classorgroup" + id, 0) + 1);

		title.setText(Camera_data.getString("Camera" + id + "_Label", "CH" + id));
		getdata_thread = new GetCamera_Data();
		getdata_thread.setName(Camera_data.getString("Camera" + id + "_Label", "CH" + id));
		get_camera.t_new_name = Camera_data.getString("Camera" + id + "_Label", "CH" + id);
		getdata_thread.start();
	}

	private void change_pref(final int id) {
		Log.v("test", "id" + id);
		get_camera.ID = id;
		get_camera.IP = Camera_data.getString("Camera" + id + "_URL", "192.168.1.171");
		get_camera.PORT = Camera_data.getInt("Camera" + id + "_Port", 80);
		get_camera.ACCOUNT = Camera_data.getString("Camera" + id + "_Account", "root");
		get_camera.PWD = Camera_data.getString("Camera" + id + "_Pwd", "27507522");
		get_camera.camera_type = Camera_data.getInt("Camera" + id + "_type", Camera_v3);
		get_camera.group_calss_num = Camera_data.getInt("classorgroup" + id, 0) + 1;
		get_camera.t_new_name = Camera_data.getString("Camera" + id + "_Label", "CH" + id);
		Message msg = new Message();
		msg.arg1 = 0x0e;
		msg.arg2 = id;
		handler.sendMessage(msg);
		// title.setText(Camera_data.getString("Camera" + id + "_Label", ""));

		new Thread() {
			public void run() {
				try {
					// Thread.sleep(500);
					get_camera.clearbuffer();
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				getdata_thread = new GetCamera_Data();
				get_camera.run = true;
				get_camera.reset = true;
				getdata_thread.start();
				getdata_thread.setName(Camera_data.getString("Camera" + id + "_Label", "CH" + id));
				change_ok = true;
			}
		}.start();

	}

	private class GetCamera_Data extends Thread {
		public void run() {
			Log.v("test", "camera thread");
			get_camera.show = true;
			get_camera.getHttp();
		}
	}

	private class Cirlce_channel extends Thread {
		public void run() {
			Message msg5 = new Message();
			msg5.arg1 = 0x2c;
			handler.sendMessage(msg5);
			int reminder_time = Camera_data.getInt("loop_time", 20);
			Message msg2 = new Message();
			msg2.arg1 = 0x2b;
			msg2.arg2 = reminder_time;
			handler.sendMessage(msg2);
			try {
				Message msga = new Message();
				msga.arg1 = 0x2d;
				msga.arg2 = 0;
				handler.sendMessage(msga);
				sleep(100);
				Message msgb = new Message();
				msgb.arg1 = 0x2d;
				msgb.arg2 = 1;
				handler.sendMessage(msgb);
				sleep(100);
				Message msgc = new Message();
				msgc.arg1 = 0x2d;
				msgc.arg2 = 2;
				handler.sendMessage(msgc);
				sleep(100);
				Message msgd = new Message();
				msgd.arg1 = 0x2d;
				msgd.arg2 = 3;
				handler.sendMessage(msgd);
				sleep(100);
				Message msge = new Message();
				msge.arg1 = 0x2d;
				msge.arg2 = 4;
				handler.sendMessage(msge);
				sleep(100);
				Message msgf = new Message();
				msgf.arg1 = 0x2d;
				msgf.arg2 = 5;
				handler.sendMessage(msgf);
				sleep(100);
				Message msgg = new Message();
				msgg.arg1 = 0x2d;
				msgg.arg2 = 6;
				handler.sendMessage(msgg);
				sleep(100);
				Message msgh = new Message();
				msgh.arg1 = 0x2d;
				msgh.arg2 = 7;
				handler.sendMessage(msgh);
				sleep(100);
				Message msgi = new Message();
				msgi.arg1 = 0x2d;
				msgi.arg2 = 8;
				handler.sendMessage(msgi);
				sleep(100);
				Message msgj = new Message();
				msgj.arg1 = 0x2d;
				msgj.arg2 = 9;
				handler.sendMessage(msgj);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			while (circle_run) {
				if (reminder_time == 1) {
					reminder_time = Camera_data.getInt("loop_time", 20);
					Message msg = new Message();
					msg.arg1 = 0x2b;
					msg.arg2 = reminder_time;
					handler.sendMessage(msg);

					msg = new Message();
					msg.arg1 = 0x2c;
					handler.sendMessage(msg);

					if (change_ok) {
						Message msg4 = new Message();
						msg4.arg1 = 0x2a;
						handler.sendMessage(msg4);

						change_ok = false;
						if (getdata_thread != null) {
							if (get_camera != null) {
								get_camera.run = false;
								if (getdata_thread != null) {
									getdata_thread.interrupt();
									getdata_thread = null;
								}
							}

							if (now_play == list.size() - 1 || now_play == list.size()) {
								now_play = 0;
							} else {
								now_play++;
							}

							for (; now_play < list.size(); now_play++) {
								if ((Boolean) list.get(now_play).get("active")) {
									change_pref((Integer) list.get(now_play).get("id"));
									break;
								}
							}

							if (now_play == list.size() - 1 || now_play == list.size()) {
								if (!(Boolean) list.get(list.size() - 1).get("active")) {
									now_play = 0;
									for (; now_play < list.size(); now_play++) {
										if ((Boolean) list.get(now_play).get("active")) {
											change_pref((Integer) list.get(now_play).get("id"));
											break;
										}
									}
								}
							}
						}
					}
					try {
						Message msga = new Message();
						msga.arg1 = 0x2d;
						msga.arg2 = 0;
						handler.sendMessage(msga);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgb = new Message();
						msgb.arg1 = 0x2d;
						msgb.arg2 = 1;
						handler.sendMessage(msgb);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgc = new Message();
						msgc.arg1 = 0x2d;
						msgc.arg2 = 2;
						handler.sendMessage(msgc);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgd = new Message();
						msgd.arg1 = 0x2d;
						msgd.arg2 = 3;
						handler.sendMessage(msgd);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msge = new Message();
						msge.arg1 = 0x2d;
						msge.arg2 = 4;
						handler.sendMessage(msge);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgf = new Message();
						msgf.arg1 = 0x2d;
						msgf.arg2 = 5;
						handler.sendMessage(msgf);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgg = new Message();
						msgg.arg1 = 0x2d;
						msgg.arg2 = 6;
						handler.sendMessage(msgg);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgh = new Message();
						msgh.arg1 = 0x2d;
						msgh.arg2 = 7;
						handler.sendMessage(msgh);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgi = new Message();
						msgi.arg1 = 0x2d;
						msgi.arg2 = 8;
						handler.sendMessage(msgi);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgj = new Message();
						msgj.arg1 = 0x2d;
						msgj.arg2 = 9;
						handler.sendMessage(msgj);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					}
				} else {
					try {
						reminder_time = reminder_time - 1;
						Message msg3 = new Message();
						msg3.arg1 = 0x2b;
						msg3.arg2 = reminder_time;
						handler.sendMessage(msg3);

						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msga = new Message();
						msga.arg1 = 0x2d;
						msga.arg2 = 0;
						handler.sendMessage(msga);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgb = new Message();
						msgb.arg1 = 0x2d;
						msgb.arg2 = 1;
						handler.sendMessage(msgb);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgc = new Message();
						msgc.arg1 = 0x2d;
						msgc.arg2 = 2;
						handler.sendMessage(msgc);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgd = new Message();
						msgd.arg1 = 0x2d;
						msgd.arg2 = 3;
						handler.sendMessage(msgd);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msge = new Message();
						msge.arg1 = 0x2d;
						msge.arg2 = 4;
						handler.sendMessage(msge);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgf = new Message();
						msgf.arg1 = 0x2d;
						msgf.arg2 = 5;
						handler.sendMessage(msgf);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgg = new Message();
						msgg.arg1 = 0x2d;
						msgg.arg2 = 6;
						handler.sendMessage(msgg);
						sleep(100);
						Message msgh = new Message();
						msgh.arg1 = 0x2d;
						msgh.arg2 = 7;
						handler.sendMessage(msgh);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgi = new Message();
						msgi.arg1 = 0x2d;
						msgi.arg2 = 8;
						handler.sendMessage(msgi);
						sleep(100);
						if (!circle_run) {
							break;
						}
						Message msgj = new Message();
						msgj.arg1 = 0x2d;
						msgj.arg2 = 9;
						handler.sendMessage(msgj);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					}
				}
			}
		}
	}

	/* 檢查版本 versionName */
	private void checkVNupdatePG() {
		try {
			PackageManager pm = getPackageManager();
			/* NewVer */
			PackageInfo info = pm.getPackageArchiveInfo(apkPath, 0);
			int newVersionCode = info.versionCode;
			String newVersionName = info.versionName;

			// pm.setInstallerPackageName(targetPackage, installerPackageName)

			/* NowVer */
			info = pm.getPackageInfo(getPackageName(), 0);
			int nowVersionCode = info.versionCode;
			String nowVersionName = info.versionName;

			Log.d("APK", "LIVEBOX+.apk = " + "VersionCode : " + newVersionCode + ", VersionName : " + newVersionName);
			Log.d("APK", "目前 = " + "VersionCode : " + nowVersionCode + ", VersionName : " + nowVersionName);

			if (newVersionCode > nowVersionCode || Float.parseFloat(newVersionName) > Float.parseFloat(nowVersionName)) {
				/* 自動更新 需ROOT權限 */
				try {
					Process localProcess = Runtime.getRuntime().exec("su");
					OutputStream os = localProcess.getOutputStream();
					DataOutputStream dos = new DataOutputStream(os);
					dos.writeBytes("pm install -r " + apkPath + "\n");
					dos.flush();
					dos.writeBytes("exit\n");
					dos.flush();
				} catch (Exception e) {
					Log.e("APK", "update erroe", e);
					// Log.d(TAG,"no root");
				}

			}
		} catch (Exception ex) {
			Log.d("APK", "install error", ex);
		}
	}

	// Thread logochange = new Thread() {
	// public void run() {
	// boolean flag = false;
	// while (run) {
	// try {
	// if (flag) {
	// flag = false;
	// Message msg = new Message();
	// msg.arg1 = 0x1a;
	// handler.sendMessage(msg);
	// } else {
	// flag = true;
	// Message msg = new Message();
	// msg.arg1 = 0x1b;
	// handler.sendMessage(msg);
	// }
	// Thread.sleep(2000);
	// } catch (InterruptedException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// break;
	// }
	// }
	// }
	// };

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case 0x1a:
				logo.setImageBitmap(logo_a);
				break;
			case 0x1b:
				logo.setImageBitmap(logo_b);
				break;
			case 0x0c:
				disconnect_img(get_camera.dis_connect);
				break;
			case 0x0d:
				if (disabletext.getVisibility() == View.GONE) {
					disabletext.setText("");
					disabletext.setVisibility(View.VISIBLE);
				}
				break;
			case 0x0e:
				Log.v("test",
						"change title" + Camera_data.getString("Camera" + msg.arg2 + "_Label", "CH" + (msg.arg2 + 1)));
				title.setText(Camera_data.getString("Camera" + msg.arg2 + "_Label", "CH" + msg.arg2));
				time.setText("");
				frame.setText("");
				break;
			case 0x2a:
				if (disabletext.getVisibility() == View.GONE) {
					disabletext.setText("");
					disabletext.setVisibility(View.VISIBLE);
					disconnect.setVisibility(View.GONE);
					image.setVisibility(View.GONE);
				} else {
					disabletext.setText("");
					disconnect.setVisibility(View.GONE);
					image.setVisibility(View.GONE);
				}
				break;
			case 0x2b:
				count_time.setText(String.valueOf(msg.arg2));
				break;
			case 0x2c:
				count_amin.setImageBitmap(circle_start);
				break;
			case 0x2d:
				count_amin.setImageBitmap(circle[msg.arg2]);
				break;
			case 0x2e:
				if (count_amin.getVisibility() == View.VISIBLE) {
					count_amin.setVisibility(View.GONE);
					count_time.setText("");
				}
				circle_icon.setImageResource(R.drawable.circulate_close);
				break;
			case 0x2f:
				Log.v("test" , "change loop");
				if (circle_run) {
					circle_run = false;
					circle_icon.setImageResource(R.drawable.circulate_close);
					if (count_amin.getVisibility() == View.VISIBLE) {
						count_amin.setVisibility(View.GONE);
						count_time.setText("");
					}
					circle_t.interrupt();
				} else {
					circle_run = true;
					circle_icon.setImageResource(R.drawable.circulate_open);
					if (count_amin.getVisibility() == View.GONE) {
						count_amin.setVisibility(View.VISIBLE);
					}
					circle_t = new Cirlce_channel();
					circle_t.start();
				}
				if(send_loop) {
					returncmd(loop);
					send_loop = false;
				}
				
				break;
			}

		}
	};

	public void disconnect_img(boolean dis_connect) {
		if (dis_connect) {
			disabletext.setText("Disconnected");
			if (disconnect.getVisibility() == View.GONE) {
				disconnect.setVisibility(View.VISIBLE);
			}
			disable_image(true);
		} else {
			if (disconnect.getVisibility() == View.VISIBLE) {
				disconnect.setVisibility(View.GONE);
			}
			disable_image(false);
		}

	}

	public void disable_image(Boolean disable) {
		if (disable) {
			disabletext.setText("Disable");
			if (disabletext.getVisibility() == View.GONE) {
				disabletext.setVisibility(View.VISIBLE);
			}
		} else {
			if (disabletext.getVisibility() == View.VISIBLE) {
				disabletext.setVisibility(View.GONE);
			}
		}
	}

	public void SES_disable(Boolean disable) {
		if (disable) {
			if (image.getVisibility() == View.GONE) {
				image.setVisibility(View.VISIBLE);
			}
		} else {
			if (image.getVisibility() == View.VISIBLE) {
				image.setVisibility(View.GONE);
			}
		}
	}

	private void returncmd(int cmd) {
		try {
			switch (cmd) {
			case OK:
				isend.sendOK();
				break;
			case Fail:
				isend.sendFail();
				break;
			case cam_type:
				isend.sendTYPE(show_type);
				break;
			case volume:
				isend.sendvolumetype();
				break;
			case loop:
				isend.sendloop_type(circle_run);
				break;
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private ServiceConnection conn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			Log.i("Vic", "ServiceConnection -> onServiceConnected");
			isend = IServiceControl.Stub.asInterface(service);
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
		};
	};

	public boolean checkroot() {
		return findBinary("su");
	}

	public boolean findBinary(String binaryName) {
		boolean found = false;
		if (!found) {
			String[] places = { "/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/",
					"/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/" };
			for (String where : places) {
				if (new File(where + binaryName).exists()) {
					found = true;
					break;
				}
			}
		}
		return found;
	}

	public void check_wifi() {
		try {
			if (checkroot) {
				Process proc = Runtime.getRuntime().exec("su");
				DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
				opt.writeBytes("ifconfig eth0 down\n");
				opt.writeBytes("exit\n");
				opt.flush();

				// DataInputStream ls_in = new
				// DataInputStream(proc.getInputStream());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.v("test", e.toString());
		}
		if (ApManager.isApOn(Ondemand_LiveBox_Activity.this)) {
			ApManager.configApState(Ondemand_LiveBox_Activity.this);
		}
		
		WifiManager wifi_service = (WifiManager) getSystemService(WIFI_SERVICE);
		WifiInfo wifiInfo = wifi_service.getConnectionInfo();
		
		try{  
			if(!wifi_service.isWifiEnabled() || wifiInfo == null) //判斷WIFI是否有開啟，沒有就打開
			{			
				wifi_service.setWifiEnabled(true);	
				
				while (wifi_service.getWifiState() == WifiManager.WIFI_STATE_ENABLING) {
		            try {
		                //为了避免程序一直while循环，让它睡个100毫秒在检测……
		                Thread.currentThread();
		                Thread.sleep(100);
		            } catch (InterruptedException ie) {
		            }
		        }
			}	
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			wifi_service.setWifiEnabled(true);		
		}
		
		if (wifiInfo.getSSID().indexOf(Camera_data.getString("wifi_ssid", "defaultssid")) != -1) {
			int ipAddress = wifiInfo.getIpAddress();
			String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
					(ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
			if (ip.indexOf(Camera_data.getString("wifi", "192.168.1.51")) == -1) {
				WifiConfigManage wificon = new WifiConfigManage(wifi_service, wifiInfo);
				wificon.openWifi("STATIC", Camera_data.getString("wifi_ssid", "defaultssid"),
						Camera_data.getString("wifi_pwd", ""), Camera_data.getInt("pwd_type", 0x00),
						Camera_data.getString("wifi", "192.168.1.51"),
						netMaskToInt(Camera_data.getString("mask", "255.255.255.0")),
						Camera_data.getString("geteway", "192.168.1.1"), "8.8.8.8", "8.8.4.4");
			}
		} else {
			WifiConfigManage wificon = new WifiConfigManage(wifi_service, wifiInfo);
			wificon.openWifi("STATIC", Camera_data.getString("wifi_ssid", "defaultssid"),
					Camera_data.getString("wifi_pwd", ""), Camera_data.getInt("pwd_type", 0x00),
					Camera_data.getString("wifi", "192.168.1.51"),
					netMaskToInt(Camera_data.getString("mask", "255.255.255.0")),
					Camera_data.getString("geteway", "192.168.1.1"), "8.8.8.8", "8.8.4.4");
		}
		
		
	}

	public void open_eth0() {
		// WifiManager wifi_service = (WifiManager)
		// getSystemService(WIFI_SERVICE);
		// if (wifi_service.isWifiEnabled()) {
		// wifi_service.setWifiEnabled(false);
		// }

		if (!ApManager.isApOn(Ondemand_LiveBox_Activity.this)) {
			Log.v("hotspot", "false");
			ApManager.configApState(Ondemand_LiveBox_Activity.this);
		} else {
			Log.v("hotspot", "true");
		}

		new Thread() {
			public void run() {
				try {
					if (checkroot) {
						Process proc = Runtime.getRuntime().exec("su");
						DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
						opt.writeBytes("ifconfig eth0 up\n");
						opt.writeBytes("ifconfig eth0 " + Camera_data.getString("eth0", "192.168.1.51") + " netmask "
								+ Camera_data.getString("mask", "255.255.255.0") + "\n");
						opt.writeBytes("route add default gw " + Camera_data.getString("geteway", "192.168.1.1")
								+ " dev eth0\n");
						opt.writeBytes("exit\n");
						opt.flush();

						// DataInputStream ls_in = new
						// DataInputStream(proc.getInputStream());

					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.v("test", e.toString());
				}
			}
		}.start();

		// WifiManager wifi_service = (WifiManager)
		// getSystemService(WIFI_SERVICE);
		//
		// WifiInfo wifiInfo = wifi_service.getConnectionInfo();
		// String wifimacdb = wifiInfo.getMacAddress();
		// Log.v("test" , "wifi mac " + wifimacdb);
	}

	/* 計算 String="255.255.255.0" to int=24 多少個1 */
	public int netMaskToInt(String netmask) {
		// networkPrefixLength
		String[] maskarray = netmask.split("\\.");
		int number1 = 0;
		for (int i = 0; i < maskarray.length; i++) {
			String binaryMask = Integer.toBinaryString(Integer.parseInt(maskarray[i])); // 255
																						// ->
																						// 11111111
			// System.out.println(maskarray[i]+"="+numeber);
			String[] ary = binaryMask.split("");
			for (int j = 0; j < ary.length; j++) {
				if (ary[j].equals("1")) {
					number1++;
				}
			}
		}
		// System.out.println("網路字首長度="+number1);
		return number1;
	}

	public Bitmap getTransparentBitmap(Bitmap sourceImg, int number) {
		int[] argb = new int[sourceImg.getWidth() * sourceImg.getHeight()];

		sourceImg.getPixels(argb, 0, sourceImg.getWidth(), 0, 0, sourceImg

		.getWidth(), sourceImg.getHeight());// 获得图片的ARGB值

		number = number * 255 / 100;

		for (int i = 0; i < argb.length; i++) {

			argb[i] = (number << 24) | (argb[i] & 0x00FFFFFF);

		}
		sourceImg = Bitmap.createBitmap(argb, sourceImg.getWidth(), sourceImg.getHeight(), Config.ARGB_8888);
		return sourceImg;
	}

	@Override
	public void onDestroy() {
		Log.v("getdata", "destory");
		run = false;
		circle_run = false;
		animationDrawable.stop();

		// if (detect_getdata.isAlive()) {
		// detect_getdata.interrupt();
		// }

		if (getdata_thread != null) {
			get_camera.run = false;
			if (getdata_thread.isAlive()) {
				getdata_thread.interrupt();
				getdata_thread = null;
			}
			get_camera = null;
		}

		if (!call) {
			stopService(new Intent(Ondemand_LiveBox_Activity.this, Remote_Service.class));
			throw new NullPointerException();
		}

		// if (logochange.isAlive()) {
		// logochange.interrupt();
		// }
		super.onDestroy();
	}

	private String getLocaleLanguage() {
		Locale l = Locale.getDefault();
		return String.format("%s-%s", l.getLanguage(), l.getCountry());
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Log.v("IR", " " + keyCode);
		return false;
		// return super.onKeyDown(keyCode, event);
	}
}
