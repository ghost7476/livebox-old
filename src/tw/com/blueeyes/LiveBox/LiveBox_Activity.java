package tw.com.blueeyes.LiveBox;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.usb.UsbManager;
import android.media.AudioManager;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LiveBox_Activity extends Activity {
	// ArrayList<HashMap<String, Object>> camera_data = new
	// ArrayList<HashMap<String, Object>>();
	// int camera = 0;
	// final int camera1 = 0xa1;
	// final int camera2 = 0xa2;
	// final int camera3 = 0xa3;
	// final int camera4 = 0xa4;
	// private MediaCodec mediaCodec;
	static Handler Change_iCam;
	static boolean debug_frame = true;
	SharedPreferences Camera_data;
	SurfaceView sv, sv1;
	static Getdata get_camera1, get_camera2, get_camera3, get_camera4;
	GetCamera1_Data getdata1_thread;
	GetCamera2_Data getdata2_thread;
	GetCamera3_Data getdata3_thread;
	// GetCamera4_Data getdata4_thread;
	public static SurfaceHolder surfaceHolder_h264;
	AnimationDrawable animationDrawable;
	int Camera_Type = 0;
	int Camera_v1 = 0x00;
	int Camera_v2 = 0x01;
	int Camera_v3 = 0x02;
	int SES_class = 0x03;
	int SES_group = 0x04;
	final int camera1 = 0xa1;
	final int camera2 = 0xa2;
	final int camera3 = 0xa3;
	final int camera4 = 0xa4;

	View notice_view;
	TextView dialog_content;
	Button dialog_ok;
	AlertDialog.Builder NAD;
	AlertDialog N_alert;
	LinearLayout banner;
	LinearLayout debug;
	TextView time, frame;
	TextView title;
	Button disabletext;
	ImageButton last, next, system, vo_up, vo_down;
	AudioManager audioManager;
	Audio_PCM pcm;
	Audio_decode g711;
	Decode_h264 de_h264;
	// Decode_mp4 de_mp4;
	static Boolean run = true;
	static ImageView image;
	ImageView disconnect;
	final int wifi = 0x00;
	final int eth0 = 0x01;
	static int width = 1920;
	static int height = 1080;
	boolean checkroot = true;
	boolean call = false;
	boolean check_key = true;
	IServiceControl isend;
	final int OK = 0x00;
	final int Fail = 0x01;
	final int cam_type = 0x03;
	final int volume = 0x04;
	String show_type;
	ImageButton logo;
	Bitmap logo_a;
	Bitmap logo_b;
	String usbpath_old = "/mnt/usb_storage/USB_DISK0/udisk0/";
	String usbpath = "/mnt/usb_storage/USB_DISK0/";
	File usbfilePath_old = new File(usbpath_old);
	File usbfilePath = new File(usbpath);
	String apkName = "LiveBox.apk";
	String apkPath;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 全畫面
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// 標題隱藏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
				WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (Build.VERSION.RELEASE.equals("4.4.2")) {
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		} 
//		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		setContentView(R.layout.activity_main);

		// key = key();

		MyLog.i("LiveBox", "APP start");
		run = true;
		checkroot = checkroot();
		Log.v("root", checkroot + "");

		// camera param use sharedoreferences
		Camera_data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		// 取得音量控制器
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		sv = (SurfaceView) findViewById(R.id.surfaceView);
		surfaceHolder_h264 = sv.getHolder();

		banner = (LinearLayout) findViewById(R.id.banner);
		// debug = (LinearLayout) findViewById(R.id.debug);
		last = (ImageButton) findViewById(R.id.last);
		next = (ImageButton) findViewById(R.id.next);
		system = (ImageButton) findViewById(R.id.system);
		vo_up = (ImageButton) findViewById(R.id.vo_up);
		vo_down = (ImageButton) findViewById(R.id.vo_down);
		time = (TextView) findViewById(R.id.tv_time);
		title = (TextView) findViewById(R.id.title);
		frame = (TextView) findViewById(R.id.tv_frame);
		image = (ImageView) findViewById(R.id.imageview);
		disconnect = (ImageView) findViewById(R.id.disconnect);
		disabletext = (Button) findViewById(R.id.disable);
		logo = (ImageButton) findViewById(R.id.logo);

		// Resources res = getResources();
		// logo_a = BitmapFactory.decodeResource(res, R.drawable.logo_a);
		// logo_a = getTransparentBitmap(logo_a, 60);
		// logo_b = BitmapFactory.decodeResource(res, R.drawable.logo_b);
		// logo_b = getTransparentBitmap(logo_b, 60);
		if (getLocaleLanguage().equalsIgnoreCase("zh-TW")) {
			logo.setBackgroundResource(R.drawable.animation_blueeyes);
		} else {
			logo.setBackgroundResource(R.drawable.animation_livebox);
		}
		animationDrawable = (AnimationDrawable) logo.getBackground();
		animationDrawable.start();

		new Thread() {
			public void run() {
				if (Camera_data.getInt("net_type", eth0) == wifi) {
					check_wifi();
					MyLog.i("LiveBox", "Connect Type: Wifi");
				} else {
					open_eth0();
					MyLog.i("LiveBox", "Connect Type: Ethernet");
				}
			}
		}.start();

		Log.v("show", "" + Camera_data.getInt("show", 1));

		SharedPreferences.Editor editor = Camera_data.edit();
		editor.putInt("activity", 0x1a);
		editor.apply();
		editor.commit();

		if (usbfilePath.exists()) {
			if (usbfilePath_old.exists()) {
				if (usbfilePath != usbfilePath_old) {
					usbfilePath = usbfilePath_old;
				}
			}
		}
		
		apkPath = usbfilePath.getPath() + "/" + apkName;
		File livebox_file = new File(apkPath);

		if (livebox_file.exists()) {
			checkVNupdatePG();
		}

		bindService(new Intent(LiveBox_Activity.this, Remote_Service.class), conn, Context.BIND_AUTO_CREATE);

		if (Camera_data.getBoolean("video_title", true)) {
			title.setVisibility(View.VISIBLE);
		} else {
			title.setVisibility(View.GONE);
		}

		if (Camera_data.getBoolean("video_time", true)) {
			time.setVisibility(View.VISIBLE);
			frame.setVisibility(View.VISIBLE);
			// time.setTextColor(Color.parseColor("#FFFFFF"));
		} else {
			time.setVisibility(View.GONE);
			frame.setVisibility(View.GONE);
		}

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			surfaceHolder_h264.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		surfaceHolder_h264.addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				// MyLog.v("surface", "h264 change");
			}

			@SuppressLint("NewApi")
			@Override
			public void surfaceCreated(SurfaceHolder arg0) {
				if (run) {
					Log.v("surface", "surface start");
					pcm = new Audio_PCM(LiveBox_Activity.this);
					g711 = new Audio_decode();
					de_h264 = new Decode_h264(arg0.getSurface());
					// de_mp4 = new Decode_mp4(arg0.getSurface());
					camera_thread();

					if (!detect_getdata.isAlive()) {
						detect_getdata.start();
					}
				}
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				// TODO Auto-generated method stub
				// MyLog.v("surface", "h264 destory");
				// thread_interupt();
			}
		});

		sv.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (banner.getVisibility() == View.VISIBLE) {
					banner.setVisibility(View.GONE);
				} else {
					banner.setVisibility(View.VISIBLE);
				}
			}
		});

		image.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (banner.getVisibility() == View.VISIBLE) {
					banner.setVisibility(View.GONE);
				} else {
					banner.setVisibility(View.VISIBLE);
				}
			}
		});

		disabletext.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (banner.getVisibility() == View.VISIBLE) {
					banner.setVisibility(View.GONE);
				} else {
					banner.setVisibility(View.VISIBLE);
				}
			}
		});

		vo_up.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
			}
		});

		vo_down.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
			}
		});
		// debug.setOnClickListener(new OnClickListener() {
		// public void onClick(View v) {
		// debug_count++;
		// if(debug_count%5 == 0) {
		// if(debug_frame) {
		// debug_frame = false;
		// } else {
		// debug_frame = true;
		// }
		// } else if(debug_count >= 1000) {
		// debug_count = 1;
		// }
		// }
		// });

		last.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (Camera_data.getInt("show", 1) == 1) {
					if (Camera_data.getBoolean("Camera1", false)) {
						if (get_camera1.show) {
							if (Camera_data.getBoolean("Camera2", false) || Camera_data.getBoolean("Camera3", false)
									|| Camera_data.getBoolean("Camera4", false)) {
								get_camera1.show = false;
							} else {
								return;
							}

						}
					}

					SES_disable(false);
					// if (Camera_data.getBoolean("Camera4", false)) {
					// get_camera4.isFirstIFrame = true;
					// get_camera4.show = true;
					// disconnect_img(get_camera4.dis_connect);
					// title.setText(Camera_data.getString("Camera4_Label",
					// "CH4"));
					// Log.v("test", "1 -> 4");
					// SharedPreferences.Editor editor = Camera_data.edit();
					// editor.putInt("show", 4);
					// editor.commit();
					// return;
					// }

					if (Camera_data.getBoolean("Camera3", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera3.clearbuffer();
						// get_camera3.isFirstIFrame = true;
						get_camera3.show = true;
						disconnect_img(get_camera3.dis_connect);
						title.setText(Camera_data.getString("Camera3_Label", "CH3"));
						Log.v("test", "1 -> 3");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 3);
						editor.commit();
						return;
					}

					if (Camera_data.getBoolean("Camera2", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera2.clearbuffer();
						// get_camera2.isFirstIFrame = true;
						get_camera2.show = true;
						disconnect_img(get_camera2.dis_connect);
						title.setText(Camera_data.getString("Camera2_Label", "CH2"));
						Log.v("test", "1 -> 2");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 2);
						editor.commit();
						return;
					}
				}

				if (Camera_data.getInt("show", 1) == 2) {
					if (Camera_data.getBoolean("Camera2", false)) {
						if (get_camera2.show) {
							if (Camera_data.getBoolean("Camera1", false) || Camera_data.getBoolean("Camera3", false)
									|| Camera_data.getBoolean("Camera4", false)) {
								get_camera2.show = false;
							} else {
								return;
							}

						}
					}

					SES_disable(false);
//					disable_image(false);
					if (Camera_data.getBoolean("Camera1", false)) {
						get_camera1.clearbuffer();
						// get_camera1.isFirstIFrame = true;
						get_camera1.show = true;
						disconnect_img(get_camera1.dis_connect);
						title.setText(Camera_data.getString("Camera1_Label", "CH1"));
						Log.v("test", "2 -> 1");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 1);
						editor.commit();
						return;
					}

					// if (Camera_data.getBoolean("Camera4", false)) {
					// get_camera4.isFirstIFrame = true;
					// get_camera4.show = true;
					// disconnect_img(get_camera4.dis_connect);
					// title.setText(Camera_data.getString("Camera4_Label",
					// "CH4"));
					// Log.v("test", "2 -> 4");
					// SharedPreferences.Editor editor = Camera_data.edit();
					// editor.putInt("show", 4);
					// editor.commit();
					// return;
					// }

					if (Camera_data.getBoolean("Camera3", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera3.clearbuffer();
						// get_camera3.isFirstIFrame = true;
						get_camera3.show = true;
						disconnect_img(get_camera3.dis_connect);
						title.setText(Camera_data.getString("Camera3_Label", "CH3"));
						Log.v("test", "2 -> 3");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 3);
						editor.commit();
						return;
					}
				}

				if (Camera_data.getInt("show", 1) == 3) {
					if (Camera_data.getBoolean("Camera3", false)) {
						if (get_camera3.show) {
							if (Camera_data.getBoolean("Camera1", false) || Camera_data.getBoolean("Camera2", false)
									|| Camera_data.getBoolean("Camera4", false)) {
								get_camera3.show = false;
							} else {
								return;
							}

						}
					}

					SES_disable(false);
					if (Camera_data.getBoolean("Camera2", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera2.clearbuffer();
						// get_camera2.isFirstIFrame = true;
						get_camera2.show = true;
						disconnect_img(get_camera2.dis_connect);
						title.setText(Camera_data.getString("Camera2_Label", "CH2"));
						Log.v("test", "3 -> 2");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 2);
						editor.commit();
						return;
					}

					if (Camera_data.getBoolean("Camera1", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera1.clearbuffer();
						// get_camera1.isFirstIFrame = true;
						get_camera1.show = true;
						disconnect_img(get_camera1.dis_connect);
						title.setText(Camera_data.getString("Camera1_Label", "CH1"));
						Log.v("test", "3 -> 1");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 1);
						editor.commit();
						return;
					}

					// if (Camera_data.getBoolean("Camera4", false)) {
					// get_camera4.isFirstIFrame = true;
					// get_camera4.show = true;
					// disconnect_img(get_camera4.dis_connect);
					// title.setText(Camera_data.getString("Camera4_Label",
					// "CH4"));
					// Log.v("test", "3 -> 4");
					// SharedPreferences.Editor editor = Camera_data.edit();
					// editor.putInt("show", 4);
					// editor.commit();
					// return;
					// }
				}

				if (Camera_data.getInt("show", 1) == 4) {
					if (Camera_data.getBoolean("Camera4", false)) {
						if (get_camera4.show) {
							if (Camera_data.getBoolean("Camera1", false) || Camera_data.getBoolean("Camera2", false)
									|| Camera_data.getBoolean("Camera3", false)) {
								get_camera4.show = false;
							} else {
								return;
							}

						}
					}

					SES_disable(false);
					if (Camera_data.getBoolean("Camera3", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera3.clearbuffer();
						// get_camera3.isFirstIFrame = true;
						get_camera3.show = true;
						disconnect_img(get_camera3.dis_connect);
						title.setText(Camera_data.getString("Camera3_Label", "CH3"));
						Log.v("test", "2 -> 3");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 3);
						editor.commit();
						return;
					}
					if (Camera_data.getBoolean("Camera2", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera2.clearbuffer();
						// get_camera2.isFirstIFrame = true;
						get_camera2.show = true;
						disconnect_img(get_camera2.dis_connect);
						title.setText(Camera_data.getString("Camera2_Label", "CH2"));
						Log.v("test", "3 -> 2");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 2);
						editor.commit();
						return;
					}

					if (Camera_data.getBoolean("Camera1", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera1.clearbuffer();
						// get_camera1.isFirstIFrame = true;
						get_camera1.show = true;
						disconnect_img(get_camera1.dis_connect);
						title.setText(Camera_data.getString("Camera1_Label", "CH1"));
						Log.v("test", "3 -> 1");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 1);
						editor.commit();
						return;
					}
				}
			}
		});

		next.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (Camera_data.getInt("show", 1) == 1) {
					if (Camera_data.getBoolean("Camera1", false)) {
						if (get_camera1.show) {
							if (Camera_data.getBoolean("Camera2", false) || Camera_data.getBoolean("Camera3", false)
									|| Camera_data.getBoolean("Camera4", false)) {
								get_camera1.show = false;
							} else {
								return;
							}

						}
					}

					SES_disable(false);
					if (Camera_data.getBoolean("Camera2", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera2.clearbuffer();
						// get_camera2.isFirstIFrame = true;
						get_camera2.show = true;
						disconnect_img(get_camera2.dis_connect);
						title.setText(Camera_data.getString("Camera2_Label", "CH2"));
						Log.v("test", "1 -> 2");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 2);
						editor.commit();
						return;
					}

					if (Camera_data.getBoolean("Camera3", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera3.clearbuffer();
						// get_camera3.isFirstIFrame = true;
						get_camera3.show = true;
						disconnect_img(get_camera3.dis_connect);
						title.setText(Camera_data.getString("Camera3_Label", "CH3"));
						Log.v("test", "1 -> 3");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 3);
						editor.commit();
						return;
					}

					// if (Camera_data.getBoolean("Camera4", false)) {
					// get_camera4.isFirstIFrame = true;
					// get_camera4.show = true;
					// disconnect_img(get_camera4.dis_connect);
					// title.setText(Camera_data.getString("Camera4_Label",
					// "CH4"));
					// Log.v("test", "1 -> 4");
					// SharedPreferences.Editor editor = Camera_data.edit();
					// editor.putInt("show", 4);
					// editor.commit();
					// return;
					// }
				}

				if (Camera_data.getInt("show", 1) == 2) {
					if (Camera_data.getBoolean("Camera2", false)) {
						if (get_camera2.show) {
							if (Camera_data.getBoolean("Camera1", false) || Camera_data.getBoolean("Camera3", false)
									|| Camera_data.getBoolean("Camera4", false)) {
								get_camera2.show = false;
							} else {
								return;
							}

						}
					}

					SES_disable(false);

					if (Camera_data.getBoolean("Camera3", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera3.clearbuffer();
						// get_camera3.isFirstIFrame = true;
						get_camera3.show = true;
						disconnect_img(get_camera3.dis_connect);
						title.setText(Camera_data.getString("Camera3_Label", "CH3"));
						Log.v("test", "2 -> 3");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 3);
						editor.commit();
						return;
					}
					// if (Camera_data.getBoolean("Camera4", false)) {
					// get_camera4.isFirstIFrame = true;
					// get_camera4.show = true;
					// disconnect_img(get_camera4.dis_connect);
					// title.setText(Camera_data.getString("Camera4_Label",
					// "CH4"));
					// Log.v("test", "2 -> 4");
					// SharedPreferences.Editor editor = Camera_data.edit();
					// editor.putInt("show", 4);
					// editor.commit();
					// return;
					// }
					if (Camera_data.getBoolean("Camera1", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera1.clearbuffer();
						// get_camera1.isFirstIFrame = true;
						get_camera1.show = true;
						disconnect_img(get_camera1.dis_connect);
						title.setText(Camera_data.getString("Camera1_Label", "CH1"));
						Log.v("test", "2 -> 1");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 1);
						editor.commit();
						return;
					}

				}

				if (Camera_data.getInt("show", 1) == 3) {
					if (Camera_data.getBoolean("Camera3", false)) {
						if (get_camera3.show) {
							if (Camera_data.getBoolean("Camera1", false) || Camera_data.getBoolean("Camera2", false)
									|| Camera_data.getBoolean("Camera4", false)) {
								get_camera3.show = false;
							} else {
								return;
							}

						}
					}

					SES_disable(false);

					// if (Camera_data.getBoolean("Camera4", false)) {
					// get_camera4.isFirstIFrame = true;
					// get_camera4.show = true;
					// disconnect_img(get_camera4.dis_connect);
					// title.setText(Camera_data.getString("Camera4_Label",
					// "CH4"));
					// Log.v("test", "3 -> 4");
					// SharedPreferences.Editor editor = Camera_data.edit();
					// editor.putInt("show", 4);
					// editor.commit();
					// return;
					// }

					if (Camera_data.getBoolean("Camera1", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera1.clearbuffer();
						// get_camera1.isFirstIFrame = true;
						get_camera1.show = true;
						disconnect_img(get_camera1.dis_connect);
						title.setText(Camera_data.getString("Camera1_Label", "CH1"));
						Log.v("test", "3 -> 1");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 1);
						editor.commit();
						return;
					}
					if (Camera_data.getBoolean("Camera2", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera2.clearbuffer();
						// get_camera2.isFirstIFrame = true;
						get_camera2.show = true;
						disconnect_img(get_camera2.dis_connect);
						title.setText(Camera_data.getString("Camera2_Label", "CH2"));
						Log.v("test", "3 -> 2");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 2);
						editor.commit();
						return;
					}
				}

				if (Camera_data.getInt("show", 1) == 4) {
					if (Camera_data.getBoolean("Camera4", false)) {
						if (get_camera4.show) {
							if (Camera_data.getBoolean("Camera1", false) || Camera_data.getBoolean("Camera2", false)
									|| Camera_data.getBoolean("Camera3", false)) {
								get_camera4.show = false;
							} else {
								return;
							}

						}
					}

					SES_disable(false);
					if (Camera_data.getBoolean("Camera1", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera1.clearbuffer();
						// get_camera1.isFirstIFrame = true;
						get_camera1.show = true;
						disconnect_img(get_camera1.dis_connect);
						title.setText(Camera_data.getString("Camera1_Label", "CH1"));
						Log.v("test", "4 -> 1");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 1);
						editor.commit();
						return;
					}
					if (Camera_data.getBoolean("Camera2", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera2.clearbuffer();
						// get_camera2.isFirstIFrame = true;
						get_camera2.show = true;
						disconnect_img(get_camera2.dis_connect);
						title.setText(Camera_data.getString("Camera2_Label", "CH2"));
						Log.v("test", "4 -> 2");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 2);
						editor.commit();
						return;
					}
					if (Camera_data.getBoolean("Camera3", false)) {
						// g711.flush();
						// de_h264.flush();
						get_camera3.clearbuffer();
						// get_camera3.isFirstIFrame = true;
						get_camera3.show = true;
						disconnect_img(get_camera3.dis_connect);
						title.setText(Camera_data.getString("Camera3_Label", "CH3"));
						Log.v("test", "4 -> 3");
						SharedPreferences.Editor editor = Camera_data.edit();
						editor.putInt("show", 3);
						editor.commit();
						return;
					}
				}
			}
		});

		system.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				notshowall();

				call = true;
				Intent intent = new Intent();
				if (Camera_data.getBoolean("OnDemand", false)) {
					intent.setClass(LiveBox_Activity.this, Ondemand_Setting_Activity.class);
				} else {
					intent.setClass(LiveBox_Activity.this, Setting_Activity.class);
				}

				startActivity(intent);
				finish();
				onDestroy();
				System.exit(0);
			}
		});

		logo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				int currentapiVersion = android.os.Build.VERSION.SDK_INT;

				notshowall();
				call = true;
				stopService(new Intent(LiveBox_Activity.this, Remote_Service.class));
				Intent intent = new Intent();
				if (Build.VERSION.RELEASE.equals("4.4.2")) {
					intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
				} else {
					ComponentName comp = new ComponentName("com.android.settings",
							"com.android.settings.ScreenSettings");
					intent.setComponent(comp);
				}
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
				startActivity(intent);
				finish();
				onDestroy();
				System.exit(0);
			}
		});

		Change_iCam = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.arg1) {
				case 0x01:
					String cam_num = (String) msg.obj;
					if (cam_num.equalsIgnoreCase("0")) {
						if (Camera_data.getInt("show", 1) == 2) {
							if (Camera_data.getBoolean("Camera2", false)) {
								if (get_camera2.show) {
									get_camera2.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) == 3) {
							if (Camera_data.getBoolean("Camera3", false)) {
								if (get_camera3.show) {
									get_camera3.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) == 4) {
							if (Camera_data.getBoolean("Camera4", false)) {
								if (get_camera4.show) {
									get_camera4.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) != 1) {
							SES_disable(false);
							if (Camera_data.getBoolean("Camera1", false)) {
								// g711.flush();
								// de_h264.flush();
								get_camera1.clearbuffer();
								// get_camera1.isFirstIFrame = true;
								get_camera1.show = true;
								disconnect_img(get_camera1.dis_connect);
								title.setText(Camera_data.getString("Camera1_Label", "CH1"));
								// disable_image(false);
							} else {
								title.setText(Camera_data.getString("Camera1_Label", "CH1"));
								disconnect_img(false);
								// disable_image(true);
							}
							SharedPreferences.Editor editor = Camera_data.edit();
							editor.putInt("show", 1);
							editor.commit();
						}
						sendicam();
						returncmd(cam_type);
					} else if (cam_num.equalsIgnoreCase("1")) {
						if (Camera_data.getInt("show", 1) == 1) {
							if (Camera_data.getBoolean("Camera1", false)) {
								if (get_camera1.show) {
									get_camera1.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) == 3) {
							if (Camera_data.getBoolean("Camera3", false)) {
								if (get_camera3.show) {
									get_camera3.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) == 4) {
							if (Camera_data.getBoolean("Camera4", false)) {
								if (get_camera4.show) {
									get_camera4.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) != 2) {
							SES_disable(false);
							if (Camera_data.getBoolean("Camera2", false)) {
								// g711.flush();
								// de_h264.flush();
								get_camera2.clearbuffer();
								// get_camera2.isFirstIFrame = true;
								get_camera2.show = true;
								disconnect_img(get_camera2.dis_connect);
								title.setText(Camera_data.getString("Camera2_Label", "CH2"));
								// disable_image(false);
							} else {
								title.setText(Camera_data.getString("Camera2_Label", "CH2"));
								disconnect_img(false);
								// disable_image(true);
							}
							SharedPreferences.Editor editor = Camera_data.edit();
							editor.putInt("show", 2);
							editor.commit();
						}
						sendicam();
						returncmd(cam_type);
					} else if (cam_num.equalsIgnoreCase("2")) {
						if (Camera_data.getInt("show", 1) == 2) {
							if (Camera_data.getBoolean("Camera2", false)) {
								if (get_camera2.show) {
									get_camera2.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) == 1) {
							if (Camera_data.getBoolean("Camera1", false)) {
								if (get_camera1.show) {
									get_camera1.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) == 4) {
							if (Camera_data.getBoolean("Camera4", false)) {
								if (get_camera4.show) {
									get_camera4.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) != 3) {
							SES_disable(false);
							if (Camera_data.getBoolean("Camera3", false)) {
								// g711.flush();
								// de_h264.flush();
								get_camera3.clearbuffer();
								// get_camera3.isFirstIFrame = true;
								get_camera3.show = true;
								disconnect_img(get_camera3.dis_connect);
								title.setText(Camera_data.getString("Camera3_Label", "CH3"));
								// disable_image(false);
							} else {
								title.setText(Camera_data.getString("Camera3_Label", "CH3"));
								disconnect_img(false);
								// disable_image(true);
							}
							SharedPreferences.Editor editor = Camera_data.edit();
							editor.putInt("show", 3);
							editor.commit();
						}
						sendicam();
						returncmd(cam_type);
					} else if (cam_num.equalsIgnoreCase("3")) {
						if (Camera_data.getInt("show", 1) == 2) {
							if (Camera_data.getBoolean("Camera2", false)) {
								if (get_camera2.show) {
									get_camera2.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) == 3) {
							if (Camera_data.getBoolean("Camera3", false)) {
								if (get_camera3.show) {
									get_camera3.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) == 1) {
							if (Camera_data.getBoolean("Camera1", false)) {
								if (get_camera1.show) {
									get_camera1.show = false;
								}
							}
						}

						if (Camera_data.getInt("show", 1) != 4) {
							SES_disable(false);
							if (Camera_data.getBoolean("Camera4", false)) {
								// g711.flush();
								// de_h264.flush();
								get_camera4.clearbuffer();
								// get_camera4.isFirstIFrame = true;
								get_camera4.show = true;
								disconnect_img(get_camera4.dis_connect);
								title.setText(Camera_data.getString("Camera4_Label", "CH4"));
								// disable_image(false);
							} else {
								title.setText(Camera_data.getString("Camera4_Label", "CH4"));
								disconnect_img(false);
								// disable_image(true);
							}
							SharedPreferences.Editor editor = Camera_data.edit();
							editor.putInt("show", 4);
							editor.commit();
						}
						sendicam();
						returncmd(cam_type);
					} else if (cam_num.equalsIgnoreCase("5")) {
						returncmd(OK);
					} else if (cam_num.equalsIgnoreCase("6")) {
						last.performClick();
						sendicam();
						returncmd(cam_type);
						// returncmd(OK);
					} else if (cam_num.equalsIgnoreCase("7")) {
						next.performClick();
						sendicam();
						returncmd(cam_type);
					} else if (cam_num.equalsIgnoreCase("8")) {
						sendicam();
						returncmd(cam_type);
					} else if (cam_num.equalsIgnoreCase("100")) {
						if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
							audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
						} else {
							audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
						}
						returncmd(volume);
					} else {
						returncmd(Fail);
					}
					break;
				case 0x02:
					if (Camera_data.getInt("net_type", eth0) == wifi) {
						check_wifi();
					} else {
						open_eth0();
					}
					break;
				case 0x03:
					Log.v("test", " intet 0");
					notshowall();
					call = true;
					Intent intent = new Intent();
					intent.setClass(LiveBox_Activity.this, ReLoading_Activity.class);
					Bundle bundle = new Bundle();
					bundle.putInt("type", 0);
					intent.putExtras(bundle);
					startActivity(intent);
					finish();
					onDestroy();
					System.exit(0);
					break;
				case 0x04:
					Log.v("test", " intet 1");
					notshowall();
					call = true;
					Intent intent1 = new Intent();
					intent1.setClass(LiveBox_Activity.this, ReLoading_Activity.class);
					Bundle bundle1 = new Bundle();
					bundle1.putInt("type", 1);
					intent1.putExtras(bundle1);
					startActivity(intent1);
					finish();
					onDestroy();
					System.exit(0);
					break;
				case 0x05:
					notshowall();
					call = true;
					Intent intent2 = new Intent();
					intent2.setClass(LiveBox_Activity.this, Loading_Activity.class);
					startActivity(intent2);
					finish();
					onDestroy();
					System.exit(0);
					break;
				}

			}
		};

		// logochange.start();
	}

	Thread logochange = new Thread() {
		public void run() {
			boolean flag = false;
			while (run) {
				try {
					if (flag) {
						flag = false;
						Message msg = new Message();
						msg.arg1 = 0x1a;
						handler.sendMessage(msg);
					} else {
						flag = true;
						Message msg = new Message();
						msg.arg1 = 0x1b;
						handler.sendMessage(msg);
					}
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				}
			}
		}
	};

	public void notshowall() {

		if (get_camera1 != null) {
			if (get_camera1.show) {
				get_camera1.show = false;
			}

		}
		if (get_camera2 != null) {
			if (get_camera2.show) {
				get_camera2.show = false;
			}

		}
		if (get_camera3 != null) {
			if (get_camera3.show) {
				get_camera3.show = false;
			}

		}
		// if (get_camera4 != null) {
		// if (get_camera4.show) {
		// get_camera4.show = false;
		// }
		// }
	}

	public void sendicam() {
		show_type = "LIVEBOX_";
		if (Camera_data.getBoolean("Camera1", false)) {
			if (get_camera1.show) {
				show_type = show_type + 1 + ":";
			} else {
				show_type = show_type + 0 + ":";
			}
		} else {
			if (Camera_data.getInt("show", 1) == 1) {
				show_type = show_type + 3 + ":";
			} else {
				show_type = show_type + 2 + ":";
			}
		}

		if (Camera_data.getBoolean("Camera2", false)) {
			if (get_camera2.show) {
				show_type = show_type + 1 + ":";
			} else {
				show_type = show_type + 0 + ":";
			}
		} else {
			if (Camera_data.getInt("show", 1) == 2) {
				show_type = show_type + 3 + ":";
			} else {
				show_type = show_type + 2 + ":";
			}
		}

		if (Camera_data.getBoolean("Camera3", false)) {
			if (get_camera3.show) {
				show_type = show_type + 1 + ":";
			} else {
				show_type = show_type + 0 + ":";
			}
		} else {
			if (Camera_data.getInt("show", 1) == 3) {
				show_type = show_type + 3 + ":";
			} else {
				show_type = show_type + 2 + ":";
			}
		}

		// if (Camera_data.getBoolean("Camera4", false)) {
		// if (get_camera4.show) {
		// show_type = show_type + 1;
		// } else {
		// show_type = show_type + 0;
		// }
		// } else {
		// if (Camera_data.getInt("show", 1) == 4) {
		// show_type = show_type + 3;
		// } else {
		// show_type = show_type + 2;
		// }
		// }

		if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
			show_type = show_type + 0;
		} else {
			show_type = show_type + 1;
		}
	}

	public void disconnect_img(boolean dis_connect) {
		if (dis_connect) {
			disabletext.setText("Disconnected");
			if (disconnect.getVisibility() == View.GONE) {
				disconnect.setVisibility(View.VISIBLE);
			}
			disable_image(true);
		} else {
			if (disconnect.getVisibility() == View.VISIBLE) {
				disconnect.setVisibility(View.GONE);
			}
			disable_image(false);
		}

	}

	public void disable_image(Boolean disable) {
		if (disable) {
			if (disabletext.getVisibility() == View.GONE) {
				disabletext.setVisibility(View.VISIBLE);
			}
		} else {
			if (disabletext.getVisibility() == View.VISIBLE) {
				disabletext.setVisibility(View.GONE);
			}
		}
	}

	public void SES_disable(Boolean disable) {
		if (disable) {
			if (image.getVisibility() == View.GONE) {
				image.setVisibility(View.VISIBLE);
			}
		} else {
			if (image.getVisibility() == View.VISIBLE) {
				image.setVisibility(View.GONE);
			}
		}
	}

	private void returncmd(int cmd) {
		try {
			switch (cmd) {
			case OK:
				isend.sendOK();
				break;
			case Fail:
				isend.sendFail();
				break;
			case cam_type:
				isend.sendTYPE(show_type);
				break;
			case volume:
				isend.sendvolumetype();
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private ServiceConnection conn = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			Log.i("Vic", "ServiceConnection -> onServiceConnected");
			isend = IServiceControl.Stub.asInterface(service);
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
		};
	};

	public boolean checkroot() {
		return findBinary("su");
	}

	public boolean findBinary(String binaryName) {
		boolean found = false;
		if (!found) {
			String[] places = { "/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/",
					"/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/" };
			for (String where : places) {
				if (new File(where + binaryName).exists()) {
					found = true;
					break;
				}
			}
		}
		return found;
	}

	public void check_wifi() {
		// new Thread() {
		// public void run() {
		try {
			if (checkroot) {
				Process proc = Runtime.getRuntime().exec("su");
				DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
				opt.writeBytes("ifconfig eth0 down\n");
				opt.writeBytes("exit\n");
				opt.flush();

				// DataInputStream ls_in = new
				// DataInputStream(proc.getInputStream());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.v("test", e.toString());
		}
		// }
		// }.start();
		if (ApManager.isApOn(LiveBox_Activity.this)) {
			ApManager.configApState(LiveBox_Activity.this);
		}

		WifiManager wifi_service = (WifiManager) getSystemService(WIFI_SERVICE);
		WifiInfo wifiInfo = wifi_service.getConnectionInfo();
		
		try{  
			if(!wifi_service.isWifiEnabled() || wifiInfo == null) //判斷WIFI是否有開啟，沒有就打開
			{			
				wifi_service.setWifiEnabled(true);	
				
				while (wifi_service.getWifiState() == WifiManager.WIFI_STATE_ENABLING) {
		            try {
		                //为了避免程序一直while循环，让它睡个100毫秒在检测……
		                Thread.currentThread();
		                Thread.sleep(100);
		            } catch (InterruptedException ie) {
		            }
		        }
			}	
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			wifi_service.setWifiEnabled(true);		
		}
		Log.v("test","wifi ssid " + Camera_data.getString("wifi_ssid", "defaultssid"));
		if (wifiInfo.getSSID().indexOf(Camera_data.getString("wifi_ssid", "defaultssid")) != -1) {
			int ipAddress = wifiInfo.getIpAddress();
			String ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
					(ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
			Log.v("test","wifi ip " + ip);
			if (ip.indexOf(Camera_data.getString("wifi", "192.168.1.51")) == -1) {
				WifiConfigManage wificon = new WifiConfigManage(wifi_service, wifiInfo);
				wificon.openWifi("STATIC", Camera_data.getString("wifi_ssid", "defaultssid"),
						Camera_data.getString("wifi_pwd", ""), Camera_data.getInt("pwd_type", 0x00),
						Camera_data.getString("wifi", "192.168.1.51"),
						netMaskToInt(Camera_data.getString("mask", "255.255.255.0")),
						Camera_data.getString("geteway", "192.168.1.1"), "8.8.8.8", "8.8.4.4");
			}
		} else {
			WifiConfigManage wificon = new WifiConfigManage(wifi_service, wifiInfo);
			Log.v("test" , Camera_data.getString("wifi_ssid", "defaultssid") + " " + 
					Camera_data.getString("wifi_pwd", "")+ " " + 
					Camera_data.getString("wifi", "192.168.1.51") +" "+
					Camera_data.getString("mask", "255.255.255.0")+" "+
					Camera_data.getString("geteway", "192.168.1.1"));
			wificon.openWifi("STATIC", Camera_data.getString("wifi_ssid", "defaultssid"),
					Camera_data.getString("wifi_pwd", ""), Camera_data.getInt("pwd_type", 0x00),
					Camera_data.getString("wifi", "192.168.1.51"),
					netMaskToInt(Camera_data.getString("mask", "255.255.255.0")),
					Camera_data.getString("geteway", "192.168.1.1"), "8.8.8.8", "8.8.4.4");
		}
	}

	public void open_eth0() {
		// WifiManager wifi_service = (WifiManager)
		// getSystemService(WIFI_SERVICE);
		// if (wifi_service.isWifiEnabled()) {
		// wifi_service.setWifiEnabled(false);
		// }

		if (!ApManager.isApOn(LiveBox_Activity.this)) {
			Log.v("hotspot", "false");
			ApManager.configApState(LiveBox_Activity.this);
		} else {
			Log.v("hotspot", "true");
		}

		new Thread() {
			public void run() {
				try {
					if (checkroot) {
						Process proc = Runtime.getRuntime().exec("su");
						DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
						opt.writeBytes("ifconfig eth0 up\n");
						opt.writeBytes("ifconfig eth0 " + Camera_data.getString("eth0", "192.168.1.51") + " netmask "
								+ Camera_data.getString("mask", "255.255.255.0") + "\n");
						opt.writeBytes("route add default gw " + Camera_data.getString("geteway", "192.168.1.1")
								+ " dev eth0\n");
						opt.writeBytes("exit\n");
						opt.flush();

						// DataInputStream ls_in = new
						// DataInputStream(proc.getInputStream());

					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.v("test", e.toString());
				}
			}
		}.start();
	}

	/* 計算 String="255.255.255.0" to int=24 多少個1 */
	public int netMaskToInt(String netmask) {
		// networkPrefixLength
		String[] maskarray = netmask.split("\\.");
		int number1 = 0;
		for (int i = 0; i < maskarray.length; i++) {
			String binaryMask = Integer.toBinaryString(Integer.parseInt(maskarray[i])); // 255
																						// ->
																						// 11111111
			// System.out.println(maskarray[i]+"="+numeber);
			String[] ary = binaryMask.split("");
			for (int j = 0; j < ary.length; j++) {
				if (ary[j].equals("1")) {
					number1++;
				}
			}
		}
		// System.out.println("網路字首長度="+number1);
		return number1;
	}

	void camera_thread() {
		if (Camera_data.getBoolean("Camera1", false)) {
			// if (get_camera1 == null) {
			Log.v("thread1", "null");
			get_camera1 = new Getdata(LiveBox_Activity.this, 1, disabletext, disconnect, image, frame, time, g711, pcm,
					de_h264, Camera_data.getString("Camera1_URL", "192.168.1.171"), Camera_data.getInt("Camera1_Port",
							80), Camera_data.getString("Camera1_Account", "root"), Camera_data.getString("Camera1_Pwd",
							"27507522"), camera1, Camera_data.getInt("Camera1_type", Camera_v3), Camera_data.getInt(
							"classorgroup1", 0) + 1);

			if (Camera_data.getInt("show", 1) == 1) {
				get_camera1.show = true;

				title.setText(Camera_data.getString("Camera1_Label", "CH1"));
				SharedPreferences.Editor editor = Camera_data.edit();
				// disable_image(false);
				editor.putInt("show", 1);
				editor.commit();
			} else {
				get_camera1.show = false;
			}
			getdata1_thread = new GetCamera1_Data();
			getdata1_thread.start();
		} else {
			if (getdata1_thread != null) {
				if (getdata1_thread.isAlive()) {
					getdata1_thread.interrupt();
					getdata1_thread = null;
				}
			}
			// if (Camera_data.getInt("show", 1) == 1) {
			// title.setText(Camera_data.getString("Camera1_Label", "CH1"));
			// disable_image(true);
			// }
		}

		if (Camera_data.getBoolean("Camera2", false)) {
			// if (get_camera2 == null) {
			get_camera2 = new Getdata(LiveBox_Activity.this, 2, disabletext, disconnect, image, frame, time, g711, pcm,
					de_h264, Camera_data.getString("Camera2_URL", "192.168.1.172"), Camera_data.getInt("Camera2_Port",
							80), Camera_data.getString("Camera2_Account", "root"), Camera_data.getString("Camera2_Pwd",
							"27507522"), camera2, Camera_data.getInt("Camera2_type", Camera_v3), Camera_data.getInt(
							"classorgroup2", 0) + 1);

			if ((!Camera_data.getBoolean("Camera1", false)) || Camera_data.getInt("show", 1) == 2) {
				get_camera2.show = true;
				title.setText(Camera_data.getString("Camera2_Label", "CH2"));
				SharedPreferences.Editor editor = Camera_data.edit();
				// disable_image(false);
				editor.putInt("show", 2);
				editor.commit();
			} else {
				get_camera2.show = false;
			}
			getdata2_thread = new GetCamera2_Data();
			getdata2_thread.start();
		} else {
			if (getdata2_thread != null) {
				if (getdata2_thread.isAlive()) {
					getdata2_thread.interrupt();
					getdata2_thread = null;
				}
			}
			Log.v("test", "show " + Camera_data.getInt("show", 1));
			// if (Camera_data.getInt("show", 1) == 2) {
			// title.setText(Camera_data.getString("Camera2_Label", "CH2"));
			// disable_image(true);
			// }
		}

		if (Camera_data.getBoolean("Camera3", false)) {
			// if (get_camera3 == null) {
			get_camera3 = new Getdata(LiveBox_Activity.this, 3, disabletext, disconnect, image, frame, time, g711, pcm,
					de_h264, Camera_data.getString("Camera3_URL", "1.192.168.173"), Camera_data.getInt("Camera3_Port",
							80), Camera_data.getString("Camera3_Account", "root"), Camera_data.getString("Camera3_Pwd",
							"27507522"), camera3, Camera_data.getInt("Camera3_type", Camera_v3), Camera_data.getInt(
							"classorgroup3", 0) + 1);
			if ((!Camera_data.getBoolean("Camera1", false) && !Camera_data.getBoolean("Camera2", false))
					|| Camera_data.getInt("show", 1) == 3) {
				get_camera3.show = true;
				title.setText(Camera_data.getString("Camera3_Label", "CH3"));
				SharedPreferences.Editor editor = Camera_data.edit();
				// disable_image(false);
				editor.putInt("show", 3);
				editor.commit();
			} else {
				get_camera3.show = false;
			}
			getdata3_thread = new GetCamera3_Data();
			getdata3_thread.start();
		} else {
			if (getdata3_thread != null) {
				if (getdata3_thread.isAlive()) {
					getdata3_thread.interrupt();
					getdata3_thread = null;
				}
			}
			
			// if (Camera_data.getInt("show", 1) == 3) {
			// title.setText(Camera_data.getString("Camera3_Label", "CH3"));
			// disable_image(true);
			// }
		}

		if (!Camera_data.getBoolean("Camera1", false) && !Camera_data.getBoolean("Camera2", false)
				&& !Camera_data.getBoolean("Camera3", false)) {
			disable_image(true);
		}

		// if (Camera_data.getBoolean("Camera4", false)) {
		// // if (get_camera4 == null) {
		// get_camera4 = new Getdata(4, disabletext, disconnect, image, frame,
		// time, g711, pcm, de_h264,
		// Camera_data.getString("Camera4_URL", "1.192.168.174"),
		// Camera_data.getInt("Camera4_Port", 80),
		// Camera_data.getString("Camera4_Account", "root"),
		// Camera_data.getString("Camera4_Pwd", "27507522"),
		// camera4, Camera_data.getInt("Camera4_type", Camera_v3),
		// Camera_data.getInt("classorgroup4", 0) + 1);
		// if ((!Camera_data.getBoolean("Camera1", false) &&
		// !Camera_data.getBoolean("Camera2", false) && !Camera_data
		// .getBoolean("Camera3", false)) || Camera_data.getInt("show", 1) == 4)
		// {
		// get_camera4.show = true;
		// title.setText(Camera_data.getString("Camera4_Label", "CH4"));
		// SharedPreferences.Editor editor = Camera_data.edit();
		// editor.putInt("show", 4);
		// editor.commit();
		// } else {
		// get_camera4.show = false;
		// }
		//
		// getdata4_thread = new GetCamera4_Data();
		// getdata4_thread.start();
		// } else {
		// if (getdata4_thread != null) {
		// if (getdata4_thread.isAlive()) {
		// getdata4_thread.interrupt();
		// getdata4_thread = null;
		// }
		// }
		// if (Camera_data.getInt("show", 1) == 4) {
		// title.setText(Camera_data.getString("Camera4_Label", "CH4"));
		// disable_image(true);
		// }
		// }
	}

	void thread_interupt() {
		if (getdata1_thread != null) {
			if (getdata1_thread.isAlive()) {
				getdata1_thread.interrupt();
			}
		}

		if (getdata2_thread != null) {
			if (getdata2_thread.isAlive()) {
				getdata2_thread.interrupt();
			}
		}

		if (getdata3_thread != null) {
			if (getdata3_thread.isAlive()) {
				getdata3_thread.interrupt();
			}
		}

		// if (getdata4_thread != null) {
		// if (getdata4_thread.isAlive()) {
		// getdata4_thread.interrupt();
		// }
		// }
	}

	private class GetCamera1_Data extends Thread {
		public void run() {
			Log.v("test", "camera1 thread");
			get_camera1.getHttp();
		}
	}

	private class GetCamera2_Data extends Thread {
		public void run() {
			Log.v("test", "camera2 thread");
			get_camera2.getHttp();
		}
	}

	private class GetCamera3_Data extends Thread {
		public void run() {
			Log.v("test", "camera3 thread");
			get_camera3.getHttp();

		}
	}

	private class GetCamera4_Data extends Thread {
		public void run() {
			Log.v("test", "camera4 thread");
			get_camera4.getHttp();
		}
	}

	Thread detect_getdata = new Thread() {
		public void run() {
			Log.v("getdetect", "get start");
			int cam1_tem = -1;
			int cam2_tem = -1;
			int cam3_tem = -1;
			int cam4_tem = -1;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while (true) {
				if (run) {
					try {
						if (Camera_data.getInt("net_type", eth0) == eth0) {
							if (!ApManager.isApOn(LiveBox_Activity.this)) {
								ApManager.configApState(LiveBox_Activity.this);
							}
						}
						// Log.v("check" , "check thread");
						if (Camera_data.getBoolean("Camera1", false)) {
							if (cam1_tem != get_camera1.count_live) {
								cam1_tem = get_camera1.count_live;
							} else {
								if (getdata1_thread.isAlive()) {
									get_camera1.run = false;
									getdata1_thread.interrupt();
									getdata1_thread = null;
									Thread.sleep(5000);
									getdata1_thread = new GetCamera1_Data();
									get_camera1.run = true;
									get_camera1.isFirstIFrame = true;
									get_camera1.audio_continue = false;
									getdata1_thread.start();
									MyLog.v("Thread", "icam1 thread force reostart");
								} else {
									get_camera1.run = false;
									getdata1_thread = null;
									Thread.sleep(5000);
									get_camera1.run = true;
									getdata1_thread = new GetCamera1_Data();
									get_camera1.isFirstIFrame = true;
									get_camera1.audio_continue = false;
									getdata1_thread.start();
									MyLog.v("Thread", "icam1 thread force reostart");
								}
							}
						}

						if (Camera_data.getBoolean("Camera2", false)) {
							if (cam2_tem != get_camera2.count_live) {
								cam2_tem = get_camera2.count_live;
							} else {
								if (getdata2_thread.isAlive()) {
									get_camera2.run = false;
									getdata2_thread.interrupt();
									getdata2_thread = null;
									Thread.sleep(5000);
									getdata2_thread = new GetCamera2_Data();
									get_camera2.run = true;
									get_camera2.isFirstIFrame = true;
									get_camera2.audio_continue = false;
									getdata2_thread.start();
									MyLog.v("Thread", "icam2 thread force reostart");
								} else {
									get_camera2.run = false;
									getdata2_thread = null;
									Thread.sleep(5000);
									get_camera2.run = true;
									getdata2_thread = new GetCamera2_Data();
									get_camera2.isFirstIFrame = true;
									get_camera2.audio_continue = false;
									getdata2_thread.start();
									MyLog.v("Thread", "icam2 thread force reostart");
								}
							}
						}

						if (Camera_data.getBoolean("Camera3", false)) {
							if (cam3_tem != get_camera3.count_live) {
								cam3_tem = get_camera3.count_live;
							} else {
								if (getdata3_thread.isAlive()) {
									get_camera3.run = false;
									getdata3_thread.interrupt();
									getdata3_thread = null;
									Thread.sleep(5000);
									getdata3_thread = new GetCamera3_Data();
									get_camera3.run = true;
									get_camera3.isFirstIFrame = true;
									get_camera3.audio_continue = false;
									getdata3_thread.start();
									MyLog.v("Thread", "icam3 thread force reostart");
								} else {
									get_camera3.run = false;
									getdata3_thread = null;
									Thread.sleep(5000);
									get_camera3.run = true;
									getdata3_thread = new GetCamera3_Data();
									get_camera3.isFirstIFrame = true;
									get_camera3.audio_continue = false;
									getdata3_thread.start();
									MyLog.v("Thread", "icam3 thread force reostart");
								}
							}
						}

						// if (Camera_data.getBoolean("Camera4", false)) {
						// if (cam4_tem != get_camera4.count_live) {
						// cam4_tem = get_camera4.count_live;
						// } else {
						// if (getdata4_thread.isAlive()) {
						// get_camera4.run = false;
						// getdata4_thread.interrupt();
						// getdata4_thread = null;
						// Thread.sleep(5000);
						// getdata4_thread = new GetCamera4_Data();
						// get_camera4.run = true;
						// getdata4_thread.start();
						// Log.v("test", "icam4 thread reon");
						// } else {
						// get_camera4.run = false;
						// getdata4_thread = null;
						// Thread.sleep(5000);
						// get_camera4.run = true;
						// getdata4_thread = new GetCamera4_Data();
						// getdata4_thread.start();
						// Log.v("test", "icam4 thread reon");
						// }
						// }
						// }
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						Log.v("test", "check icam insterupt");
						break;
					}
				} else {
					break;
				}
			}
		}
	};

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case 0x1a:
				logo.setImageBitmap(logo_a);
				break;
			case 0x1b:
				logo.setImageBitmap(logo_b);
				break;
			case 0x0c:
				disconnect_img(get_camera1.dis_connect);
				break;
			case 0x0d:
				disconnect_img(get_camera2.dis_connect);
				break;
			case 0x0e:
				disconnect_img(get_camera3.dis_connect);
				break;
			case 0x0f:
				disconnect_img(get_camera4.dis_connect);
				break;
			}
		}
	};

	// @Override
	// public void onPause() {
	// super.onPause();
	// Log.v("TEST", "pause");
	// finish();
	// }

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Log.v("IR", " " + keyCode);
		return false;
		// return super.onKeyDown(keyCode, event);
	}

	/* 檢查版本 versionName */
	private void checkVNupdatePG() {

		try {
			PackageManager pm = getPackageManager();
			/* NewVer */
			PackageInfo info = pm.getPackageArchiveInfo(apkPath, 0);
			int newVersionCode = info.versionCode;
			String newVersionName = info.versionName;

			// pm.setInstallerPackageName(targetPackage, installerPackageName)

			/* NowVer */
			info = pm.getPackageInfo(getPackageName(), 0);
			int nowVersionCode = info.versionCode;
			String nowVersionName = info.versionName;

			Log.d("APK", "LIVEBOX+.apk = " + "VersionCode : " + newVersionCode + ", VersionName : " + newVersionName);
			Log.d("APK", "目前 = " + "VersionCode : " + nowVersionCode + ", VersionName : " + nowVersionName);

			if (newVersionCode > nowVersionCode || Float.parseFloat(newVersionName) > Float.parseFloat(nowVersionName)) {
				/* 自動更新 需ROOT權限 */
				try {
					Process localProcess = Runtime.getRuntime().exec("su");
					OutputStream os = localProcess.getOutputStream();
					DataOutputStream dos = new DataOutputStream(os);
					dos.writeBytes("pm install -r " + apkPath + "\n");
					dos.flush();
					dos.writeBytes("exit\n");
					dos.flush();
				} catch (Exception e) {
					Log.e("APK", "update erroe", e);
					// Log.d(TAG,"no root");
				}

			}
		} catch (Exception ex) {
			Log.d("APK", "install error", ex);
		}
	}

	class MyUncaughtHandler implements UncaughtExceptionHandler {
		@Override
		public void uncaughtException(Thread thread, Throwable ex) {
			Log.e("MyUncaughtHandler", ex.getMessage(), ex);
		}
	}

	@Override
	public void onDestroy() {
		Log.v("getdata", "destory");
		run = false;
		animationDrawable.stop();

		if (detect_getdata.isAlive()) {
			detect_getdata.interrupt();
		}

		if (getdata1_thread != null) {
			get_camera1.run = false;
			if (getdata1_thread.isAlive()) {
				getdata1_thread.interrupt();
				getdata1_thread = null;
			}
			get_camera1 = null;
		}

		if (getdata2_thread != null) {
			get_camera2.run = false;
			if (getdata2_thread.isAlive()) {
				getdata2_thread.interrupt();
				getdata2_thread = null;
			}
			get_camera2 = null;
		}

		if (getdata3_thread != null) {
			get_camera3.run = false;
			if (getdata3_thread.isAlive()) {
				getdata3_thread.interrupt();
				getdata3_thread = null;
			}
			get_camera3 = null;
		}

		// if (getdata4_thread != null) {
		// get_camera4.run = false;
		// if (getdata4_thread.isAlive()) {
		// getdata4_thread.interrupt();
		// getdata4_thread = null;
		// }
		// get_camera4 = null;
		// }

		if (!call) {
			stopService(new Intent(LiveBox_Activity.this, Remote_Service.class));
			throw new NullPointerException();
		}

		// if (logochange.isAlive()) {
		// logochange.interrupt();
		// }
		super.onDestroy();
	}

	public Bitmap getTransparentBitmap(Bitmap sourceImg, int number) {
		int[] argb = new int[sourceImg.getWidth() * sourceImg.getHeight()];

		sourceImg.getPixels(argb, 0, sourceImg.getWidth(), 0, 0, sourceImg

		.getWidth(), sourceImg.getHeight());// 获得图片的ARGB值

		number = number * 255 / 100;

		for (int i = 0; i < argb.length; i++) {

			argb[i] = (number << 24) | (argb[i] & 0x00FFFFFF);

		}

		sourceImg = Bitmap.createBitmap(argb, sourceImg.getWidth(), sourceImg

		.getHeight(), Config.ARGB_8888);

		return sourceImg;
	}

	public void NoticeDialog() {
		notice_view = getLayoutInflater().inflate(R.layout.notice_dialog, null);

		dialog_content = (TextView) notice_view.findViewById(R.id.content_tv);
		dialog_ok = (Button) notice_view.findViewById(R.id.ok_btn);
		dialog_content.setText("Now LiveBox is updating new version.");

		NAD = new AlertDialog.Builder(this).setView(notice_view);
		N_alert = NAD.create();
		N_alert.setCancelable(false);

		N_alert.show();

		dialog_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				N_alert.dismiss();
			}
		});
	}

	private String getLocaleLanguage() {
		Locale l = Locale.getDefault();
		return String.format("%s-%s", l.getLanguage(), l.getCountry());
	}
}
