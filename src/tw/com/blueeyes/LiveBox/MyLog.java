package tw.com.blueeyes.LiveBox;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class MyLog {
	public static Boolean MYLOG_SWITCH = true; // LOG 開關
	public static Boolean MYLOG_WRITE_TO_FILE = true; // LOG 寫日文件開關
	private static char MYLOG_TYPE = 'v'; // LOG類型，w代表只输出告警信息等，v代表输出所有信息
	// private static String path
	// =Environment.getExternalStorageDirectory().getPath();;
	private static String path = Environment.getExternalStorageDirectory().getPath()
			+ "/Android/data/tw.com.blueeyes.LiveBox/";
	// private static String path ="/mnt/usb_storage/USB_DISK0/udisk0/";
	private static String MYLOG_PATH_SDCARD_DIR = path;// LOG 輸出目錄
	private static int SDCARD_LOG_FILE_SAVE_DAYS = 0; // 保留天數
	private static String MYLOGFILEName = "debug.txt"; // 輸出名稱
	private static SimpleDateFormat myLogSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // LOG格式
	private static SimpleDateFormat logfile = new SimpleDateFormat("yyyy-MM-dd");// LOG文件格式

	public static void w(String tag, Object msg) { // warn警告信息
		log(tag, msg.toString(), 'w');
	}

	public static void e(String tag, Object msg) { // error信息
		log(tag, msg.toString(), 'e');
	}

	public static void d(String tag, Object msg) {// debug信息
		log(tag, msg.toString(), 'd');
	}

	public static void i(String tag, Object msg) {// System.out.println
		log(tag, msg.toString(), 'i');
	}

	public static void v(String tag, Object msg) {
		log(tag, msg.toString(), 'v');
	}

	public static void w(String tag, String text) {
		log(tag, text, 'w');
	}

	public static void e(String tag, String text) {
		log(tag, text, 'e');
	}

	public static void d(String tag, String text) {
		log(tag, text, 'd');
	}

	public static void i(String tag, String text) {
		log(tag, text, 'i');
	}

	public static void v(String tag, String text) {
		log(tag, text, 'v');
	}

	/**
	 * 根据tag, msg和等级，输出日志
	 * 
	 * @param tag
	 * @param msg
	 * @param level
	 * @return void
	 * @since v 1.0
	 */
	private static void log(String tag, String msg, char level) {
		if (MYLOG_SWITCH) {
			if ('e' == level && ('e' == MYLOG_TYPE || 'v' == MYLOG_TYPE)) { // 输出错误信息
				Log.e(tag, msg);
			} else if ('w' == level && ('w' == MYLOG_TYPE || 'v' == MYLOG_TYPE)) {
				Log.w(tag, msg);
			} else if ('d' == level && ('d' == MYLOG_TYPE || 'v' == MYLOG_TYPE)) {
				Log.d(tag, msg);
			} else if ('i' == level && ('d' == MYLOG_TYPE || 'v' == MYLOG_TYPE)) {
				Log.i(tag, msg);
			} else {
				Log.v(tag, msg);
			}
			if (MYLOG_WRITE_TO_FILE)
				writeLogtoFile(String.valueOf(level), tag, msg);
		}
	}

	/**
	 * 打开日志文件并写入日志
	 * 
	 * @return
	 * **/
	private static void writeLogtoFile(String mylogtype, String tag, String text) {// 新建或打开日志文件

		File file = new File(MYLOG_PATH_SDCARD_DIR);

		if (!file.exists()) {
			file.mkdirs();
		}

		Date nowtime = new Date();
		String needWriteFiel = logfile.format(nowtime);
		// String needWriteMessage = myLogSdf.format(nowtime) + "    " +
		// mylogtype + "    " + tag + "    " + text + "\r\n";
//		String needWriteMessage = myLogSdf.format(nowtime) + "    " + text;
		// file = new File(MYLOG_PATH_SDCARD_DIR, needWriteFiel +
		// MYLOGFILEName);
		file = new File(MYLOG_PATH_SDCARD_DIR, MYLOGFILEName);

		// try {
		// FileOutputStream fos = new FileOutputStream(file);
		// Writer out = new OutputStreamWriter(fos, "UTF8");
		// out.write("\n" + needWriteMessage);
		// out.close();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }

		try {
			int count_line = 0;
			if (file.exists()) {
				BufferedReader br = new BufferedReader(new FileReader(file));
				while (br.readLine() != null) {
					count_line++;
				}
			}
		
			FileWriter filerWriter = new FileWriter(file, true); // 后面这个参数代表是不是要接上文件中原来的数据，不进行覆盖
			BufferedWriter bufWriter = new BufferedWriter(filerWriter);
			if (count_line >= 300) {
				removeLine(file);
			}
			bufWriter.write(text);
			bufWriter.newLine();
			bufWriter.close();
			filerWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.v("write bug", e.toString());
		}
	}

	public static void removeLine(final File file) throws IOException{
	    final List<String> lines = new LinkedList<String>();
	    final Scanner reader = new Scanner(new FileInputStream(file), "UTF-8");
	    while(reader.hasNextLine()) {
	        lines.add(reader.nextLine());
	    }
	    final BufferedWriter writer = new BufferedWriter(new FileWriter(file, false));
	    int count_line = 0;
	    for(final String line : lines) {
	    	if(count_line >= 100) {
	    	writer.write(line);
	    	}
	    	count_line++;
	    }
	    writer.flush();
	    writer.close();
	}
	
	/**
	 * 删除制定的日志文件
	 * */
	public static void delFile() {// 删除日志文件
		String needDelFiel = logfile.format(getDateBefore());
		File file = new File(MYLOG_PATH_SDCARD_DIR, MYLOGFILEName);
		if (file.exists()) {
			file.delete();
		}
	}

	/**
	 * 得到现在时间前的几天日期，用来得到需要删除的日志文件名
	 * */
	private static Date getDateBefore() {
		Date nowtime = new Date();
		Calendar now = Calendar.getInstance();
		now.setTime(nowtime);
		now.set(Calendar.DATE, now.get(Calendar.DATE) - SDCARD_LOG_FILE_SAVE_DAYS);
		return now.getTime();
	}
}
