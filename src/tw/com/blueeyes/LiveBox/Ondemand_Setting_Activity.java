package tw.com.blueeyes.LiveBox;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class Ondemand_Setting_Activity extends Activity {
	SharedPreferences Camera_data;
	ArrayAdapter<String> sipnneradapter, class_adapter, group_adapter;
	Button btn1, btn_setting, btn_log, save, cancel;
	Button loadMoreButton, cleanlog, cleansetting;
	LinearLayout content, intentlayout, hotspot_layout;
	View cam_setview, net_setview, log_setview, loadMoreView;
	ListView loglist;
	ArrayList<String> items;
	ListViewAdapter adapter;
	Switch ative, title_ative, time_ative, circle_ative;
	EditText e_ip1, e_ip2, e_ip3, e_ip4;
	EditText e_gateway_ip1, e_gateway_ip2, e_gateway_ip3, e_gateway_ip4;
	EditText e_mask_ip1, e_mask_ip2, e_mask_ip3, e_mask_ip4, e_wifi_pwd;
	EditText e_loginpwd, e_hotspot;
	TextView log_text, tv_ssid, tv_wifipwd, tv_ver, tv_hotspot;
	RadioButton smooth, close, general;
	Spinner net, ssid, loop_select;
	static Handler Change_DATA;
	String wifi_ip[], eth0_ip[], gateway_ip[], mask_ip[];
	String wifi_pwd, wifi_ssid, hotspot_ssid;
	String login_pwd;
	boolean title_on, time_on;
	boolean loop_on;
	int loop_time = 20;
	WIFI_Adapter wifi_adapter;
	WifiManager wifi_magager;
	WifiInfo wifiInfo;
	private String[] connect_type = { "Wifi", "Ethernet" };
	private String[] icam = { "iCam v2", "iCam v3", "SES", "SES iFollow" };
	private String[] time = { "20", "30", "40", "50", "60" };
	// private String[] class_port = { "1", "2", "3", "4", "5", "6" };
	// private String[] group_port = { "1", "2", "3" };
	private String[] class_port = new String[100];
	private String[] group_port = new String[100];
	ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
	int wifi_size;
	List results;
	private Handler handler = new Handler();
	private ArrayAdapter<String> net_adapter, time_adapter;
	private int visibleLastIndex = 0; // 最后的可视项索引
	private int visibleItemCount = 0; // 当前窗口可见项总数

	final int btn_channel = 0x01;
	final int btn_set = 0x02;
	final int btn_logtype = 0x03;
	int btn_type = 0x00;
	final int wifi = 0x00;
	final int eth0 = 0x01;
	int net_type = eth0;
	final int no_pwd = 0x00;
	final int WEP = 0xa0;
	final int WPA2 = 0xb0;
	final int WPA = 0xc0;
	int pwd_type;
	int Camera_Type = 0;
	int Camera_v2 = 0x00;
	int Camera_v3 = 0x01;
	int SES_class = 0x02;
	int SES_group = 0x03;
	int real_time = 0x00;
	final int realtime_close = 0x0a;
	final int realtime_general = 0x0b;
	final int realtime_smooth = 0x0c;
	int countintent = 0;
	boolean user_change = false;
	boolean icam1_on, icam2_on, icam3_on, icam4_on, icam5_on, icam6_on, icam7_on, icam8_on, icam9_on, icam10_on,
			icam11_on, icam12_on, icam13_on, icam14_on, icam15_on, icam16_on, icam17_on, icam18_on, icam19_on,
			icam20_on;
	String label1, label2, label3, label4, label5, label6, label7, label8, label9, label10, label11, label12, label13,
			label14, label15, label16, label17, label18, label19, label20;
	String URL_1, URL_2, URL_3, URL_4, URL_5, URL_6, URL_7, URL_8, URL_9, URL_10, URL_11, URL_12, URL_13, URL_14,
			URL_15, URL_16, URL_17, URL_18, URL_19, URL_20;
	int port1, port2, port3, port4, port5, port6, port7, port8, port9, port10, port11, port12, port13, port14, port15,
			port16, port17, port18, port19, port20;
	String account1, account2, account3, account4, account5, account6, account7, account8, account9, account10,
			account11, account12, account13, account14, account15, account16, account17, account18, account19,
			account20;
	String pwd1, pwd2, pwd3, pwd4, pwd5, pwd6, pwd7, pwd8, pwd9, pwd10, pwd11, pwd12, pwd13, pwd14, pwd15, pwd16,
			pwd17, pwd18, pwd19, pwd20;
	int icam_type1, icam_type2, icam_type3, icam_type4, icam_type5, icam_type6, icam_type7, icam_type8, icam_type9,
			icam_type10, icam_type11, icam_type12, icam_type13, icam_type14, icam_type15, icam_type16, icam_type17,
			icam_type18, icam_type19, icam_type20;
	int corg_num1, corg_num2, corg_num3, corg_num4, corg_num5, corg_num6, corg_num7, corg_num8, corg_num9, corg_num10,
			corg_num11, corg_num12, corg_num13, corg_num14, corg_num15, corg_num16, corg_num17, corg_num18, corg_num19,
			corg_num20;
	EditText E_URL1, E_label1, E_port1, E_account1, E_pwd1;
	EditText E_URL2, E_label2, E_port2, E_account2, E_pwd2;
	EditText E_URL3, E_label3, E_port3, E_account3, E_pwd3;
	EditText E_URL4, E_label4, E_port4, E_account4, E_pwd4;
	EditText E_URL5, E_label5, E_port5, E_account5, E_pwd5;
	EditText E_URL6, E_label6, E_port6, E_account6, E_pwd6;
	EditText E_URL7, E_label7, E_port7, E_account7, E_pwd7;
	EditText E_URL8, E_label8, E_port8, E_account8, E_pwd8;
	EditText E_URL9, E_label9, E_port9, E_account9, E_pwd9;
	EditText E_URL10, E_label10, E_port10, E_account10, E_pwd10;
	EditText E_URL11, E_label11, E_port11, E_account11, E_pwd11;
	EditText E_URL12, E_label12, E_port12, E_account12, E_pwd12;
	EditText E_URL13, E_label13, E_port13, E_account13, E_pwd13;
	EditText E_URL14, E_label14, E_port14, E_account14, E_pwd14;
	EditText E_URL15, E_label15, E_port15, E_account15, E_pwd15;
	EditText E_URL16, E_label16, E_port16, E_account16, E_pwd16;
	EditText E_URL17, E_label17, E_port17, E_account17, E_pwd17;
	EditText E_URL18, E_label18, E_port18, E_account18, E_pwd18;
	EditText E_URL19, E_label19, E_port19, E_account19, E_pwd19;
	EditText E_URL20, E_label20, E_port20, E_account20, E_pwd20;
	CheckBox active1, active2, active3, active4, active5, active6, active7, active8, active9, active10, active11,
			active12, active13, active14, active15, active16, active17, active18, active19, active20;
	Spinner cam_type1, cam_type2, cam_type3, cam_type4, cam_type5, cam_type6, cam_type7, cam_type8, cam_type9,
			cam_type10, cam_type11, cam_type12, cam_type13, cam_type14, cam_type15, cam_type16, cam_type17, cam_type18,
			cam_type19, cam_type20;
	Spinner videoport1, videoport2, videoport3, videoport4, videoport5, videoport6, videoport7, videoport8, videoport9,
			videoport10, videoport11, videoport12, videoport13, videoport14, videoport15, videoport16, videoport17,
			videoport18, videoport19, videoport20;
	ImageButton check1, check2, check3, check4, check5, check6, check7, check8, check9, check10, check11, check12,
			check13, check14, check15, check16, check17, check18, check19, check20;

	boolean icam_on1, icam_on2, icam_on3, icam_on4, icam_on5, icam_on6, icam_on7, icam_on8, icam_on9, icam_on10,
			icam_on11, icam_on12, icam_on13, icam_on14, icam_on15, icam_on16, icam_on17, icam_on18, icam_on19,
			icam_on20;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 全畫面
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// 標題隱藏
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (Build.VERSION.RELEASE.equals("4.4.2")) {
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		} 

		setContentView(R.layout.activity_ondemand_setting);

		Camera_data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		startService(new Intent(Ondemand_Setting_Activity.this, Remote_Service.class));
		btn1 = (Button) findViewById(R.id.channel);
		btn_setting = (Button) findViewById(R.id.set);
		btn_log = (Button) findViewById(R.id.log_btn);
		content = (LinearLayout) findViewById(R.id.setting_content);
		intentlayout = (LinearLayout) findViewById(R.id.intentlayout);
		save = (Button) findViewById(R.id.save);
		cancel = (Button) findViewById(R.id.cancel);
		tv_ver = (TextView) findViewById(R.id.version);

		for (int i = 0; i < 100; i++) {
			class_port[i] = String.valueOf(i + 1);
			group_port[i] = String.valueOf(i + 1);
		}

		try {
			PackageManager pm = getPackageManager();
			/* NowVer */
			PackageInfo info;
			info = pm.getPackageInfo(getPackageName(), 0);
			String nowVersionName = info.versionName;
			tv_ver.setText("PRO Ver" + nowVersionName);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		wifi_magager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		wifiInfo = wifi_magager.getConnectionInfo();

		SharedPreferences.Editor editor = Camera_data.edit();
		editor.putInt("activity", 0x1b);
		editor.commit();

		cam_content();
		load();
		setbtn();
		btn1.performClick();

		Change_DATA = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.arg1) {
				case 0x01:
					Intent intent = new Intent();
					if (Camera_data.getBoolean("OnDemand", false)) {
						intent.setClass(Ondemand_Setting_Activity.this, Ondemand_LiveBox_Activity.class);
					} else {
						intent.setClass(Ondemand_Setting_Activity.this, Ondemand_LiveBox_Activity.class);
					}
					startActivity(intent);
					finish();
					break;
				case 0x02:
					load();
					if (btn_type == btn_channel) {
						btn_type = 0;
						btn1.performClick();
					} else if (btn_type == btn_set) {
						btn_type = 0;
						btn_setting.performClick();
					}
					break;
				case 0x03:
					Intent intent1 = new Intent();
					intent1.setClass(Ondemand_Setting_Activity.this, Loading_Activity.class);
					startActivity(intent1);
					finish();
					break;
				case 0x04:
					if (!loop_on) {
						loop_select.setEnabled(false);
					} else {
						loop_select.setEnabled(true);
						loop_select.setSelection((loop_time - 20) / 10);
					}
					break;
				}
			}
		};

	}

	private void setbtn() {
		intentlayout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				countintent++;
				if (countintent == 9) {
					countintent = 0;
					stopService(new Intent(Ondemand_Setting_Activity.this, Remote_Service.class));
					Intent intent = new Intent("android.intent.action.MAIN");
					if (Build.VERSION.RELEASE.equals("4.4.2")) {
						intent.setClassName("com.android.launcher3", "com.android.launcher3.Launcher");
					} else {
						intent.setClassName("com.google.android.launcher", "com.google.android.launcher.StubApp"); // package
					}
					;
					// intent.setComponent(comp);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
					startActivity(intent);
					finish();
					System.exit(0);
				}
			}
		});

		btn1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (btn_type != btn_channel) {
					save_tem();
					cam_content();
				}
				btn_type = btn_channel;
				load_tem();

				btn1.setBackgroundColor(Color.parseColor("#1714DF"));
				btn1.setTextColor(Color.WHITE);

				btn_setting.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_setting.setTextColor(Color.BLACK);

				btn_log.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_log.setTextColor(Color.BLACK);
			}
		});

		btn_setting.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (btn_type != btn_set) {
					save_tem();
					ethernet_content();
				}
				btn_type = btn_set;

				btn1.setBackgroundColor(Color.parseColor("#4183D7"));
				btn1.setTextColor(Color.BLACK);

				btn_setting.setBackgroundColor(Color.parseColor("#1714DF"));
				btn_setting.setTextColor(Color.WHITE);

				btn_log.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_log.setTextColor(Color.BLACK);
			}
		});

		btn_log.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (btn_type != btn_logtype) {
					save_tem();
					log_content();
				}
				btn_type = btn_logtype;

				btn1.setBackgroundColor(Color.parseColor("#4183D7"));
				btn1.setTextColor(Color.BLACK);

				btn_setting.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_setting.setTextColor(Color.BLACK);

				btn_log.setBackgroundColor(Color.parseColor("#1714DF"));
				btn_log.setTextColor(Color.WHITE);
			}
		});

		save.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				save_tem();
				if(login_pwd.equals("")) {
					String obj = "password not allow blank.";
					Message msg = _handler.obtainMessage(1, obj);
					_handler.sendMessage(msg);
					return;
				}
				SharedPreferences.Editor editor = Camera_data.edit();

				editor.putBoolean("Camera1", icam1_on);
				editor.putString("Camera1_URL", URL_1);
				editor.putInt("Camera1_Port", Integer.valueOf(port1).intValue());
				editor.putString("Camera1_Account", account1);
				editor.putString("Camera1_Pwd", pwd1);
				editor.putInt("Camera1_type", icam_type1);
				editor.putString("Camera1_Label", label1);
				editor.putInt("classorgroup1", corg_num1);

				editor.putBoolean("Camera2", icam2_on);
				editor.putString("Camera2_URL", URL_2);
				editor.putInt("Camera2_Port", Integer.valueOf(port2).intValue());
				editor.putString("Camera2_Account", account2);
				editor.putString("Camera2_Pwd", pwd2);
				editor.putInt("Camera2_type", icam_type2);
				editor.putString("Camera2_Label", label2);
				editor.putInt("classorgroup2", corg_num2);

				editor.putBoolean("Camera3", icam3_on);
				editor.putString("Camera3_URL", URL_3);
				editor.putInt("Camera3_Port", Integer.valueOf(port3).intValue());
				editor.putString("Camera3_Account", account3);
				editor.putString("Camera3_Pwd", pwd3);
				editor.putInt("Camera3_type", icam_type3);
				editor.putString("Camera3_Label", label3);
				editor.putInt("classorgroup3", corg_num3);

				editor.putBoolean("Camera4", icam4_on);
				editor.putString("Camera4_URL", URL_4);
				editor.putInt("Camera4_Port", Integer.valueOf(port4).intValue());
				editor.putString("Camera4_Account", account4);
				editor.putString("Camera4_Pwd", pwd4);
				editor.putInt("Camera4_type", icam_type4);
				editor.putString("Camera4_Label", label4);
				editor.putInt("classorgroup4", corg_num4);
				Log.v("test", "test icam4_on " + corg_num4);

				editor.putBoolean("Camera5", icam5_on);
				editor.putString("Camera5_URL", URL_5);
				editor.putInt("Camera5_Port", Integer.valueOf(port5).intValue());
				editor.putString("Camera5_Account", account5);
				editor.putString("Camera5_Pwd", pwd5);
				editor.putInt("Camera5_type", icam_type5);
				editor.putString("Camera5_Label", label5);
				editor.putInt("classorgroup5", corg_num5);

				editor.putBoolean("Camera6", icam6_on);
				editor.putString("Camera6_URL", URL_6);
				editor.putInt("Camera6_Port", Integer.valueOf(port6).intValue());
				editor.putString("Camera6_Account", account6);
				editor.putString("Camera6_Pwd", pwd6);
				editor.putInt("Camera6_type", icam_type6);
				editor.putString("Camera6_Label", label6);
				editor.putInt("classorgroup6", corg_num6);

				editor.putBoolean("Camera7", icam7_on);
				editor.putString("Camera7_URL", URL_7);
				editor.putInt("Camera7_Port", Integer.valueOf(port7).intValue());
				editor.putString("Camera7_Account", account7);
				editor.putString("Camera7_Pwd", pwd7);
				editor.putInt("Camera7_type", icam_type7);
				editor.putString("Camera7_Label", label7);
				editor.putInt("classorgroup7", corg_num7);

				editor.putBoolean("Camera8", icam8_on);
				editor.putString("Camera8_URL", URL_8);
				editor.putInt("Camera8_Port", Integer.valueOf(port8).intValue());
				editor.putString("Camera8_Account", account8);
				editor.putString("Camera8_Pwd", pwd8);
				editor.putInt("Camera8_type", icam_type8);
				editor.putString("Camera8_Label", label8);
				editor.putInt("classorgroup8", corg_num8);

				editor.putBoolean("Camera9", icam9_on);
				editor.putString("Camera9_URL", URL_9);
				editor.putInt("Camera9_Port", Integer.valueOf(port9).intValue());
				editor.putString("Camera9_Account", account9);
				editor.putString("Camera9_Pwd", pwd9);
				editor.putInt("Camera9_type", icam_type9);
				editor.putString("Camera9_Label", label9);
				editor.putInt("classorgroup9", corg_num9);

				editor.putBoolean("Camera10", icam10_on);
				editor.putString("Camera10_URL", URL_10);
				editor.putInt("Camera10_Port", Integer.valueOf(port10).intValue());
				editor.putString("Camera10_Account", account10);
				editor.putString("Camera10_Pwd", pwd10);
				editor.putInt("Camera10_type", icam_type10);
				editor.putString("Camera10_Label", label10);
				editor.putInt("classorgroup10", corg_num10);

				editor.putBoolean("Camera11", icam11_on);
				editor.putString("Camera11_URL", URL_11);
				editor.putInt("Camera11_Port", Integer.valueOf(port11).intValue());
				editor.putString("Camera11_Account", account11);
				editor.putString("Camera11_Pwd", pwd11);
				editor.putInt("Camera11_type", icam_type11);
				editor.putString("Camera11_Label", label11);
				editor.putInt("classorgroup11", corg_num11);

				editor.putBoolean("Camera12", icam12_on);
				editor.putString("Camera12_URL", URL_12);
				editor.putInt("Camera12_Port", Integer.valueOf(port12).intValue());
				editor.putString("Camera12_Account", account12);
				editor.putString("Camera12_Pwd", pwd12);
				editor.putInt("Camera12_type", icam_type12);
				editor.putString("Camera12_Label", label12);
				editor.putInt("classorgroup12", corg_num12);

				editor.putBoolean("Camera13", icam13_on);
				editor.putString("Camera13_URL", URL_13);
				editor.putInt("Camera13_Port", Integer.valueOf(port13).intValue());
				editor.putString("Camera13_Account", account13);
				editor.putString("Camera13_Pwd", pwd13);
				editor.putInt("Camera13_type", icam_type13);
				editor.putString("Camera13_Label", label13);
				editor.putInt("classorgroup13", corg_num13);

				editor.putBoolean("Camera14", icam14_on);
				editor.putString("Camera14_URL", URL_14);
				editor.putInt("Camera14_Port", Integer.valueOf(port14).intValue());
				editor.putString("Camera14_Account", account14);
				editor.putString("Camera14_Pwd", pwd14);
				editor.putInt("Camera14_type", icam_type14);
				editor.putString("Camera14_Label", label14);
				editor.putInt("classorgroup4", corg_num4);

				editor.putBoolean("Camera15", icam15_on);
				editor.putString("Camera15_URL", URL_15);
				editor.putInt("Camera15_Port", Integer.valueOf(port15).intValue());
				editor.putString("Camera15_Account", account15);
				editor.putString("Camera15_Pwd", pwd15);
				editor.putInt("Camera15_type", icam_type15);
				editor.putString("Camera15_Label", label15);
				editor.putInt("classorgroup15", corg_num15);

				editor.putBoolean("Camera16", icam16_on);
				editor.putString("Camera16_URL", URL_16);
				editor.putInt("Camera16_Port", Integer.valueOf(port16).intValue());
				editor.putString("Camera16_Account", account16);
				editor.putString("Camera16_Pwd", pwd16);
				editor.putInt("Camera16_type", icam_type16);
				editor.putString("Camera16_Label", label16);
				editor.putInt("classorgroup16", corg_num16);

				editor.putBoolean("Camera17", icam17_on);
				editor.putString("Camera17_URL", URL_17);
				editor.putInt("Camera17_Port", Integer.valueOf(port17).intValue());
				editor.putString("Camera17_Account", account17);
				editor.putString("Camera17_Pwd", pwd17);
				editor.putInt("Camera17_type", icam_type17);
				editor.putString("Camera17_Label", label17);
				editor.putInt("classorgroup17", corg_num17);

				editor.putBoolean("Camera18", icam18_on);
				editor.putString("Camera18_URL", URL_18);
				editor.putInt("Camera18_Port", Integer.valueOf(port18).intValue());
				editor.putString("Camera18_Account", account18);
				editor.putString("Camera18_Pwd", pwd18);
				editor.putInt("Camera18_type", icam_type18);
				editor.putString("Camera18_Label", label18);
				editor.putInt("classorgroup18", corg_num18);

				editor.putBoolean("Camera19", icam19_on);
				editor.putString("Camera19_URL", URL_19);
				editor.putInt("Camera19_Port", Integer.valueOf(port19).intValue());
				editor.putString("Camera19_Account", account19);
				editor.putString("Camera19_Pwd", pwd19);
				editor.putInt("Camera19_type", icam_type19);
				editor.putString("Camera19_Label", label19);
				editor.putInt("classorgroup19", corg_num19);

				editor.putBoolean("Camera20", icam20_on);
				editor.putString("Camera20_URL", URL_20);
				editor.putInt("Camera20_Port", Integer.valueOf(port20).intValue());
				editor.putString("Camera20_Account", account20);
				editor.putString("Camera20_Pwd", pwd20);
				editor.putInt("Camera20_type", icam_type20);
				editor.putString("Camera20_Label", label20);
				editor.putInt("classorgroup20", corg_num20);

				editor.putBoolean("video_title", title_on);
				editor.putBoolean("video_time", time_on);
				editor.putBoolean("loop_on", loop_on);
				editor.putInt("loop_time", loop_time);

				if (net_type == eth0) {
					Log.e("test", gateway_ip[0] + "." + gateway_ip[1] + "." + gateway_ip[2] + "." + gateway_ip[3]);
					Log.e("test", Camera_data.getString("geteway", "192.168.1.1"));
					if (!(gateway_ip[0] + "." + gateway_ip[1] + "." + gateway_ip[2] + "." + gateway_ip[3])
							.equals(Camera_data.getString("geteway", "192.168.1.1"))) {
						Log.e("test", "gw changed");
						try {
							Process proc = Runtime.getRuntime().exec("su");
							DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
							DataInputStream dis = new DataInputStream(proc.getInputStream());
							opt = new DataOutputStream(proc.getOutputStream());
							opt.writeBytes("ip route del all\n");
							// opt.writeBytes("ip route del  " +
							// Camera_data.getString("geteway", "192.168.1.1")
							// + "/24 via " + Camera_data.getString("eth0",
							// "192.168.1.1") + " dev eth0\n");
							opt.writeBytes("exit\n");
							opt.flush();
							String line = "";
							while ((line = dis.readLine()) != null) {
								Log.v("socket", line);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							Log.e("test", e.toString());
						}
					}
				}

				editor.putInt("net_type", net_type);
				editor.putString("wifi", wifi_ip[0] + "." + wifi_ip[1] + "." + wifi_ip[2] + "." + wifi_ip[3]);
				editor.putString("eth0", eth0_ip[0] + "." + eth0_ip[1] + "." + eth0_ip[2] + "." + eth0_ip[3]);
				editor.putString("geteway", gateway_ip[0] + "." + gateway_ip[1] + "." + gateway_ip[2] + "."
						+ gateway_ip[3]);
				editor.putString("mask", mask_ip[0] + "." + mask_ip[1] + "." + mask_ip[2] + "." + mask_ip[3]);
				editor.putInt("pwd_type", pwd_type);
				editor.putString("wifi_pwd", wifi_pwd);
				editor.putString("hotspot", hotspot_ssid);
				editor.putInt("real_time", real_time);
				if (net_type == wifi && e_wifi_pwd != null) {
					editor.putString("wifi_pwd", e_wifi_pwd.getText().toString().trim());
				} else {
					editor.putString("wifi_pwd", wifi_pwd);
				}
				editor.putString("wifi_ssid", wifi_ssid.trim());

				// editor.putString("login_account", login_account);
				editor.putString("login_pwd", login_pwd);

				editor.commit();
				Intent intent = new Intent();
				if (Camera_data.getBoolean("OnDemand", false)) {
					intent.setClass(Ondemand_Setting_Activity.this, Ondemand_LiveBox_Activity.class);
				} else {
					intent.setClass(Ondemand_Setting_Activity.this, LiveBox_Activity.class);
				}
				startActivity(intent);
				finish();
				System.exit(0);
			}
		});

		cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent();
				if (Camera_data.getBoolean("OnDemand", false)) {
					intent.setClass(Ondemand_Setting_Activity.this, Ondemand_LiveBox_Activity.class);
				} else {
					intent.setClass(Ondemand_Setting_Activity.this, LiveBox_Activity.class);
				}
				startActivity(intent);
				finish();
				System.exit(0);
			}
		});
	}

	public void realtime_Clicked(View view) {
		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();

		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.r_close:
			if (checked)
				real_time = realtime_close;
			break;
		case R.id.r_general:
			if (checked)
				real_time = realtime_general;
			break;
		case R.id.r_smooth:
			if (checked)
				real_time = realtime_smooth;
			break;
		}
	}

	private void load() {
		icam1_on = Camera_data.getBoolean("Camera1", false);
		URL_1 = Camera_data.getString("Camera1_URL", "192.168.1.170");
		label1 = Camera_data.getString("Camera1_Label", "CH1");
		port1 = Camera_data.getInt("Camera1_Port", 80);
		account1 = Camera_data.getString("Camera1_Account", "root");
		pwd1 = Camera_data.getString("Camera1_Pwd", "");
		icam_type1 = Camera_data.getInt("Camera1_type", Camera_v3);
		corg_num1 = Camera_data.getInt("classorgroup1", 0);

		icam2_on = Camera_data.getBoolean("Camera2", false);
		URL_2 = Camera_data.getString("Camera2_URL", "");
		label2 = Camera_data.getString("Camera2_Label", "CH2");
		port2 = Camera_data.getInt("Camera2_Port", 80);
		account2 = Camera_data.getString("Camera2_Account", "root");
		pwd2 = Camera_data.getString("Camera2_Pwd", "");
		icam_type2 = Camera_data.getInt("Camera2_type", Camera_v3);
		corg_num2 = Camera_data.getInt("classorgroup2", 0);

		icam3_on = Camera_data.getBoolean("Camera3", false);
		URL_3 = Camera_data.getString("Camera3_URL", "");
		label3 = Camera_data.getString("Camera3_Label", "CH3");
		port3 = Camera_data.getInt("Camera3_Port", 80);
		account3 = Camera_data.getString("Camera3_Account", "root");
		pwd3 = Camera_data.getString("Camera3_Pwd", "");
		icam_type3 = Camera_data.getInt("Camera3_type", Camera_v3);
		corg_num3 = Camera_data.getInt("classorgroup3", 0);

		icam4_on = Camera_data.getBoolean("Camera4", false);
		URL_4 = Camera_data.getString("Camera4_URL", "");
		label4 = Camera_data.getString("Camera4_Label", "CH4");
		port4 = Camera_data.getInt("Camera4_Port", 80);
		account4 = Camera_data.getString("Camera4_Account", "root");
		pwd4 = Camera_data.getString("Camera4_Pwd", "");
		icam_type4 = Camera_data.getInt("Camera4_type", Camera_v3);
		corg_num4 = Camera_data.getInt("classorgroup4", 0);

		icam5_on = Camera_data.getBoolean("Camera5", false);
		URL_5 = Camera_data.getString("Camera5_URL", "");
		label5 = Camera_data.getString("Camera5_Label", "CH5");
		port5 = Camera_data.getInt("Camera5_Port", 80);
		account5 = Camera_data.getString("Camera5_Account", "root");
		pwd5 = Camera_data.getString("Camera5_Pwd", "");
		icam_type5 = Camera_data.getInt("Camera5_type", Camera_v3);
		corg_num5 = Camera_data.getInt("classorgroup5", 0);

		icam6_on = Camera_data.getBoolean("Camera6", false);
		URL_6 = Camera_data.getString("Camera6_URL", "");
		label6 = Camera_data.getString("Camera6_Label", "CH6");
		port6 = Camera_data.getInt("Camera6_Port", 80);
		account6 = Camera_data.getString("Camera6_Account", "root");
		pwd6 = Camera_data.getString("Camera6_Pwd", "");
		icam_type6 = Camera_data.getInt("Camera6_type", Camera_v3);
		corg_num6 = Camera_data.getInt("classorgroup6", 0);

		icam7_on = Camera_data.getBoolean("Camera7", false);
		URL_7 = Camera_data.getString("Camera7_URL", "");
		label7 = Camera_data.getString("Camera7_Label", "CH7");
		port7 = Camera_data.getInt("Camera7_Port", 80);
		account7 = Camera_data.getString("Camera7_Account", "root");
		pwd7 = Camera_data.getString("Camera7_Pwd", "");
		icam_type7 = Camera_data.getInt("Camera7_type", Camera_v3);
		corg_num7 = Camera_data.getInt("classorgroup7", 0);

		icam8_on = Camera_data.getBoolean("Camera8", false);
		URL_8 = Camera_data.getString("Camera8_URL", "");
		label8 = Camera_data.getString("Camera8_Label", "CH8");
		port8 = Camera_data.getInt("Camera8_Port", 80);
		account8 = Camera_data.getString("Camera8_Account", "root");
		pwd8 = Camera_data.getString("Camera8_Pwd", "");
		icam_type8 = Camera_data.getInt("Camera8_type", Camera_v3);
		corg_num8 = Camera_data.getInt("classorgroup8", 0);

		icam9_on = Camera_data.getBoolean("Camera9", false);
		URL_9 = Camera_data.getString("Camera9_URL", "");
		label9 = Camera_data.getString("Camera9_Label", "CH9");
		port9 = Camera_data.getInt("Camera9_Port", 80);
		account9 = Camera_data.getString("Camera9_Account", "root");
		pwd9 = Camera_data.getString("Camera9_Pwd", "");
		icam_type9 = Camera_data.getInt("Camera9_type", Camera_v3);
		corg_num9 = Camera_data.getInt("classorgroup9", 0);

		icam10_on = Camera_data.getBoolean("Camera10", false);
		URL_10 = Camera_data.getString("Camera10_URL", "");
		label10 = Camera_data.getString("Camera10_Label", "CH10");
		port10 = Camera_data.getInt("Camera10_Port", 80);
		account10 = Camera_data.getString("Camera10_Account", "root");
		pwd10 = Camera_data.getString("Camera10_Pwd", "");
		icam_type10 = Camera_data.getInt("Camera10_type", Camera_v3);
		corg_num10 = Camera_data.getInt("classorgroup10", 0);

		icam11_on = Camera_data.getBoolean("Camera11", false);
		URL_11 = Camera_data.getString("Camera11_URL", "");
		label11 = Camera_data.getString("Camera11_Label", "CH11");
		port11 = Camera_data.getInt("Camera11_Port", 80);
		account11 = Camera_data.getString("Camera11_Account", "root");
		pwd11 = Camera_data.getString("Camera11_Pwd", "");
		icam_type11 = Camera_data.getInt("Camera11_type", Camera_v3);
		corg_num11 = Camera_data.getInt("classorgroup11", 0);

		icam12_on = Camera_data.getBoolean("Camera12", false);
		URL_12 = Camera_data.getString("Camera12_URL", "");
		label12 = Camera_data.getString("Camera12_Label", "CH12");
		port12 = Camera_data.getInt("Camera12_Port", 80);
		account12 = Camera_data.getString("Camera12_Account", "root");
		pwd12 = Camera_data.getString("Camera12_Pwd", "");
		icam_type12 = Camera_data.getInt("Camera12_type", Camera_v3);
		corg_num12 = Camera_data.getInt("classorgroup12", 0);

		icam13_on = Camera_data.getBoolean("Camera13", false);
		URL_13 = Camera_data.getString("Camera13_URL", "");
		label13 = Camera_data.getString("Camera13_Label", "CH13");
		port13 = Camera_data.getInt("Camera13_Port", 80);
		account13 = Camera_data.getString("Camera13_Account", "root");
		pwd13 = Camera_data.getString("Camera13_Pwd", "");
		icam_type13 = Camera_data.getInt("Camera13_type", Camera_v3);
		corg_num13 = Camera_data.getInt("classorgroup13", 0);

		icam14_on = Camera_data.getBoolean("Camera14", false);
		URL_14 = Camera_data.getString("Camera14_URL", "");
		label14 = Camera_data.getString("Camera14_Label", "CH14");
		port14 = Camera_data.getInt("Camera14_Port", 80);
		account14 = Camera_data.getString("Camera14_Account", "root");
		pwd14 = Camera_data.getString("Camera14_Pwd", "");
		icam_type14 = Camera_data.getInt("Camera14_type", Camera_v3);
		corg_num14 = Camera_data.getInt("classorgroup14", 0);

		icam15_on = Camera_data.getBoolean("Camera15", false);
		URL_15 = Camera_data.getString("Camera15_URL", "");
		label15 = Camera_data.getString("Camera15_Label", "CH15");
		port15 = Camera_data.getInt("Camera15_Port", 80);
		account15 = Camera_data.getString("Camera15_Account", "root");
		pwd15 = Camera_data.getString("Camera15_Pwd", "");
		icam_type15 = Camera_data.getInt("Camera15_type", Camera_v3);
		corg_num15 = Camera_data.getInt("classorgroup15", 0);

		icam16_on = Camera_data.getBoolean("Camera16", false);
		URL_16 = Camera_data.getString("Camera16_URL", "");
		label16 = Camera_data.getString("Camera16_Label", "CH16");
		port16 = Camera_data.getInt("Camera16_Port", 80);
		account16 = Camera_data.getString("Camera16_Account", "root");
		pwd16 = Camera_data.getString("Camera16_Pwd", "");
		icam_type16 = Camera_data.getInt("Camera16_type", Camera_v3);
		corg_num16 = Camera_data.getInt("classorgroup16", 0);

		icam17_on = Camera_data.getBoolean("Camera17", false);
		URL_17 = Camera_data.getString("Camera17_URL", "");
		label17 = Camera_data.getString("Camera17_Label", "CH17");
		port17 = Camera_data.getInt("Camera17_Port", 80);
		account17 = Camera_data.getString("Camera17_Account", "root");
		pwd17 = Camera_data.getString("Camera17_Pwd", "");
		icam_type17 = Camera_data.getInt("Camera17_type", Camera_v3);
		corg_num17 = Camera_data.getInt("classorgroup17", 0);

		icam18_on = Camera_data.getBoolean("Camera18", false);
		URL_18 = Camera_data.getString("Camera18_URL", "");
		label18 = Camera_data.getString("Camera18_Label", "CH18");
		port18 = Camera_data.getInt("Camera18_Port", 80);
		account18 = Camera_data.getString("Camera18_Account", "root");
		pwd18 = Camera_data.getString("Camera18_Pwd", "");
		icam_type18 = Camera_data.getInt("Camera18_type", Camera_v3);
		corg_num18 = Camera_data.getInt("classorgroup18", 0);

		icam19_on = Camera_data.getBoolean("Camera19", false);
		URL_19 = Camera_data.getString("Camera19_URL", "");
		label19 = Camera_data.getString("Camera19_Label", "CH19");
		port19 = Camera_data.getInt("Camera19_Port", 80);
		account19 = Camera_data.getString("Camera19_Account", "root");
		pwd19 = Camera_data.getString("Camera19_Pwd", "");
		icam_type19 = Camera_data.getInt("Camera19_type", Camera_v3);
		corg_num19 = Camera_data.getInt("classorgroup19", 0);

		icam20_on = Camera_data.getBoolean("Camera20", false);
		URL_20 = Camera_data.getString("Camera20_URL", "");
		label20 = Camera_data.getString("Camera20_Label", "CH20");
		port20 = Camera_data.getInt("Camera20_Port", 80);
		account20 = Camera_data.getString("Camera20_Account", "root");
		pwd20 = Camera_data.getString("Camera20_Pwd", "");
		icam_type20 = Camera_data.getInt("Camera20_type", Camera_v3);
		corg_num20 = Camera_data.getInt("classorgroup20", 0);

		title_on = Camera_data.getBoolean("video_title", true);
		time_on = Camera_data.getBoolean("video_time", true);

		net_type = Camera_data.getInt("net_type", eth0);
		wifi_ip = (Camera_data.getString("wifi", "192.168.1.51")).split("\\.");
		eth0_ip = (Camera_data.getString("eth0", "192.168.1.51")).split("\\.");
		gateway_ip = (Camera_data.getString("geteway", "192.168.1.1")).split("\\.");
		mask_ip = (Camera_data.getString("mask", "255.255.255.0")).split("\\.");

		pwd_type = Camera_data.getInt("pwd_type", no_pwd);
		wifi_pwd = Camera_data.getString("wifi_pwd", "");
		wifi_ssid = Camera_data.getString("wifi_ssid", "defaultssid");
		hotspot_ssid = Camera_data.getString("hotspot", "001");
		login_pwd = Camera_data.getString("login_pwd", "9999");
		real_time = Camera_data.getInt("real_time", realtime_smooth);
		loop_on = Camera_data.getBoolean("loop_on", false);
		loop_time = Camera_data.getInt("loop_time", 20);
	}

	public void save_tem() {
		if (btn_type == btn_channel) {
			icam1_on = active1.isChecked();
			URL_1 = E_URL1.getText().toString();
			label1 = E_label1.getText().toString();
			port1 = Integer.valueOf(E_port1.getText().toString()).intValue();
			account1 = E_account1.getText().toString();
			pwd1 = E_pwd1.getText().toString();
			icam_type1 = cam_type1.getSelectedItemPosition();
			if (icam_type1 < SES_class) {
				corg_num1 = 0;
			} else {
				corg_num1 = videoport1.getSelectedItemPosition();
			}

			icam2_on = active2.isChecked();
			URL_2 = E_URL2.getText().toString();
			label2 = E_label2.getText().toString();
			port2 = Integer.valueOf(E_port2.getText().toString()).intValue();
			account2 = E_account2.getText().toString();
			pwd2 = E_pwd2.getText().toString();
			icam_type2 = cam_type2.getSelectedItemPosition();
			if (icam_type2 < SES_class) {
				corg_num2 = 0;
			} else {
				corg_num2 = videoport2.getSelectedItemPosition();
			}

			icam3_on = active3.isChecked();
			URL_3 = E_URL3.getText().toString();
			label3 = E_label3.getText().toString();
			port3 = Integer.valueOf(E_port3.getText().toString()).intValue();
			account3 = E_account3.getText().toString();
			pwd3 = E_pwd3.getText().toString();
			icam_type3 = cam_type3.getSelectedItemPosition();
			if (icam_type3 < SES_class) {
				corg_num3 = 0;
			} else {
				corg_num3 = videoport3.getSelectedItemPosition();
			}

			icam4_on = active4.isChecked();
			URL_4 = E_URL4.getText().toString();
			label4 = E_label4.getText().toString();
			port4 = Integer.valueOf(E_port4.getText().toString()).intValue();
			account4 = E_account4.getText().toString();
			pwd4 = E_pwd4.getText().toString();
			icam_type4 = cam_type4.getSelectedItemPosition();
			if (icam_type4 < SES_class) {
				corg_num4 = 0;
			} else {
				corg_num4 = videoport4.getSelectedItemPosition();
			}

			icam5_on = active5.isChecked();
			URL_5 = E_URL5.getText().toString();
			label5 = E_label5.getText().toString();
			port5 = Integer.valueOf(E_port5.getText().toString()).intValue();
			account5 = E_account5.getText().toString();
			pwd5 = E_pwd5.getText().toString();
			icam_type5 = cam_type5.getSelectedItemPosition();
			if (icam_type5 < SES_class) {
				corg_num5 = 0;
			} else {
				corg_num5 = videoport5.getSelectedItemPosition();
			}

			icam6_on = active6.isChecked();
			URL_6 = E_URL6.getText().toString();
			label6 = E_label6.getText().toString();
			port6 = Integer.valueOf(E_port6.getText().toString()).intValue();
			account6 = E_account6.getText().toString();
			pwd6 = E_pwd6.getText().toString();
			icam_type6 = cam_type6.getSelectedItemPosition();
			if (icam_type6 < SES_class) {
				corg_num6 = 0;
			} else {
				corg_num6 = videoport6.getSelectedItemPosition();
			}

			icam7_on = active7.isChecked();
			URL_7 = E_URL7.getText().toString();
			label7 = E_label7.getText().toString();
			port17 = Integer.valueOf(E_port7.getText().toString()).intValue();
			account7 = E_account7.getText().toString();
			pwd7 = E_pwd7.getText().toString();
			icam_type7 = cam_type7.getSelectedItemPosition();
			if (icam_type7 < SES_class) {
				corg_num7 = 0;
			} else {
				corg_num7 = videoport7.getSelectedItemPosition();
			}

			icam8_on = active8.isChecked();
			URL_8 = E_URL8.getText().toString();
			label8 = E_label8.getText().toString();
			port8 = Integer.valueOf(E_port8.getText().toString()).intValue();
			account8 = E_account8.getText().toString();
			pwd8 = E_pwd8.getText().toString();
			icam_type8 = cam_type8.getSelectedItemPosition();
			if (icam_type8 < SES_class) {
				corg_num8 = 0;
			} else {
				corg_num8 = videoport8.getSelectedItemPosition();
			}

			icam9_on = active9.isChecked();
			URL_9 = E_URL9.getText().toString();
			label9 = E_label9.getText().toString();
			port9 = Integer.valueOf(E_port9.getText().toString()).intValue();
			account9 = E_account9.getText().toString();
			pwd9 = E_pwd9.getText().toString();
			icam_type9 = cam_type9.getSelectedItemPosition();
			if (icam_type9 < SES_class) {
				corg_num9 = 0;
			} else {
				corg_num9 = videoport9.getSelectedItemPosition();
			}

			icam10_on = active10.isChecked();
			URL_10 = E_URL10.getText().toString();
			label10 = E_label10.getText().toString();
			port10 = Integer.valueOf(E_port10.getText().toString()).intValue();
			account10 = E_account10.getText().toString();
			pwd10 = E_pwd10.getText().toString();
			icam_type10 = cam_type10.getSelectedItemPosition();
			if (icam_type10 < SES_class) {
				corg_num10 = 0;
			} else {
				corg_num10 = videoport10.getSelectedItemPosition();
			}

			icam1_on = active1.isChecked();
			URL_1 = E_URL1.getText().toString();
			label1 = E_label1.getText().toString();
			port1 = Integer.valueOf(E_port1.getText().toString()).intValue();
			account1 = E_account1.getText().toString();
			pwd1 = E_pwd1.getText().toString();
			icam_type1 = cam_type1.getSelectedItemPosition();
			if (icam_type1 < SES_class) {
				corg_num1 = 0;
			} else {
				corg_num1 = videoport1.getSelectedItemPosition();
			}

			icam2_on = active2.isChecked();
			URL_2 = E_URL2.getText().toString();
			label2 = E_label2.getText().toString();
			port2 = Integer.valueOf(E_port2.getText().toString()).intValue();
			account2 = E_account2.getText().toString();
			pwd2 = E_pwd2.getText().toString();
			icam_type2 = cam_type2.getSelectedItemPosition();
			if (icam_type2 < SES_class) {
				corg_num2 = 0;
			} else {
				corg_num2 = videoport2.getSelectedItemPosition();
			}

			icam3_on = active3.isChecked();
			URL_3 = E_URL3.getText().toString();
			label3 = E_label3.getText().toString();
			port3 = Integer.valueOf(E_port3.getText().toString()).intValue();
			account3 = E_account3.getText().toString();
			pwd3 = E_pwd3.getText().toString();
			icam_type3 = cam_type3.getSelectedItemPosition();
			if (icam_type3 < SES_class) {
				corg_num3 = 0;
			} else {
				corg_num3 = videoport3.getSelectedItemPosition();
			}

			icam4_on = active4.isChecked();
			URL_4 = E_URL4.getText().toString();
			label4 = E_label4.getText().toString();
			port4 = Integer.valueOf(E_port4.getText().toString()).intValue();
			account4 = E_account4.getText().toString();
			pwd4 = E_pwd4.getText().toString();
			icam_type4 = cam_type4.getSelectedItemPosition();
			if (icam_type4 < SES_class) {
				corg_num4 = 0;
			} else {
				corg_num4 = videoport4.getSelectedItemPosition();
			}

			icam5_on = active5.isChecked();
			URL_5 = E_URL5.getText().toString();
			label5 = E_label5.getText().toString();
			port5 = Integer.valueOf(E_port5.getText().toString()).intValue();
			account5 = E_account5.getText().toString();
			pwd5 = E_pwd5.getText().toString();
			icam_type5 = cam_type5.getSelectedItemPosition();
			if (icam_type5 < SES_class) {
				corg_num5 = 0;
			} else {
				corg_num5 = videoport5.getSelectedItemPosition();
			}

			icam6_on = active6.isChecked();
			URL_6 = E_URL6.getText().toString();
			label6 = E_label6.getText().toString();
			port6 = Integer.valueOf(E_port6.getText().toString()).intValue();
			account6 = E_account6.getText().toString();
			pwd6 = E_pwd6.getText().toString();
			icam_type6 = cam_type6.getSelectedItemPosition();
			if (icam_type6 < SES_class) {
				corg_num6 = 0;
			} else {
				corg_num6 = videoport6.getSelectedItemPosition();
			}

			icam7_on = active7.isChecked();
			URL_7 = E_URL7.getText().toString();
			label7 = E_label7.getText().toString();
			port17 = Integer.valueOf(E_port7.getText().toString()).intValue();
			account7 = E_account7.getText().toString();
			pwd7 = E_pwd7.getText().toString();
			icam_type7 = cam_type7.getSelectedItemPosition();
			if (icam_type7 < SES_class) {
				corg_num7 = 0;
			} else {
				corg_num7 = videoport7.getSelectedItemPosition();
			}

			icam8_on = active8.isChecked();
			URL_8 = E_URL8.getText().toString();
			label8 = E_label8.getText().toString();
			port8 = Integer.valueOf(E_port8.getText().toString()).intValue();
			account8 = E_account8.getText().toString();
			pwd8 = E_pwd8.getText().toString();
			icam_type8 = cam_type8.getSelectedItemPosition();
			if (icam_type8 < SES_class) {
				corg_num8 = 0;
			} else {
				corg_num8 = videoport8.getSelectedItemPosition();
			}

			icam9_on = active9.isChecked();
			URL_9 = E_URL9.getText().toString();
			label9 = E_label9.getText().toString();
			port9 = Integer.valueOf(E_port9.getText().toString()).intValue();
			account9 = E_account9.getText().toString();
			pwd9 = E_pwd9.getText().toString();
			icam_type9 = cam_type9.getSelectedItemPosition();
			if (icam_type9 < SES_class) {
				corg_num9 = 0;
			} else {
				corg_num9 = videoport9.getSelectedItemPosition();
			}

			icam10_on = active10.isChecked();
			URL_10 = E_URL10.getText().toString();
			label10 = E_label10.getText().toString();
			port10 = Integer.valueOf(E_port10.getText().toString()).intValue();
			account10 = E_account10.getText().toString();
			pwd10 = E_pwd10.getText().toString();
			icam_type10 = cam_type10.getSelectedItemPosition();
			if (icam_type10 < SES_class) {
				corg_num10 = 0;
			} else {
				corg_num10 = videoport10.getSelectedItemPosition();
			}

			icam11_on = active11.isChecked();
			URL_11 = E_URL11.getText().toString();
			label11 = E_label11.getText().toString();
			port11 = Integer.valueOf(E_port11.getText().toString()).intValue();
			account11 = E_account11.getText().toString();
			pwd11 = E_pwd11.getText().toString();
			icam_type11 = cam_type11.getSelectedItemPosition();
			if (icam_type11 < SES_class) {
				corg_num11 = 0;
			} else {
				corg_num11 = videoport11.getSelectedItemPosition();
			}

			icam12_on = active12.isChecked();
			URL_12 = E_URL12.getText().toString();
			label2 = E_label2.getText().toString();
			port12 = Integer.valueOf(E_port12.getText().toString()).intValue();
			account12 = E_account12.getText().toString();
			pwd12 = E_pwd12.getText().toString();
			icam_type12 = cam_type12.getSelectedItemPosition();
			if (icam_type12 < SES_class) {
				corg_num12 = 0;
			} else {
				corg_num12 = videoport12.getSelectedItemPosition();
			}

			icam13_on = active13.isChecked();
			URL_13 = E_URL13.getText().toString();
			label13 = E_label13.getText().toString();
			port13 = Integer.valueOf(E_port13.getText().toString()).intValue();
			account13 = E_account13.getText().toString();
			pwd13 = E_pwd13.getText().toString();
			icam_type13 = cam_type13.getSelectedItemPosition();
			if (icam_type13 < SES_class) {
				corg_num13 = 0;
			} else {
				corg_num13 = videoport13.getSelectedItemPosition();
			}

			icam14_on = active14.isChecked();
			URL_14 = E_URL14.getText().toString();
			label14 = E_label14.getText().toString();
			port14 = Integer.valueOf(E_port14.getText().toString()).intValue();
			account14 = E_account14.getText().toString();
			pwd14 = E_pwd14.getText().toString();
			icam_type14 = cam_type14.getSelectedItemPosition();
			if (icam_type14 < SES_class) {
				corg_num14 = 0;
			} else {
				corg_num14 = videoport14.getSelectedItemPosition();
			}

			icam15_on = active15.isChecked();
			URL_15 = E_URL15.getText().toString();
			label15 = E_label15.getText().toString();
			port15 = Integer.valueOf(E_port15.getText().toString()).intValue();
			account15 = E_account15.getText().toString();
			pwd15 = E_pwd15.getText().toString();
			icam_type15 = cam_type15.getSelectedItemPosition();
			if (icam_type15 < SES_class) {
				corg_num15 = 0;
			} else {
				corg_num15 = videoport15.getSelectedItemPosition();
			}

			icam16_on = active16.isChecked();
			URL_16 = E_URL16.getText().toString();
			label16 = E_label16.getText().toString();
			port16 = Integer.valueOf(E_port16.getText().toString()).intValue();
			account16 = E_account16.getText().toString();
			pwd16 = E_pwd16.getText().toString();
			icam_type16 = cam_type16.getSelectedItemPosition();
			if (icam_type16 < SES_class) {
				corg_num16 = 0;
			} else {
				corg_num16 = videoport16.getSelectedItemPosition();
			}

			icam17_on = active17.isChecked();
			URL_17 = E_URL17.getText().toString();
			label17 = E_label17.getText().toString();
			port17 = Integer.valueOf(E_port17.getText().toString()).intValue();
			account17 = E_account17.getText().toString();
			pwd17 = E_pwd17.getText().toString();
			icam_type17 = cam_type17.getSelectedItemPosition();
			if (icam_type17 < SES_class) {
				corg_num17 = 0;
			} else {
				corg_num17 = videoport17.getSelectedItemPosition();
			}

			icam18_on = active18.isChecked();
			URL_18 = E_URL18.getText().toString();
			label18 = E_label18.getText().toString();
			port18 = Integer.valueOf(E_port18.getText().toString()).intValue();
			account18 = E_account18.getText().toString();
			pwd18 = E_pwd18.getText().toString();
			icam_type18 = cam_type18.getSelectedItemPosition();
			if (icam_type18 < SES_class) {
				corg_num18 = 0;
			} else {
				corg_num18 = videoport18.getSelectedItemPosition();
			}

			icam19_on = active19.isChecked();
			URL_19 = E_URL19.getText().toString();
			label19 = E_label19.getText().toString();
			port19 = Integer.valueOf(E_port9.getText().toString()).intValue();
			account19 = E_account19.getText().toString();
			pwd19 = E_pwd19.getText().toString();
			icam_type19 = cam_type19.getSelectedItemPosition();
			if (icam_type19 < SES_class) {
				corg_num19 = 0;
			} else {
				corg_num19 = videoport19.getSelectedItemPosition();
			}

			icam20_on = active20.isChecked();
			URL_20 = E_URL20.getText().toString();
			label20 = E_label20.getText().toString();
			port20 = Integer.valueOf(E_port20.getText().toString()).intValue();
			account20 = E_account20.getText().toString();
			pwd20 = E_pwd20.getText().toString();
			icam_type20 = cam_type20.getSelectedItemPosition();
			if (icam_type20 < SES_class) {
				corg_num20 = 0;
			} else {
				corg_num20 = videoport20.getSelectedItemPosition();
			}
		} else if (btn_type == btn_set) {
			if (net_type == wifi) {
				wifi_ip[0] = e_ip1.getText().toString();
				wifi_ip[1] = e_ip2.getText().toString();
				wifi_ip[2] = e_ip3.getText().toString();
				wifi_ip[3] = e_ip4.getText().toString();
			} else {
				eth0_ip[0] = e_ip1.getText().toString();
				eth0_ip[1] = e_ip2.getText().toString();
				eth0_ip[2] = e_ip3.getText().toString();
				eth0_ip[3] = e_ip4.getText().toString();
			}

			gateway_ip[0] = e_gateway_ip1.getText().toString();
			gateway_ip[1] = e_gateway_ip2.getText().toString();
			gateway_ip[2] = e_gateway_ip3.getText().toString();
			gateway_ip[3] = e_gateway_ip4.getText().toString();

			mask_ip[0] = e_mask_ip1.getText().toString();
			mask_ip[1] = e_mask_ip2.getText().toString();
			mask_ip[2] = e_mask_ip3.getText().toString();
			mask_ip[3] = e_mask_ip4.getText().toString();

			// login_account = account.getText().toString();
			login_pwd = e_loginpwd.getText().toString();
			hotspot_ssid = e_hotspot.getText().toString();

			if (!loop_on) {
				loop_time = 20;
			} else {
				loop_time = 20 + loop_select.getSelectedItemPosition() * 10;
			}
		}
	}

	public void load_tem() {
		user_change = false;

		if (btn_type == btn_channel) {
			cam_type1.setSelection(icam_type1);
			if (icam_type1 == SES_class) {
				videoport1.setSelection(0);
				videoport1.setAdapter(class_adapter);
				videoport1.setSelection(corg_num1);
			} else if (icam_type1 == SES_group) {
				videoport1.setSelection(0);
				videoport1.setAdapter(group_adapter);
				videoport1.setSelection(corg_num1);
			}
			active1.setChecked(icam1_on);
			E_pwd1.setText(pwd1);
			E_URL1.setText(URL_1);
			E_label1.setText(label1);
			E_account1.setText(account1);
			E_port1.setText(String.valueOf(port1));

			cam_type2.setSelection(icam_type2);
			if (icam_type2 == SES_class) {
				videoport2.setSelection(0);
				videoport2.setAdapter(class_adapter);
				videoport2.setSelection(corg_num2);
			} else if (icam_type2 == SES_group) {
				videoport2.setSelection(0);
				videoport2.setAdapter(group_adapter);
				videoport2.setSelection(corg_num2);
			}
			active2.setChecked(icam2_on);
			E_pwd2.setText(pwd2);
			E_URL2.setText(URL_2);
			E_label2.setText(label2);
			E_account2.setText(account2);
			E_port2.setText(String.valueOf(port2));

			cam_type3.setSelection(icam_type3);
			if (icam_type3 == SES_class) {
				videoport3.setSelection(0);
				videoport3.setAdapter(class_adapter);
				videoport3.setSelection(corg_num3);
			} else if (icam_type3 == SES_group) {
				videoport3.setSelection(0);
				videoport3.setAdapter(group_adapter);
				videoport3.setSelection(corg_num3);
			}
			active3.setChecked(icam3_on);
			E_pwd3.setText(pwd3);
			E_URL3.setText(URL_3);
			E_label3.setText(label3);
			E_account3.setText(account3);
			E_port3.setText(String.valueOf(port3));

			cam_type4.setSelection(icam_type4);
			if (icam_type4 == SES_class) {
				videoport4.setSelection(0);
				videoport4.setAdapter(class_adapter);
				videoport4.setSelection(corg_num4);
			} else if (icam_type4 == SES_group) {
				videoport4.setSelection(0);
				videoport4.setAdapter(group_adapter);
				videoport4.setSelection(corg_num4);
			}
			active4.setChecked(icam4_on);
			E_pwd4.setText(pwd4);
			E_URL4.setText(URL_4);
			E_label4.setText(label4);
			E_account4.setText(account4);
			E_port4.setText(String.valueOf(port4));

			cam_type5.setSelection(icam_type5);
			if (icam_type5 == SES_class) {
				videoport5.setSelection(0);
				videoport5.setAdapter(class_adapter);
				videoport5.setSelection(corg_num5);
			} else if (icam_type5 == SES_group) {
				videoport5.setSelection(0);
				videoport5.setAdapter(group_adapter);
				videoport5.setSelection(corg_num5);
			}
			active5.setChecked(icam5_on);
			E_pwd5.setText(pwd5);
			E_URL5.setText(URL_5);
			E_label5.setText(label5);
			E_account5.setText(account5);
			E_port5.setText(String.valueOf(port5));

			cam_type6.setSelection(icam_type6);
			if (icam_type6 == SES_class) {
				videoport6.setSelection(0);
				videoport6.setAdapter(class_adapter);
				videoport6.setSelection(corg_num6);
			} else if (icam_type6 == SES_group) {
				videoport6.setSelection(0);
				videoport6.setAdapter(group_adapter);
				videoport6.setSelection(corg_num6);
			}
			active6.setChecked(icam6_on);
			E_pwd6.setText(pwd6);
			E_URL6.setText(URL_6);
			E_label6.setText(label6);
			E_account6.setText(account6);
			E_port6.setText(String.valueOf(port6));

			cam_type7.setSelection(icam_type7);
			if (icam_type7 == SES_class) {
				videoport7.setSelection(0);
				videoport7.setAdapter(class_adapter);
				videoport7.setSelection(corg_num7);
			} else if (icam_type7 == SES_group) {
				videoport7.setSelection(0);
				videoport7.setAdapter(group_adapter);
				videoport7.setSelection(corg_num7);
			}
			active7.setChecked(icam7_on);
			E_pwd7.setText(pwd7);
			E_URL7.setText(URL_7);
			E_label7.setText(label7);
			E_account7.setText(account7);
			E_port7.setText(String.valueOf(port7));

			cam_type8.setSelection(icam_type8);
			if (icam_type8 == SES_class) {
				videoport8.setSelection(0);
				videoport8.setAdapter(class_adapter);
				videoport8.setSelection(corg_num8);
			} else if (icam_type8 == SES_group) {
				videoport8.setSelection(0);
				videoport8.setAdapter(group_adapter);
				videoport8.setSelection(corg_num8);
			}
			active8.setChecked(icam8_on);
			E_pwd8.setText(pwd8);
			E_URL8.setText(URL_8);
			E_label8.setText(label8);
			E_account8.setText(account8);
			E_port8.setText(String.valueOf(port8));

			cam_type9.setSelection(icam_type9);
			if (icam_type9 == SES_class) {
				videoport9.setSelection(0);
				videoport9.setAdapter(class_adapter);
				videoport9.setSelection(corg_num9);
			} else if (icam_type9 == SES_group) {
				videoport9.setSelection(0);
				videoport9.setAdapter(group_adapter);
				videoport9.setSelection(corg_num9);
			}
			active9.setChecked(icam9_on);
			E_pwd9.setText(pwd9);
			E_URL9.setText(URL_9);
			E_label9.setText(label9);
			E_account9.setText(account9);
			E_port9.setText(String.valueOf(port9));

			cam_type10.setSelection(icam_type10);
			if (icam_type10 == SES_class) {
				videoport10.setSelection(0);
				videoport10.setAdapter(class_adapter);
				videoport10.setSelection(corg_num10);
			} else if (icam_type10 == SES_group) {
				videoport10.setSelection(0);
				videoport10.setAdapter(group_adapter);
				videoport10.setSelection(corg_num10);
			}
			active10.setChecked(icam10_on);
			E_pwd10.setText(pwd10);
			E_URL10.setText(URL_10);
			E_label10.setText(label10);
			E_account10.setText(account10);
			E_port10.setText(String.valueOf(port10));

			cam_type11.setSelection(icam_type11);
			if (icam_type11 == SES_class) {
				videoport11.setSelection(0);
				videoport11.setAdapter(class_adapter);
				videoport11.setSelection(corg_num11);
			} else if (icam_type11 == SES_group) {
				videoport11.setSelection(0);
				videoport11.setAdapter(group_adapter);
				videoport11.setSelection(corg_num11);
			}
			active11.setChecked(icam11_on);
			E_pwd11.setText(pwd11);
			E_URL11.setText(URL_11);
			E_label11.setText(label11);
			E_account11.setText(account11);
			E_port11.setText(String.valueOf(port11));

			cam_type12.setSelection(icam_type12);
			if (icam_type12 == SES_class) {
				videoport12.setSelection(0);
				videoport12.setAdapter(class_adapter);
				videoport12.setSelection(corg_num12);
			} else if (icam_type2 == SES_group) {
				videoport12.setSelection(0);
				videoport12.setAdapter(group_adapter);
				videoport12.setSelection(corg_num12);
			}
			active12.setChecked(icam12_on);
			E_pwd12.setText(pwd12);
			E_URL12.setText(URL_12);
			E_label12.setText(label12);
			E_account12.setText(account12);
			E_port12.setText(String.valueOf(port12));

			cam_type13.setSelection(icam_type13);
			if (icam_type13 == SES_class) {
				videoport13.setSelection(0);
				videoport13.setAdapter(class_adapter);
				videoport13.setSelection(corg_num13);
			} else if (icam_type13 == SES_group) {
				videoport13.setSelection(0);
				videoport13.setAdapter(group_adapter);
				videoport13.setSelection(corg_num13);
			}
			active13.setChecked(icam13_on);
			E_pwd13.setText(pwd13);
			E_URL13.setText(URL_13);
			E_label13.setText(label13);
			E_account13.setText(account13);
			E_port13.setText(String.valueOf(port13));

			cam_type14.setSelection(icam_type14);
			if (icam_type14 == SES_class) {
				videoport14.setSelection(0);
				videoport14.setAdapter(class_adapter);
				videoport14.setSelection(corg_num14);
			} else if (icam_type14 == SES_group) {
				videoport14.setSelection(0);
				videoport14.setAdapter(group_adapter);
				videoport14.setSelection(corg_num14);
			}
			active14.setChecked(icam14_on);
			E_pwd14.setText(pwd14);
			E_URL14.setText(URL_14);
			E_label14.setText(label14);
			E_account14.setText(account14);
			E_port14.setText(String.valueOf(port14));

			cam_type15.setSelection(icam_type15);
			if (icam_type15 == SES_class) {
				videoport15.setSelection(0);
				videoport15.setAdapter(class_adapter);
				videoport15.setSelection(corg_num15);
			} else if (icam_type15 == SES_group) {
				videoport15.setSelection(0);
				videoport15.setAdapter(group_adapter);
				videoport15.setSelection(corg_num15);
			}
			active15.setChecked(icam15_on);
			E_pwd15.setText(pwd15);
			E_URL15.setText(URL_15);
			E_label15.setText(label15);
			E_account15.setText(account15);
			E_port15.setText(String.valueOf(port15));

			cam_type16.setSelection(icam_type16);
			if (icam_type16 == SES_class) {
				videoport16.setSelection(0);
				videoport16.setAdapter(class_adapter);
				videoport16.setSelection(corg_num16);
			} else if (icam_type16 == SES_group) {
				videoport16.setSelection(0);
				videoport16.setAdapter(group_adapter);
				videoport16.setSelection(corg_num16);
			}
			active16.setChecked(icam16_on);
			E_pwd16.setText(pwd16);
			E_URL16.setText(URL_16);
			E_label16.setText(label16);
			E_account16.setText(account16);
			E_port16.setText(String.valueOf(port16));

			cam_type17.setSelection(icam_type17);
			if (icam_type17 == SES_class) {
				videoport17.setSelection(0);
				videoport17.setAdapter(class_adapter);
				videoport17.setSelection(corg_num17);
			} else if (icam_type17 == SES_group) {
				videoport17.setSelection(0);
				videoport17.setAdapter(group_adapter);
				videoport17.setSelection(corg_num17);
			}
			active17.setChecked(icam17_on);
			E_pwd17.setText(pwd17);
			E_URL17.setText(URL_17);
			E_label17.setText(label17);
			E_account17.setText(account17);
			E_port17.setText(String.valueOf(port17));

			cam_type18.setSelection(icam_type18);
			if (icam_type18 == SES_class) {
				videoport18.setSelection(0);
				videoport18.setAdapter(class_adapter);
				videoport18.setSelection(corg_num18);
			} else if (icam_type18 == SES_group) {
				videoport18.setSelection(0);
				videoport18.setAdapter(group_adapter);
				videoport18.setSelection(corg_num18);
			}
			active18.setChecked(icam18_on);
			E_pwd18.setText(pwd18);
			E_URL18.setText(URL_18);
			E_label18.setText(label18);
			E_account18.setText(account18);
			E_port18.setText(String.valueOf(port18));

			cam_type19.setSelection(icam_type19);
			if (icam_type19 == SES_class) {
				videoport19.setSelection(0);
				videoport19.setAdapter(class_adapter);
				videoport19.setSelection(corg_num19);
			} else if (icam_type19 == SES_group) {
				videoport19.setSelection(0);
				videoport19.setAdapter(group_adapter);
				videoport19.setSelection(corg_num19);
			}
			active19.setChecked(icam19_on);
			E_pwd19.setText(pwd19);
			E_URL19.setText(URL_19);
			E_label19.setText(label19);
			E_account19.setText(account19);
			E_port19.setText(String.valueOf(port19));

			cam_type20.setSelection(icam_type20);
			if (icam_type20 == SES_class) {
				videoport20.setSelection(0);
				videoport20.setAdapter(class_adapter);
				videoport20.setSelection(corg_num20);
			} else if (icam_type20 == SES_group) {
				videoport20.setSelection(0);
				videoport20.setAdapter(group_adapter);
				videoport20.setSelection(corg_num20);
			}
			active20.setChecked(icam20_on);
			E_pwd20.setText(pwd20);
			E_URL20.setText(URL_20);
			E_label20.setText(label20);
			E_account20.setText(account20);
			E_port20.setText(String.valueOf(port20));

		} else if (btn_type == btn_set) {
			if (net_type == wifi) {
				wifi_ip[0] = e_ip1.getText().toString();
				wifi_ip[1] = e_ip2.getText().toString();
				wifi_ip[2] = e_ip3.getText().toString();
				wifi_ip[3] = e_ip4.getText().toString();
			} else {
				eth0_ip[0] = e_ip1.getText().toString();
				eth0_ip[1] = e_ip2.getText().toString();
				eth0_ip[2] = e_ip3.getText().toString();
				eth0_ip[3] = e_ip4.getText().toString();
			}

			Log.v("test", "ip" + eth0_ip[0] + eth0_ip[1] + eth0_ip[2] + eth0_ip[3]);
			gateway_ip[0] = e_gateway_ip1.getText().toString();
			gateway_ip[1] = e_gateway_ip2.getText().toString();
			gateway_ip[2] = e_gateway_ip3.getText().toString();
			gateway_ip[3] = e_gateway_ip4.getText().toString();

			mask_ip[0] = e_mask_ip1.getText().toString();
			mask_ip[1] = e_mask_ip2.getText().toString();
			mask_ip[2] = e_mask_ip3.getText().toString();
			mask_ip[3] = e_mask_ip4.getText().toString();

			// login_account = account.getText().toString();
			login_pwd = e_loginpwd.getText().toString();
			hotspot_ssid = e_hotspot.getText().toString();

		}
	}

	private void cam_content() {
		cam_setview = getLayoutInflater().inflate(R.layout.ondemand_cam_setting, null);

		E_URL1 = (EditText) cam_setview.findViewById(R.id.ip1);
		E_URL2 = (EditText) cam_setview.findViewById(R.id.ip2);
		E_URL3 = (EditText) cam_setview.findViewById(R.id.ip3);
		E_URL4 = (EditText) cam_setview.findViewById(R.id.ip4);
		E_URL5 = (EditText) cam_setview.findViewById(R.id.ip5);
		E_URL6 = (EditText) cam_setview.findViewById(R.id.ip6);
		E_URL7 = (EditText) cam_setview.findViewById(R.id.ip7);
		E_URL8 = (EditText) cam_setview.findViewById(R.id.ip8);
		E_URL9 = (EditText) cam_setview.findViewById(R.id.ip9);
		E_URL10 = (EditText) cam_setview.findViewById(R.id.ip10);
		E_URL11 = (EditText) cam_setview.findViewById(R.id.ip11);
		E_URL12 = (EditText) cam_setview.findViewById(R.id.ip12);
		E_URL13 = (EditText) cam_setview.findViewById(R.id.ip13);
		E_URL14 = (EditText) cam_setview.findViewById(R.id.ip14);
		E_URL15 = (EditText) cam_setview.findViewById(R.id.ip15);
		E_URL16 = (EditText) cam_setview.findViewById(R.id.ip16);
		E_URL17 = (EditText) cam_setview.findViewById(R.id.ip17);
		E_URL18 = (EditText) cam_setview.findViewById(R.id.ip18);
		E_URL19 = (EditText) cam_setview.findViewById(R.id.ip19);
		E_URL20 = (EditText) cam_setview.findViewById(R.id.ip20);

		E_label1 = (EditText) cam_setview.findViewById(R.id.label1);
		E_label2 = (EditText) cam_setview.findViewById(R.id.label2);
		E_label3 = (EditText) cam_setview.findViewById(R.id.label3);
		E_label4 = (EditText) cam_setview.findViewById(R.id.label4);
		E_label5 = (EditText) cam_setview.findViewById(R.id.label5);
		E_label6 = (EditText) cam_setview.findViewById(R.id.label6);
		E_label7 = (EditText) cam_setview.findViewById(R.id.label7);
		E_label8 = (EditText) cam_setview.findViewById(R.id.label8);
		E_label9 = (EditText) cam_setview.findViewById(R.id.label9);
		E_label10 = (EditText) cam_setview.findViewById(R.id.label10);
		E_label11 = (EditText) cam_setview.findViewById(R.id.label11);
		E_label12 = (EditText) cam_setview.findViewById(R.id.label12);
		E_label13 = (EditText) cam_setview.findViewById(R.id.label13);
		E_label14 = (EditText) cam_setview.findViewById(R.id.label14);
		E_label15 = (EditText) cam_setview.findViewById(R.id.label15);
		E_label16 = (EditText) cam_setview.findViewById(R.id.label16);
		E_label17 = (EditText) cam_setview.findViewById(R.id.label17);
		E_label18 = (EditText) cam_setview.findViewById(R.id.label18);
		E_label19 = (EditText) cam_setview.findViewById(R.id.label19);
		E_label20 = (EditText) cam_setview.findViewById(R.id.label20);

		E_port1 = (EditText) cam_setview.findViewById(R.id.port1);
		E_port2 = (EditText) cam_setview.findViewById(R.id.port2);
		E_port3 = (EditText) cam_setview.findViewById(R.id.port3);
		E_port4 = (EditText) cam_setview.findViewById(R.id.port4);
		E_port5 = (EditText) cam_setview.findViewById(R.id.port5);
		E_port6 = (EditText) cam_setview.findViewById(R.id.port6);
		E_port7 = (EditText) cam_setview.findViewById(R.id.port7);
		E_port8 = (EditText) cam_setview.findViewById(R.id.port8);
		E_port9 = (EditText) cam_setview.findViewById(R.id.port9);
		E_port10 = (EditText) cam_setview.findViewById(R.id.port10);
		E_port11 = (EditText) cam_setview.findViewById(R.id.port11);
		E_port12 = (EditText) cam_setview.findViewById(R.id.port12);
		E_port13 = (EditText) cam_setview.findViewById(R.id.port13);
		E_port14 = (EditText) cam_setview.findViewById(R.id.port14);
		E_port15 = (EditText) cam_setview.findViewById(R.id.port15);
		E_port16 = (EditText) cam_setview.findViewById(R.id.port16);
		E_port17 = (EditText) cam_setview.findViewById(R.id.port17);
		E_port18 = (EditText) cam_setview.findViewById(R.id.port18);
		E_port19 = (EditText) cam_setview.findViewById(R.id.port19);
		E_port20 = (EditText) cam_setview.findViewById(R.id.port20);

		E_account1 = (EditText) cam_setview.findViewById(R.id.account1);
		E_account2 = (EditText) cam_setview.findViewById(R.id.account2);
		E_account3 = (EditText) cam_setview.findViewById(R.id.account3);
		E_account4 = (EditText) cam_setview.findViewById(R.id.account4);
		E_account5 = (EditText) cam_setview.findViewById(R.id.account5);
		E_account6 = (EditText) cam_setview.findViewById(R.id.account6);
		E_account7 = (EditText) cam_setview.findViewById(R.id.account7);
		E_account8 = (EditText) cam_setview.findViewById(R.id.account8);
		E_account9 = (EditText) cam_setview.findViewById(R.id.account9);
		E_account10 = (EditText) cam_setview.findViewById(R.id.account10);
		E_account11 = (EditText) cam_setview.findViewById(R.id.account11);
		E_account12 = (EditText) cam_setview.findViewById(R.id.account12);
		E_account13 = (EditText) cam_setview.findViewById(R.id.account13);
		E_account14 = (EditText) cam_setview.findViewById(R.id.account14);
		E_account15 = (EditText) cam_setview.findViewById(R.id.account15);
		E_account16 = (EditText) cam_setview.findViewById(R.id.account16);
		E_account17 = (EditText) cam_setview.findViewById(R.id.account17);
		E_account18 = (EditText) cam_setview.findViewById(R.id.account18);
		E_account19 = (EditText) cam_setview.findViewById(R.id.account19);
		E_account20 = (EditText) cam_setview.findViewById(R.id.account20);

		E_pwd1 = (EditText) cam_setview.findViewById(R.id.pwd1);
		E_pwd2 = (EditText) cam_setview.findViewById(R.id.pwd2);
		E_pwd3 = (EditText) cam_setview.findViewById(R.id.pwd3);
		E_pwd4 = (EditText) cam_setview.findViewById(R.id.pwd4);
		E_pwd5 = (EditText) cam_setview.findViewById(R.id.pwd5);
		E_pwd6 = (EditText) cam_setview.findViewById(R.id.pwd6);
		E_pwd7 = (EditText) cam_setview.findViewById(R.id.pwd7);
		E_pwd8 = (EditText) cam_setview.findViewById(R.id.pwd8);
		E_pwd9 = (EditText) cam_setview.findViewById(R.id.pwd9);
		E_pwd10 = (EditText) cam_setview.findViewById(R.id.pwd10);
		E_pwd11 = (EditText) cam_setview.findViewById(R.id.pwd11);
		E_pwd12 = (EditText) cam_setview.findViewById(R.id.pwd12);
		E_pwd13 = (EditText) cam_setview.findViewById(R.id.pwd13);
		E_pwd14 = (EditText) cam_setview.findViewById(R.id.pwd14);
		E_pwd15 = (EditText) cam_setview.findViewById(R.id.pwd15);
		E_pwd16 = (EditText) cam_setview.findViewById(R.id.pwd16);
		E_pwd17 = (EditText) cam_setview.findViewById(R.id.pwd17);
		E_pwd18 = (EditText) cam_setview.findViewById(R.id.pwd18);
		E_pwd19 = (EditText) cam_setview.findViewById(R.id.pwd19);
		E_pwd20 = (EditText) cam_setview.findViewById(R.id.pwd20);

		active1 = (CheckBox) cam_setview.findViewById(R.id.enable1);
		active2 = (CheckBox) cam_setview.findViewById(R.id.enable2);
		active3 = (CheckBox) cam_setview.findViewById(R.id.enable3);
		active4 = (CheckBox) cam_setview.findViewById(R.id.enable4);
		active5 = (CheckBox) cam_setview.findViewById(R.id.enable5);
		active6 = (CheckBox) cam_setview.findViewById(R.id.enable6);
		active7 = (CheckBox) cam_setview.findViewById(R.id.enable7);
		active8 = (CheckBox) cam_setview.findViewById(R.id.enable8);
		active9 = (CheckBox) cam_setview.findViewById(R.id.enable9);
		active10 = (CheckBox) cam_setview.findViewById(R.id.enable10);
		active11 = (CheckBox) cam_setview.findViewById(R.id.enable11);
		active12 = (CheckBox) cam_setview.findViewById(R.id.enable12);
		active13 = (CheckBox) cam_setview.findViewById(R.id.enable13);
		active14 = (CheckBox) cam_setview.findViewById(R.id.enable14);
		active15 = (CheckBox) cam_setview.findViewById(R.id.enable15);
		active16 = (CheckBox) cam_setview.findViewById(R.id.enable16);
		active17 = (CheckBox) cam_setview.findViewById(R.id.enable17);
		active18 = (CheckBox) cam_setview.findViewById(R.id.enable18);
		active19 = (CheckBox) cam_setview.findViewById(R.id.enable19);
		active20 = (CheckBox) cam_setview.findViewById(R.id.enable20);

		cam_type1 = (Spinner) cam_setview.findViewById(R.id.type1);
		cam_type2 = (Spinner) cam_setview.findViewById(R.id.type2);
		cam_type3 = (Spinner) cam_setview.findViewById(R.id.type3);
		cam_type4 = (Spinner) cam_setview.findViewById(R.id.type4);
		cam_type5 = (Spinner) cam_setview.findViewById(R.id.type5);
		cam_type6 = (Spinner) cam_setview.findViewById(R.id.type6);
		cam_type7 = (Spinner) cam_setview.findViewById(R.id.type7);
		cam_type8 = (Spinner) cam_setview.findViewById(R.id.type8);
		cam_type9 = (Spinner) cam_setview.findViewById(R.id.type9);
		cam_type10 = (Spinner) cam_setview.findViewById(R.id.type10);
		cam_type11 = (Spinner) cam_setview.findViewById(R.id.type11);
		cam_type12 = (Spinner) cam_setview.findViewById(R.id.type12);
		cam_type13 = (Spinner) cam_setview.findViewById(R.id.type13);
		cam_type14 = (Spinner) cam_setview.findViewById(R.id.type14);
		cam_type15 = (Spinner) cam_setview.findViewById(R.id.type15);
		cam_type16 = (Spinner) cam_setview.findViewById(R.id.type16);
		cam_type17 = (Spinner) cam_setview.findViewById(R.id.type17);
		cam_type18 = (Spinner) cam_setview.findViewById(R.id.type18);
		cam_type19 = (Spinner) cam_setview.findViewById(R.id.type19);
		cam_type20 = (Spinner) cam_setview.findViewById(R.id.type20);

		videoport1 = (Spinner) cam_setview.findViewById(R.id.classgroup1);
		videoport2 = (Spinner) cam_setview.findViewById(R.id.classgroup2);
		videoport3 = (Spinner) cam_setview.findViewById(R.id.classgroup3);
		videoport4 = (Spinner) cam_setview.findViewById(R.id.classgroup4);
		videoport5 = (Spinner) cam_setview.findViewById(R.id.classgroup5);
		videoport6 = (Spinner) cam_setview.findViewById(R.id.classgroup6);
		videoport7 = (Spinner) cam_setview.findViewById(R.id.classgroup7);
		videoport8 = (Spinner) cam_setview.findViewById(R.id.classgroup8);
		videoport9 = (Spinner) cam_setview.findViewById(R.id.classgroup9);
		videoport10 = (Spinner) cam_setview.findViewById(R.id.classgroup10);
		videoport11 = (Spinner) cam_setview.findViewById(R.id.classgroup11);
		videoport12 = (Spinner) cam_setview.findViewById(R.id.classgroup12);
		videoport13 = (Spinner) cam_setview.findViewById(R.id.classgroup13);
		videoport14 = (Spinner) cam_setview.findViewById(R.id.classgroup14);
		videoport15 = (Spinner) cam_setview.findViewById(R.id.classgroup15);
		videoport16 = (Spinner) cam_setview.findViewById(R.id.classgroup16);
		videoport17 = (Spinner) cam_setview.findViewById(R.id.classgroup17);
		videoport18 = (Spinner) cam_setview.findViewById(R.id.classgroup18);
		videoport19 = (Spinner) cam_setview.findViewById(R.id.classgroup19);
		videoport20 = (Spinner) cam_setview.findViewById(R.id.classgroup20);

		check1 = (ImageButton) cam_setview.findViewById(R.id.test1);
		check2 = (ImageButton) cam_setview.findViewById(R.id.test2);
		check3 = (ImageButton) cam_setview.findViewById(R.id.test3);
		check4 = (ImageButton) cam_setview.findViewById(R.id.test4);
		check5 = (ImageButton) cam_setview.findViewById(R.id.test5);
		check6 = (ImageButton) cam_setview.findViewById(R.id.test6);
		check7 = (ImageButton) cam_setview.findViewById(R.id.test7);
		check8 = (ImageButton) cam_setview.findViewById(R.id.test8);
		check9 = (ImageButton) cam_setview.findViewById(R.id.test9);
		check10 = (ImageButton) cam_setview.findViewById(R.id.test10);
		check11 = (ImageButton) cam_setview.findViewById(R.id.test11);
		check12 = (ImageButton) cam_setview.findViewById(R.id.test12);
		check13 = (ImageButton) cam_setview.findViewById(R.id.test13);
		check14 = (ImageButton) cam_setview.findViewById(R.id.test14);
		check15 = (ImageButton) cam_setview.findViewById(R.id.test15);
		check16 = (ImageButton) cam_setview.findViewById(R.id.test16);
		check17 = (ImageButton) cam_setview.findViewById(R.id.test17);
		check18 = (ImageButton) cam_setview.findViewById(R.id.test18);
		check19 = (ImageButton) cam_setview.findViewById(R.id.test19);
		check20 = (ImageButton) cam_setview.findViewById(R.id.test20);

		sipnneradapter = new ArrayAdapter<String>(this, R.layout.ondemand_myspinner, icam);
		sipnneradapter.setDropDownViewResource(R.layout.ondemand_myspinner);
		cam_type1.setAdapter(sipnneradapter);
		cam_type2.setAdapter(sipnneradapter);
		cam_type3.setAdapter(sipnneradapter);
		cam_type4.setAdapter(sipnneradapter);
		cam_type5.setAdapter(sipnneradapter);
		cam_type6.setAdapter(sipnneradapter);
		cam_type7.setAdapter(sipnneradapter);
		cam_type8.setAdapter(sipnneradapter);
		cam_type9.setAdapter(sipnneradapter);
		cam_type10.setAdapter(sipnneradapter);
		cam_type11.setAdapter(sipnneradapter);
		cam_type12.setAdapter(sipnneradapter);
		cam_type13.setAdapter(sipnneradapter);
		cam_type14.setAdapter(sipnneradapter);
		cam_type15.setAdapter(sipnneradapter);
		cam_type16.setAdapter(sipnneradapter);
		cam_type17.setAdapter(sipnneradapter);
		cam_type18.setAdapter(sipnneradapter);
		cam_type19.setAdapter(sipnneradapter);
		cam_type20.setAdapter(sipnneradapter);

		class_adapter = new ArrayAdapter<String>(this, R.layout.ondemand_myspinner, class_port);
		class_adapter.setDropDownViewResource(R.layout.ondemand_myspinner);
		group_adapter = new ArrayAdapter<String>(this, R.layout.ondemand_myspinner, group_port);
		group_adapter.setDropDownViewResource(R.layout.ondemand_myspinner);

		cam_type1.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				// Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport1.getVisibility() == View.VISIBLE) {
						videoport1.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port1.setText("80");
						E_account1.setText("root");
					}
				} else {
					if (videoport1.getVisibility() != View.VISIBLE) {
						videoport1.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport1.setSelection(0);
							videoport1.setAdapter(class_adapter);
						} else {
							videoport1.setSelection(0);
							videoport1.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port1.setText("8888");
						E_account1.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type1.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL1.getText().toString(), E_port1.getText().toString(), E_account1.getText()
							.toString(), E_pwd1.getText().toString(), cam_type1.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL1.getText().toString(), E_port1.getText().toString(), E_account1.getText()
							.toString(), E_pwd1.getText().toString(), cam_type1.getSelectedItemPosition(), videoport1
							.getSelectedItemPosition());
				}
			}
		});

		cam_type2.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport2.getVisibility() == View.VISIBLE) {
						videoport2.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port2.setText("80");
						E_account2.setText("root");
					}
				} else {
					if (videoport2.getVisibility() != View.VISIBLE) {
						videoport2.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport2.setSelection(0);
							videoport2.setAdapter(class_adapter);
						} else {
							videoport2.setSelection(0);
							videoport2.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port2.setText("8888");
						E_account2.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type2.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL2.getText().toString(), E_port2.getText().toString(), E_account2.getText()
							.toString(), E_pwd2.getText().toString(), cam_type2.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL2.getText().toString(), E_port2.getText().toString(), E_account2.getText()
							.toString(), E_pwd2.getText().toString(), cam_type2.getSelectedItemPosition(), videoport2
							.getSelectedItemPosition());
				}
			}
		});

		cam_type3.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport3.getVisibility() == View.VISIBLE) {
						videoport3.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port3.setText("80");
						E_account3.setText("root");
					}
				} else {
					if (videoport3.getVisibility() != View.VISIBLE) {
						videoport3.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport3.setSelection(0);
							videoport3.setAdapter(class_adapter);
						} else {
							videoport3.setSelection(0);
							videoport3.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port3.setText("8888");
						E_account3.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type3.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL3.getText().toString(), E_port3.getText().toString(), E_account3.getText()
							.toString(), E_pwd3.getText().toString(), cam_type3.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL3.getText().toString(), E_port3.getText().toString(), E_account3.getText()
							.toString(), E_pwd3.getText().toString(), cam_type3.getSelectedItemPosition(), videoport3
							.getSelectedItemPosition());
				}
			}
		});

		cam_type4.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport4.getVisibility() == View.VISIBLE) {
						videoport4.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port4.setText("80");
						E_account4.setText("root");
					}
				} else {
					if (videoport4.getVisibility() != View.VISIBLE) {
						videoport4.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport4.setSelection(0);
							videoport4.setAdapter(class_adapter);
						} else {
							videoport4.setSelection(0);
							videoport4.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port4.setText("8888");
						E_account4.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type4.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL4.getText().toString(), E_port4.getText().toString(), E_account4.getText()
							.toString(), E_pwd4.getText().toString(), cam_type4.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL4.getText().toString(), E_port4.getText().toString(), E_account4.getText()
							.toString(), E_pwd4.getText().toString(), cam_type4.getSelectedItemPosition(), videoport4
							.getSelectedItemPosition());
				}
			}
		});

		cam_type5.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport5.getVisibility() == View.VISIBLE) {
						videoport5.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port5.setText("80");
						E_account5.setText("root");
					}
				} else {
					if (videoport5.getVisibility() != View.VISIBLE) {
						videoport5.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport5.setSelection(0);
							videoport5.setAdapter(class_adapter);
						} else {
							videoport5.setSelection(0);
							videoport5.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port5.setText("8888");
						E_account5.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check5.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type5.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL5.getText().toString(), E_port5.getText().toString(), E_account5.getText()
							.toString(), E_pwd5.getText().toString(), cam_type5.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL5.getText().toString(), E_port5.getText().toString(), E_account5.getText()
							.toString(), E_pwd5.getText().toString(), cam_type5.getSelectedItemPosition(), videoport5
							.getSelectedItemPosition());
				}
			}
		});

		cam_type6.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport6.getVisibility() == View.VISIBLE) {
						videoport6.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port6.setText("80");
						E_account6.setText("root");
					}
				} else {
					if (videoport6.getVisibility() != View.VISIBLE) {
						videoport6.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport6.setSelection(0);
							videoport6.setAdapter(class_adapter);
						} else {
							videoport6.setSelection(0);
							videoport6.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port6.setText("8888");
						E_account6.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check6.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type6.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL6.getText().toString(), E_port6.getText().toString(), E_account6.getText()
							.toString(), E_pwd6.getText().toString(), cam_type6.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL6.getText().toString(), E_port6.getText().toString(), E_account6.getText()
							.toString(), E_pwd6.getText().toString(), cam_type6.getSelectedItemPosition(), videoport6
							.getSelectedItemPosition());
				}
			}
		});

		cam_type7.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport7.getVisibility() == View.VISIBLE) {
						videoport7.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port7.setText("80");
						E_account7.setText("root");
					}
				} else {
					if (videoport7.getVisibility() != View.VISIBLE) {
						videoport7.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport7.setSelection(0);
							videoport7.setAdapter(class_adapter);
						} else {
							videoport7.setSelection(0);
							videoport7.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port7.setText("8888");
						E_account7.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check7.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type7.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL7.getText().toString(), E_port7.getText().toString(), E_account7.getText()
							.toString(), E_pwd7.getText().toString(), cam_type7.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL7.getText().toString(), E_port7.getText().toString(), E_account7.getText()
							.toString(), E_pwd7.getText().toString(), cam_type7.getSelectedItemPosition(), videoport7
							.getSelectedItemPosition());
				}
			}
		});

		cam_type8.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport8.getVisibility() == View.VISIBLE) {
						videoport8.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port8.setText("80");
						E_account8.setText("root");
					}
				} else {
					if (videoport8.getVisibility() != View.VISIBLE) {
						videoport8.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport8.setSelection(0);
							videoport8.setAdapter(class_adapter);
						} else {
							videoport8.setSelection(0);
							videoport8.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port8.setText("8888");
						E_account8.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check8.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type8.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL8.getText().toString(), E_port8.getText().toString(), E_account8.getText()
							.toString(), E_pwd8.getText().toString(), cam_type8.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL8.getText().toString(), E_port8.getText().toString(), E_account8.getText()
							.toString(), E_pwd8.getText().toString(), cam_type8.getSelectedItemPosition(), videoport8
							.getSelectedItemPosition());
				}
			}
		});

		cam_type9.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport9.getVisibility() == View.VISIBLE) {
						videoport9.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port9.setText("80");
						E_account9.setText("root");
					}
				} else {
					if (videoport9.getVisibility() != View.VISIBLE) {
						videoport9.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport9.setSelection(0);
							videoport9.setAdapter(class_adapter);
						} else {
							videoport9.setSelection(0);
							videoport9.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port9.setText("8888");
						E_account9.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check9.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type9.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL9.getText().toString(), E_port9.getText().toString(), E_account9.getText()
							.toString(), E_pwd9.getText().toString(), cam_type9.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL9.getText().toString(), E_port9.getText().toString(), E_account9.getText()
							.toString(), E_pwd9.getText().toString(), cam_type9.getSelectedItemPosition(), videoport9
							.getSelectedItemPosition());
				}
			}
		});

		cam_type10.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport10.getVisibility() == View.VISIBLE) {
						videoport10.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port10.setText("80");
						E_account10.setText("root");
					}
				} else {
					if (videoport10.getVisibility() != View.VISIBLE) {
						videoport10.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport10.setSelection(0);
							videoport10.setAdapter(class_adapter);
						} else {
							videoport10.setSelection(0);
							videoport10.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port10.setText("8888");
						E_account10.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check10.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type10.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL10.getText().toString(), E_port10.getText().toString(), E_account10.getText()
							.toString(), E_pwd10.getText().toString(), cam_type10.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL10.getText().toString(), E_port10.getText().toString(), E_account10.getText()
							.toString(), E_pwd10.getText().toString(), cam_type10.getSelectedItemPosition(),
							videoport10.getSelectedItemPosition());
				}
			}
		});

		cam_type11.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport11.getVisibility() == View.VISIBLE) {
						videoport11.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port11.setText("80");
						E_account11.setText("root");
					}
				} else {
					if (videoport11.getVisibility() != View.VISIBLE) {
						videoport11.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport11.setSelection(0);
							videoport11.setAdapter(class_adapter);
						} else {
							videoport11.setSelection(0);
							videoport11.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port11.setText("8888");
						E_account11.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check11.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type11.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL11.getText().toString(), E_port11.getText().toString(), E_account11.getText()
							.toString(), E_pwd11.getText().toString(), cam_type11.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL11.getText().toString(), E_port11.getText().toString(), E_account11.getText()
							.toString(), E_pwd11.getText().toString(), cam_type11.getSelectedItemPosition(),
							videoport11.getSelectedItemPosition());
				}
			}
		});

		cam_type12.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport12.getVisibility() == View.VISIBLE) {
						videoport12.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port12.setText("80");
						E_account12.setText("root");
					}
				} else {
					if (videoport12.getVisibility() != View.VISIBLE) {
						videoport12.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport12.setSelection(0);
							videoport12.setAdapter(class_adapter);
						} else {
							videoport12.setSelection(0);
							videoport12.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port12.setText("8888");
						E_account12.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check12.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type12.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL12.getText().toString(), E_port12.getText().toString(), E_account12.getText()
							.toString(), E_pwd12.getText().toString(), cam_type12.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL12.getText().toString(), E_port12.getText().toString(), E_account12.getText()
							.toString(), E_pwd12.getText().toString(), cam_type12.getSelectedItemPosition(),
							videoport12.getSelectedItemPosition());
				}
			}
		});

		cam_type13.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport13.getVisibility() == View.VISIBLE) {
						videoport13.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port13.setText("80");
						E_account13.setText("root");
					}
				} else {
					if (videoport13.getVisibility() != View.VISIBLE) {
						videoport13.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport13.setSelection(0);
							videoport13.setAdapter(class_adapter);
						} else {
							videoport13.setSelection(0);
							videoport13.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port13.setText("8888");
						E_account13.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check13.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type13.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL13.getText().toString(), E_port13.getText().toString(), E_account13.getText()
							.toString(), E_pwd3.getText().toString(), cam_type3.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL13.getText().toString(), E_port13.getText().toString(), E_account13.getText()
							.toString(), E_pwd13.getText().toString(), cam_type13.getSelectedItemPosition(), videoport3
							.getSelectedItemPosition());
				}
			}
		});

		cam_type14.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport14.getVisibility() == View.VISIBLE) {
						videoport14.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port14.setText("80");
						E_account14.setText("root");
					}
				} else {
					if (videoport14.getVisibility() != View.VISIBLE) {
						videoport14.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport14.setSelection(0);
							videoport14.setAdapter(class_adapter);
						} else {
							videoport14.setSelection(0);
							videoport14.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port14.setText("8888");
						E_account14.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check14.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type14.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL14.getText().toString(), E_port14.getText().toString(), E_account14.getText()
							.toString(), E_pwd14.getText().toString(), cam_type14.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL14.getText().toString(), E_port14.getText().toString(), E_account14.getText()
							.toString(), E_pwd14.getText().toString(), cam_type14.getSelectedItemPosition(),
							videoport14.getSelectedItemPosition());
				}
			}
		});

		cam_type15.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport15.getVisibility() == View.VISIBLE) {
						videoport15.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port15.setText("80");
						E_account15.setText("root");
					}
				} else {
					if (videoport15.getVisibility() != View.VISIBLE) {
						videoport15.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport15.setSelection(0);
							videoport15.setAdapter(class_adapter);
						} else {
							videoport15.setSelection(0);
							videoport15.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port15.setText("8888");
						E_account15.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check15.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type15.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL15.getText().toString(), E_port15.getText().toString(), E_account15.getText()
							.toString(), E_pwd15.getText().toString(), cam_type15.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL15.getText().toString(), E_port15.getText().toString(), E_account15.getText()
							.toString(), E_pwd15.getText().toString(), cam_type15.getSelectedItemPosition(),
							videoport15.getSelectedItemPosition());
				}
			}
		});

		cam_type16.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport16.getVisibility() == View.VISIBLE) {
						videoport16.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port16.setText("80");
						E_account16.setText("root");
					}
				} else {
					if (videoport16.getVisibility() != View.VISIBLE) {
						videoport16.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport16.setSelection(0);
							videoport16.setAdapter(class_adapter);
						} else {
							videoport16.setSelection(0);
							videoport16.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port16.setText("8888");
						E_account16.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check16.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type16.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL16.getText().toString(), E_port16.getText().toString(), E_account16.getText()
							.toString(), E_pwd16.getText().toString(), cam_type16.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL16.getText().toString(), E_port16.getText().toString(), E_account16.getText()
							.toString(), E_pwd16.getText().toString(), cam_type16.getSelectedItemPosition(),
							videoport16.getSelectedItemPosition());
				}
			}
		});

		cam_type17.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport17.getVisibility() == View.VISIBLE) {
						videoport17.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port17.setText("80");
						E_account17.setText("root");
					}
				} else {
					if (videoport17.getVisibility() != View.VISIBLE) {
						videoport17.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport17.setSelection(0);
							videoport17.setAdapter(class_adapter);
						} else {
							videoport17.setSelection(0);
							videoport17.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port17.setText("8888");
						E_account17.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check17.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type17.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL17.getText().toString(), E_port17.getText().toString(), E_account17.getText()
							.toString(), E_pwd17.getText().toString(), cam_type17.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL17.getText().toString(), E_port17.getText().toString(), E_account17.getText()
							.toString(), E_pwd17.getText().toString(), cam_type17.getSelectedItemPosition(),
							videoport17.getSelectedItemPosition());
				}
			}
		});

		cam_type18.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport18.getVisibility() == View.VISIBLE) {
						videoport18.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port18.setText("80");
						E_account18.setText("root");
					}
				} else {
					if (videoport18.getVisibility() != View.VISIBLE) {
						videoport18.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport18.setSelection(0);
							videoport18.setAdapter(class_adapter);
						} else {
							videoport18.setSelection(0);
							videoport18.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port18.setText("8888");
						E_account18.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check18.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type18.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL18.getText().toString(), E_port18.getText().toString(), E_account18.getText()
							.toString(), E_pwd8.getText().toString(), cam_type18.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL18.getText().toString(), E_port18.getText().toString(), E_account18.getText()
							.toString(), E_pwd18.getText().toString(), cam_type18.getSelectedItemPosition(),
							videoport18.getSelectedItemPosition());
				}
			}
		});

		cam_type19.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport19.getVisibility() == View.VISIBLE) {
						videoport19.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port19.setText("80");
						E_account19.setText("root");
					}
				} else {
					if (videoport19.getVisibility() != View.VISIBLE) {
						videoport19.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport19.setSelection(0);
							videoport19.setAdapter(class_adapter);
						} else {
							videoport19.setSelection(0);
							videoport19.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port19.setText("8888");
						E_account19.setText("admin");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});

		check19.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type19.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL19.getText().toString(), E_port19.getText().toString(), E_account19.getText()
							.toString(), E_pwd19.getText().toString(), cam_type19.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL19.getText().toString(), E_port19.getText().toString(), E_account19.getText()
							.toString(), E_pwd19.getText().toString(), cam_type19.getSelectedItemPosition(),
							videoport19.getSelectedItemPosition());
				}
			}
		});

		cam_type20.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (videoport20.getVisibility() == View.VISIBLE) {
						videoport20.setVisibility(View.INVISIBLE);
					}
					if (user_change) {
						E_port20.setText("80");
						E_account20.setText("root");
					}
				} else {
					if (videoport20.getVisibility() != View.VISIBLE) {
						videoport20.setVisibility(View.VISIBLE);
					}

					if (user_change) {
						if (position == SES_class) {
							videoport20.setSelection(0);
							videoport20.setAdapter(class_adapter);
						} else {
							videoport20.setSelection(0);
							videoport20.setAdapter(group_adapter);
						}
					}

					if (user_change) {
						E_port20.setText("8888");
						E_account20.setText("admin");
					}
				}
				user_change = true;
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
				user_change = true;
			}
		});

		check20.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (cam_type20.getSelectedItemPosition() < SES_class) {
					check_connect(E_URL20.getText().toString(), E_port20.getText().toString(), E_account20.getText()
							.toString(), E_pwd20.getText().toString(), cam_type20.getSelectedItemPosition(), 0);
				} else {
					check_connect(E_URL20.getText().toString(), E_port20.getText().toString(), E_account20.getText()
							.toString(), E_pwd20.getText().toString(), cam_type20.getSelectedItemPosition(),
							videoport20.getSelectedItemPosition());
				}
			}
		});
		content.removeAllViews();
		content.addView(cam_setview, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	}

	private void ethernet_content() {
		net_setview = getLayoutInflater().inflate(R.layout.ondemand_ethernet_setting, null);
		title_ative = (Switch) net_setview.findViewById(R.id.title_enable);
		time_ative = (Switch) net_setview.findViewById(R.id.time_enable);
		cleanlog = (Button) net_setview.findViewById(R.id.cleanlog);
		cleansetting = (Button) net_setview.findViewById(R.id.cleanpre);
		// account = (EditText) net_setview.findViewById(R.id.login_account);
		e_loginpwd = (EditText) net_setview.findViewById(R.id.login_pwd);
		tv_hotspot = (TextView) net_setview.findViewById(R.id.tv_hotspot);
		e_hotspot = (EditText) net_setview.findViewById(R.id.hotspot_num);
		circle_ative = (Switch) net_setview.findViewById(R.id.loop_enable);
		hotspot_layout = (LinearLayout) net_setview.findViewById(R.id.hotspot_layout);

		e_hotspot.setText(hotspot_ssid);
		title_ative.setChecked(title_on);
		time_ative.setChecked(time_on);
		circle_ative.setChecked(loop_on);

		net = (Spinner) net_setview.findViewById(R.id.net_spinner);
		e_ip1 = (EditText) net_setview.findViewById(R.id.static_ip1);
		e_ip2 = (EditText) net_setview.findViewById(R.id.static_ip2);
		e_ip3 = (EditText) net_setview.findViewById(R.id.static_ip3);
		e_ip4 = (EditText) net_setview.findViewById(R.id.static_ip4);

		e_gateway_ip1 = (EditText) net_setview.findViewById(R.id.gateway_ip1);
		e_gateway_ip2 = (EditText) net_setview.findViewById(R.id.gateway_ip2);
		e_gateway_ip3 = (EditText) net_setview.findViewById(R.id.gateway_ip3);
		e_gateway_ip4 = (EditText) net_setview.findViewById(R.id.gateway_ip4);

		e_mask_ip1 = (EditText) net_setview.findViewById(R.id.submask_ip1);
		e_mask_ip2 = (EditText) net_setview.findViewById(R.id.submask_ip2);
		e_mask_ip3 = (EditText) net_setview.findViewById(R.id.submask_ip3);
		e_mask_ip4 = (EditText) net_setview.findViewById(R.id.submask_ip4);

		tv_ssid = (TextView) net_setview.findViewById(R.id.tv_ssid);
		ssid = (Spinner) net_setview.findViewById(R.id.ssid_spinner);
		tv_wifipwd = (TextView) net_setview.findViewById(R.id.tv_wifipwd);
		e_wifi_pwd = (EditText) net_setview.findViewById(R.id.wifipwd);

		close = (RadioButton) net_setview.findViewById(R.id.r_close);
		general = (RadioButton) net_setview.findViewById(R.id.r_general);
		smooth = (RadioButton) net_setview.findViewById(R.id.r_smooth);
		loop_select = (Spinner) net_setview.findViewById(R.id.time_spinner);
		

		wifi_adapter = new WIFI_Adapter(getApplicationContext(), list);
		ssid.setAdapter(wifi_adapter);

		net_adapter = new ArrayAdapter<String>(this, R.layout.myspinner, connect_type);
		net.setAdapter(net_adapter);
		net.setSelection(net_type);

		time_adapter = new ArrayAdapter<String>(this, R.layout.myspinner, time);
		loop_select.setAdapter(time_adapter);
		if (loop_on) {
			if (loop_time == 20) {
				loop_select.setSelection(0);
			} else if (loop_time == 30) {
				loop_select.setSelection(1);
			} else if (loop_time == 40) {
				loop_select.setSelection(2);
			} else if (loop_time == 50) {
				loop_select.setSelection(3);
			} else if (loop_time == 60) {
				loop_select.setSelection(4);
			}
		} else {
			loop_select.setSelection(0);
			loop_select.setEnabled(false);
		}
		
		e_gateway_ip1.setText(gateway_ip[0]);
		e_gateway_ip2.setText(gateway_ip[1]);
		e_gateway_ip3.setText(gateway_ip[2]);
		e_gateway_ip4.setText(gateway_ip[3]);

		e_mask_ip1.setText(mask_ip[0]);
		e_mask_ip2.setText(mask_ip[1]);
		e_mask_ip3.setText(mask_ip[2]);
		e_mask_ip4.setText(mask_ip[3]);

		e_loginpwd.setText(login_pwd);

		if (net_type == wifi) {
			e_ip1.setText(wifi_ip[0]);
			e_ip2.setText(wifi_ip[1]);
			e_ip3.setText(wifi_ip[2]);
			e_ip4.setText(wifi_ip[3]);
		} else {
			e_ip1.setText(eth0_ip[0]);
			e_ip2.setText(eth0_ip[1]);
			e_ip3.setText(eth0_ip[2]);
			e_ip4.setText(eth0_ip[3]);
		}

		if (real_time == realtime_close) {
			close.setChecked(true);
		} else if (real_time == realtime_general) {
			general.setChecked(true);
		} else if (real_time == realtime_smooth) {
			smooth.setChecked(true);
		}

		ssid.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				wifi_ssid = (String) list.get(position).get("SSID");
				if (((String) list.get(position).get("PWD_TYPE")).indexOf("WEP") != -1) {
					pwd_type = WEP;
				} else if (((String) list.get(position).get("PWD_TYPE")).indexOf("WPA2") != -1) {
					pwd_type = WPA2;
				} else if (((String) list.get(position).get("PWD_TYPE")).indexOf("WPA") != -1) {
					pwd_type = WPA;
				} else {
					pwd_type = no_pwd;
				}

				if (pwd_type != no_pwd) {
					if (tv_wifipwd.getVisibility() == View.GONE) {
						tv_wifipwd.setVisibility(View.VISIBLE);
						e_wifi_pwd.setVisibility(View.VISIBLE);
						e_wifi_pwd.setText(wifi_pwd);
					}
				} else {
					if (tv_wifipwd.getVisibility() == View.VISIBLE) {
						tv_wifipwd.setVisibility(View.GONE);
						e_wifi_pwd.setVisibility(View.GONE);
						e_wifi_pwd.setText("");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		net.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				if (position == wifi) {
					tv_hotspot.setVisibility(View.GONE);
					hotspot_layout.setVisibility(View.GONE);
					net_type = wifi;
					e_ip1.setText(wifi_ip[0]);
					e_ip2.setText(wifi_ip[1]);
					e_ip3.setText(wifi_ip[2]);
					e_ip4.setText(wifi_ip[3]);

					if (tv_ssid.getVisibility() == View.GONE) {
						tv_ssid.setVisibility(View.VISIBLE);
						ssid.setVisibility(View.VISIBLE);
					}

					if (ApManager.isApOn(Ondemand_Setting_Activity.this)) {
						ApManager.configApState(Ondemand_Setting_Activity.this);
					}

					if (!wifi_magager.isWifiEnabled()) {
						wifi_magager.setWifiEnabled(true);
					}

					wifi_magager.startScan();

					registerReceiver(wifi_receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

				} else {
					tv_hotspot.setVisibility(View.VISIBLE);
					hotspot_layout.setVisibility(View.VISIBLE);
					net_type = eth0;
					if (tv_ssid.getVisibility() == View.VISIBLE) {
						tv_ssid.setVisibility(View.GONE);
						ssid.setVisibility(View.GONE);
						e_wifi_pwd.setVisibility(View.GONE);
					}
					e_ip1.setText(eth0_ip[0]);
					e_ip2.setText(eth0_ip[1]);
					e_ip3.setText(eth0_ip[2]);
					e_ip4.setText(eth0_ip[3]);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		loop_select.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				if (position == 0) {
					loop_time = 20;
				} else if (position == 1) {
					loop_time = 30;
				} else if (position == 2) {
					loop_time = 40;
				} else if (position == 3) {
					loop_time = 50;
				} else if (position == 4) {
					loop_time = 60;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		title_ative.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				title_on = isChecked;
			}
		});

		time_ative.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				time_on = isChecked;
			}
		});

		circle_ative.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				loop_on = isChecked;
				if (loop_on) {
					loop_select.setEnabled(true);
				} else {
					loop_select.setEnabled(false);
				}
			}
		});

		cleanlog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				MyLog.delFile();
				Toast.makeText(Ondemand_Setting_Activity.this, "Log clear!!!", Toast.LENGTH_SHORT).show();
			}
		});

		cleansetting.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				SharedPreferences.Editor editor = Camera_data.edit();
				editor.clear();
				editor.commit();
				// load();
				btn_type = 0;
				btn_setting.performClick();
				Toast.makeText(Ondemand_Setting_Activity.this, "setting clear!!!", Toast.LENGTH_SHORT).show();
			}
		});

		content.removeAllViews();
		content.addView(net_setview, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	}

	private void log_content() {
		log_setview = getLayoutInflater().inflate(R.layout.log, null);

		loadMoreView = getLayoutInflater().inflate(R.layout.load_more, null);
		loadMoreButton = (Button) loadMoreView.findViewById(R.id.loadMoreButton);

		loglist = (ListView) log_setview.findViewById(R.id.list);
		loglist.addFooterView(loadMoreView);
		items = new ArrayList<String>();
		initAdapter();
		loglist.setAdapter(adapter);

		loglist.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemcount, int totalItemCount) {
				visibleItemCount = visibleItemcount;
				visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
			}

			/**
			 * 滑动状态改变时被调用
			 */
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				int itemsLastIndex = adapter.getCount() - 1; // 数据集最后一项的索引
				int lastIndex = itemsLastIndex + 1; // 加上底部的loadMoreView项
				if (scrollState == OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == lastIndex) {
					// 如果是自动加载,可以在这里放置异步加载数据的代码
					Log.i("LOADMORE", "loading...");
				}
			}
		});

		content.removeAllViews();
		content.addView(log_setview, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	}

	/**
	 * 点击按钮事件
	 * 
	 * @param view
	 */
	public void loadMore(View view) {
		loadMoreButton.setText("loading..."); // 设置按钮文字loading
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {

				loadData();

				adapter.notifyDataSetChanged(); // 数据集变化后,通知adapter
				loglist.setSelection(visibleLastIndex - visibleItemCount + 1); // 设置选中项

				loadMoreButton.setText("load more"); // 恢复按钮文字
			}
		}, 2000);
	}

	private void check_connect(final String ip, final String port, final String username, final String password,
			final int cam_type, final int videoport) {
		new Thread() {
			public void run() {
				try {
					if (cam_type < SES_class) {
						icamcheck(cam_type, ip, port, username, password);
					} else if (cam_type == SES_class) {
						ses("Class", videoport + 1, cam_type, ip, port, username, password);
					} else if (cam_type == SES_group) {
						ses("Group", videoport + 1, cam_type, ip, port, username, password);
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Message msg;
					String obj = channel_set(cam_type, ip, port) + "Connect timeout";
					msg = _handler.obtainMessage(1, obj);
					_handler.sendMessage(msg);
				}

			}
		}.start();
	}

	public void icamcheck(int cam_type, String ip, String port, String username, String password) throws Exception {
		// 1. 獲取並設置url地址，一個字符串變量，一個URL對象
		String urlStr;
		if (cam_type == Camera_v2) {
			urlStr = "http://" + ip + "/ipcam/stream.cgi?nowprofileid=2&audiostream=1";
		} else {
			urlStr = "http://" + ip + "/ipcam/stream.cgi";
		}
		URL url = new URL(urlStr);
		// 2. 創建一個密碼證書，(UsernamePasswordCredentials類)
		UsernamePasswordCredentials upc = new UsernamePasswordCredentials(username, password);
		// 3. 設置認證範圍 (AuthScore類)
		String strRealm = "iCam1080";
		String strHost = url.getHost();
		int iPort = url.getPort();
		AuthScope as = new AuthScope(strHost, iPort, strRealm);
		// 4. 創建認證提供者(BasicCredentials類) ，基於as和upc
		BasicCredentialsProvider bcp = new BasicCredentialsProvider();
		bcp.setCredentials(as, upc);
		// 5. 創建Http客戶端(DefaultHttpClient類)
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 15000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 15000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		DefaultHttpClient client = new DefaultHttpClient(httpParameters);
		// 6. 為此客戶端設置認證提供者
		client.setCredentialsProvider(bcp);
		// 7. 創建一個get 方法(HttpGet類)，基於所訪問的url地址
		HttpGet hg = new HttpGet(urlStr);
		// 8. 執行get方法並返回 response
		HttpResponse hr = client.execute(hg);
		Log.v("http getdata", "hr code" + hr.getStatusLine().getStatusCode());
		// 9. 從response中取回數據，使用InputStreamReader讀取Response的Entity：
		if (hr.getStatusLine().getStatusCode() == 200) {
			Message msg;
			String obj = channel_set(cam_type, ip, port) + "Connect Sucess";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		} else if (hr.getStatusLine().getStatusCode() == 401) {
			Message msg;
			String obj = channel_set(cam_type, ip, port) + "Account or password not correct";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		} else if (hr.getStatusLine().getStatusCode() == 404) {
			Message msg;
			String obj = channel_set(cam_type, ip, port) + "Connect fail";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		}
	}

	public void ses(String GorC, int num, int cam_type, String ip, String port, String username, String password)
			throws Exception {
		String urlstr = "http://" + ip + ":" + port + "/sesstream.cgi?" + GorC + "=" + num;
		HttpGet mGet = new HttpGet(urlstr);
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 15000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 15000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

		DefaultHttpClient mHttpClient = new DefaultHttpClient(httpParameters);
		mGet.setHeader("Authorization",
				"Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP));
		HttpResponse hr = mHttpClient.execute(mGet);
		int res = hr.getStatusLine().getStatusCode();
		Log.v("http getdata", "hr code" + res);
		if (res == 200) {
			Message msg;
			String obj = channel_set(cam_type, ip, port) + "Connect Sucess";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		} else if (res == 401) {
			Message msg;
			String obj = channel_set(cam_type, ip, port) + "Account or password not correct";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		} else if (res == 404) {
			Message msg;
			String obj = channel_set(cam_type, ip, port) + "Connect fail";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		}
	}

	public String channel_set(int cam_type, String url, String port) {
		if (cam_type == SES_class) {
			return "IP: " + url + "  " + "Port: " + port + "  " + "Type: SES" + "\r\n";
		} else if (cam_type == SES_group) {
			return "IP: " + url + "  " + "Port: " + port + "  " + "Type: iFollow" + "\r\n";
		} else if (cam_type == Camera_v3) {
			return "IP: " + url + "  " + "Port: " + port + "  " + "Type: iCam v3" + "\r\n";
		} else if (cam_type == Camera_v2) {
			return "IP: " + url + "  " + "Port: " + port + "  " + "Type: iCam v2" + "\r\n";
		} else {
			return "";
		}
	}

	private Handler _handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			new AlertDialog.Builder(Ondemand_Setting_Activity.this).setTitle("Info")
					.setMessage((String) msg.obj).setPositiveButton("OK", null).show();
		}
	};

	private void loadData() {
		int count = adapter.getCount();
		if (count + 20 > items.size()) {
			for (int i = count; i < items.size() - 1; i++) {
				adapter.addItem(String.valueOf((i + 1)) + ". " + items.get(i + 1));
			}
			loadMoreButton.setVisibility(View.GONE);
		} else {
			for (int i = count; i < count + 20; i++) {
				adapter.addItem(String.valueOf((i + 1)) + ". " + items.get(i + 1));
			}
		}

	}

	private void initAdapter() {
		displaylog(this.items);
		ArrayList<String> items = new ArrayList<String>();
		if (this.items.size() < 20) {
			for (int i = 0; i < this.items.size(); i++) {
				items.add(String.valueOf((i + 1)) + ". " + (String) this.items.get(i));
			}
		} else {
			for (int i = 0; i < 20; i++) {
				items.add(String.valueOf((i + 1)) + ". " + this.items.get(i));
			}
		}

		adapter = new ListViewAdapter(this, items);
	}

	private void displaylog(ArrayList<String> items) {
		String path = Environment.getExternalStorageDirectory().getPath() + "/Android/data/tw.com.blueeyes.LiveBox/";
		File file = new File(path, "debug.txt");
		// StringBuilder text = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				items.add(line);
			}
		} catch (IOException e) {
			Toast.makeText(getApplicationContext(), "File empty!!", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
		Collections.reverse(items);
	}

	private class ListViewAdapter extends BaseAdapter {
		private List<String> items;
		private LayoutInflater inflater;

		public ListViewAdapter(Context context, List<String> items) {
			this.items = items;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			return items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				view = inflater.inflate(R.layout.list_item, null);
			}
			TextView text = (TextView) view.findViewById(R.id.log_text);
			text.setText(items.get(position));
			return view;
		}

		/**
		 * 添加列表项
		 * 
		 * @param item
		 */
		public void addItem(String item) {
			items.add(item);
		}
	}

	private class WIFI_Adapter extends BaseAdapter {
		private ArrayList<HashMap<String, Object>> items;
		private LayoutInflater inflater;

		public WIFI_Adapter(Context context, ArrayList<HashMap<String, Object>> items) {
			this.items = items;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			return items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				view = inflater.inflate(R.layout.myspinner, null);
			}
			TextView text = (TextView) view.findViewById(R.id.text1);
			text.setText((String) items.get(position).get("SSID"));

			return view;
		}
	}

	private BroadcastReceiver wifi_receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			results = wifi_magager.getScanResults();
			wifi_size = results.size();
			Log.v("ssid", "size " + wifi_size);

			list.clear();
			String[] tem;
			int getindex = -1;
			if (wifi_size >= 0) {
				for (int i = 0; i < wifi_size; i++) {
					HashMap<String, Object> item = new HashMap<String, Object>();
					tem = results.get(i).toString().split(",");
					String ssid_tem = tem[0].split(":")[1].trim();
					if (ssid_tem.indexOf(wifi_ssid) != -1) {
						getindex = i;
					}
					item.put("SSID", ssid_tem);
					item.put("PWD_TYPE", tem[2].split(":")[1].trim());
					Log.v("abc", tem[2].split(":")[1].trim());
					list.add(item);
				}

				unregisterReceiver(wifi_receiver);
				wifi_adapter.notifyDataSetChanged();
				if (getindex >= 0) {
					ssid.setSelection(getindex);
				} else {
					ssid.setSelection(0);
				}
			}
		}
	};

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Log.v("IR", " " + keyCode);
		return false;
		// return super.onKeyDown(keyCode, event);
	}
}
