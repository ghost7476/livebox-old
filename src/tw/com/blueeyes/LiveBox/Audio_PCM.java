package tw.com.blueeyes.LiveBox;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.preference.PreferenceManager;
import android.util.Log;

public class Audio_PCM {
	// implements OnPlaybackPositionUpdateListener {
	private AudioTrack audioTrack = null;
	int minBufferSize;
	List<byte[]> bytes = new ArrayList<byte[]>();
	int count = 0;
	int countsize = 0, offset = 0;
	boolean getdata = false;
	byte pcm[] = null;
	String TAG = "pcm";
	long startTime = 0;
	long endTime = 0;
	long totTime = 0;
	byte[] audio;
	byte[] tem;
	boolean start = false;
	int getdata_count = 0;
	int count_frame = 0;
	SharedPreferences Camera_data;
	Context cntex;
	final int realtime_close = 0x0a;
	final int realtime_general = 0x0b;
	final int realtime_smooth = 0x0c;

	public Audio_PCM(Context context) {
		cntex = context;
		initAudioTrack();
	}

	public void write(final byte buffer[]) {
		count_frame++;
		audioTrack.write(buffer, 0, buffer.length);
		// getdata_count++;
		// if (getdata_count >= 10000) {
		// audioTrack.flush();
		// getdata_count = 0;
		// }
		//
		// tem = buffer;
		// getdata = true;
	}

	// Thread audio_w = new Thread() {
	// public void run() {
	// count = 0;
	// while (true) {
	// try {
	// if (getdata) {
	// audio = tem;
	// getdata = false;
	// count_frame++;
	// audioTrack.write(audio, 0, audio.length);
	// // Log.v(TAG, "write finish");
	// count = 0;
	// } else {
	// // Log.v(TAG, "wait data");
	// // if (count > 10000) {
	// // Thread.sleep(100);
	// // count = 0;
	// // } else {
	// // Thread.sleep(2);
	// // count++;
	// // }
	// Thread.sleep(5);
	//
	// if (!LiveBox_Activity.run) {
	// break;
	// }
	// }
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	//
	// audioTrack.pause();
	// audioTrack.stop();
	// audioTrack.release();
	//
	// }
	// };

	public void flush() {
		audioTrack.flush();
	}

	protected void initAudioTrack() {
		// Camera_data = PreferenceManager.getDefaultSharedPreferences(cntex);

		minBufferSize = AudioTrack.getMinBufferSize(8000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
		audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, 8000, AudioFormat.CHANNEL_OUT_MONO,
				AudioFormat.ENCODING_PCM_16BIT, minBufferSize * 3, AudioTrack.MODE_STREAM);

		audioTrack.setStereoVolume(1.0f, 1.0f);// 设置当前音量大小
		audioTrack.play();
		// audio_w.start();
	}

	public void initAudioTrack(int samplerate, int channelConfig) {
//		if (realtime_close == Camera_data.getInt("real_time", realtime_smooth)) {
//		 samplerate = (samplerate/1000)*1024;
//		 }
		minBufferSize = AudioTrack.getMinBufferSize(samplerate, channelConfig, AudioFormat.ENCODING_PCM_16BIT);
		audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, samplerate, channelConfig,
				AudioFormat.ENCODING_PCM_16BIT, minBufferSize * 2, AudioTrack.MODE_STREAM);
		audioTrack.setStereoVolume(1.0f, 1.0f);// 设置当前音量大小
		audioTrack.play();
		// audio_w.start();
	}

	public void clsoe() {
		audioTrack.pause();
		audioTrack.stop();
		audioTrack.release();
	}

}
