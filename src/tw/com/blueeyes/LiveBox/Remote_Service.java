package tw.com.blueeyes.LiveBox;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.media.AudioManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

public class Remote_Service extends Service {
	public ServerSocket serverSocket;
	public Socket client;
	static boolean service_start = false;
	static boolean debug = false;
//	static boolean debug = true;
	InputStream fin;
	PrintStream opt;
	Context ctx;
	SharedPreferences Camera_data;
	Boolean RESET_CAM = false;
	Boolean RESET_NET = false;
	Boolean run = true;

	int Camera_v2 = 0x00;
	int Camera_v3 = 0x01;
	int SES_class = 0x02;
	int SES_group = 0x03;
	final int wifi = 0x00;
	final int eth0 = 0x01;
	final int no_pwd = 0x00;
	final int WEP = 0xa0;
	final int WPA2 = 0xb0;
	final int WPA = 0xc0;
	public Handler handler = new Handler();
	Thread socket;
	String ip;
	Boolean icam1, icam2, icam3, icam4, icam5, icam6, icam7, icam8, icam9, icam10;
	Boolean icam11, icam12, icam13, icam14, icam15, icam16, icam17, icam18, icam19, icam20;
	String usbpath_old = "/mnt/usb_storage/USB_DISK0/udisk0/";
	String usbpath = "/mnt/usb_storage/USB_DISK0/";
	File usbfilePath_old = new File(usbpath_old);
	File usbfilePath = new File(usbpath);
	String apkName = "LiveBox.apk";
	String dataName = "Livebox.bin";
	String initName = "Livebox_init";

	String debugName = "debug";
	String apkPath, dataPath, debugPath, initPath;
	int changetype = 0;
	AudioManager audioManager;
	static int count = 0;
	final int realtime_close = 0x0a;
	final int realtime_general = 0x0b;
	final int realtime_smooth = 0x0c;

	@Override
	public IBinder onBind(Intent intent) {
		return stub;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.v("service", "service start");
		ctx = getApplicationContext();
		Camera_data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		service_start = true;
		socket = new Thread(socket_server);
		socket.start();
		time_thread.start();
		registerUsbStateReceiver();
	}

	Thread time_thread = new Thread() {
		public void run() {

			while (run) {
				try {
					Thread.sleep(15000);

//					if (count % 3000 == 0) {
//						clear_media_bug();
//						count = 0;
//					}
//					count++;

					// if (count * 15 >= 60 * 60 * 24) {
					// reseteth0();
					// count = 0;
					// }

					// if (serverSocket.isClosed()) {
					// socket = new Thread(socket_server);
					// socket.start();
					// }
					if (Camera_data.getInt("net_type", eth0) == eth0) {
						Process proc = Runtime.getRuntime().exec("su");
						DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
						DataInputStream dis = new DataInputStream(proc.getInputStream());
						// Log.v("eth0","check ip");
						opt.writeBytes("ifconfig eth0\n");
//						opt.writeBytes("ip route show\n");
						opt.writeBytes("exit\n");
						opt.flush();

						String line = "getdata";
						while ((line = dis.readLine()) != null) {
//							Log.v("socket", line);
							if (line.indexOf("eth0: ip " + Camera_data.getString("eth0", "192.168.1.51")) == -1) {
								// Log.v("socket", "get ip");
								setip();
							}
						}
						if (line == null) {
							setip();
						}
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.e("test", e.toString());
				}
			}
		}
	};

	public void clear_media_bug() throws IOException {
		Process proc = Runtime.getRuntime().exec("su");
		// Runtime.getRuntime().exec("pm clear com.android.providers.media");
		DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
		opt = new DataOutputStream(proc.getOutputStream());
		opt.writeBytes("pm clear com.android.providers.media\n");
		opt.writeBytes("exit\n");
		opt.flush();
		Log.v("test", "clean process meida");

		// PackageManager pm = getPackageManager();
		// // Get all methods on the PackageManager
		// Method[] methods = pm.getClass().getDeclaredMethods();
		// for (Method m : methods) {
		// if (m.getName().equals("freeStorage")) {
		// // Found the method I want to use
		// try {
		// long desiredFreeStorage = 8 * 1024 * 1024 * 1024; // Request for 8GB
		// of free space
		// m.invoke(pm, desiredFreeStorage , null);
		// } catch (Exception e) {
		// // Method invocation failed. Could be a permission problem
		// Log.v("test", "clean cache fail");
		// }
		// break;
		// }
		// }
	}

	public void reseteth0() throws IOException {
		if (Camera_data.getInt("net_type", eth0) == eth0) {
			Process proc = Runtime.getRuntime().exec("su");
			DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
			opt.writeBytes("ifconfig eth0 down\n");
			opt.writeBytes("exit\n");
			opt.flush();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			opt.writeBytes("ifconfig eth0 up\n");
			opt.writeBytes("ifconfig eth0 " + Camera_data.getString("eth0", "192.168.1.51") + " netmask "
					+ Camera_data.getString("mask", "255.255.255.0") + "\n");
			opt.writeBytes("route add default gw " + Camera_data.getString("geteway", "192.168.1.1") + " dev eth0\n");
			opt.writeBytes("exit\n");
			opt.flush();
		}
	}

	public void setip() throws IOException {
		Process proc = Runtime.getRuntime().exec("su");
		DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
		opt = new DataOutputStream(proc.getOutputStream());
		opt.writeBytes("ifconfig eth0 " + Camera_data.getString("eth0", "192.168.1.51") + " netmask "
				+ Camera_data.getString("mask", "255.255.255.0") + "\n");
		opt.writeBytes("route add default gw " + Camera_data.getString("geteway", "192.168.1.1") + " dev eth0\n");
		opt.writeBytes("exit\n");
		opt.flush();
	}

	/**
	 * Web Server
	 **/
	public Runnable socket_server = new Runnable() {
		@Override
		public void run() {
			// TODO Auto-generated method stub

			Log.w("Socket", "Socket_Server 啟動");
			// deuge server用
			MyLog.i("Socket", "Remoter service start");

			try {

				// 建立serverSocket
				// MyLog.i("Socket", "Socket_Server creat");
				Log.w("Socket", "Socket_Server 建立");
				serverSocket = new ServerSocket(8080);
				serverSocket.setSoTimeout(0); // 0為notimeout read方法的超時時間
				serverSocket.setPerformancePreferences(100, 100, 1); // 表示重要性 //
																		// (表示最少時間建立連線,表示最小延遲,表示最高帶寬)
				while (run) {
					Log.w("Socket", "Socket_Server 接受連線");

					// 接收連線
					client = serverSocket.accept();
					client.setPerformancePreferences(10, 100, 1); // 表示重要性
																	// (表示最少時間建立連線,表示最小延遲,表示最高帶寬)
					client.setSoTimeout(0);
					client.setTrafficClass(0x1C);
					// Log.v("socket" ,"" + client.getRemoteSocketAddress());
					// ip = "0.0.0.0";
					ip = client.getInetAddress().getHostAddress();
					Log.i("Socket", "client 接受成功::" + ip);

					// serverIP = client.getLocalAddress().getHostAddress();

					try {
						opt = new PrintStream(client.getOutputStream());
						fin = client.getInputStream();
						readStream(fin, opt);
					} catch (Exception e) {
						Log.w("Network", "連線關閉--" + e.toString());
					}
				}
			} catch (Exception e) {
				Log.w("Network", e.toString());
				if(LiveBox_Activity.run || Ondemand_LiveBox_Activity.run) {
					 Thread socket = new Thread(socket_server);
					 socket.start();
				}
				
			}
		}
	};

	/**
	 * WEB
	 * 
	 * @throws InterruptedException
	 * @throws RemoteException
	 */
	public void readStream(InputStream fin, PrintStream opt) throws IOException, InterruptedException, RemoteException {
		ByteArrayOutputStream dout_temp = new ByteArrayOutputStream();
		// dout = new FileOutputStream(sdcardpath +"/filename");
		int buffer_count = 1024 * 1024;
		byte[] buffer = new byte[buffer_count];
		byte[] buffer_temp = new byte[buffer_count];
		byte[] buffer_tem2;
		char temp_char1 = 0, temp_char2 = 0, temp_char3 = 0, temp_char4 = 0;
		String post_str = "";
		int i = 0;
		boolean account = false;
		int length = 0;
		int readCount = 0;
		int readtem = 0;
		boolean data_end = false;
		try {
			while ((length = fin.read(buffer)) != -1) {
				dout_temp.write(buffer, 0, length);
				// readtem += fin.read(buffer, readCount, buffer_count -
				// readCount);
				// dout_temp.write(buffer, 0, readtem);
				buffer_temp = dout_temp.toByteArray();
				// Log.v("remoter", "buffer_temp " + new String(buffer_temp));
				if (new String(buffer_temp).indexOf("LIVEBOX_SEND_") == -1) {
					break;
				} else {
					if (new String(buffer_temp).indexOf(URLEncoder.encode("}", "utf8")) != -1) {
						break;
					} else {
						// Log.v("remoter", "no livebox end");
					}
				}
				readCount = readtem;
			}
			// Log.v("remoter", "break");
			// while ((length = fin.read(buffer)) != -1) {
			// dout_temp.write(buffer, 0, length);
			// buffer_temp = dout_temp.toByteArray();

			// Log.v("PWD", "buffer_temp + " + new String(buffer_temp));
			while (true) // 分析使否為我們要的檔案
			{
				if (temp_char1 == '\r' && temp_char2 == '\n') {
					for (int j = 0; j <= i - 1; j++) {
						post_str += "" + (char) buffer_temp[j];
						// System.out.println(post_str);
						// Log.i(TAG,post_str);
					}
					break;
				} else {
					i++;
					temp_char1 = (char) buffer_temp[i];
					temp_char2 = (char) buffer_temp[i + 1];
				}
				// Log.d("Post :", "" + post_str);
			}
			Log.i("Socket", "表單 :" + post_str); // 目前接收得表單

			// 認證登入帳密 admin:admin_password //Authorization: Basic
			try {
				int Start = 0;
				String usernamePWD = "";
				while (true) {
					temp_char1 = (char) buffer_temp[i];
					temp_char2 = (char) buffer_temp[i + 1];
					temp_char3 = (char) buffer_temp[i + 2];
					temp_char4 = (char) buffer_temp[i + 3];
					char temp_char5 = (char) buffer_temp[i + 4];
					i++;
					// System.out.println("Basic="+(char)buffer_temp[i]);
					if (temp_char1 == 'B' && temp_char2 == 'a' && temp_char3 == 's' && temp_char4 == 'i'
							&& temp_char5 == 'c') {
						Start = i + 5;
						// Log.e("Start :",
						// Start+","+(char)buffer_temp[Start]); //debug
						break;
					}
				}
				while (true) {
					temp_char1 = (char) buffer_temp[i];
					temp_char2 = (char) buffer_temp[i + 1];
					temp_char3 = (char) buffer_temp[i + 2];
					i++;
					// System.out.println("PWD="+(char)buffer_temp[i]);
					if (temp_char1 == '\r' && temp_char2 == '\n') {
						for (int j = Start; j <= i - 1; j++) {
							usernamePWD += "" + (char) buffer_temp[j];
						}
						Log.e("Socket", "usernamePWD :" + usernamePWD);
						// //debug
						break;
					}
				}
				String decodeString = new String(Base64.decode(usernamePWD.getBytes(), Base64.DEFAULT));
				Log.e("Socket", "decodeString :" + decodeString);
				// Log.v("Socket", "test " + "admin" + ":" +
				// Camera_data.getString("login_pwd", "27507522"));
				String[] user_pwd = decodeString.split(":");
				String admindb = user_pwd[0];
				String pwd = user_pwd[1];
				Log.e("Socket", admindb + ":" + pwd);

				if (admindb.equals("admin") && pwd.equals(Camera_data.getString("login_pwd", "9999"))) {
					account = true;

				} else {
					String LiveBox = "LiveBox";

					opt.print("HTTP/1.1 401 Unauthorized\r\n");
					opt.print("Content-Type: text/html; charset=UTF-8\r\n");
					opt.print("WWW-Authenticate: Basic realm=\"" + LiveBox + "\"\r\n\r\n");
					opt.print("Account or Password not correct");
					fin.close();
					client.close();
				}

			} catch (ArrayIndexOutOfBoundsException e) // 會出現錯誤 表示沒有認證
														// (找不到Authorization:
														// Basic)
			{
				String LiveBox = "LiveBox";
				opt.print("HTTP/1.1 401 Unauthorized\r\n");
				opt.print("Content-Type: text/html; charset=UTF-8\r\n");
				opt.print("WWW-Authenticate: Basic realm=\"" + LiveBox + "\"\r\n\r\n");

				fin.close();
				client.close();
			}

			if (account) {
				String str = post_str.replaceAll("GET (.*) HTTP/1.[0-9]", "$1");
				str = str.replaceAll("/vb.cgi\\?layoutmode=", "");
				Log.v("Socket", "cam " + str);

//				 if (str.equals("ondemand")) {
//				 stub.setondemand();
//				 } else if (str.equals("default")) {
//				 stub.restoredefault();
//				 }

				if (!Camera_data.getBoolean("OnDemand", false)) {
					if (str.equals("5")) {
						stub.sendOK();
					} else if (str.equals("9")) {
						stub.sendSetting();
					} else if (str.equals("10")) {
						Log.v("Socket", "i " + i);
						while (true) // 分析使否為我們要的檔案
						{
							if (temp_char1 == '\r' && temp_char2 == '\n' && temp_char3 == '\r' && temp_char4 == '\n') {
								post_str = "";
								for (int j = i + 4; j < buffer_temp.length; j++) {
									post_str += "" + (char) buffer_temp[j];
								}
								break;
							} else {
								if (i < buffer_temp.length - 4) {
									i++;
									temp_char1 = (char) buffer_temp[i];
									temp_char2 = (char) buffer_temp[i + 1];
									temp_char3 = (char) buffer_temp[i + 2];
									temp_char4 = (char) buffer_temp[i + 3];
								} else {
									break;
								}
							}
						}
						if (post_str.indexOf("LIVEBOX_SEND_") != -1) {
							post_str = URLDecoder.decode(post_str, "utf8");
							if (getjsondata(post_str.replace("LIVEBOX_SEND_", ""), 0)) {
								stub.sendOK();
							} else {
								stub.sendFail();
							}
						}
					} else {
						// Log.v("test", "geticam cmd");
						if (Camera_data.getInt("activity", 0x1a) == 0x1a) {
							Message msg = new Message();
							msg.arg1 = 0x01;
							msg.obj = str;
							LiveBox_Activity.Change_iCam.sendMessage(msg);
						} else {
							Log.v("test", "sendicam");
							sendicam();
						}
					}
				} else {
					if (str.equals("1001")) {
						stub.check_ondemand();
					} else if (str.equals("1002")) {
						stub.send_OndemandSetting();
					} else if (str.equals("1003")) {
						Log.v("test", "get 1003");
						while (true) // 分析使否為我們要的檔案
						{
							if (temp_char1 == '\r' && temp_char2 == '\n' && temp_char3 == '\r' && temp_char4 == '\n') {
								post_str = "";
								for (int j = i + 4; j < buffer_temp.length; j++) {
									post_str += "" + (char) buffer_temp[j];
								}
								break;
							} else {
								if (i < buffer_temp.length - 4) {
									i++;
									temp_char1 = (char) buffer_temp[i];
									temp_char2 = (char) buffer_temp[i + 1];
									temp_char3 = (char) buffer_temp[i + 2];
									temp_char4 = (char) buffer_temp[i + 3];
								} else {
									break;
								}
							}
						}
						if (post_str.indexOf("ONDEMAND_SEND_") != -1) {
							post_str = URLDecoder.decode(post_str, "utf8");
							if (getondemand_jsondata(post_str.replace("ONDEMAND_SEND_", ""))) {
								stub.sendOK();
							} else {
								stub.sendFail();
							}
						}
					} else {
						// Log.v("test", "geticam cmd");
						if (Camera_data.getInt("activity", 0x1a) == 0x1a) {
							Message msg = new Message();
							msg.arg1 = 0x01;
							msg.obj = str;
							if (Camera_data.getBoolean("OnDemand", false)) {
								Ondemand_LiveBox_Activity.Change_iCam.sendMessage(msg);
							} else {
								LiveBox_Activity.Change_iCam.sendMessage(msg);
							}

						} else {
							Log.v("test", "sendicam");
//							sendicam();
						}
					}
				}
			}
//			client.close();
			// }
		} catch (Exception e) {
			stub.sendFail();
//			fin.close();
			client.close();
			Log.v("Socket", e.toString());
		}
	}

	public void sendicam() {
		String show_type = "LIVEBOX_";
		if (Camera_data.getBoolean("Camera1", false)) {
			if (Camera_data.getInt("show", 1) == 1) {
				show_type = show_type + 1 + ":";
			} else {
				show_type = show_type + 0 + ":";
			}
		} else {
			if (Camera_data.getInt("show", 1) == 1) {
				show_type = show_type + 3 + ":";
			} else {
				show_type = show_type + 2 + ":";
			}
		}

		if (Camera_data.getBoolean("Camera2", false)) {
			if (Camera_data.getInt("show", 1) == 2) {
				show_type = show_type + 1 + ":";
			} else {
				show_type = show_type + 0 + ":";
			}
		} else {
			if (Camera_data.getInt("show", 1) == 2) {
				show_type = show_type + 3 + ":";
			} else {
				show_type = show_type + 2 + ":";
			}
		}

		if (Camera_data.getBoolean("Camera3", false)) {
			if (Camera_data.getInt("show", 1) == 3) {
				show_type = show_type + 1 + ":";
			} else {
				show_type = show_type + 0 + ":";
			}
		} else {
			if (Camera_data.getInt("show", 1) == 3) {
				show_type = show_type + 3 + ":";
			} else {
				show_type = show_type + 2 + ":";
			}
		}

		// if (Camera_data.getBoolean("Camera4", false)) {
		// if (Camera_data.getInt("show", 1) == 4) {
		// show_type = show_type + 1;
		// } else {
		// show_type = show_type + 0;
		// }
		// } else {
		// if (Camera_data.getInt("show", 1) == 4) {
		// show_type = show_type + 3 + ":";
		// } else {
		// show_type = show_type + 2 + ":";
		// }
		// }

		if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
			show_type = show_type + 0;
		} else {
			show_type = show_type + 1;
		}

		try {
			Log.v("test", show_type);
			stub.sendTYPE(show_type);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean getondemand_jsondata(String cmd) {
		try {
			icam1 = false;
			icam2 = false;
			icam3 = false;
			icam4 = false;
			icam5 = false;
			icam6 = false;
			icam7 = false;
			icam8 = false;
			icam9 = false;
			icam10 = false;
			icam11 = false;
			icam12 = false;
			icam13 = false;
			icam14 = false;
			icam15 = false;
			icam16 = false;
			icam17 = false;
			icam18 = false;
			icam19 = false;
			icam20 = false;
			RESET_CAM = false;
			RESET_NET = false;
			Log.v("test", cmd);
			JSONObject json = new JSONObject(cmd);
			Camera_data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			SharedPreferences.Editor editor = Camera_data.edit();
			if (Camera_data.getBoolean("Camera1", false) != json.getBoolean("Camera1")) {
				editor.putBoolean("Camera1", json.getBoolean("Camera1"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (!Camera_data.getString("Camera1_URL", "192.168.1.171").equals(json.getString("Camera1_URL"))) {
				editor.putString("Camera1_URL", json.getString("Camera1_URL"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (!Camera_data.getString("Camera1_Label", "CH1").equals(json.getString("Camera1_Label"))) {
				editor.putString("Camera1_Label", json.getString("Camera1_Label"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (Camera_data.getInt("Camera1_Port", 80) != json.getInt("Camera1_Port")) {
				editor.putInt("Camera1_Port", json.getInt("Camera1_Port"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (!Camera_data.getString("Camera1_Account", "root").equals(json.getString("Camera1_Account"))) {
				editor.putString("Camera1_Account", json.getString("Camera1_Account"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (!Camera_data.getString("Camera1_Pwd", "").equals(json.getString("Camera1_Pwd"))) {
				editor.putString("Camera1_Pwd", json.getString("Camera1_Pwd"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (Camera_data.getInt("Camera1_type", Camera_v3) != json.getInt("Camera1_type")) {
				editor.putInt("Camera1_type", json.getInt("Camera1_type"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (Camera_data.getInt("classorgroup1", 0) != json.getInt("classorgroup1")) {
				editor.putInt("classorgroup1", json.getInt("classorgroup1"));
				RESET_CAM = true;
				icam1 = true;
			}

			if (Camera_data.getBoolean("Camera2", false) != json.getBoolean("Camera2")) {
				editor.putBoolean("Camera2", json.getBoolean("Camera2"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (!Camera_data.getString("Camera2_URL", "192.168.1.172").equals(json.getString("Camera2_URL"))) {
				editor.putString("Camera2_URL", json.getString("Camera2_URL"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (!Camera_data.getString("Camera2_Label", "CH1").equals(json.getString("Camera2_Label"))) {
				editor.putString("Camera2_Label", json.getString("Camera2_Label"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (Camera_data.getInt("Camera2_Port", 80) != json.getInt("Camera2_Port")) {
				editor.putInt("Camera2_Port", json.getInt("Camera2_Port"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (!Camera_data.getString("Camera2_Account", "root").equals(json.getString("Camera2_Account"))) {
				editor.putString("Camera2_Account", json.getString("Camera2_Account"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (!Camera_data.getString("Camera2_Pwd", "").equals(json.getString("Camera2_Pwd"))) {
				editor.putString("Camera2_Pwd", json.getString("Camera2_Pwd"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (Camera_data.getInt("Camera2_type", Camera_v3) != json.getInt("Camera2_type")) {
				editor.putInt("Camera2_type", json.getInt("Camera2_type"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (Camera_data.getInt("classorgroup2", 0) != json.getInt("classorgroup2")) {
				editor.putInt("classorgroup2", json.getInt("classorgroup2"));
				RESET_CAM = true;
				icam2 = true;
			}

			if (Camera_data.getBoolean("Camera3", false) != json.getBoolean("Camera3")) {
				editor.putBoolean("Camera3", json.getBoolean("Camera3"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (!Camera_data.getString("Camera3_URL", "192.168.1.173").equals(json.getString("Camera3_URL"))) {
				editor.putString("Camera3_URL", json.getString("Camera3_URL"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (!Camera_data.getString("Camera3_Label", "CH3").equals(json.getString("Camera3_Label"))) {
				editor.putString("Camera3_Label", json.getString("Camera3_Label"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (Camera_data.getInt("Camera3_Port", 80) != json.getInt("Camera3_Port")) {
				editor.putInt("Camera3_Port", json.getInt("Camera3_Port"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (!Camera_data.getString("Camera3_Account", "root").equals(json.getString("Camera3_Account"))) {
				editor.putString("Camera3_Account", json.getString("Camera3_Account"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (!Camera_data.getString("Camera3_Pwd", "").equals(json.getString("Camera3_Pwd"))) {
				editor.putString("Camera3_Pwd", json.getString("Camera3_Pwd"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (Camera_data.getInt("Camera3_type", Camera_v3) != json.getInt("Camera3_type")) {
				editor.putInt("Camera3_type", json.getInt("Camera3_type"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (Camera_data.getInt("classorgroup3", 0) != json.getInt("classorgroup3")) {
				editor.putInt("classorgroup3", json.getInt("classorgroup3"));
				RESET_CAM = true;
				icam3 = true;
			}

			if (Camera_data.getBoolean("Camera4", false) != json.getBoolean("Camera4")) {
				editor.putBoolean("Camera4", json.getBoolean("Camera4"));
				RESET_CAM = true;
				icam4 = true;
			}
			if (!Camera_data.getString("Camera4_URL", "192.168.1.174").equals(json.getString("Camera4_URL"))) {
				editor.putString("Camera4_URL", json.getString("Camera4_URL"));
				RESET_CAM = true;
				icam4 = true;
			}
			if (!Camera_data.getString("Camera4_Label", "CH4").equals(json.getString("Camera4_Label"))) {
				editor.putString("Camera4_Label", json.getString("Camera4_Label"));
				RESET_CAM = true;
				icam4 = true;
			}
			if (Camera_data.getInt("Camera4_Port", 80) != json.getInt("Camera4_Port")) {
				editor.putInt("Camera4_Port", json.getInt("Camera4_Port"));
				RESET_CAM = true;
				icam4 = true;
			}
			if (!Camera_data.getString("Camera4_Account", "root").equals(json.getString("Camera4_Account"))) {
				editor.putString("Camera4_Account", json.getString("Camera4_Account"));
				RESET_CAM = true;
				icam4 = true;
			}
			if (!Camera_data.getString("Camera4_Pwd", "").equals(json.getString("Camera4_Pwd"))) {
				editor.putString("Camera4_Pwd", json.getString("Camera4_Pwd"));
				RESET_CAM = true;
				icam4 = true;
			}
			if (Camera_data.getInt("Camera4_type", Camera_v3) != json.getInt("Camera4_type")) {
				editor.putInt("Camera4_type", json.getInt("Camera4_type"));
				RESET_CAM = true;
				icam4 = true;
			}
			if (Camera_data.getInt("classorgroup4", 0) != json.getInt("classorgroup4")) {
				editor.putInt("classorgroup4", json.getInt("classorgroup4"));
				RESET_CAM = true;
				icam4 = true;
			}

			if (Camera_data.getBoolean("Camera5", false) != json.getBoolean("Camera5")) {
				editor.putBoolean("Camera5", json.getBoolean("Camera5"));
				RESET_CAM = true;
				icam5 = true;
			}
			if (!Camera_data.getString("Camera5_URL", "192.168.1.175").equals(json.getString("Camera5_URL"))) {
				editor.putString("Camera5_URL", json.getString("Camera5_URL"));
				RESET_CAM = true;
				icam5 = true;
			}
			if (!Camera_data.getString("Camera5_Label", "CH5").equals(json.getString("Camera5_Label"))) {
				editor.putString("Camera5_Label", json.getString("Camera5_Label"));
				RESET_CAM = true;
				icam5 = true;
			}
			if (Camera_data.getInt("Camera5_Port", 80) != json.getInt("Camera5_Port")) {
				editor.putInt("Camera5_Port", json.getInt("Camera5_Port"));
				RESET_CAM = true;
				icam5 = true;
			}
			if (!Camera_data.getString("Camera5_Account", "root").equals(json.getString("Camera5_Account"))) {
				editor.putString("Camera5_Account", json.getString("Camera5_Account"));
				RESET_CAM = true;
				icam5 = true;
			}
			if (!Camera_data.getString("Camera5_Pwd", "").equals(json.getString("Camera5_Pwd"))) {
				editor.putString("Camera5_Pwd", json.getString("Camera5_Pwd"));
				RESET_CAM = true;
				icam5 = true;
			}
			if (Camera_data.getInt("Camera5_type", Camera_v3) != json.getInt("Camera5_type")) {
				editor.putInt("Camera5_type", json.getInt("Camera5_type"));
				RESET_CAM = true;
				icam5 = true;
			}
			if (Camera_data.getInt("classorgroup5", 0) != json.getInt("classorgroup5")) {
				editor.putInt("classorgroup5", json.getInt("classorgroup5"));
				RESET_CAM = true;
				icam5 = true;
			}

			if (Camera_data.getBoolean("Camera6", false) != json.getBoolean("Camera6")) {
				editor.putBoolean("Camera6", json.getBoolean("Camera6"));
				RESET_CAM = true;
				icam6 = true;
			}
			if (!Camera_data.getString("Camera6_URL", "192.168.1.176").equals(json.getString("Camera6_URL"))) {
				editor.putString("Camera6_URL", json.getString("Camera6_URL"));
				RESET_CAM = true;
				icam6 = true;
			}
			if (!Camera_data.getString("Camera6_Label", "CH6").equals(json.getString("Camera6_Label"))) {
				editor.putString("Camera6_Label", json.getString("Camera6_Label"));
				RESET_CAM = true;
				icam6 = true;
			}
			if (Camera_data.getInt("Camera6_Port", 80) != json.getInt("Camera6_Port")) {
				editor.putInt("Camera6_Port", json.getInt("Camera6_Port"));
				RESET_CAM = true;
				icam6 = true;
			}
			if (!Camera_data.getString("Camera6_Account", "root").equals(json.getString("Camera6_Account"))) {
				editor.putString("Camera6_Account", json.getString("Camera6_Account"));
				RESET_CAM = true;
				icam6 = true;
			}
			if (!Camera_data.getString("Camera6_Pwd", "").equals(json.getString("Camera6_Pwd"))) {
				editor.putString("Camera6_Pwd", json.getString("Camera6_Pwd"));
				RESET_CAM = true;
				icam6 = true;
			}
			if (Camera_data.getInt("Camera6_type", Camera_v3) != json.getInt("Camera6_type")) {
				editor.putInt("Camera6_type", json.getInt("Camera6_type"));
				RESET_CAM = true;
				icam6 = true;
			}
			if (Camera_data.getInt("classorgroup6", 0) != json.getInt("classorgroup6")) {
				editor.putInt("classorgroup6", json.getInt("classorgroup6"));
				RESET_CAM = true;
				icam6 = true;
			}

			if (Camera_data.getBoolean("Camera7", false) != json.getBoolean("Camera7")) {
				editor.putBoolean("Camera7", json.getBoolean("Camera7"));
				RESET_CAM = true;
				icam7 = true;
			}
			if (!Camera_data.getString("Camera7_URL", "192.168.1.177").equals(json.getString("Camera7_URL"))) {
				editor.putString("Camera7_URL", json.getString("Camera7_URL"));
				RESET_CAM = true;
				icam7 = true;
			}
			if (!Camera_data.getString("Camera7_Label", "CH7").equals(json.getString("Camera7_Label"))) {
				editor.putString("Camera7_Label", json.getString("Camera7_Label"));
				RESET_CAM = true;
				icam7 = true;
			}
			if (Camera_data.getInt("Camera7_Port", 80) != json.getInt("Camera7_Port")) {
				editor.putInt("Camera7_Port", json.getInt("Camera7_Port"));
				RESET_CAM = true;
				icam7 = true;
			}
			if (!Camera_data.getString("Camera7_Account", "root").equals(json.getString("Camera7_Account"))) {
				editor.putString("Camera7_Account", json.getString("Camera7_Account"));
				RESET_CAM = true;
				icam7 = true;
			}
			if (!Camera_data.getString("Camera7_Pwd", "").equals(json.getString("Camera7_Pwd"))) {
				editor.putString("Camera7_Pwd", json.getString("Camera7_Pwd"));
				RESET_CAM = true;
				icam7 = true;
			}
			if (Camera_data.getInt("Camera7_type", Camera_v3) != json.getInt("Camera7_type")) {
				editor.putInt("Camera7_type", json.getInt("Camera7_type"));
				RESET_CAM = true;
				icam7 = true;
			}
			if (Camera_data.getInt("classorgroup7", 0) != json.getInt("classorgroup7")) {
				editor.putInt("classorgroup7", json.getInt("classorgroup7"));
				RESET_CAM = true;
				icam7 = true;
			}

			if (Camera_data.getBoolean("Camera8", false) != json.getBoolean("Camera8")) {
				editor.putBoolean("Camera8", json.getBoolean("Camera8"));
				RESET_CAM = true;
				icam8 = true;
			}
			if (!Camera_data.getString("Camera8_URL", "192.168.1.178").equals(json.getString("Camera8_URL"))) {
				editor.putString("Camera8_URL", json.getString("Camera8_URL"));
				RESET_CAM = true;
				icam8 = true;
			}
			if (!Camera_data.getString("Camera8_Label", "CH7").equals(json.getString("Camera8_Label"))) {
				editor.putString("Camera8_Label", json.getString("Camera8_Label"));
				RESET_CAM = true;
				icam8 = true;
			}
			if (Camera_data.getInt("Camera8_Port", 80) != json.getInt("Camera8_Port")) {
				editor.putInt("Camera8_Port", json.getInt("Camera8_Port"));
				RESET_CAM = true;
				icam8 = true;
			}
			if (!Camera_data.getString("Camera8_Account", "root").equals(json.getString("Camera8_Account"))) {
				editor.putString("Camera8_Account", json.getString("Camera8_Account"));
				RESET_CAM = true;
				icam8 = true;
			}
			if (!Camera_data.getString("Camera7_Pwd", "").equals(json.getString("Camera7_Pwd"))) {
				editor.putString("Camera7_Pwd", json.getString("Camera7_Pwd"));
				RESET_CAM = true;
				icam8 = true;
			}
			if (Camera_data.getInt("Camera8_type", Camera_v3) != json.getInt("Camera8_type")) {
				editor.putInt("Camera8_type", json.getInt("Camera8_type"));
				RESET_CAM = true;
				icam8 = true;
			}
			if (Camera_data.getInt("classorgroup8", 0) != json.getInt("classorgroup8")) {
				editor.putInt("classorgroup8", json.getInt("classorgroup8"));
				RESET_CAM = true;
				icam8 = true;
			}

			if (Camera_data.getBoolean("Camera9", false) != json.getBoolean("Camera9")) {
				editor.putBoolean("Camera9", json.getBoolean("Camera9"));
				RESET_CAM = true;
				icam9 = true;
			}
			if (!Camera_data.getString("Camera9_URL", "192.168.1.179").equals(json.getString("Camera9_URL"))) {
				editor.putString("Camera9_URL", json.getString("Camera9_URL"));
				RESET_CAM = true;
				icam9 = true;
			}
			if (!Camera_data.getString("Camera9_Label", "CH9").equals(json.getString("Camera9_Label"))) {
				editor.putString("Camera9_Label", json.getString("Camera9_Label"));
				RESET_CAM = true;
				icam9 = true;
			}
			if (Camera_data.getInt("Camera9_Port", 80) != json.getInt("Camera9_Port")) {
				editor.putInt("Camera9_Port", json.getInt("Camera9_Port"));
				RESET_CAM = true;
				icam9 = true;
			}
			if (!Camera_data.getString("Camera9_Account", "root").equals(json.getString("Camera9_Account"))) {
				editor.putString("Camera9_Account", json.getString("Camera9_Account"));
				RESET_CAM = true;
				icam9 = true;
			}
			if (!Camera_data.getString("Camera9_Pwd", "").equals(json.getString("Camera9_Pwd"))) {
				editor.putString("Camera9_Pwd", json.getString("Camera9_Pwd"));
				RESET_CAM = true;
				icam9 = true;
			}
			if (Camera_data.getInt("Camera9_type", Camera_v3) != json.getInt("Camera9_type")) {
				editor.putInt("Camera9_type", json.getInt("Camera9_type"));
				RESET_CAM = true;
				icam8 = true;
			}
			if (Camera_data.getInt("classorgroup9", 0) != json.getInt("classorgroup9")) {
				editor.putInt("classorgroup9", json.getInt("classorgroup9"));
				RESET_CAM = true;
				icam9 = true;
			}

			if (Camera_data.getBoolean("Camera10", false) != json.getBoolean("Camera10")) {
				editor.putBoolean("Camera10", json.getBoolean("Camera10"));
				RESET_CAM = true;
				icam10 = true;
			}
			if (!Camera_data.getString("Camera10_URL", "192.168.1.180").equals(json.getString("Camera10_URL"))) {
				editor.putString("Camera10_URL", json.getString("Camera10_URL"));
				RESET_CAM = true;
				icam10 = true;
			}
			if (!Camera_data.getString("Camera10_Label", "CH10").equals(json.getString("Camera10_Label"))) {
				editor.putString("Camera10_Label", json.getString("Camera10_Label"));
				RESET_CAM = true;
				icam10 = true;
			}
			if (Camera_data.getInt("Camera10_Port", 80) != json.getInt("Camera10_Port")) {
				editor.putInt("Camera10_Port", json.getInt("Camera10_Port"));
				RESET_CAM = true;
				icam10 = true;
			}
			if (!Camera_data.getString("Camera10_Account", "root").equals(json.getString("Camera10_Account"))) {
				editor.putString("Camera10_Account", json.getString("Camera10_Account"));
				RESET_CAM = true;
				icam10 = true;
			}
			if (!Camera_data.getString("Camera10_Pwd", "").equals(json.getString("Camera10_Pwd"))) {
				editor.putString("Camera10_Pwd", json.getString("Camera10_Pwd"));
				RESET_CAM = true;
				icam10 = true;
			}
			if (Camera_data.getInt("Camera10_type", Camera_v3) != json.getInt("Camera10_type")) {
				editor.putInt("Camera10_type", json.getInt("Camera10_type"));
				RESET_CAM = true;
				icam10 = true;
			}
			if (Camera_data.getInt("classorgroup10", 0) != json.getInt("classorgroup10")) {
				editor.putInt("classorgroup10", json.getInt("classorgroup10"));
				RESET_CAM = true;
				icam10 = true;
			}
			

			if (Camera_data.getBoolean("Camera11", false) != json.getBoolean("Camera11")) {
				editor.putBoolean("Camera11", json.getBoolean("Camera11"));
				RESET_CAM = true;
				icam11 = true;
			}
			if (!Camera_data.getString("Camera11_URL", "").equals(json.getString("Camera11_URL"))) {
				editor.putString("Camera11_URL", json.getString("Camera11_URL"));
				RESET_CAM = true;
				icam11 = true;
			}
			if (!Camera_data.getString("Camera11_Label", "CH1").equals(json.getString("Camera11_Label"))) {
				editor.putString("Camera11_Label", json.getString("Camera11_Label"));
				RESET_CAM = true;
				icam11 = true;
			}
			if (Camera_data.getInt("Camera11_Port", 80) != json.getInt("Camera11_Port")) {
				editor.putInt("Camera11_Port", json.getInt("Camera11_Port"));
				RESET_CAM = true;
				icam11 = true;
			}
			if (!Camera_data.getString("Camera11_Account", "root").equals(json.getString("Camera11_Account"))) {
				editor.putString("Camera11_Account", json.getString("Camera11_Account"));
				RESET_CAM = true;
				icam11 = true;
			}
			if (!Camera_data.getString("Camera11_Pwd", "").equals(json.getString("Camera11_Pwd"))) {
				editor.putString("Camera11_Pwd", json.getString("Camera11_Pwd"));
				RESET_CAM = true;
				icam11 = true;
			}
			if (Camera_data.getInt("Camera11_type", Camera_v3) != json.getInt("Camera11_type")) {
				editor.putInt("Camera11_type", json.getInt("Camera11_type"));
				RESET_CAM = true;
				icam11 = true;
			}
			if (Camera_data.getInt("classorgroup11", 0) != json.getInt("classorgroup11")) {
				editor.putInt("classorgroup11", json.getInt("classorgroup11"));
				RESET_CAM = true;
				icam11 = true;
			}

			if (Camera_data.getBoolean("Camera12", false) != json.getBoolean("Camera12")) {
				editor.putBoolean("Camera12", json.getBoolean("Camera12"));
				RESET_CAM = true;
				icam12 = true;
			}
			if (!Camera_data.getString("Camera2_URL", "").equals(json.getString("Camera12_URL"))) {
				editor.putString("Camera12_URL", json.getString("Camera12_URL"));
				RESET_CAM = true;
				icam12 = true;
			}
			if (!Camera_data.getString("Camera12_Label", "CH12").equals(json.getString("Camera12_Label"))) {
				editor.putString("Camera12_Label", json.getString("Camera12_Label"));
				RESET_CAM = true;
				icam12 = true;
			}
			if (Camera_data.getInt("Camera12_Port", 80) != json.getInt("Camera12_Port")) {
				editor.putInt("Camera12_Port", json.getInt("Camera12_Port"));
				RESET_CAM = true;
				icam12 = true;
			}
			if (!Camera_data.getString("Camera12_Account", "root").equals(json.getString("Camera12_Account"))) {
				editor.putString("Camera12_Account", json.getString("Camera12_Account"));
				RESET_CAM = true;
				icam12 = true;
			}
			if (!Camera_data.getString("Camera12_Pwd", "").equals(json.getString("Camera12_Pwd"))) {
				editor.putString("Camera12_Pwd", json.getString("Camera12_Pwd"));
				RESET_CAM = true;
				icam12 = true;
			}
			if (Camera_data.getInt("Camera12_type", Camera_v3) != json.getInt("Camera12_type")) {
				editor.putInt("Camera12_type", json.getInt("Camera12_type"));
				RESET_CAM = true;
				icam12 = true;
			}
			if (Camera_data.getInt("classorgroup12", 0) != json.getInt("classorgroup12")) {
				editor.putInt("classorgroup12", json.getInt("classorgroup12"));
				RESET_CAM = true;
				icam12 = true;
			}

			if (Camera_data.getBoolean("Camera13", false) != json.getBoolean("Camera13")) {
				editor.putBoolean("Camera13", json.getBoolean("Camera13"));
				RESET_CAM = true;
				icam13 = true;
			}
			if (!Camera_data.getString("Camera13_URL", "").equals(json.getString("Camera13_URL"))) {
				editor.putString("Camera13_URL", json.getString("Camera13_URL"));
				RESET_CAM = true;
				icam13 = true;
			}
			if (!Camera_data.getString("Camera13_Label", "CH13").equals(json.getString("Camera13_Label"))) {
				editor.putString("Camera13_Label", json.getString("Camera13_Label"));
				RESET_CAM = true;
				icam13 = true;
			}
			if (Camera_data.getInt("Camera13_Port", 80) != json.getInt("Camera13_Port")) {
				editor.putInt("Camera13_Port", json.getInt("Camera13_Port"));
				RESET_CAM = true;
				icam13 = true;
			}
			if (!Camera_data.getString("Camera13_Account", "root").equals(json.getString("Camera13_Account"))) {
				editor.putString("Camera13_Account", json.getString("Camera13_Account"));
				RESET_CAM = true;
				icam13 = true;
			}
			if (!Camera_data.getString("Camera13_Pwd", "").equals(json.getString("Camera13_Pwd"))) {
				editor.putString("Camera13_Pwd", json.getString("Camera13_Pwd"));
				RESET_CAM = true;
				icam13 = true;
			}
			if (Camera_data.getInt("Camera13_type", Camera_v3) != json.getInt("Camera13_type")) {
				editor.putInt("Camera13_type", json.getInt("Camera13_type"));
				RESET_CAM = true;
				icam13 = true;
			}
			if (Camera_data.getInt("classorgroup13", 0) != json.getInt("classorgroup13")) {
				editor.putInt("classorgroup13", json.getInt("classorgroup13"));
				RESET_CAM = true;
				icam13 = true;
			}

			if (Camera_data.getBoolean("Camera14", false) != json.getBoolean("Camera14")) {
				editor.putBoolean("Camera14", json.getBoolean("Camera14"));
				RESET_CAM = true;
				icam14 = true;
			}
			if (!Camera_data.getString("Camera14_URL", "").equals(json.getString("Camera14_URL"))) {
				editor.putString("Camera14_URL", json.getString("Camera14_URL"));
				RESET_CAM = true;
				icam14 = true;
			}
			if (!Camera_data.getString("Camera14_Label", "CH14").equals(json.getString("Camera14_Label"))) {
				editor.putString("Camera14_Label", json.getString("Camera14_Label"));
				RESET_CAM = true;
				icam14 = true;
			}
			if (Camera_data.getInt("Camera14_Port", 80) != json.getInt("Camera14_Port")) {
				editor.putInt("Camera14_Port", json.getInt("Camera14_Port"));
				RESET_CAM = true;
				icam14 = true;
			}
			if (!Camera_data.getString("Camera14_Account", "root").equals(json.getString("Camera14_Account"))) {
				editor.putString("Camera14_Account", json.getString("Camera14_Account"));
				RESET_CAM = true;
				icam14 = true;
			}
			if (!Camera_data.getString("Camera14_Pwd", "").equals(json.getString("Camera14_Pwd"))) {
				editor.putString("Camera14_Pwd", json.getString("Camera14_Pwd"));
				RESET_CAM = true;
				icam14 = true;
			}
			if (Camera_data.getInt("Camera14_type", Camera_v3) != json.getInt("Camera14_type")) {
				editor.putInt("Camera14_type", json.getInt("Camera14_type"));
				RESET_CAM = true;
				icam14 = true;
			}
			if (Camera_data.getInt("classorgroup14", 0) != json.getInt("classorgroup14")) {
				editor.putInt("classorgroup14", json.getInt("classorgroup14"));
				RESET_CAM = true;
				icam14 = true;
			}

			if (Camera_data.getBoolean("Camera15", false) != json.getBoolean("Camera15")) {
				editor.putBoolean("Camera15", json.getBoolean("Camera15"));
				RESET_CAM = true;
				icam15 = true;
			}
			if (!Camera_data.getString("Camera15_URL", "").equals(json.getString("Camera15_URL"))) {
				editor.putString("Camera15_URL", json.getString("Camera15_URL"));
				RESET_CAM = true;
				icam5 = true;
			}
			if (!Camera_data.getString("Camera15_Label", "CH15").equals(json.getString("Camera15_Label"))) {
				editor.putString("Camera15_Label", json.getString("Camera15_Label"));
				RESET_CAM = true;
				icam15 = true;
			}
			if (Camera_data.getInt("Camera15_Port", 80) != json.getInt("Camera15_Port")) {
				editor.putInt("Camera15_Port", json.getInt("Camera15_Port"));
				RESET_CAM = true;
				icam15 = true;
			}
			if (!Camera_data.getString("Camera15_Account", "root").equals(json.getString("Camera15_Account"))) {
				editor.putString("Camera15_Account", json.getString("Camera15_Account"));
				RESET_CAM = true;
				icam15 = true;
			}
			if (!Camera_data.getString("Camera15_Pwd", "").equals(json.getString("Camera15_Pwd"))) {
				editor.putString("Camera15_Pwd", json.getString("Camera15_Pwd"));
				RESET_CAM = true;
				icam15 = true;
			}
			if (Camera_data.getInt("Camera15_type", Camera_v3) != json.getInt("Camera15_type")) {
				editor.putInt("Camera15_type", json.getInt("Camera15_type"));
				RESET_CAM = true;
				icam15 = true;
			}
			if (Camera_data.getInt("classorgroup15", 0) != json.getInt("classorgroup15")) {
				editor.putInt("classorgroup15", json.getInt("classorgroup15"));
				RESET_CAM = true;
				icam15 = true;
			}

			if (Camera_data.getBoolean("Camera16", false) != json.getBoolean("Camera16")) {
				editor.putBoolean("Camera16", json.getBoolean("Camera16"));
				RESET_CAM = true;
				icam16 = true;
			}
			if (!Camera_data.getString("Camera16_URL", "").equals(json.getString("Camera16_URL"))) {
				editor.putString("Camera16_URL", json.getString("Camera16_URL"));
				RESET_CAM = true;
				icam16 = true;
			}
			if (!Camera_data.getString("Camera16_Label", "CH16").equals(json.getString("Camera16_Label"))) {
				editor.putString("Camera16_Label", json.getString("Camera16_Label"));
				RESET_CAM = true;
				icam16 = true;
			}
			if (Camera_data.getInt("Camera16_Port", 80) != json.getInt("Camera16_Port")) {
				editor.putInt("Camera16_Port", json.getInt("Camera16_Port"));
				RESET_CAM = true;
				icam16 = true;
			}
			if (!Camera_data.getString("Camera16_Account", "root").equals(json.getString("Camera16_Account"))) {
				editor.putString("Camera16_Account", json.getString("Camera16_Account"));
				RESET_CAM = true;
				icam16 = true;
			}
			if (!Camera_data.getString("Camera16_Pwd", "").equals(json.getString("Camera16_Pwd"))) {
				editor.putString("Camera16_Pwd", json.getString("Camera16_Pwd"));
				RESET_CAM = true;
				icam16 = true;
			}
			if (Camera_data.getInt("Camera16_type", Camera_v3) != json.getInt("Camera16_type")) {
				editor.putInt("Camera16_type", json.getInt("Camera16_type"));
				RESET_CAM = true;
				icam16 = true;
			}
			if (Camera_data.getInt("classorgroup16", 0) != json.getInt("classorgroup16")) {
				editor.putInt("classorgroup16", json.getInt("classorgroup16"));
				RESET_CAM = true;
				icam16 = true;
			}

			if (Camera_data.getBoolean("Camera17", false) != json.getBoolean("Camera17")) {
				editor.putBoolean("Camera17", json.getBoolean("Camera17"));
				RESET_CAM = true;
				icam17 = true;
			}
			if (!Camera_data.getString("Camera17_URL", "").equals(json.getString("Camera17_URL"))) {
				editor.putString("Camera17_URL", json.getString("Camera17_URL"));
				RESET_CAM = true;
				icam17 = true;
			}
			if (!Camera_data.getString("Camera17_Label", "CH17").equals(json.getString("Camera17_Label"))) {
				editor.putString("Camera17_Label", json.getString("Camera17_Label"));
				RESET_CAM = true;
				icam17 = true;
			}
			if (Camera_data.getInt("Camera17_Port", 80) != json.getInt("Camera17_Port")) {
				editor.putInt("Camera17_Port", json.getInt("Camera17_Port"));
				RESET_CAM = true;
				icam17 = true;
			}
			if (!Camera_data.getString("Camera17_Account", "root").equals(json.getString("Camera17_Account"))) {
				editor.putString("Camera17_Account", json.getString("Camera17_Account"));
				RESET_CAM = true;
				icam17 = true;
			}
			if (!Camera_data.getString("Camera17_Pwd", "").equals(json.getString("Camera17_Pwd"))) {
				editor.putString("Camera17_Pwd", json.getString("Camera17_Pwd"));
				RESET_CAM = true;
				icam17 = true;
			}
			if (Camera_data.getInt("Camera17_type", Camera_v3) != json.getInt("Camera17_type")) {
				editor.putInt("Camera17_type", json.getInt("Camera17_type"));
				RESET_CAM = true;
				icam17 = true;
			}
			if (Camera_data.getInt("classorgroup17", 0) != json.getInt("classorgroup17")) {
				editor.putInt("classorgroup17", json.getInt("classorgroup17"));
				RESET_CAM = true;
				icam17 = true;
			}

			if (Camera_data.getBoolean("Camera18", false) != json.getBoolean("Camera18")) {
				editor.putBoolean("Camera18", json.getBoolean("Camera18"));
				RESET_CAM = true;
				icam18 = true;
			}
			if (!Camera_data.getString("Camera18_URL", "").equals(json.getString("Camera18_URL"))) {
				editor.putString("Camera18_URL", json.getString("Camera18_URL"));
				RESET_CAM = true;
				icam18 = true;
			}
			if (!Camera_data.getString("Camera18_Label", "CH18").equals(json.getString("Camera18_Label"))) {
				editor.putString("Camera18_Label", json.getString("Camera18_Label"));
				RESET_CAM = true;
				icam18 = true;
			}
			if (Camera_data.getInt("Camera18_Port", 80) != json.getInt("Camera18_Port")) {
				editor.putInt("Camera18_Port", json.getInt("Camera18_Port"));
				RESET_CAM = true;
				icam18 = true;
			}
			if (!Camera_data.getString("Camera18_Account", "root").equals(json.getString("Camera18_Account"))) {
				editor.putString("Camera18_Account", json.getString("Camera18_Account"));
				RESET_CAM = true;
				icam18 = true;
			}
			if (!Camera_data.getString("Camera18_Pwd", "").equals(json.getString("Camera18_Pwd"))) {
				editor.putString("Camera18_Pwd", json.getString("Camera18_Pwd"));
				RESET_CAM = true;
				icam18 = true;
			}
			if (Camera_data.getInt("Camera18_type", Camera_v3) != json.getInt("Camera18_type")) {
				editor.putInt("Camera18_type", json.getInt("Camera18_type"));
				RESET_CAM = true;
				icam18 = true;
			}
			if (Camera_data.getInt("classorgroup18", 0) != json.getInt("classorgroup18")) {
				editor.putInt("classorgroup18", json.getInt("classorgroup18"));
				RESET_CAM = true;
				icam18 = true;
			}

			if (Camera_data.getBoolean("Camera19", false) != json.getBoolean("Camera19")) {
				editor.putBoolean("Camera19", json.getBoolean("Camera19"));
				RESET_CAM = true;
				icam19 = true;
			}
			if (!Camera_data.getString("Camera19_URL", "").equals(json.getString("Camera19_URL"))) {
				editor.putString("Camera19_URL", json.getString("Camera19_URL"));
				RESET_CAM = true;
				icam19 = true;
			}
			if (!Camera_data.getString("Camera19_Label", "CH9").equals(json.getString("Camera19_Label"))) {
				editor.putString("Camera19_Label", json.getString("Camera19_Label"));
				RESET_CAM = true;
				icam19 = true;
			}
			if (Camera_data.getInt("Camera19_Port", 80) != json.getInt("Camera19_Port")) {
				editor.putInt("Camera19_Port", json.getInt("Camera19_Port"));
				RESET_CAM = true;
				icam19 = true;
			}
			if (!Camera_data.getString("Camera19_Account", "root").equals(json.getString("Camera19_Account"))) {
				editor.putString("Camera19_Account", json.getString("Camera19_Account"));
				RESET_CAM = true;
				icam19 = true;
			}
			if (!Camera_data.getString("Camera19_Pwd", "").equals(json.getString("Camera19_Pwd"))) {
				editor.putString("Camera19_Pwd", json.getString("Camera19_Pwd"));
				RESET_CAM = true;
				icam19 = true;
			}
			if (Camera_data.getInt("Camera19_type", Camera_v3) != json.getInt("Camera19_type")) {
				editor.putInt("Camera19_type", json.getInt("Camera19_type"));
				RESET_CAM = true;
				icam19 = true;
			}
			if (Camera_data.getInt("classorgroup19", 0) != json.getInt("classorgroup19")) {
				editor.putInt("classorgroup19", json.getInt("classorgroup19"));
				RESET_CAM = true;
				icam19 = true;
			}

			if (Camera_data.getBoolean("Camera20", false) != json.getBoolean("Camera20")) {
				editor.putBoolean("Camera20", json.getBoolean("Camera20"));
				RESET_CAM = true;
				icam20 = true;
			}
			if (!Camera_data.getString("Camera20_URL", "").equals(json.getString("Camera20_URL"))) {
				editor.putString("Camera20_URL", json.getString("Camera20_URL"));
				RESET_CAM = true;
				icam20 = true;
			}
			if (!Camera_data.getString("Camera20_Label", "CH20").equals(json.getString("Camera20_Label"))) {
				editor.putString("Camera20_Label", json.getString("Camera20_Label"));
				RESET_CAM = true;
				icam20 = true;
			}
			if (Camera_data.getInt("Camera20_Port", 80) != json.getInt("Camera20_Port")) {
				editor.putInt("Camera20_Port", json.getInt("Camera20_Port"));
				RESET_CAM = true;
				icam20 = true;
			}
			if (!Camera_data.getString("Camera20_Account", "root").equals(json.getString("Camera20_Account"))) {
				editor.putString("Camera20_Account", json.getString("Camera20_Account"));
				RESET_CAM = true;
				icam20 = true;
			}
			if (!Camera_data.getString("Camera20_Pwd", "").equals(json.getString("Camera20_Pwd"))) {
				editor.putString("Camera20_Pwd", json.getString("Camera20_Pwd"));
				RESET_CAM = true;
				icam20 = true;
			}
			if (Camera_data.getInt("Camera20_type", Camera_v3) != json.getInt("Camera20_type")) {
				editor.putInt("Camera20_type", json.getInt("Camera20_type"));
				RESET_CAM = true;
				icam20 = true;
			}
			if (Camera_data.getInt("classorgroup20", 0) != json.getInt("classorgroup20")) {
				editor.putInt("classorgroup20", json.getInt("classorgroup20"));
				RESET_CAM = true;
				icam20 = true;
			}

			if (Camera_data.getInt("net_type", eth0) != json.getInt("net_type")) {
				editor.putInt("net_type", json.getInt("net_type"));
				RESET_NET = true;
			}
			
			if (json.getInt("net_type") == wifi) {
				if (!Camera_data.getString("wifi", "192.168.1.51").equals(json.getString("net_ip"))) {
					editor.putString("wifi", json.getString("net_ip"));
					RESET_NET = true;
					MyLog.i("net", "Change eth0 ip from " + Camera_data.getString("wifi", "192.168.1.51") + " to "
							+ json.getString("net_ip") + " by " + ip);
				}
			} else {
				if (!Camera_data.getString("eth0", "192.168.1.51").equals(json.getString("net_ip"))) {
					editor.putString("eth0", json.getString("net_ip"));
					Log.v("test", json.getString("net_ip"));
					RESET_NET = true;
					MyLog.i("net", "Change eth0 ip from " + Camera_data.getString("wifi", "192.168.1.51") + " to "
							+ json.getString("net_ip") + " by " + ip);
				}
			}

			if (!Camera_data.getString("geteway", "192.168.1.1").equals(json.getString("geteway"))) {
				try {
					Process proc = Runtime.getRuntime().exec("su");
					DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
					opt = new DataOutputStream(proc.getOutputStream());
					opt.writeBytes("ip route del all\n");
					opt.writeBytes("exit\n");
					opt.flush();
					
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Log.e("test", e.toString());
					}
				editor.putString("geteway", json.getString("geteway"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("mask", "255.255.255.0").equals(json.getString("mask"))) {
				editor.putString("mask", json.getString("mask"));
				RESET_NET = true;
			}

			if (Camera_data.getInt("pwd_type", no_pwd) != json.getInt("pwd_type")) {
				editor.putInt("pwd_type", json.getInt("pwd_type"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("wifi_pwd", "").equals(json.getString("wifi_pwd"))) {
				editor.putString("wifi_pwd", json.getString("wifi_pwd"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("wifi_ssid", "defaultssid").equals(json.getString("wifi_ssid"))) {
				editor.putString("wifi_ssid", json.getString("wifi_ssid"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("login_pwd", "9999").equals(json.getString("login_pwd"))) {
				editor.putString("login_pwd", json.getString("login_pwd"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("hotspot", "001").equals(json.getString("hotspot"))) {
				RESET_CAM = true;
				editor.putString("hotspot", json.getString("hotspot"));
				editor.putBoolean("ap_reon", true);
			} else {
				editor.putBoolean("ap_reon", false);
			}

			if (Camera_data.getBoolean("video_title", true) != json.getBoolean("video_title")) {
				editor.putBoolean("video_title", json.getBoolean("video_title"));
				RESET_CAM = true;
			}
			if (Camera_data.getBoolean("video_time", true) != json.getBoolean("video_time")) {
				editor.putBoolean("video_time", json.getBoolean("video_time"));
				RESET_CAM = true;
			}
			
			if (Camera_data.getInt("real_time", realtime_smooth) != json.getInt("real_time")) {
				editor.putInt("real_time", json.getInt("real_time"));
				RESET_CAM = true;
			}

			if(Camera_data.getBoolean("loop_on", false) !=  json.getBoolean("loop_on")) {
				editor.putBoolean("loop_on", json.getBoolean("loop_on"));
				RESET_CAM = true;
			}
			
			if(Camera_data.getInt("loop_time", 20) != json.getInt("loop_time")) {
				editor.putInt("loop_time", json.getInt("loop_time"));
				RESET_CAM = true;
			}
			
			editor.commit();

			if (icam1) {
				MyLog.i("CH1", "Channel 1 data has been changed by " + ip);
			}
			if (icam2) {
				MyLog.i("CH2", "Channel 2 data has been changed by " + ip);
			}
			if (icam3) {
				MyLog.i("CH3", "Channel 3 data has been changed by " + ip);
			}
			if (icam4) {
				MyLog.i("CH4", "Channel 4 data has been changed by " + ip);
			}
			if (icam5) {
				MyLog.i("CH5", "Channel 5 data has been changed by " + ip);
			}
			if (icam6) {
				MyLog.i("CH6", "Channel 6 data has been changed by " + ip);
			}
			if (icam7) {
				MyLog.i("CH7", "Channel 7 data has been changed by " + ip);
			}
			if (icam8) {
				MyLog.i("CH8", "Channel 8 data has been changed by " + ip);
			}
			if (icam9) {
				MyLog.i("CH9", "Channel 9 data has been changed by " + ip);
			}
			if (icam10) {
				MyLog.i("CH10", "Channel 10 data has been changed by " + ip);
			}
			if (icam11) {
				MyLog.i("CH11", "Channel 11 data has been changed by " + ip);
			}
			if (icam12) {
				MyLog.i("CH12", "Channel 12 data has been changed by " + ip);
			}
			if (icam13) {
				MyLog.i("CH13", "Channel 13 data has been changed by " + ip);
			}
			if (icam14) {
				MyLog.i("CH14", "Channel 14 data has been changed by " + ip);
			}
			if (icam15) {
				MyLog.i("CH15", "Channel 15 data has been changed by " + ip);
			}
			if (icam16) {
				MyLog.i("CH16", "Channel 16 data has been changed by " + ip);
			}
			if (icam17) {
				MyLog.i("CH17", "Channel 17 data has been changed by " + ip);
			}
			if (icam18) {
				MyLog.i("CH18", "Channel 18 data has been changed by " + ip);
			}
			if (icam19) {
				MyLog.i("CH19", "Channel 19 data has been changed by " + ip);
			}
			if (icam20) {
				MyLog.i("CH20", "Channel 20 data has been changed by " + ip);
			}

			new Thread() {
				public void run() {
					if (Camera_data.getInt("activity", 0x1a) == 0x1a) {
						if (changetype == 0) {
							if (RESET_CAM) {
								Message msg = new Message();
								msg.arg1 = 0x03;
								Ondemand_LiveBox_Activity.Change_iCam.sendMessage(msg);
							}

							if (RESET_NET) {
								Message msg = new Message();
								msg.arg1 = 0x02;
								Ondemand_LiveBox_Activity.Change_iCam.sendMessage(msg);
							}
						} else {
							Message msg = new Message();
							msg.arg1 = 0x04;
							Ondemand_LiveBox_Activity.Change_iCam.sendMessage(msg);
						}

					} else if (Camera_data.getInt("activity", 0x1a) == 0x1b) {
						Message msg = new Message();
						msg.arg1 = 0x01;
						Ondemand_Setting_Activity.Change_DATA.sendMessage(msg);
					}
				}
			}.start();

			return true;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.v("error", e.toString());
			return false;
		}

	}

	public boolean getjsondata(String cmd, int inputtype) {
		try {

			icam1 = false;
			icam2 = false;
			icam3 = false;
			icam4 = false;
			RESET_CAM = false;
			RESET_NET = false;
			changetype = inputtype;
			JSONObject json = new JSONObject(cmd);
			Log.v("test", cmd);
			Camera_data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			SharedPreferences.Editor editor = Camera_data.edit();
			if (Camera_data.getBoolean("Camera1", false) != json.getBoolean("Camera1")) {
				editor.putBoolean("Camera1", json.getBoolean("Camera1"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (!Camera_data.getString("Camera1_URL", "192.168.1.171").equals(json.getString("Camera1_URL"))) {
				editor.putString("Camera1_URL", json.getString("Camera1_URL"));
				RESET_CAM = true;
				icam1 = true;
				if (inputtype == 0) {
					MyLog.i("CH1",
							"change camera1 ip change from " + Camera_data.getString("Camera1_URL", "192.168.1.171")
									+ " to " + json.getString("Camera1_URL") + " by " + ip);
				} else {
					MyLog.i("CH1",
							"change camera1 ip change from " + Camera_data.getString("Camera1_URL", "192.168.1.171")
									+ " to " + json.getString("Camera1_URL") + " by USB");
				}
			}
			if (!Camera_data.getString("Camera1_Label", "CH1").equals(json.getString("Camera1_Label"))) {
				editor.putString("Camera1_Label", json.getString("Camera1_Label"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (Camera_data.getInt("Camera1_Port", 80) != json.getInt("Camera1_Port")) {
				editor.putInt("Camera1_Port", json.getInt("Camera1_Port"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (!Camera_data.getString("Camera1_Account", "root").equals(json.getString("Camera1_Account"))) {
				editor.putString("Camera1_Account", json.getString("Camera1_Account"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (!Camera_data.getString("Camera1_Pwd", "27507522").equals(json.getString("Camera1_Pwd"))) {
				editor.putString("Camera1_Pwd", json.getString("Camera1_Pwd"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (Camera_data.getInt("Camera1_type", Camera_v3) != json.getInt("Camera1_type")) {
				editor.putInt("Camera1_type", json.getInt("Camera1_type"));
				RESET_CAM = true;
				icam1 = true;
			}
			if (Camera_data.getInt("classorgroup1", 0) != json.getInt("classorgroup1")) {
				editor.putInt("classorgroup1", json.getInt("classorgroup1"));
				RESET_CAM = true;
				icam1 = true;
			}

			if (Camera_data.getBoolean("Camera2", false) != json.getBoolean("Camera2")) {
				editor.putBoolean("Camera2", json.getBoolean("Camera2"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (!Camera_data.getString("Camera2_URL", "192.168.1.172").equals(json.getString("Camera2_URL"))) {
				editor.putString("Camera2_URL", json.getString("Camera2_URL"));
				RESET_CAM = true;
				icam2 = true;
				if (inputtype == 0) {
					MyLog.i("CH2",
							"change camera2 ip change from " + Camera_data.getString("Camera2_URL", "192.168.1.172")
									+ " to " + json.getString("Camera2_URL") + " by " + ip);
				} else {
					MyLog.i("CH2",
							"change camera2 ip change from " + Camera_data.getString("Camera2_URL", "192.168.1.172")
									+ " to " + json.getString("Camera2_URL") + " by USB");
				}

			}
			if (!Camera_data.getString("Camera2_Label", "CH2").equals(json.getString("Camera2_Label"))) {
				editor.putString("Camera2_Label", json.getString("Camera2_Label"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (Camera_data.getInt("Camera2_Port", 80) != json.getInt("Camera2_Port")) {
				editor.putInt("Camera2_Port", json.getInt("Camera2_Port"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (!Camera_data.getString("Camera2_Account", "root").equals(json.getString("Camera2_Account"))) {
				editor.putString("Camera2_Account", json.getString("Camera2_Account"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (!Camera_data.getString("Camera2_Pwd", "27507522").equals(json.getString("Camera2_Pwd"))) {
				editor.putString("Camera2_Pwd", json.getString("Camera2_Pwd"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (Camera_data.getInt("Camera2_type", Camera_v3) != json.getInt("Camera2_type")) {
				editor.putInt("Camera2_type", json.getInt("Camera2_type"));
				RESET_CAM = true;
				icam2 = true;
			}
			if (Camera_data.getInt("classorgroup2", 0) != json.getInt("classorgroup2")) {
				editor.putInt("classorgroup2", json.getInt("classorgroup2"));
				RESET_CAM = true;
				icam2 = true;
			}

			if (Camera_data.getBoolean("Camera3", false) != json.getBoolean("Camera3")) {
				editor.putBoolean("Camera3", json.getBoolean("Camera3"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (!Camera_data.getString("Camera3_URL", "192.168.1.173").equals(json.getString("Camera3_URL"))) {
				editor.putString("Camera3_URL", json.getString("Camera3_URL"));
				RESET_CAM = true;
				icam3 = true;
				if (inputtype == 0) {
					MyLog.i("CH3",
							"change camera3 ip change from " + Camera_data.getString("Camera3_URL", "192.168.1.173")
									+ " to " + json.getString("Camera3_URL") + " by " + ip);
				} else {
					MyLog.i("CH3",
							"change camera3 ip change from " + Camera_data.getString("Camera3_URL", "192.168.1.173")
									+ " to " + json.getString("Camera3_URL") + " by USB");
				}

			}
			if (!Camera_data.getString("Camera3_Label", "CH3").equals(json.getString("Camera3_Label"))) {
				editor.putString("Camera3_Label", json.getString("Camera3_Label"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (Camera_data.getInt("Camera3_Port", 80) != json.getInt("Camera3_Port")) {
				editor.putInt("Camera3_Port", json.getInt("Camera3_Port"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (!Camera_data.getString("Camera3_Account", "root").equals(json.getString("Camera3_Account"))) {
				editor.putString("Camera3_Account", json.getString("Camera3_Account"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (!Camera_data.getString("Camera3_Pwd", "27507522").equals(json.getString("Camera3_Pwd"))) {
				editor.putString("Camera3_Pwd", json.getString("Camera3_Pwd"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (Camera_data.getInt("Camera3_type", Camera_v3) != json.getInt("Camera3_type")) {
				editor.putInt("Camera3_type", json.getInt("Camera3_type"));
				RESET_CAM = true;
				icam3 = true;
			}
			if (Camera_data.getInt("classorgroup3", 0) != json.getInt("classorgroup3")) {
				editor.putInt("classorgroup3", json.getInt("classorgroup3"));
				RESET_CAM = true;
				icam3 = true;
			}

			if (Camera_data.getBoolean("Camera4", false) != json.getBoolean("Camera4")) {
				editor.putBoolean("Camera4", json.getBoolean("Camera4"));
				RESET_CAM = true;
			}

			if (Camera_data.getBoolean("video_title", true) != json.getBoolean("video_title")) {
				editor.putBoolean("video_title", json.getBoolean("video_title"));
				RESET_CAM = true;
			}
			if (Camera_data.getBoolean("video_time", true) != json.getBoolean("video_time")) {
				editor.putBoolean("video_time", json.getBoolean("video_time"));
				RESET_CAM = true;
			}

			if (Camera_data.getInt("net_type", eth0) != json.getInt("net_type")) {
				editor.putInt("net_type", json.getInt("net_type"));
				RESET_NET = true;
			}

			if (json.getInt("net_type") == wifi) {
				if (!Camera_data.getString("wifi", "192.168.1.51").equals(json.getString("net_ip"))) {
					editor.putString("wifi", json.getString("net_ip"));
					RESET_NET = true;
					if (inputtype == 0) {
						MyLog.i("net", "Change eth0 ip from " + Camera_data.getString("wifi", "192.168.1.51") + " to "
								+ json.getString("net_ip") + " by " + ip);
					} else {
						MyLog.i("net", "Change eth0 ip from " + Camera_data.getString("wifi", "192.168.1.51") + " to "
								+ json.getString("net_ip") + " by USB");
					}

				}
			} else {
				if (!Camera_data.getString("eth0", "192.168.1.51").equals(json.getString("net_ip"))) {
					editor.putString("eth0", json.getString("net_ip"));
					Log.v("test", json.getString("net_ip"));
					RESET_NET = true;
					if (inputtype == 0) {
						MyLog.i("net", "Change eth0 ip from " + Camera_data.getString("wifi", "192.168.1.51") + " to "
								+ json.getString("net_ip") + " by " + ip);
					} else {
						MyLog.i("net", "Change eth0 ip from " + Camera_data.getString("wifi", "192.168.1.51") + " to "
								+ json.getString("net_ip") + " by USB");
					}
				}
			}

			if (!Camera_data.getString("geteway", "192.168.1.1").equals(json.getString("geteway"))) {
				try {
					Process proc = Runtime.getRuntime().exec("su");
					DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
					opt = new DataOutputStream(proc.getOutputStream());
					opt.writeBytes("ip route del all\n");
					opt.writeBytes("exit\n");
					opt.flush();
					
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Log.e("test", e.toString());
					}
				editor.putString("geteway", json.getString("geteway"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("mask", "255.255.255.0").equals(json.getString("mask"))) {
				editor.putString("mask", json.getString("mask"));
				RESET_NET = true;
			}

			if (Camera_data.getInt("pwd_type", no_pwd) != json.getInt("pwd_type")) {
				editor.putInt("pwd_type", json.getInt("pwd_type"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("wifi_pwd", "").equals(json.getString("wifi_pwd"))) {
				editor.putString("wifi_pwd", json.getString("wifi_pwd"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("wifi_ssid", "defaultssid").equals(json.getString("wifi_ssid"))) {
				editor.putString("wifi_ssid", json.getString("wifi_ssid"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("login_pwd", "9999").equals(json.getString("login_pwd"))) {
				editor.putString("login_pwd", json.getString("login_pwd"));
				RESET_NET = true;
			}

			if (!Camera_data.getString("hotspot", "001").equals(json.getString("hotspot"))) {
				RESET_CAM = true;
				editor.putString("hotspot", json.getString("hotspot"));
				editor.putBoolean("ap_reon", true);
			} else {
				editor.putBoolean("ap_reon", false);
			}

			if (Camera_data.getInt("real_time", realtime_smooth) != json.getInt("real_time")) {
				editor.putInt("real_time", json.getInt("real_time"));
				RESET_CAM = true;
			}

			editor.commit();
			Log.v("test", "json finish");

			if (icam1) {
				if (inputtype == 0) {
					MyLog.i("CH1", "camera1 data has been changed by " + ip);
				} else {
					MyLog.i("CH1", "camera1 data has been changed by USB");
				}
			}
			if (icam2) {
				if (inputtype == 0) {
					MyLog.i("CH2", "camera2 data has been changed by " + ip);
				} else {
					MyLog.i("CH2", "camera2 data has been changed by USB");
				}

			}
			if (icam3) {
				if (inputtype == 0) {
					MyLog.i("CH3", "camera3 data has been changed by " + ip);
				} else {
					MyLog.i("CH3", "camera3 data has been changed by USB");
				}
			}
			if (icam4) {
				if (inputtype == 0) {
					MyLog.i("CH4", "camera4 data has been changed by " + ip);
				} else {
					MyLog.i("CH4", "camera4 data has been changed by USB");
				}
			}

			new Thread() {
				public void run() {
					if (Camera_data.getInt("activity", 0x1a) == 0x1a) {
						if (changetype == 0) {
							if (RESET_CAM) {
								Message msg = new Message();
								msg.arg1 = 0x03;
								LiveBox_Activity.Change_iCam.sendMessage(msg);
							}

							if (RESET_NET) {
								Message msg = new Message();
								msg.arg1 = 0x02;
								LiveBox_Activity.Change_iCam.sendMessage(msg);
							}
						} else {
							Message msg = new Message();
							msg.arg1 = 0x04;
							LiveBox_Activity.Change_iCam.sendMessage(msg);
						}

					} else if (Camera_data.getInt("activity", 0x1a) == 0x1b) {
						Message msg = new Message();
						msg.arg1 = 0x01;
						Setting_Activity.Change_DATA.sendMessage(msg);
					}
				}
			}.start();

			return true;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.v("error", e.toString());
			return false;
		}

	}

	IServiceControl.Stub stub = new IServiceControl.Stub() {
		@Override
		public void setondemand() throws RemoteException {
			try {
				Log.v("stub", "Change ondemand sucess");
				SharedPreferences.Editor editor = Camera_data.edit();
				editor.putBoolean("OnDemand", true);
				editor.commit();
				opt.print("HTTP/1.1 200 OK\r\n");
				opt.print("Connection: close\r\n");
				opt.print("Expires: 0\r\n");
				opt.print("Cache-Control: no-cache\r\n");
				opt.print("Pragma: no-cache\r\n");
				opt.print("Content-Type: text/html; charset=UTF-8\r\n\r\n");
				opt.print("Change ondemand sucess.");
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void restoredefault() throws RemoteException {
			try {
				Log.v("stub", "restore default");
				SharedPreferences.Editor editor = Camera_data.edit();
				editor.putBoolean("OnDemand", false);
				editor.commit();
				opt.print("HTTP/1.1 200 OK\r\n");
				opt.print("Connection: close\r\n");
				opt.print("Expires: 0\r\n");
				opt.print("Cache-Control: no-cache\r\n");
				opt.print("Pragma: no-cache\r\n");
				opt.print("Content-Type: text/html; charset=UTF-8\r\n\r\n");
				opt.print("Restore default sucess.");
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void check_ondemand() throws RemoteException {
			try {
				if (Camera_data.getBoolean("OnDemand", false)) {
					opt.print("HTTP/1.1 200 OK\r\n");
					opt.print("Connection: close\r\n");
					opt.print("Expires: 0\r\n");
					opt.print("Cache-Control: no-cache\r\n");
					opt.print("Pragma: no-cache\r\n");
					opt.print("Content-Type: text/html; charset=UTF-8\r\n\r\n");
					opt.print("now ondemand.");
				} else {
					opt.print("HTTP/1.1 400 Bad Request\r\n");
					opt.print("Content-Type: text/html\r\n\r\n");
					opt.print("fail");
				}
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void sendOK() throws RemoteException {
			try {
				Log.v("stub", "send sucess");
				opt.print("HTTP/1.1 200 OK\r\n");
				opt.print("Connection: close\r\n");
				opt.print("Expires: 0\r\n");
				opt.print("Cache-Control: no-cache\r\n");
				opt.print("Pragma: no-cache\r\n");
				opt.print("Content-Type: text/html; charset=UTF-8\r\n\r\n");
				opt.print("Sucess");
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void sendFail() throws RemoteException {
			try {
				// TODO Auto-generated method stub
				Log.v("stub", "send fail");

				opt.print("HTTP/1.1 400 Bad Request\r\n");
				opt.print("Content-Type: text/html\r\n\r\n");
				opt.print("fail");
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void sendloop_type(boolean loop) throws RemoteException {
			try {
				// TODO Auto-generated method stub
				if(loop) {
					opt.print("loop");
				} else {
					opt.print("none");	
				}
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		@Override
		public void sendTYPE(String cmd) throws RemoteException {
			try {
				// TODO Auto-generated method stub
				Log.v("stub", "send type");
				opt.print(cmd);
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void sendvolumetype() throws RemoteException {
			try {
				// TODO Auto-generated method stub
				if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
					opt.print("volume_0");
				} else {
					opt.print("volume_1");
				}
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void send_OndemandSetting() throws RemoteException {
			try {
				// TODO Auto-generated method stub
				Log.v("stub", "send setting");
				JSONObject json = new JSONObject();
				json.put("Camera1", Camera_data.getBoolean("Camera1", false));
				json.put("Camera1_URL", Camera_data.getString("Camera1_URL", "192.168.1.171"));
				json.put("Camera1_Label", Camera_data.getString("Camera1_Label", "CH1"));
				json.put("Camera1_Port", Camera_data.getInt("Camera1_Port", 80));
				json.put("Camera1_Account", Camera_data.getString("Camera1_Account", "root"));
				json.put("Camera1_Pwd", Camera_data.getString("Camera1_Pwd", "27507522"));
				json.put("Camera1_type", Camera_data.getInt("Camera1_type", Camera_v3));
				json.put("classorgroup1", Camera_data.getInt("classorgroup1", 0));

				json.put("Camera2", Camera_data.getBoolean("Camera2", false));
				json.put("Camera2_URL", Camera_data.getString("Camera2_URL", ""));
				json.put("Camera2_Label", Camera_data.getString("Camera2_Label", "CH2"));
				json.put("Camera2_Port", Camera_data.getInt("Camera2_Port", 80));
				json.put("Camera2_Account", Camera_data.getString("Camera2_Account", "root"));
				json.put("Camera2_Pwd", Camera_data.getString("Camera2_Pwd", ""));
				json.put("Camera2_type", Camera_data.getInt("Camera2_type", Camera_v3));
				json.put("classorgroup2", Camera_data.getInt("classorgroup2", 0));

				json.put("Camera3", Camera_data.getBoolean("Camera3", false));
				json.put("Camera3_URL", Camera_data.getString("Camera3_URL", ""));
				json.put("Camera3_Label", Camera_data.getString("Camera3_Label", "CH3"));
				json.put("Camera3_Port", Camera_data.getInt("Camera3_Port", 80));
				json.put("Camera3_Account", Camera_data.getString("Camera3_Account", "root"));
				json.put("Camera3_Pwd", Camera_data.getString("Camera3_Pwd", ""));
				json.put("Camera3_type", Camera_data.getInt("Camera3_type", Camera_v3));
				json.put("classorgroup3", Camera_data.getInt("classorgroup3", 0));

				json.put("Camera4", Camera_data.getBoolean("Camera4", false));
				json.put("Camera4_URL", Camera_data.getString("Camera4_URL", ""));
				json.put("Camera4_Label", Camera_data.getString("Camera4_Label", "CH4"));
				json.put("Camera4_Port", Camera_data.getInt("Camera4_Port", 80));
				json.put("Camera4_Account", Camera_data.getString("Camera4_Account", "root"));
				json.put("Camera4_Pwd", Camera_data.getString("Camera4_Pwd", ""));
				json.put("Camera4_type", Camera_data.getInt("Camera4_type", Camera_v3));
				json.put("classorgroup4", Camera_data.getInt("classorgroup4", 0));

				json.put("Camera5", Camera_data.getBoolean("Camera5", false));
				json.put("Camera5_URL", Camera_data.getString("Camera5_URL", ""));
				json.put("Camera5_Label", Camera_data.getString("Camera5_Label", "CH5"));
				json.put("Camera5_Port", Camera_data.getInt("Camera5_Port", 80));
				json.put("Camera5_Account", Camera_data.getString("Camera5_Account", "root"));
				json.put("Camera5_Pwd", Camera_data.getString("Camera5_Pwd", ""));
				json.put("Camera5_type", Camera_data.getInt("Camera5_type", Camera_v3));
				json.put("classorgroup5", Camera_data.getInt("classorgroup5", 0));

				json.put("Camera6", Camera_data.getBoolean("Camera6", false));
				json.put("Camera6_URL", Camera_data.getString("Camera6_URL", ""));
				json.put("Camera6_Label", Camera_data.getString("Camera6_Label", "CH6"));
				json.put("Camera6_Port", Camera_data.getInt("Camera6_Port", 80));
				json.put("Camera6_Account", Camera_data.getString("Camera6_Account", "root"));
				json.put("Camera6_Pwd", Camera_data.getString("Camera6_Pwd", ""));
				json.put("Camera6_type", Camera_data.getInt("Camera6_type", Camera_v3));
				json.put("classorgroup6", Camera_data.getInt("classorgroup6", 0));

				json.put("Camera7", Camera_data.getBoolean("Camera7", false));
				json.put("Camera7_URL", Camera_data.getString("Camera7_URL", ""));
				json.put("Camera7_Label", Camera_data.getString("Camera7_Label", "CH7"));
				json.put("Camera7_Port", Camera_data.getInt("Camera7_Port", 80));
				json.put("Camera7_Account", Camera_data.getString("Camera7_Account", "root"));
				json.put("Camera7_Pwd", Camera_data.getString("Camera7_Pwd", ""));
				json.put("Camera7_type", Camera_data.getInt("Camera7_type", Camera_v3));
				json.put("classorgroup7", Camera_data.getInt("classorgroup7", 0));

				json.put("Camera8", Camera_data.getBoolean("Camera8", false));
				json.put("Camera8_URL", Camera_data.getString("Camera8_URL", ""));
				json.put("Camera8_Label", Camera_data.getString("Camera8_Label", "CH8"));
				json.put("Camera8_Port", Camera_data.getInt("Camera8_Port", 80));
				json.put("Camera8_Account", Camera_data.getString("Camera8_Account", "root"));
				json.put("Camera8_Pwd", Camera_data.getString("Camera8_Pwd", ""));
				json.put("Camera8_type", Camera_data.getInt("Camera8_type", Camera_v3));
				json.put("classorgroup8", Camera_data.getInt("classorgroup8", 0));

				json.put("Camera9", Camera_data.getBoolean("Camera9", false));
				json.put("Camera9_URL", Camera_data.getString("Camera9_URL", ""));
				json.put("Camera9_Label", Camera_data.getString("Camera9_Label", "CH9"));
				json.put("Camera9_Port", Camera_data.getInt("Camera9_Port", 80));
				json.put("Camera9_Account", Camera_data.getString("Camera9_Account", "root"));
				json.put("Camera9_Pwd", Camera_data.getString("Camera9_Pwd", ""));
				json.put("Camera9_type", Camera_data.getInt("Camera9_type", Camera_v3));
				json.put("classorgroup9", Camera_data.getInt("classorgroup9", 0));

				json.put("Camera10", Camera_data.getBoolean("Camera10", false));
				json.put("Camera10_URL", Camera_data.getString("Camera10_URL", ""));
				json.put("Camera10_Label", Camera_data.getString("Camera10_Label", "CH10"));
				json.put("Camera10_Port", Camera_data.getInt("Camera10_Port", 80));
				json.put("Camera10_Account", Camera_data.getString("Camera10_Account", "root"));
				json.put("Camera10_Pwd", Camera_data.getString("Camera10_Pwd", ""));
				json.put("Camera10_type", Camera_data.getInt("Camera10_type", Camera_v3));
				json.put("classorgroup10", Camera_data.getInt("classorgroup10", 0));

				json.put("Camera11", Camera_data.getBoolean("Camera11", false));
				json.put("Camera11_URL", Camera_data.getString("Camera11_URL", ""));
				json.put("Camera11_Label", Camera_data.getString("Camera11_Label", "CH11"));
				json.put("Camera11_Port", Camera_data.getInt("Camera11_Port", 80));
				json.put("Camera11_Account", Camera_data.getString("Camera11_Account", "root"));
				json.put("Camera11_Pwd", Camera_data.getString("Camera11_Pwd", ""));
				json.put("Camera11_type", Camera_data.getInt("Camera11_type", Camera_v3));
				json.put("classorgroup11", Camera_data.getInt("classorgroup11", 0));

				json.put("Camera12", Camera_data.getBoolean("Camera12", false));
				json.put("Camera12_URL", Camera_data.getString("Camera12_URL", ""));
				json.put("Camera12_Label", Camera_data.getString("Camera12_Label", "CH12"));
				json.put("Camera12_Port", Camera_data.getInt("Camera12_Port", 80));
				json.put("Camera12_Account", Camera_data.getString("Camera12_Account", "root"));
				json.put("Camera12_Pwd", Camera_data.getString("Camera12_Pwd", ""));
				json.put("Camera12_type", Camera_data.getInt("Camera12_type", Camera_v3));
				json.put("classorgroup12", Camera_data.getInt("classorgroup12", 0));

				json.put("Camera13", Camera_data.getBoolean("Camera13", false));
				json.put("Camera13_URL", Camera_data.getString("Camera13_URL", ""));
				json.put("Camera13_Label", Camera_data.getString("Camera13_Label", "CH13"));
				json.put("Camera13_Port", Camera_data.getInt("Camera13_Port", 80));
				json.put("Camera13_Account", Camera_data.getString("Camera13_Account", "root"));
				json.put("Camera13_Pwd", Camera_data.getString("Camera13_Pwd", ""));
				json.put("Camera13_type", Camera_data.getInt("Camera13_type", Camera_v3));
				json.put("classorgroup13", Camera_data.getInt("classorgroup13", 0));

				json.put("Camera14", Camera_data.getBoolean("Camera14", false));
				json.put("Camera14_URL", Camera_data.getString("Camera14_URL", ""));
				json.put("Camera14_Label", Camera_data.getString("Camera14_Label", "CH14"));
				json.put("Camera14_Port", Camera_data.getInt("Camera14_Port", 80));
				json.put("Camera14_Account", Camera_data.getString("Camera14_Account", "root"));
				json.put("Camera14_Pwd", Camera_data.getString("Camera14_Pwd", ""));
				json.put("Camera14_type", Camera_data.getInt("Camera14_type", Camera_v3));
				json.put("classorgroup14", Camera_data.getInt("classorgroup14", 0));

				json.put("Camera15", Camera_data.getBoolean("Camera15", false));
				json.put("Camera15_URL", Camera_data.getString("Camera15_URL", ""));
				json.put("Camera15_Label", Camera_data.getString("Camera15_Label", "CH15"));
				json.put("Camera15_Port", Camera_data.getInt("Camera15_Port", 80));
				json.put("Camera15_Account", Camera_data.getString("Camera15_Account", "root"));
				json.put("Camera15_Pwd", Camera_data.getString("Camera15_Pwd", ""));
				json.put("Camera15_type", Camera_data.getInt("Camera15_type", Camera_v3));
				json.put("classorgroup15", Camera_data.getInt("classorgroup15", 0));

				json.put("Camera16", Camera_data.getBoolean("Camera16", false));
				json.put("Camera16_URL", Camera_data.getString("Camera16_URL", ""));
				json.put("Camera16_Label", Camera_data.getString("Camera16_Label", "CH16"));
				json.put("Camera16_Port", Camera_data.getInt("Camera16_Port", 80));
				json.put("Camera16_Account", Camera_data.getString("Camera16_Account", "root"));
				json.put("Camera16_Pwd", Camera_data.getString("Camera16_Pwd", ""));
				json.put("Camera16_type", Camera_data.getInt("Camera16_type", Camera_v3));
				json.put("classorgroup16", Camera_data.getInt("classorgroup16", 0));

				json.put("Camera17", Camera_data.getBoolean("Camera17", false));
				json.put("Camera17_URL", Camera_data.getString("Camera17_URL", ""));
				json.put("Camera17_Label", Camera_data.getString("Camera17_Label", "CH17"));
				json.put("Camera17_Port", Camera_data.getInt("Camera17_Port", 80));
				json.put("Camera17_Account", Camera_data.getString("Camera17_Account", "root"));
				json.put("Camera17_Pwd", Camera_data.getString("Camera17_Pwd", ""));
				json.put("Camera17_type", Camera_data.getInt("Camera17_type", Camera_v3));
				json.put("classorgroup17", Camera_data.getInt("classorgroup17", 0));

				json.put("Camera18", Camera_data.getBoolean("Camera18", false));
				json.put("Camera18_URL", Camera_data.getString("Camera18_URL", ""));
				json.put("Camera18_Label", Camera_data.getString("Camera18_Label", "CH18"));
				json.put("Camera18_Port", Camera_data.getInt("Camera18_Port", 80));
				json.put("Camera18_Account", Camera_data.getString("Camera18_Account", "root"));
				json.put("Camera18_Pwd", Camera_data.getString("Camera18_Pwd", ""));
				json.put("Camera18_type", Camera_data.getInt("Camera18_type", Camera_v3));
				json.put("classorgroup18", Camera_data.getInt("classorgroup18", 0));

				json.put("Camera19", Camera_data.getBoolean("Camera19", false));
				json.put("Camera19_URL", Camera_data.getString("Camera19_URL", ""));
				json.put("Camera19_Label", Camera_data.getString("Camera19_Label", "CH19"));
				json.put("Camera19_Port", Camera_data.getInt("Camera19_Port", 80));
				json.put("Camera19_Account", Camera_data.getString("Camera19_Account", "root"));
				json.put("Camera19_Pwd", Camera_data.getString("Camera19_Pwd", ""));
				json.put("Camera19_type", Camera_data.getInt("Camera19_type", Camera_v3));
				json.put("classorgroup19", Camera_data.getInt("classorgroup19", 0));

				json.put("Camera20", Camera_data.getBoolean("Camera20", false));
				json.put("Camera20_URL", Camera_data.getString("Camera20_URL", ""));
				json.put("Camera20_Label", Camera_data.getString("Camera20_Label", "CH20"));
				json.put("Camera20_Port", Camera_data.getInt("Camera20_Port", 80));
				json.put("Camera20_Account", Camera_data.getString("Camera20_Account", "root"));
				json.put("Camera20_Pwd", Camera_data.getString("Camera20_Pwd", ""));
				json.put("Camera20_type", Camera_data.getInt("Camera20_type", Camera_v3));
				json.put("classorgroup20", Camera_data.getInt("classorgroup20", 0));

				json.put("net_type", Camera_data.getInt("net_type", eth0));
				if (Camera_data.getInt("net_type", eth0) == wifi) {
					json.put("net_ip", Camera_data.getString("wifi", "192.168.1.51"));
				} else {
					json.put("net_ip", Camera_data.getString("eth0", "192.168.1.51"));
				}

				json.put("geteway", Camera_data.getString("geteway", "192.168.1.1"));
				json.put("mask", Camera_data.getString("mask", "255.255.255.0"));

				json.put("pwd_type", Camera_data.getInt("pwd_type", no_pwd));
				json.put("wifi_pwd", Camera_data.getString("wifi_pwd", ""));
				json.put("wifi_ssid", Camera_data.getString("wifi_ssid", "defaultssid"));

				json.put("video_title", Camera_data.getBoolean("video_title", true));
				json.put("video_time", Camera_data.getBoolean("video_time", true));
				json.put("login_pwd", Camera_data.getString("login_pwd", "9999"));
				json.put("hotspot", Camera_data.getString("hotspot", "001"));
				json.put("real_time", Camera_data.getInt("real_time", realtime_smooth));
				json.put("loop_on", Camera_data.getBoolean("loop_on", false));
				json.put("loop_time", Camera_data.getInt("loop_time", 20));
				
				opt.print(URLEncoder.encode("Ondemand_Setting_" + json.toString(), "utf8"));
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void sendSetting() throws RemoteException {
			try {
				// TODO Auto-generated method stub
				Log.v("stub", "send setting");
				JSONObject json = new JSONObject();
				json.put("Camera1", Camera_data.getBoolean("Camera1", false));
				json.put("Camera1_URL", Camera_data.getString("Camera1_URL", "192.168.1.171"));
				json.put("Camera1_Label", Camera_data.getString("Camera1_Label", "CH1"));
				json.put("Camera1_Port", Camera_data.getInt("Camera1_Port", 80));
				json.put("Camera1_Account", Camera_data.getString("Camera1_Account", "root"));
				json.put("Camera1_Pwd", Camera_data.getString("Camera1_Pwd", "27507522"));
				json.put("Camera1_type", Camera_data.getInt("Camera1_type", Camera_v3));
				json.put("classorgroup1", Camera_data.getInt("classorgroup1", 0));

				json.put("Camera2", Camera_data.getBoolean("Camera2", false));
				json.put("Camera2_URL", Camera_data.getString("Camera2_URL", "192.168.1.172"));
				json.put("Camera2_Label", Camera_data.getString("Camera2_Label", "CH2"));
				json.put("Camera2_Port", Camera_data.getInt("Camera2_Port", 80));
				json.put("Camera2_Account", Camera_data.getString("Camera2_Account", "root"));
				json.put("Camera2_Pwd", Camera_data.getString("Camera2_Pwd", "27507522"));
				json.put("Camera2_type", Camera_data.getInt("Camera2_type", Camera_v3));
				json.put("classorgroup2", Camera_data.getInt("classorgroup2", 0));

				json.put("Camera3", Camera_data.getBoolean("Camera3", false));
				json.put("Camera3_URL", Camera_data.getString("Camera3_URL", "192.168.1.173"));
				json.put("Camera3_Label", Camera_data.getString("Camera3_Label", "CH3"));
				json.put("Camera3_Port", Camera_data.getInt("Camera3_Port", 80));
				json.put("Camera3_Account", Camera_data.getString("Camera3_Account", "root"));
				json.put("Camera3_Pwd", Camera_data.getString("Camera3_Pwd", "27507522"));
				json.put("Camera3_type", Camera_data.getInt("Camera3_type", Camera_v3));
				json.put("classorgroup3", Camera_data.getInt("classorgroup3", 0));

				json.put("Camera4", Camera_data.getBoolean("Camera4", false));
				json.put("Camera4_URL", Camera_data.getString("Camera4_URL", "192.168.1.174"));
				// json.put("Camera4_Label",
				// Camera_data.getString("Camera4_Label", "CH4"));
				json.put("Camera4_Label", "");
				json.put("Camera4_Port", Camera_data.getInt("Camera4_Port", 80));
				json.put("Camera4_Account", Camera_data.getString("Camera4_Account", "root"));
				json.put("Camera4_Pwd", Camera_data.getString("Camera4_Pwd", "27507522"));
				json.put("Camera4_Pwd", Camera_data.getString("Camera4_Pwd", "27507522"));
				json.put("Camera4_type", Camera_data.getInt("Camera4_type", Camera_v3));
				json.put("classorgroup4", Camera_data.getInt("classorgroup4", 0));

				json.put("net_type", Camera_data.getInt("net_type", eth0));
				if (Camera_data.getInt("net_type", eth0) == wifi) {
					json.put("net_ip", Camera_data.getString("wifi", "192.168.1.51"));
				} else {
					json.put("net_ip", Camera_data.getString("eth0", "192.168.1.51"));
				}

				json.put("geteway", Camera_data.getString("geteway", "192.168.1.1"));
				json.put("mask", Camera_data.getString("mask", "255.255.255.0"));

				json.put("pwd_type", Camera_data.getInt("pwd_type", no_pwd));
				json.put("wifi_pwd", Camera_data.getString("wifi_pwd", ""));
				json.put("wifi_ssid", Camera_data.getString("wifi_ssid", "defaultssid"));

				json.put("video_title", Camera_data.getBoolean("video_title", true));
				json.put("video_time", Camera_data.getBoolean("video_time", true));
				json.put("login_pwd", Camera_data.getString("login_pwd", "9999"));
				json.put("hotspot", Camera_data.getString("hotspot", "001"));
				json.put("real_time", Camera_data.getInt("real_time", realtime_smooth));

				opt.print(URLEncoder.encode("LiveBox_Setting_" + json.toString(), "utf8"));
				fin.close();
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};

	private void registerUsbStateReceiver() {
		// Log.i(TAG, "Register usb Receiver");
		try {
			IntentFilter filter = new IntentFilter();
			filter.addAction("android.hardware.usb.action.USB_STATE");
			filter.addAction("android.hardware.action.USB_DISCONNECTED");
			filter.addAction("android.hardware.action.USB_CONNECTED");
			filter.addAction("android.intent.action.UMS_CONNECTED");
			filter.addAction("android.intent.action.UMS_DISCONNECTED");
			filter.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED);
			filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
			filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
			filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
			// filter.addDataScheme("file");
			registerReceiver(mUsbStateReceiver, filter);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 監控USB連接狀態
	private BroadcastReceiver mUsbStateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
				Toast.makeText(Remote_Service.this, "USB DETECT!!!", Toast.LENGTH_SHORT).show();
				new Thread() {
					public void run() {
						try {
							Thread.sleep(2000);

							if (usbfilePath.exists()) {
								if (usbfilePath_old.exists()) {
									if (usbfilePath != usbfilePath_old) {
										usbpath = usbpath_old;
										usbfilePath = usbfilePath_old;
									}
								}
							}

							apkPath = usbfilePath.getPath() + "/" + apkName;
							File livebox_file = new File(apkPath);

							Log.v("test", "get usb test");
							if (livebox_file.exists()) {
								Log.v("test", "livebox");
								checkVNupdatePG();
							} else {
								Log.v("test", "nolivebox");
							}

							dataPath = usbfilePath.getPath() + "/" + dataName;
							File data_file = new File(dataPath);
							if (data_file.exists()) {
								Log.v("test", "livebox.bin");
								BufferedReader br = new BufferedReader(new FileReader(data_file));
								String line;
								while ((line = br.readLine()) != null) {
									if (line.indexOf("LIVEBOX_SEND_") != -1) {
										String cmd = URLDecoder.decode(line.replace("LIVEBOX_SEND_", ""), "utf8");
										getjsondata(cmd, 1);
									}
								}
								br.close();
							} else {
								Log.v("test", "nolivebox.bin");
							}

							debugPath = usbfilePath.getPath() + "/" + debugName;
							File debug_file = new File(debugPath);
							if (debug_file.exists()) {
								debug = true;
							} else {
								debug = false;
							}

							initPath = usbfilePath.getPath() + "/" + initName;
							File init_file = new File(initPath);
							if (init_file.exists()) {
								if (Camera_data.getInt("activity", 0x1a) == 0x1a) {
									SharedPreferences.Editor editor = Camera_data.edit();
									editor.clear();
									editor.commit();
									Message msg = new Message();
									msg.arg1 = 0x04;
									LiveBox_Activity.Change_iCam.sendMessage(msg);
								} else if (Camera_data.getInt("activity", 0x1a) == 0x1b) {
									SharedPreferences.Editor editor = Camera_data.edit();
									editor.clear();
									editor.putInt("activity", 0x1b);
									editor.commit();
									Message msg = new Message();
									msg.arg1 = 0x02;
									Setting_Activity.Change_DATA.sendMessage(msg);
								}
							}

							File licdatakeyPath = new File(usbpath + "licdata_livebox.key");
							File licdatakeyPath_pro = new File(usbpath + "licdata_livebox+.key");

							if (licdatakeyPath.exists()) {
								if (Camera_data.getBoolean("OnDemand", false)) {
									if (Camera_data.getInt("activity", 0x1a) == 0x1a) {
										Message msg = new Message();
										msg.arg1 = 0x05;
										Ondemand_LiveBox_Activity.Change_iCam.sendMessage(msg);
									} else if (Camera_data.getInt("activity", 0x1a) == 0x1b) {
										Message msg = new Message();
										msg.arg1 = 0x03;
										Ondemand_Setting_Activity.Change_DATA.sendMessage(msg);
									}
								}
							} else if (licdatakeyPath_pro.exists()) {
								if (!Camera_data.getBoolean("OnDemand", false)) {
									if (Camera_data.getInt("activity", 0x1a) == 0x1a) {
										Message msg = new Message();
										msg.arg1 = 0x05;
										LiveBox_Activity.Change_iCam.sendMessage(msg);
									} else if (Camera_data.getInt("activity", 0x1a) == 0x1b) {
										Message msg = new Message();
										msg.arg1 = 0x03;
										Setting_Activity.Change_DATA.sendMessage(msg);

									}
								}
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}.start();
			} else {
				if (debug) {
					debug = false;
				}
			}
		}
	};

	/* 檢查版本 versionName */
	private void checkVNupdatePG() {

		try {
			PackageManager pm = getPackageManager();
			/* NewVer */
			PackageInfo info = pm.getPackageArchiveInfo(apkPath, 0);
			int newVersionCode = info.versionCode;
			String newVersionName = info.versionName;

			// pm.setInstallerPackageName(targetPackage, installerPackageName)

			/* NowVer */
			info = pm.getPackageInfo(getPackageName(), 0);
			int nowVersionCode = info.versionCode;
			String nowVersionName = info.versionName;

			Log.d("APK", "LIVEBOX+.apk = " + "VersionCode : " + newVersionCode + ", VersionName : " + newVersionName);
			Log.d("APK", "目前 = " + "VersionCode : " + nowVersionCode + ", VersionName : " + nowVersionName);

			if ((newVersionCode > nowVersionCode)
					|| (Float.parseFloat(newVersionName) > Float.parseFloat(nowVersionName))) {
				/* 自動更新 需ROOT權限 */
				try {
					Process localProcess = Runtime.getRuntime().exec("su");
					OutputStream os = localProcess.getOutputStream();
					DataOutputStream dos = new DataOutputStream(os);
					dos.writeBytes("pm install -r " + apkPath + "\n");
					dos.flush();
					dos.writeBytes("exit\n");
					dos.flush();
				} catch (Exception e) {
					Log.e("APK", "update erroe", e);
					// Log.d(TAG,"no root");
				}

			}
		} catch (Exception ex) {
			Log.d("APK", "install error", ex);
		}
	}

	private void unRegisterUsbStateReceiver() {
		// Log.i(TAG, "UnRegister usb Receiver");
		try {
			if (mUsbStateReceiver != null) {
				unregisterReceiver(mUsbStateReceiver);
			}
			mUsbStateReceiver = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		Log.v("service", "service close");
		unRegisterUsbStateReceiver();
		service_start = false;
		run = false;
		if (socket.isAlive()) {
			socket.interrupt();
		}
		if (time_thread.isAlive()) {
			time_thread.interrupt();
		}
		super.onDestroy();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.d("TestService", "onUnbind");
		onDestroy();
		return super.onUnbind(intent);
	}

}
