package tw.com.blueeyes.LiveBox;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class Setting_Activity extends Activity {
	SharedPreferences Camera_data;
	LinearLayout content;
	View cam_setview, net_setview, log_setview;
	Spinner cam_type, videoport, net, ssid;
	ArrayAdapter<String> sipnneradapter, class_adapter, group_adapter;
	Button btn1, btn2, btn3, btn4, btn_setting, btn_log;
	Button save, cancel, check_connect;
	Button net_save, net_cancel;
	EditText URL, label, port, account, pwd;
	EditText e_ip1, e_ip2, e_ip3, e_ip4;
	EditText e_gateway_ip1, e_gateway_ip2, e_gateway_ip3, e_gateway_ip4;
	EditText e_mask_ip1, e_mask_ip2, e_mask_ip3, e_mask_ip4, e_wifi_pwd;
	EditText e_loginpwd, e_hotspot;
	LinearLayout intentlayout;
	TextView tv_cg, log_text, tv_ssid, tv_wifipwd, tv_ver, tv_hotspot;
	Switch ative, title_ative, time_ative;
	String iptext[];
	LinearLayout hotspot_layout;
	RadioButton smooth, close, general;
	final int btn_cam1 = 0x01;
	final int btn_cam2 = 0x02;
	final int btn_cam3 = 0x03;
	final int btn_cam4 = 0x04;
	final int btn_set = 0x05;
	final int btn_logtype = 0x06;
	int btn_type = 0x00;
	int Camera_Type = 0;
	int Camera_v2 = 0x00;
	int Camera_v3 = 0x01;
	int SES_class = 0x02;
	int SES_group = 0x03;
	private String[] icam = { "iCam v2", "iCam v3", "SES", "SES iFollow" };
	private String[] class_port = { "1", "2", "3", "4" , "5" , "6" };
	private String[] group_port =  { "1", "2", "3" };
//	private String[] class_port = new String[100];
//	private String[] group_port =  new String[100];
	private String[] connect_type = { "Wifi", "Ethernet" };
	private ArrayAdapter<String> net_adapter;
	String label_icam1, label_icam2, label_icam3, label_icam4;
	boolean icam1_on, icam2_on, icam3_on, icam4_on;
	boolean title_on, time_on;
	String URL_1, URL_2, URL_3, URL_4;
	int port1, port2, port3, port4;
	String account1, account2, account3, account4;
	String pwd1, pwd2, pwd3, pwd4;
	int icam_type1, icam_type2, icam_type3, icam_type4;
	int corg_num1, corg_num2, corg_num3, corg_num4;
	int wifi_ip1, wifi_ip2, wifi_ip3, wifi_ip4;
	int eth0_ip1, eth0_ip2, eth0_ip3, eth0_ip4;
	int gateway_ip1, gateway_ip2, gateway_ip3, gateway_ip4;
	int mask_ip1, mask_ip2, mask_ip3, mask_ip4;
	String wifi_ip[], eth0_ip[], gateway_ip[], mask_ip[];
	String wifi_pwd, wifi_ssid, hotspot_ssid;
	final int no_pwd = 0x00;
	final int WEP = 0xa0;
	final int WPA2 = 0xb0;
	final int WPA = 0xc0;
	int pwd_type;
	final int wifi = 0x00;
	final int eth0 = 0x01;
	int net_type = eth0;
	ListView loglist;
	View loadMoreView;
	Button loadMoreButton, cleanlog, cleansetting;
	ListViewAdapter adapter;
	WIFI_Adapter wifi_adapter;
	WifiManager wifi_magager;
	WifiInfo wifiInfo;
	int wifi_size;
	List results;
	private int visibleLastIndex = 0; // 最后的可视项索引
	private int visibleItemCount = 0; // 当前窗口可见项总数
	ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
	private Handler handler = new Handler();
	ArrayList<String> items;
	boolean connect = false;
	String login_account, login_pwd;
	static Handler Change_DATA;
	int countintent = 0;
	boolean icam1 = false, icam2 = false, icam3 = false, icam4 = false;
	boolean user_change = false;
	int real_time = 0x00;
	final int realtime_close = 0x0a;
	final int realtime_general = 0x0b;
	final int realtime_smooth = 0x0c;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 全畫面
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		 WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// 標題隱藏
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (Build.VERSION.RELEASE.equals("4.4.2")) {
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		} 

		setContentView(R.layout.activity_setting);

		Camera_data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		startService(new Intent(Setting_Activity.this, Remote_Service.class));

		btn1 = (Button) findViewById(R.id.cam1);
		btn2 = (Button) findViewById(R.id.cam2);
		btn3 = (Button) findViewById(R.id.cam3);
		btn4 = (Button) findViewById(R.id.cam4);
		btn_setting = (Button) findViewById(R.id.set);
		btn_log = (Button) findViewById(R.id.log_btn);

		content = (LinearLayout) findViewById(R.id.setting_content);
		intentlayout = (LinearLayout) findViewById(R.id.intentlayout);
		save = (Button) findViewById(R.id.save);
		cancel = (Button) findViewById(R.id.cancel);
		tv_ver = (TextView) findViewById(R.id.version);

		try {
			PackageManager pm = getPackageManager();
			/* NowVer */
			PackageInfo info;
			info = pm.getPackageInfo(getPackageName(), 0);
			String nowVersionName = info.versionName;
			tv_ver.setText("Ver" + nowVersionName);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		for (int i = 0; i < 100; i++) {
//			class_port[i] = String.valueOf(i + 1);
//			group_port[i] = String.valueOf(i + 1);
//		}
		
		wifi_magager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		wifiInfo = wifi_magager.getConnectionInfo();

		SharedPreferences.Editor editor = Camera_data.edit();
		editor.putInt("activity", 0x1b);
		editor.commit();

		cam_content();
		load();
		setBtn();
		btn1.performClick();

		Change_DATA = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.arg1) {
				case 0x01:
					Intent intent = new Intent();
					if(Camera_data.getBoolean("OnDemand", false)) {
						intent.setClass(Setting_Activity.this, Ondemand_LiveBox_Activity.class);
					} else {
						intent.setClass(Setting_Activity.this, LiveBox_Activity.class);
					}
					startActivity(intent);
					finish();
					break;
				case 0x02:
					load();
					if (btn_type == btn_cam1) {
						btn_type = 0;
						btn1.performClick();
					} else if (btn_type == btn_cam2) {
						btn_type = 0;
						btn2.performClick();
					} else if (btn_type == btn_cam3) {
						btn_type = 0;
						btn3.performClick();
					} else if (btn_type == btn_cam4) {
						btn_type = 0;
						btn4.performClick();
					} else if (btn_type == btn_set) {
						btn_type = 0;
						btn_setting.performClick();
					}
				case 0x03:
					Intent intent1 = new Intent();
					intent1.setClass(Setting_Activity.this, Loading_Activity.class);
					startActivity(intent1);
					finish();
					break;
				}
			}
		};
	}

	private void load() {
		icam1_on = Camera_data.getBoolean("Camera1", false);
		URL_1 = Camera_data.getString("Camera1_URL", "192.168.1.171");
		label_icam1 = Camera_data.getString("Camera1_Label", "CH1");
		port1 = Camera_data.getInt("Camera1_Port", 80);
		account1 = Camera_data.getString("Camera1_Account", "root");
		pwd1 = Camera_data.getString("Camera1_Pwd", "27507522");
		icam_type1 = Camera_data.getInt("Camera1_type", Camera_v3);
		corg_num1 = Camera_data.getInt("classorgroup1", 0);
		if(corg_num1 > 6) {
			corg_num1 = 0;
		}

		icam2_on = Camera_data.getBoolean("Camera2", false);
		URL_2 = Camera_data.getString("Camera2_URL", "192.168.1.172");
		label_icam2 = Camera_data.getString("Camera2_Label", "CH2");
		port2 = Camera_data.getInt("Camera2_Port", 80);
		account2 = Camera_data.getString("Camera2_Account", "root");
		pwd2 = Camera_data.getString("Camera2_Pwd", "27507522");
		icam_type2 = Camera_data.getInt("Camera2_type", Camera_v3);
		corg_num2 = Camera_data.getInt("classorgroup2", 0);
		if(corg_num2 > 6) {
			corg_num2 = 0;
		}
		
		icam3_on = Camera_data.getBoolean("Camera3", false);
		URL_3 = Camera_data.getString("Camera3_URL", "192.168.1.173");
		label_icam3 = Camera_data.getString("Camera3_Label", "CH3");
		port3 = Camera_data.getInt("Camera3_Port", 80);
		account3 = Camera_data.getString("Camera3_Account", "root");
		pwd3 = Camera_data.getString("Camera3_Pwd", "27507522");
		icam_type3 = Camera_data.getInt("Camera3_type", Camera_v3);
		corg_num3 = Camera_data.getInt("classorgroup3", 0);
		if(corg_num3 > 6) {
			corg_num3 = 0;
		}
		
//		icam4_on = Camera_data.getBoolean("Camera4", false);
//		URL_4 = Camera_data.getString("Camera4_URL", "192.168.1.174");
//		label_icam4 = Camera_data.getString("Camera4_Label", "CH4");
//		port4 = Camera_data.getInt("Camera4_Port", 80);
//		account4 = Camera_data.getString("Camera4_Account", "root");
//		pwd4 = Camera_data.getString("Camera4_Pwd", "27507522");
//		icam_type4 = Camera_data.getInt("Camera4_type", Camera_v3);
//		corg_num4 = Camera_data.getInt("classorgroup4", 0);

		title_on = Camera_data.getBoolean("video_title", true);
		time_on = Camera_data.getBoolean("video_time", true);

		net_type = Camera_data.getInt("net_type", eth0);
		wifi_ip = (Camera_data.getString("wifi", "192.168.1.51")).split("\\.");
		eth0_ip = (Camera_data.getString("eth0", "192.168.1.51")).split("\\.");
		gateway_ip = (Camera_data.getString("geteway", "192.168.1.1")).split("\\.");
		mask_ip = (Camera_data.getString("mask", "255.255.255.0")).split("\\.");

		pwd_type = Camera_data.getInt("pwd_type", no_pwd);
		wifi_pwd = Camera_data.getString("wifi_pwd", "");
		wifi_ssid = Camera_data.getString("wifi_ssid", "defaultssid");
		hotspot_ssid = Camera_data.getString("hotspot", "001");
		// login_account = Camera_data.getString("login_account", "root");
		login_pwd = Camera_data.getString("login_pwd", "9999");
		real_time = Camera_data.getInt("real_time", realtime_smooth);
	}

	public void save_tem() {
		if (btn_type == btn_cam1) {
			icam1_on = ative.isChecked();
			URL_1 = URL.getText().toString();
			label_icam1 = label.getText().toString();
			port1 = Integer.valueOf(port.getText().toString()).intValue();
			account1 = account.getText().toString();
			pwd1 = pwd.getText().toString();
			Log.v("test", account1);
			icam_type1 = cam_type.getSelectedItemPosition();
			if (icam_type1 < SES_class) {
				corg_num1 = -1;
			} else {
				corg_num1 = videoport.getSelectedItemPosition();
			}

		} else if (btn_type == btn_cam2) {
			icam2_on = ative.isChecked();
			URL_2 = URL.getText().toString();
			label_icam2 = label.getText().toString();
			port2 = Integer.valueOf(port.getText().toString()).intValue();
			account2 = account.getText().toString();
			pwd2 = pwd.getText().toString();
			icam_type2 = cam_type.getSelectedItemPosition();
			if (icam_type2 < SES_class) {
				corg_num2 = -1;
			} else {
				corg_num2 = videoport.getSelectedItemPosition();
			}
		} else if (btn_type == btn_cam3) {
			icam3_on = ative.isChecked();
			URL_3 = URL.getText().toString();
			label_icam3 = label.getText().toString();
			port3 = Integer.valueOf(port.getText().toString()).intValue();
			account3 = account.getText().toString();
			pwd3 = pwd.getText().toString();
			icam_type3 = cam_type.getSelectedItemPosition();
			if (icam_type3 < SES_class) {
				corg_num3 = -1;
			} else {
				corg_num3 = videoport.getSelectedItemPosition();
			}
		} else if (btn_type == btn_cam4) {
			icam4_on = ative.isChecked();
			URL_4 = URL.getText().toString();
			label_icam4 = label.getText().toString();
			port4 = Integer.valueOf(port.getText().toString()).intValue();
			account4 = account.getText().toString();
			pwd4 = pwd.getText().toString();
			icam_type4 = cam_type.getSelectedItemPosition();
			if (icam_type4 < SES_class) {
				corg_num4 = -1;
			} else {
				corg_num4 = videoport.getSelectedItemPosition();
			}
		} else if (btn_type == btn_set) {
			if (net_type == wifi) {
				wifi_ip[0] = e_ip1.getText().toString();
				wifi_ip[1] = e_ip2.getText().toString();
				wifi_ip[2] = e_ip3.getText().toString();
				wifi_ip[3] = e_ip4.getText().toString();
			} else {
				eth0_ip[0] = e_ip1.getText().toString();
				eth0_ip[1] = e_ip2.getText().toString();
				eth0_ip[2] = e_ip3.getText().toString();
				eth0_ip[3] = e_ip4.getText().toString();
			}

			gateway_ip[0] = e_gateway_ip1.getText().toString();
			gateway_ip[1] = e_gateway_ip2.getText().toString();
			gateway_ip[2] = e_gateway_ip3.getText().toString();
			gateway_ip[3] = e_gateway_ip4.getText().toString();

			mask_ip[0] = e_mask_ip1.getText().toString();
			mask_ip[1] = e_mask_ip2.getText().toString();
			mask_ip[2] = e_mask_ip3.getText().toString();
			mask_ip[3] = e_mask_ip4.getText().toString();

			// login_account = account.getText().toString();
			login_pwd = e_loginpwd.getText().toString();
			hotspot_ssid = e_hotspot.getText().toString();
		}
	}

	public void load_tem() {
		user_change = false;
		if (btn_type == btn_cam1) {
			if (cam_type.getSelectedItemPosition() == icam_type1) {
				if (icam_type1 >= SES_class) {
					videoport.setSelection(corg_num1);
				}
				user_change = true;
			}
			cam_type.setSelection(icam_type1);
			ative.setChecked(icam1_on);
			pwd.setText(pwd1);
			URL.setText(URL_1);
			label.setText(label_icam1);
			account.setText(account1);
			port.setText(String.valueOf(port1));
		} else if (btn_type == btn_cam2) {
			if (cam_type.getSelectedItemPosition() == icam_type2) {
				if (icam_type2 >= SES_class) {
					videoport.setSelection(corg_num2);
				}
				user_change = true;
			}
			cam_type.setSelection(icam_type2);
			ative.setChecked(icam2_on);
			URL.setText(URL_2);
			label.setText(label_icam2);
			pwd.setText(pwd2);
			account.setText(account2);
			port.setText(String.valueOf(port2));
		} else if (btn_type == btn_cam3) {
			if (cam_type.getSelectedItemPosition() == icam_type3) {
				if (icam_type3 >= SES_class) {
					videoport.setSelection(corg_num3);
				}
				user_change = true;
			}
			cam_type.setSelection(icam_type3);
			ative.setChecked(icam3_on);
			URL.setText(URL_3);
			label.setText(label_icam3);
			pwd.setText(pwd3);
			account.setText(account3);
			port.setText(String.valueOf(port3));
		}
		// else if (btn_type == btn_cam4) {
		// cam_type.setSelection(icam_type4);
		// if (icam_type4 > Camera_v3) {
		// videoport.setSelection(corg_num4);
		// }
		// ative.setChecked(icam4_on);
		// URL.setText(URL_4);
		// label.setText(label_icam4);
		// pwd.setText(pwd4);
		// account.setText(account4);
		// port.setText(String.valueOf(port4));
		// }
	}

	private void cam_content() {
		cam_setview = getLayoutInflater().inflate(R.layout.cam_setting, null);

		cam_type = (Spinner) cam_setview.findViewById(R.id.icam_spinner);
		sipnneradapter = new ArrayAdapter<String>(this, R.layout.myspinner, icam);
		sipnneradapter.setDropDownViewResource(R.layout.myspinner);
		cam_type.setAdapter(sipnneradapter);

		class_adapter = new ArrayAdapter<String>(this, R.layout.myspinner, class_port);
		class_adapter.setDropDownViewResource(R.layout.myspinner);
		group_adapter = new ArrayAdapter<String>(this, R.layout.myspinner, group_port);
		group_adapter.setDropDownViewResource(R.layout.myspinner);

		URL = (EditText) cam_setview.findViewById(R.id.url);
		label = (EditText) cam_setview.findViewById(R.id.label);
		port = (EditText) cam_setview.findViewById(R.id.port);
		account = (EditText) cam_setview.findViewById(R.id.account);
		pwd = (EditText) cam_setview.findViewById(R.id.pwd);
		ative = (Switch) cam_setview.findViewById(R.id.enable);
		tv_cg = (TextView) cam_setview.findViewById(R.id.tv_classgroup);
		videoport = (Spinner) cam_setview.findViewById(R.id.cg_spinner);
		check_connect = (Button) cam_setview.findViewById(R.id.test);

		content.removeAllViews();
		content.addView(cam_setview, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

		check_connect.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				check_connect();
			}
		});

		cam_type.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				Log.v("button", "get ses position" + position);
				if (position < SES_class) {
					if (tv_cg.getVisibility() == View.VISIBLE) {
						tv_cg.setVisibility(View.GONE);
						videoport.setVisibility(View.GONE);
						if (user_change) {
							port.setText("80");
							account.setText("root");
						}
					}
				} else {
					if (tv_cg.getVisibility() == View.GONE) {
						tv_cg.setVisibility(View.VISIBLE);
						videoport.setVisibility(View.VISIBLE);
					}

					if (position == SES_class) {
						tv_cg.setText("Channel");
						videoport.setSelection(0);
						videoport.setAdapter(class_adapter);
						if (!user_change) {
							if (btn_type == btn_cam1) {
								videoport.setSelection(corg_num1);
							} else if (btn_type == btn_cam2) {
								videoport.setSelection(corg_num2);
							} else if (btn_type == btn_cam3) {
								videoport.setSelection(corg_num3);
							} else if (btn_type == btn_cam4) {
								videoport.setSelection(corg_num4);
							}
						}
					} else {
						tv_cg.setText("Group");
						videoport.setSelection(0);
						videoport.setAdapter(group_adapter);

						if (!user_change) {
							if (btn_type == btn_cam1) {
								videoport.setSelection(corg_num1);
							} else if (btn_type == btn_cam2) {
								videoport.setSelection(corg_num2);
							} else if (btn_type == btn_cam3) {
								videoport.setSelection(corg_num3);
							} else if (btn_type == btn_cam4) {
								videoport.setSelection(corg_num4);
							}
						}
					}

					if (user_change) {
						port.setText("8888");
						account.setText("admin");
					}
				}
				user_change = true;
			}

			@Override
			public void onNothingSelected(AdapterView parent) {
			}
		});
	}

	private void check_connect() {
		new Thread() {
			public void run() {
				connect = false;
				try {
					if (cam_type.getSelectedItemPosition() < SES_class) {
						icamcheck();
					} else if (cam_type.getSelectedItemPosition() == SES_class) {
						ses("Class", videoport.getSelectedItemPosition() + 1);
					} else if (cam_type.getSelectedItemPosition() == SES_group) {
						ses("Group", videoport.getSelectedItemPosition() + 1);
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Message msg;
					String obj = channel_set() + "Connect timeout";
					msg = _handler.obtainMessage(1, obj);
					_handler.sendMessage(msg);
				}
			}
		}.start();
	}

	public String channel_set() {
		if (cam_type.getSelectedItemPosition() == SES_class) {
			return "IP: " + URL.getText().toString() + "  " + "Port: " + port.getText().toString() + "  "
					+ "Type: SES" + "\r\n";
		} else if (cam_type.getSelectedItemPosition() == SES_group) {
			return "IP: " + URL.getText().toString() + "  " + "Port: " + port.getText().toString() + "  "
					+ "Type: iFollow" + "\r\n";
		} else if (cam_type.getSelectedItemPosition() == Camera_v3) {
			return "IP: " + URL.getText().toString() + "  " + "Port: " + port.getText().toString() + "  "
					+ "Type: iCam v3" + "\r\n";
		} else if (cam_type.getSelectedItemPosition() == Camera_v2) {
			return "IP: " + URL.getText().toString() + "  " + "Port: " + port.getText().toString() + "  "
					+ "Type: iCam v2" + "\r\n";
		} else {
			return "";
		}
	}

	public void icamcheck() throws Exception {
		// 1. 獲取並設置url地址，一個字符串變量，一個URL對象
		String urlStr;
		if (cam_type.getSelectedItemPosition() == Camera_v2) {
			urlStr = "http://" + URL.getText().toString() + "/ipcam/stream.cgi?nowprofileid=2&audiostream=1";
		} else {
			urlStr = "http://" + URL.getText().toString() + "/ipcam/stream.cgi";
		}
		URL url = new URL(urlStr);
		// 2. 創建一個密碼證書，(UsernamePasswordCredentials類)
		String username = account.getText().toString();
		String password = pwd.getText().toString();
		UsernamePasswordCredentials upc = new UsernamePasswordCredentials(username, password);
		// 3. 設置認證範圍 (AuthScore類)
		String strRealm = "iCam1080";
		String strHost = url.getHost();
		int iPort = url.getPort();
		AuthScope as = new AuthScope(strHost, iPort, strRealm);
		// 4. 創建認證提供者(BasicCredentials類) ，基於as和upc
		BasicCredentialsProvider bcp = new BasicCredentialsProvider();
		bcp.setCredentials(as, upc);
		// 5. 創建Http客戶端(DefaultHttpClient類)
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 15000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 15000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		DefaultHttpClient client = new DefaultHttpClient(httpParameters);
		// 6. 為此客戶端設置認證提供者
		client.setCredentialsProvider(bcp);
		// 7. 創建一個get 方法(HttpGet類)，基於所訪問的url地址
		HttpGet hg = new HttpGet(urlStr);
		// 8. 執行get方法並返回 response
		HttpResponse hr = client.execute(hg);
		Log.v("http getdata", "hr code" + hr.getStatusLine().getStatusCode());
		// 9. 從response中取回數據，使用InputStreamReader讀取Response的Entity：
		if (hr.getStatusLine().getStatusCode() == 200) {
			Message msg;
			String obj = channel_set() + "Connect Sucess";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		} else if (hr.getStatusLine().getStatusCode() == 401) {
			Message msg;
			String obj = channel_set() + "Account or password not correct";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		} else if (hr.getStatusLine().getStatusCode() == 404) {
			Message msg;
			String obj = channel_set() + "Connect fail";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		}
	}

	public void ses(String GorC, int num) throws Exception {
		String urlstr = "http://" + URL.getText().toString() + ":" + port.getText().toString() + "/sesstream.cgi?"
				+ GorC + "=" + num;
		String username = account.getText().toString();
		String password = pwd.getText().toString();
		HttpGet mGet = new HttpGet(urlstr);
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 15000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 15000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

		DefaultHttpClient mHttpClient = new DefaultHttpClient(httpParameters);
		mGet.setHeader("Authorization",
				"Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP));
		HttpResponse hr = mHttpClient.execute(mGet);
		int res = hr.getStatusLine().getStatusCode();
		Log.v("http getdata", "hr code" + res);
		if (res == 200) {
			Message msg;
			String obj = channel_set() + "Connect Sucess";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		} else if (res == 401) {
			Message msg;
			String obj = channel_set() + "Account or password not correct";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		} else if (res == 404) {
			Message msg;
			String obj = channel_set() + "Connect fail";
			msg = _handler.obtainMessage(1, obj);
			_handler.sendMessage(msg);
		}
	}

	private Handler _handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			new AlertDialog.Builder(Setting_Activity.this).setTitle("Info").setMessage((String) msg.obj)
					.setPositiveButton("OK", null).show();
		}
	};

	private void ethernet_content() {
		net_setview = getLayoutInflater().inflate(R.layout.ethernet_setting, null);
		title_ative = (Switch) net_setview.findViewById(R.id.title_enable);
		time_ative = (Switch) net_setview.findViewById(R.id.time_enable);
		cleanlog = (Button) net_setview.findViewById(R.id.cleanlog);
		cleansetting = (Button) net_setview.findViewById(R.id.cleanpre);
		// account = (EditText) net_setview.findViewById(R.id.login_account);
		e_loginpwd = (EditText) net_setview.findViewById(R.id.login_pwd);
		tv_hotspot = (TextView) net_setview.findViewById(R.id.tv_hotspot);
		e_hotspot = (EditText) net_setview.findViewById(R.id.hotspot_num);
		hotspot_layout = (LinearLayout) net_setview.findViewById(R.id.hotspot_layout);

		e_hotspot.setText(hotspot_ssid);
		title_ative.setChecked(title_on);
		time_ative.setChecked(time_on);
		net = (Spinner) net_setview.findViewById(R.id.net_spinner);
		e_ip1 = (EditText) net_setview.findViewById(R.id.static_ip1);
		e_ip2 = (EditText) net_setview.findViewById(R.id.static_ip2);
		e_ip3 = (EditText) net_setview.findViewById(R.id.static_ip3);
		e_ip4 = (EditText) net_setview.findViewById(R.id.static_ip4);

		e_gateway_ip1 = (EditText) net_setview.findViewById(R.id.gateway_ip1);
		e_gateway_ip2 = (EditText) net_setview.findViewById(R.id.gateway_ip2);
		e_gateway_ip3 = (EditText) net_setview.findViewById(R.id.gateway_ip3);
		e_gateway_ip4 = (EditText) net_setview.findViewById(R.id.gateway_ip4);

		e_mask_ip1 = (EditText) net_setview.findViewById(R.id.submask_ip1);
		e_mask_ip2 = (EditText) net_setview.findViewById(R.id.submask_ip2);
		e_mask_ip3 = (EditText) net_setview.findViewById(R.id.submask_ip3);
		e_mask_ip4 = (EditText) net_setview.findViewById(R.id.submask_ip4);

		tv_ssid = (TextView) net_setview.findViewById(R.id.tv_ssid);
		ssid = (Spinner) net_setview.findViewById(R.id.ssid_spinner);
		tv_wifipwd = (TextView) net_setview.findViewById(R.id.tv_wifipwd);
		e_wifi_pwd = (EditText) net_setview.findViewById(R.id.wifipwd);

		close = (RadioButton) net_setview.findViewById(R.id.r_close);
		general = (RadioButton) net_setview.findViewById(R.id.r_general);
		smooth = (RadioButton) net_setview.findViewById(R.id.r_smooth);
		
		wifi_adapter = new WIFI_Adapter(getApplicationContext(), list);
		ssid.setAdapter(wifi_adapter);

		net_adapter = new ArrayAdapter<String>(this, R.layout.myspinner, connect_type);
		net.setAdapter(net_adapter);
		net.setSelection(net_type);

		e_gateway_ip1.setText(gateway_ip[0]);
		e_gateway_ip2.setText(gateway_ip[1]);
		e_gateway_ip3.setText(gateway_ip[2]);
		e_gateway_ip4.setText(gateway_ip[3]);

		e_mask_ip1.setText(mask_ip[0]);
		e_mask_ip2.setText(mask_ip[1]);
		e_mask_ip3.setText(mask_ip[2]);
		e_mask_ip4.setText(mask_ip[3]);

		// account.setText(login_account);
		e_loginpwd.setText(login_pwd);

		if(net_type == wifi) {
			e_ip1.setText(wifi_ip[0]);
			e_ip2.setText(wifi_ip[1]);
			e_ip3.setText(wifi_ip[2]);
			e_ip4.setText(wifi_ip[3]);
		} else {
			e_ip1.setText(eth0_ip[0]);
			e_ip2.setText(eth0_ip[1]);
			e_ip3.setText(eth0_ip[2]);
			e_ip4.setText(eth0_ip[3]);
		}
		
		
		ssid.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				wifi_ssid = (String) list.get(position).get("SSID");
				if (((String) list.get(position).get("PWD_TYPE")).indexOf("WEP") != -1) {
					pwd_type = WEP;
				} else if (((String) list.get(position).get("PWD_TYPE")).indexOf("WPA2") != -1) {
					pwd_type = WPA2;
				} else if (((String) list.get(position).get("PWD_TYPE")).indexOf("WPA") != -1) {
					pwd_type = WPA;
				} else {
					pwd_type = no_pwd;
				}

				if (pwd_type != no_pwd) {
					if (tv_wifipwd.getVisibility() == View.GONE) {
						tv_wifipwd.setVisibility(View.VISIBLE);
						e_wifi_pwd.setVisibility(View.VISIBLE);
						e_wifi_pwd.setText(wifi_pwd);
					}
				} else {
					if (tv_wifipwd.getVisibility() == View.VISIBLE) {
						tv_wifipwd.setVisibility(View.GONE);
						e_wifi_pwd.setVisibility(View.GONE);
						e_wifi_pwd.setText("");
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		net.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView parent, View v, int position, long id) {
				if (position == wifi) {
					tv_hotspot.setVisibility(View.GONE);
					hotspot_layout.setVisibility(View.GONE);
					net_type = wifi;
					e_ip1.setText(wifi_ip[0]);
					e_ip2.setText(wifi_ip[1]);
					e_ip3.setText(wifi_ip[2]);
					e_ip4.setText(wifi_ip[3]);

					if (tv_ssid.getVisibility() == View.GONE) {
						tv_ssid.setVisibility(View.VISIBLE);
						ssid.setVisibility(View.VISIBLE);
					}

					if (ApManager.isApOn(Setting_Activity.this)) {
						ApManager.configApState(Setting_Activity.this);
					}

					if (!wifi_magager.isWifiEnabled()) {
						wifi_magager.setWifiEnabled(true);
					}

					wifi_magager.startScan();

					registerReceiver(wifi_receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

				} else {
					tv_hotspot.setVisibility(View.VISIBLE);
					hotspot_layout.setVisibility(View.VISIBLE);
					net_type = eth0;
					if (tv_ssid.getVisibility() == View.VISIBLE) {
						tv_ssid.setVisibility(View.GONE);
						ssid.setVisibility(View.GONE);
						e_wifi_pwd.setVisibility(View.GONE);
					}
					e_ip1.setText(eth0_ip[0]);
					e_ip2.setText(eth0_ip[1]);
					e_ip3.setText(eth0_ip[2]);
					e_ip4.setText(eth0_ip[3]);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		title_ative.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				title_on = isChecked;
			}
		});

		time_ative.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				time_on = isChecked;
			}
		});

		cleanlog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				MyLog.delFile();
				Toast.makeText(Setting_Activity.this, "Log clear!!!", Toast.LENGTH_SHORT).show();
			}
		});

		cleansetting.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				SharedPreferences.Editor editor = Camera_data.edit();
				editor.clear();
				editor.commit();
				load();
				btn_type = 0;
				btn_setting.performClick();
				Toast.makeText(Setting_Activity.this, "setting clear!!!", Toast.LENGTH_SHORT).show();
			}
		});

		if(real_time == realtime_close) {
			close.setChecked(true);
		} else if(real_time == realtime_general) {
			general.setChecked(true);
		} else if (real_time == realtime_smooth) {
			smooth.setChecked(true);
		}
		
		content.removeAllViews();
		content.addView(net_setview, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	}

	private void log_content() {
		log_setview = getLayoutInflater().inflate(R.layout.log, null);

		loadMoreView = getLayoutInflater().inflate(R.layout.load_more, null);
		loadMoreButton = (Button) loadMoreView.findViewById(R.id.loadMoreButton);

		loglist = (ListView) log_setview.findViewById(R.id.list);
		loglist.addFooterView(loadMoreView);
		items = new ArrayList<String>();
		initAdapter();
		loglist.setAdapter(adapter);

		loglist.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemcount, int totalItemCount) {
				visibleItemCount = visibleItemcount;
				visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
			}

			/**
			 * 滑动状态改变时被调用
			 */
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				int itemsLastIndex = adapter.getCount() - 1; // 数据集最后一项的索引
				int lastIndex = itemsLastIndex + 1; // 加上底部的loadMoreView项
				if (scrollState == OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == lastIndex) {
					// 如果是自动加载,可以在这里放置异步加载数据的代码
					Log.i("LOADMORE", "loading...");
				}
			}
		});

		content.removeAllViews();
		content.addView(log_setview, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
	}

	/**
	 * 点击按钮事件
	 * 
	 * @param view
	 */
	public void loadMore(View view) {
		loadMoreButton.setText("loading..."); // 设置按钮文字loading
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {

				loadData();

				adapter.notifyDataSetChanged(); // 数据集变化后,通知adapter
				loglist.setSelection(visibleLastIndex - visibleItemCount + 1); // 设置选中项

				loadMoreButton.setText("load more"); // 恢复按钮文字
			}
		}, 2000);
	}

	private void loadData() {
		int count = adapter.getCount();
		if (count + 20 > items.size()) {
			for (int i = count; i < items.size() - 1; i++) {
				adapter.addItem(String.valueOf((i + 1)) + ". " + items.get(i + 1));
			}
			loadMoreButton.setVisibility(View.GONE);
		} else {
			for (int i = count; i < count + 20; i++) {
				adapter.addItem(String.valueOf((i + 1)) + ". " + items.get(i + 1));
			}
		}

	}

	private void initAdapter() {
		displaylog(this.items);
		ArrayList<String> items = new ArrayList<String>();
		if (this.items.size() < 20) {
			for (int i = 0; i < this.items.size(); i++) {
				items.add(String.valueOf((i + 1)) + ". " + (String) this.items.get(i));
			}
		} else {
			for (int i = 0; i < 20; i++) {
				items.add(String.valueOf((i + 1)) + ". " + this.items.get(i));
			}
		}

		adapter = new ListViewAdapter(this, items);
	}

	private void displaylog(ArrayList<String> items) {
		String path = Environment.getExternalStorageDirectory().getPath() + "/Android/data/tw.com.blueeyes.LiveBox/";
		File file = new File(path, "debug.txt");
		// StringBuilder text = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				items.add(line);
			}
		} catch (IOException e) {
			Toast.makeText(getApplicationContext(), "File empty!!", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
		Collections.reverse(items);
	}

	private void setBtn() {
		intentlayout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				countintent++;
				if (countintent == 9) {
					countintent = 0;
					stopService(new Intent(Setting_Activity.this, Remote_Service.class));
					Intent intent = new Intent("android.intent.action.MAIN");
					if(Build.VERSION.RELEASE.equals("4.4.2")) {
						intent.setClassName("com.android.launcher3", "com.android.launcher3.Launcher");
					} else {
						intent.setClassName("com.google.android.launcher", "com.google.android.launcher.StubApp"); // package
					}
					
					// Intent intent = new Intent();
					// ComponentName comp = new
					// ComponentName("com.android.settings",
					// "com.android.settings.Settings");
					// intent.setComponent(comp);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
					startActivity(intent);
					finish();
					System.exit(0);
				}
			}
		});

		btn1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (btn_type == btn_set || btn_type == btn_logtype) {
					cam_content();
				}
				save_tem();
				btn_type = btn_cam1;
				load_tem();
				btn1.setBackgroundColor(Color.parseColor("#1714DF"));
				btn1.setTextColor(Color.WHITE);

				btn2.setBackgroundColor(Color.parseColor("#4183D7"));
				btn2.setTextColor(Color.BLACK);

				btn3.setBackgroundColor(Color.parseColor("#4183D7"));
				btn3.setTextColor(Color.BLACK);

				btn4.setBackgroundColor(Color.parseColor("#4183D7"));
				btn4.setTextColor(Color.BLACK);

				btn_setting.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_setting.setTextColor(Color.BLACK);

				btn_log.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_log.setTextColor(Color.BLACK);
			}
		});

		btn2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (btn_type == btn_set || btn_type == btn_logtype) {
					cam_content();
				}
				save_tem();
				btn_type = btn_cam2;
				load_tem();
				btn1.setBackgroundColor(Color.parseColor("#4183D7"));
				btn1.setTextColor(Color.BLACK);

				btn2.setBackgroundColor(Color.parseColor("#1714DF"));
				btn2.setTextColor(Color.WHITE);

				btn3.setBackgroundColor(Color.parseColor("#4183D7"));
				btn3.setTextColor(Color.BLACK);

				btn4.setBackgroundColor(Color.parseColor("#4183D7"));
				btn4.setTextColor(Color.BLACK);

				btn_setting.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_setting.setTextColor(Color.BLACK);

				btn_log.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_log.setTextColor(Color.BLACK);
			}
		});

		btn3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (btn_type == btn_set || btn_type == btn_logtype) {
					cam_content();
				}
				save_tem();
				btn_type = btn_cam3;
				load_tem();
				btn1.setBackgroundColor(Color.parseColor("#4183D7"));
				btn1.setTextColor(Color.BLACK);

				btn2.setBackgroundColor(Color.parseColor("#4183D7"));
				btn2.setTextColor(Color.BLACK);

				btn3.setBackgroundColor(Color.parseColor("#1714DF"));
				btn3.setTextColor(Color.WHITE);

				btn4.setBackgroundColor(Color.parseColor("#4183D7"));
				btn4.setTextColor(Color.BLACK);

				btn_setting.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_setting.setTextColor(Color.BLACK);

				btn_log.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_log.setTextColor(Color.BLACK);
			}
		});

		btn4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (btn_type == btn_set || btn_type == btn_logtype) {
					cam_content();
				}
				save_tem();
				btn_type = btn_cam4;
				load_tem();
				btn1.setBackgroundColor(Color.parseColor("#4183D7"));
				btn1.setTextColor(Color.BLACK);

				btn2.setBackgroundColor(Color.parseColor("#4183D7"));
				btn2.setTextColor(Color.BLACK);

				btn3.setBackgroundColor(Color.parseColor("#4183D7"));
				btn3.setTextColor(Color.BLACK);

				btn4.setBackgroundColor(Color.parseColor("#1714DF"));
				btn4.setTextColor(Color.WHITE);

				btn_setting.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_setting.setTextColor(Color.BLACK);

				btn_log.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_log.setTextColor(Color.BLACK);
			}
		});

		btn_setting.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (btn_type != btn_set) {
					ethernet_content();
				}
				save_tem();
				btn_type = btn_set;
				load_tem();
				btn1.setBackgroundColor(Color.parseColor("#4183D7"));
				btn1.setTextColor(Color.BLACK);

				btn2.setBackgroundColor(Color.parseColor("#4183D7"));
				btn2.setTextColor(Color.BLACK);

				btn3.setBackgroundColor(Color.parseColor("#4183D7"));
				btn3.setTextColor(Color.BLACK);

				btn4.setBackgroundColor(Color.parseColor("#4183D7"));
				btn4.setTextColor(Color.BLACK);

				btn_setting.setBackgroundColor(Color.parseColor("#1714DF"));
				btn_setting.setTextColor(Color.WHITE);

				btn_log.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_log.setTextColor(Color.BLACK);
			}
		});

		btn_log.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (btn_type != btn_logtype) {
					log_content();
				}
				save_tem();
				btn_type = btn_logtype;

				btn1.setBackgroundColor(Color.parseColor("#4183D7"));
				btn1.setTextColor(Color.BLACK);

				btn2.setBackgroundColor(Color.parseColor("#4183D7"));
				btn2.setTextColor(Color.BLACK);

				btn3.setBackgroundColor(Color.parseColor("#4183D7"));
				btn3.setTextColor(Color.BLACK);

				btn4.setBackgroundColor(Color.parseColor("#4183D7"));
				btn4.setTextColor(Color.BLACK);

				btn_setting.setBackgroundColor(Color.parseColor("#4183D7"));
				btn_setting.setTextColor(Color.BLACK);

				btn_log.setBackgroundColor(Color.parseColor("#1714DF"));
				btn_log.setTextColor(Color.WHITE);
			}
		});

		save.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				save_tem();
				if(login_pwd.equals("")) {
					String obj = "password not allowed blank.";
					Message msg = _handler.obtainMessage(1, obj);
					_handler.sendMessage(msg);
					return;
				}
				SharedPreferences.Editor editor = Camera_data.edit();
				// if (!icam1_on) {
				// if (Camera_data.getInt("show", 1) == 1) {
				// editor.putInt("show", 1);
				// }
				// }
				editor.putInt("show", 1);
				if (icam1_on != Camera_data.getBoolean("Camera1", false)) {
					icam1 = true;
				}

				if (!URL_1.equals(Camera_data.getString("Camera1_URL", "192.168.1.171"))) {
					icam1 = true;
				}
				if (Integer.valueOf(port1).intValue() != Camera_data.getInt("Camera1_Port", 80)) {
					icam1 = true;
				}
				if (!account1.equals(Camera_data.getString("Camera1_Account", "root"))) {
					icam1 = true;
				}
				if (!pwd1.equals(Camera_data.getString("Camera1_Pwd", "27507522"))) {
					icam1 = true;
				}
				if (icam_type1 != Camera_data.getInt("Camera1_type", Camera_v3)) {
					icam1 = true;
				}
				if (!label_icam1.equals(Camera_data.getString("Camera1_Label", "CH1"))) {
					icam1 = true;
				}
				if (corg_num1 != Camera_data.getInt("classorgroup1", 0)) {
					icam1 = true;
				}
				editor.putBoolean("Camera1", icam1_on);
				editor.putString("Camera1_URL", URL_1);
				editor.putInt("Camera1_Port", Integer.valueOf(port1).intValue());
				editor.putString("Camera1_Account", account1);
				editor.putString("Camera1_Pwd", pwd1);
				editor.putInt("Camera1_type", icam_type1);
				editor.putString("Camera1_Label", label_icam1);
				editor.putInt("classorgroup1", corg_num1);

				// if (!icam2_on) {
				// if (Camera_data.getInt("show", 1) == 2) {
				// editor.putInt("show", 1);
				// }
				// }
				if (icam2_on != Camera_data.getBoolean("Camera2", false)) {
					icam2 = true;
				}
				if (!URL_2.equals(Camera_data.getString("Camera2_URL", "192.168.1.172"))) {
					icam2 = true;
				}
				if (Integer.valueOf(port2).intValue() != Camera_data.getInt("Camera2_Port", 80)) {
					icam2 = true;
				}
				if (!account2.equals(Camera_data.getString("Camera2_Account", "root"))) {
					icam2 = true;
				}
				if (!pwd2.equals(Camera_data.getString("Camera2_Pwd", "27507522"))) {
					icam2 = true;
				}
				if (icam_type2 != Camera_data.getInt("Camera2_type", Camera_v3)) {
					icam2 = true;
				}
				if (!label_icam2.equals(Camera_data.getString("Camera2_Label", "CH2"))) {
					icam2 = true;
				}
				if (corg_num2 != Camera_data.getInt("classorgroup2", 0)) {
					icam2 = true;
				}
				editor.putBoolean("Camera2", icam2_on);
				editor.putString("Camera2_URL", URL_2);
				editor.putInt("Camera2_Port", Integer.valueOf(port2).intValue());
				editor.putString("Camera2_Account", account2);
				editor.putString("Camera2_Pwd", pwd2);
				editor.putInt("Camera2_type", icam_type2);
				editor.putString("Camera2_Label", label_icam2);
				editor.putInt("classorgroup2", corg_num2);

				// if (!icam3_on) {
				// if (Camera_data.getInt("show", 1) == 3) {
				// editor.putInt("show", 1);
				// }
				// }
				if (icam3_on != Camera_data.getBoolean("Camera3", false)) {
					icam3 = true;
				}
				if (!URL_3.equals(Camera_data.getString("Camera3_URL", "192.168.1.173"))) {
					icam3 = true;
				}
				if (Integer.valueOf(port3).intValue() != Camera_data.getInt("Camera3_Port", 80)) {
					icam3 = true;
				}
				if (!account3.equals(Camera_data.getString("Camera3_Account", "root"))) {
					icam3 = true;
				}
				if (!pwd3.equals(Camera_data.getString("Camera3_Pwd", "27507522"))) {
					icam3 = true;
				}
				if (icam_type3 != Camera_data.getInt("Camera3_type", Camera_v3)) {
					icam3 = true;
				}
				if (!label_icam3.equals(Camera_data.getString("Camera3_Label", "CH3"))) {
					icam3 = true;
				}
				if (corg_num3 != Camera_data.getInt("classorgroup3", 0)) {
					icam3 = true;
				}

				editor.putBoolean("Camera3", icam3_on);
				editor.putString("Camera3_URL", URL_3);
				editor.putInt("Camera3_Port", Integer.valueOf(port3).intValue());
				editor.putString("Camera3_Account", account3);
				editor.putString("Camera3_Pwd", pwd3);
				editor.putInt("Camera3_type", icam_type3);
				editor.putString("Camera3_Label", label_icam3);
				editor.putInt("classorgroup3", corg_num3);

				// if (!icam4_on) {
				// if (Camera_data.getInt("show", 1) == 4) {
				// editor.putInt("show", 1);
				// }
				// }
//				if (icam4_on != Camera_data.getBoolean("Camera4", false)) {
//					icam4 = true;
//				}
//				if (!URL_4.equals(Camera_data.getString("Camera4_URL", "192.168.1.174"))) {
//					icam4 = true;
//				}
//				if (Integer.valueOf(port4).intValue() != Camera_data.getInt("Camera4_Port", 80)) {
//					icam4 = true;
//				}
//				if (!account4.equals(Camera_data.getString("Camera4_Account", "root"))) {
//					icam4 = true;
//				}
//				if (!pwd4.equals(Camera_data.getString("Camera4_Pwd", "27507522"))) {
//					icam4 = true;
//				}
//				if (icam_type4 != Camera_data.getInt("Camera4_type", Camera_v3)) {
//					icam4 = true;
//				}
//				if (!label_icam4.equals(Camera_data.getString("Camera4_Label", "CH4"))) {
//					icam4 = true;
//				}
//				if (corg_num4 != Camera_data.getInt("classorgroup4", 0)) {
//					icam4 = true;
//				}
//				editor.putBoolean("Camera4", icam4_on);
//				editor.putString("Camera4_URL", URL_4);
//				editor.putInt("Camera4_Port", Integer.valueOf(port4).intValue());
//				editor.putString("Camera4_Account", account4);
//				editor.putString("Camera4_Pwd", pwd4);
//				editor.putInt("Camera4_type", icam_type4);
//				editor.putString("Camera4_Label", label_icam4);
//				editor.putInt("classorgroup4", corg_num4);

				
				boolean checkap = false;
				if (Camera_data.getString("hotspot", "001").equals(hotspot_ssid)) {
					checkap = true;
				}

				if (net_type != Camera_data.getInt("net_type", eth0)) {
					if (net_type == eth0) {
						MyLog.i("net", "Change network type to Ethernet , local ip is" + eth0_ip[0] + "." + eth0_ip[1]
								+ "." + eth0_ip[2] + "." + eth0_ip[3] + "by local");
					} else {
						MyLog.i("net", "Change network type to Ethernet , local ip is" + wifi_ip[0] + "." + wifi_ip[1]
								+ "." + wifi_ip[2] + "." + wifi_ip[3] + "by local");
					}
				} else {
					if (net_type == eth0) {
						if (!(eth0_ip[0] + "." + eth0_ip[1] + "." + eth0_ip[2] + "." + eth0_ip[3]).equals(Camera_data
								.getString("eth0", "192.168.1.51"))) {
							MyLog.i("net", "Change eth0 ip from " + Camera_data.getString("eth0", "192.168.1.51")
									+ " to " + eth0_ip[0] + "." + eth0_ip[1] + "." + eth0_ip[2] + "." + eth0_ip[3]
									+ " by local");
						}
					} else {
						if (!(wifi_ip[0] + "." + wifi_ip[1] + "." + wifi_ip[2] + "." + wifi_ip[3]).equals(Camera_data
								.getString("wifi", "192.168.1.51"))) {
							MyLog.i("net", "Change eth0 ip from " + Camera_data.getString("wifi", "192.168.1.51")
									+ " to " + eth0_ip[0] + "." + wifi_ip[0] + "." + wifi_ip[1] + "." + wifi_ip[2]
									+ "." + wifi_ip[3] + " by local");
						}
					}
				}

				editor.putBoolean("video_title", title_on);
				editor.putBoolean("video_time", time_on);

				editor.putInt("net_type", net_type);
				editor.putString("wifi", wifi_ip[0] + "." + wifi_ip[1] + "." + wifi_ip[2] + "." + wifi_ip[3]);
				editor.putString("eth0", eth0_ip[0] + "." + eth0_ip[1] + "." + eth0_ip[2] + "." + eth0_ip[3]);
				
				if (net_type == eth0) { 
					if (!(gateway_ip[0] + "." + gateway_ip[1] + "." + gateway_ip[2] + "." + gateway_ip[3]).equals(Camera_data
							.getString("geteway", "192.168.1.1"))) {
						try {
						Process proc = Runtime.getRuntime().exec("su");
						DataOutputStream opt = new DataOutputStream(proc.getOutputStream());
						opt = new DataOutputStream(proc.getOutputStream());
						opt.writeBytes("ip route del all\n");
						opt.writeBytes("exit\n");
						opt.flush();
						
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							Log.e("test", e.toString());
						}
					}
				}
				editor.putString("geteway", gateway_ip[0] + "." + gateway_ip[1] + "." + gateway_ip[2] + "."
						+ gateway_ip[3]);
				editor.putString("mask", mask_ip[0] + "." + mask_ip[1] + "." + mask_ip[2] + "." + mask_ip[3]);
				editor.putInt("pwd_type", pwd_type);
				editor.putString("hotspot", hotspot_ssid);
				editor.putInt("real_time", real_time);
				
				if (net_type == wifi && e_wifi_pwd != null) {
					editor.putString("wifi_pwd", e_wifi_pwd.getText().toString().trim());
				} else {
					editor.putString("wifi_pwd", wifi_pwd);
				}
				editor.putString("wifi_ssid", wifi_ssid.trim());

				// editor.putString("login_account", login_account);
				editor.putString("login_pwd", login_pwd);
				editor.commit();

				if (icam1) {
					MyLog.i("CH1", "camera1 data has been changed by local");
				}
				if (icam2) {
					MyLog.i("CH2", "camera2 data has been changed by local");
				}
				if (icam3) {
					MyLog.i("CH3", "camera3 data has been changed by local");
				}
//				if (icam4) {
//					MyLog.i("CH4", "camera4 data has been changed by local");
//				}

				if (Camera_data.getInt("net_type", wifi) == eth0) {
					Log.v("ap", "準備設定AP");
					if (!checkap) {
						if (ApManager.isApOn(Setting_Activity.this)) {
							ApManager.configApState(Setting_Activity.this);
						}
					}
				}

				Intent intent = new Intent();
				if(Camera_data.getBoolean("OnDemand", false)) {
					intent.setClass(Setting_Activity.this, Ondemand_LiveBox_Activity.class);
				} else {
					intent.setClass(Setting_Activity.this, LiveBox_Activity.class);
				}
				startActivity(intent);
				finish();
				System.exit(0);
			}
		});

		cancel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				SharedPreferences.Editor editor = Camera_data.edit();
				editor.putInt("show", 1);
				editor.commit();
				Intent intent = new Intent();
				if(Camera_data.getBoolean("OnDemand", false)) {
					intent.setClass(Setting_Activity.this, Ondemand_LiveBox_Activity.class);
				} else {
					intent.setClass(Setting_Activity.this, LiveBox_Activity.class);
				}
				startActivity(intent);
				finish();
				System.exit(0);
			}
		});
	}

	public void realtime_Clicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.r_close:
	            if (checked)
	            	real_time = realtime_close;
	            break;
	        case R.id.r_general:
	            if (checked)
	            	real_time = realtime_general;
	            break;
	        case R.id.r_smooth:
	            if (checked)
	            	real_time = realtime_smooth;
	            break;
	    }
	}
	
	private BroadcastReceiver wifi_receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			results = wifi_magager.getScanResults();
			wifi_size = results.size();
			Log.v("ssid", "size " + wifi_size);

			list.clear();
			String[] tem;
			int getindex = -1;
			if (wifi_size >= 0) {
				for (int i = 0; i < wifi_size; i++) {
					HashMap<String, Object> item = new HashMap<String, Object>();
					tem = results.get(i).toString().split(",");
					String ssid_tem = tem[0].split(":")[1].trim();
					if (ssid_tem.indexOf(wifi_ssid) != -1) {
						getindex = i;
					}
					item.put("SSID", ssid_tem);
					item.put("PWD_TYPE", tem[2].split(":")[1].trim());
					Log.v("abc", tem[2].split(":")[1].trim());
					list.add(item);
				}

				unregisterReceiver(wifi_receiver);
				wifi_adapter.notifyDataSetChanged();
				if (getindex >= 0) {
					ssid.setSelection(getindex);
				} else {
					ssid.setSelection(0);
				}
			}
		}
	};

	private class ListViewAdapter extends BaseAdapter {
		private List<String> items;
		private LayoutInflater inflater;

		public ListViewAdapter(Context context, List<String> items) {
			this.items = items;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			return items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				view = inflater.inflate(R.layout.list_item, null);
			}
			TextView text = (TextView) view.findViewById(R.id.log_text);
			text.setText(items.get(position));
			return view;
		}

		/**
		 * 添加列表项
		 * 
		 * @param item
		 */
		public void addItem(String item) {
			items.add(item);
		}
	}

	private class WIFI_Adapter extends BaseAdapter {
		private ArrayList<HashMap<String, Object>> items;
		private LayoutInflater inflater;

		public WIFI_Adapter(Context context, ArrayList<HashMap<String, Object>> items) {
			this.items = items;
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			return items.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				view = inflater.inflate(R.layout.myspinner, null);
			}
			TextView text = (TextView) view.findViewById(R.id.text1);
			text.setText((String) items.get(position).get("SSID"));

			return view;
		}
	}

	/* 計算 String="255.255.255.0" to int=24 多少個1 */
	public int netMaskToInt(String netmask) {
		// networkPrefixLength
		String[] maskarray = netmask.split("\\.");
		int number1 = 0;
		for (int i = 0; i < maskarray.length; i++) {
			String binaryMask = Integer.toBinaryString(Integer.parseInt(maskarray[i])); // 255
																						// ->
																						// 11111111
			// System.out.println(maskarray[i]+"="+numeber);
			String[] ary = binaryMask.split("");
			for (int j = 0; j < ary.length; j++) {
				if (ary[j].equals("1")) {
					number1++;
				}
			}
		}
		// System.out.println("網路字首長度="+number1);
		return number1;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Log.v("IR", " " + keyCode);
		return false;
		// return super.onKeyDown(keyCode, event);
	}
}
