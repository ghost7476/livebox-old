package tw.com.blueeyes.LiveBox;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.util.Log;
import android.view.Surface;

public class Audio_decode {
	// implements OnPlaybackPositionUpdateListener {
	int minBufferSize;
	boolean getdata = false;
	String TAG = "ULAW";
	long startTime = 0;
	long endTime = 0;
	long totTime = 0;
	byte[] audio;
	byte[] tem;
	boolean start = false;
	int count = 0;
	boolean inputfinish = false;
	MediaFormat format;
	MediaCodec mediaCodec;
	MediaCodec.BufferInfo info;
	Audio_PCM pcm;
	int audio_type = 0;
	final int aac = 0x0a;
	final int g711 = 0x0b;
	int samplerate;
	int channelcount;
	int bitrate;

	public Audio_decode() {
		initMeiacodec();
		info = new MediaCodec.BufferInfo();
	}

	public void write(final byte audio_data[], Audio_PCM pcm) {
		try {

			this.pcm = pcm;
			ByteBuffer[] inputBuffers = mediaCodec.getInputBuffers();
			// MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
			ByteBuffer[] outputBuffers = mediaCodec.getOutputBuffers();

			int inIndex = mediaCodec.dequeueInputBuffer(0);
			if (inIndex >= 0) {
				ByteBuffer buffer = inputBuffers[inIndex];
				buffer.clear();
				buffer.put(audio_data);
				mediaCodec.queueInputBuffer(inIndex, 0, audio_data.length, System.currentTimeMillis(), 0);
			}
			inputfinish = true;
			int outIndex = mediaCodec.dequeueOutputBuffer(info, 0);
			switch (outIndex) {
			case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
				outputBuffers = mediaCodec.getOutputBuffers();
				break;
			case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
				Log.d(TAG, "New format " + mediaCodec.getOutputFormat());
				format = mediaCodec.getOutputFormat();
				// audioTrack.setPlaybackRate(mMediaFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE));
				break;
			case MediaCodec.INFO_TRY_AGAIN_LATER:
				Log.d(TAG, "dequeue audio OutputBuffer timed out!");
				break;
			default:
				ByteBuffer tmpBuffer = outputBuffers[outIndex];
				final byte[] chunk = new byte[info.size];
				tmpBuffer.get(chunk); // Read the buffer all at once
				tmpBuffer.clear(); // ** MUST DO!!! OTHERWISE THE NEXT TIME YOU
									// GET
									// THIS SAME BUFFER BAD THINGS WILL HAPPEN

				if (chunk.length > 0) {
					pcm.write(chunk);
				}
				mediaCodec.releaseOutputBuffer(outIndex, false /* render */);
				break;
			}

		} catch (Exception e) {
			if (audio_type == g711) {
				G711();
			} else {
				AAC(samplerate, channelcount, bitrate);
			}
		}
	}

	// Thread show_audio = new Thread() {
	// public void run() {
	// MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
	// ByteBuffer[] outputBuffers = mediaCodec.getOutputBuffers();
	//
	// while (true) {
	// try {
	// if (inputfinish) {
	// inputfinish = false;
	// int outIndex = mediaCodec.dequeueOutputBuffer(info, 0);
	// switch (outIndex) {
	// case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
	// outputBuffers = mediaCodec.getOutputBuffers();
	// break;
	// case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
	// Log.d(TAG, "New format " + mediaCodec.getOutputFormat());
	// format = mediaCodec.getOutputFormat();
	// //
	// audioTrack.setPlaybackRate(mMediaFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE));
	// break;
	// case MediaCodec.INFO_TRY_AGAIN_LATER:
	// Log.d(TAG, "dequeueOutputBuffer timed out!");
	// break;
	// default:
	// ByteBuffer tmpBuffer = outputBuffers[outIndex];
	// final byte[] chunk = new byte[info.size];
	// tmpBuffer.get(chunk); // Read the buffer all at once
	// tmpBuffer.clear(); // ** MUST DO!!! OTHERWISE THE
	// // NEXT TIME YOU GET THIS SAME
	// // BUFFER BAD THINGS WILL HAPPEN
	//
	// if (chunk.length > 0) {
	// pcm.write(chunk);
	// }
	// // count = 0;
	// mediaCodec.releaseOutputBuffer(outIndex, false);
	// break;
	// }
	//
	// } else {
	// // Log.v(TAG, "wait data");
	// Thread.sleep(5);
	//
	// if (!LiveBox_Activity.run) {
	// break;
	// }
	// }
	// } catch (Exception e) {
	// Log.v(TAG, "show 掛了");
	// Log.v(TAG, e.toString());
	// }
	// }
	//
	// mediaCodec.stop();
	// mediaCodec.release();
	// }
	// };

	public void flush() {
		mediaCodec.flush();
	}

	private void initMeiacodec() {
		audio_type = g711;
		String key_mime = "audio/g711-mlaw";
		mediaCodec = MediaCodec.createDecoderByType(key_mime);
		format = MediaFormat.createAudioFormat(key_mime, 8192, 1);
		try {
			mediaCodec.configure(format, null, null, 0);
		} catch (Exception e) {
			Log.v("error", e.toString());
			e.getStackTrace();
		}
		mediaCodec.start();
		// show_audio.start();
	}

	protected void G711() {
		audio_type = g711;
		Log.v("decode", "G711 rest");
		// flush();
		if (mediaCodec == null) {
			mediaCodec.stop();
			mediaCodec.release();
			mediaCodec = null;
		}
		String key_mime = "audio/g711-mlaw";
		mediaCodec = MediaCodec.createDecoderByType(key_mime);
		format = MediaFormat.createAudioFormat(key_mime, 8192, 1);
		try {
			mediaCodec.configure(format, null, null, 0);
		} catch (Exception e) {
			Log.v("error", e.toString());
			e.getStackTrace();
		}
		mediaCodec.start();
		// show_audio.start();
		info = new MediaCodec.BufferInfo();
	}

	protected void AAC(int samplerate, int channelcount, int bitrate) {
		audio_type = aac;
		this.samplerate = samplerate;
		this.channelcount = channelcount;
		this.bitrate = bitrate;
		Log.v("test", "AAC rest");
//		samplerate = (samplerate / 1000) * 1024;
		// flush();
		if (mediaCodec == null) {
			mediaCodec.stop();
			mediaCodec.release();
			mediaCodec = null;
		}

		// String key_mime = "audio/mp4a-latm";
		mediaCodec = MediaCodec.createDecoderByType("audio/mp4a-latm");
		// Log.v("test", samplerate + " " + channelcount);
		// format = MediaFormat.createAudioFormat("audio/mp4a-latm", samplerate,
		// channelcount);

		MediaFormat format = new MediaFormat();
		format.setString(MediaFormat.KEY_MIME, "audio/mp4a-latm");
//		format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectHE);
		format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, channelcount);
		format.setInteger(MediaFormat.KEY_SAMPLE_RATE, samplerate);
//		format.setInteger(MediaFormat.KEY_BIT_RATE, bitrate);
		format.setInteger(MediaFormat.KEY_IS_ADTS, 1);
		byte[] bytes = new byte[] { (byte) 0x11, (byte) 0x90 };
//		 byte[] bytes = new byte[]{(byte) 0x12, (byte)0x12};
		ByteBuffer bb = ByteBuffer.wrap(bytes);
		format.setByteBuffer("csd-0", bb);
		
		try {
			mediaCodec.configure(format, null, null, 0);
		} catch (Exception e) {
			Log.v("error", e.toString());
			e.getStackTrace();
		}
		mediaCodec.start();
		// show_audio.start();
		info = new MediaCodec.BufferInfo();

	}

}
