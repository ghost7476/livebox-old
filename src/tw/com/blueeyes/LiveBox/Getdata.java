package tw.com.blueeyes.LiveBox;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.net.SocketFactory;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jcodec.codecs.h264.H264Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.AudioFormat;
import android.net.Credentials;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Getdata {
	SharedPreferences Camera_data;
	String boundary = "";
	char[] boundary_C;
	int Data_Type = 0x00;
	int Video_Type = -1;
	final int MP4_Type = 0x2a;
	final int H264_Type = 0x2b;
	int Audio_Type = 0x00;
	final int PCM_Type = 0x1a;
	final int ULAW_Type = 0x1b;
	final int AAC_Type = 0x1c;
	final int video_data = 0x0a;
	final int audio_data = 0x0b;
	final int image_data = 0x0c;
	byte[] data_send = null;
	// InputStream is;
	long video_frametime = 0;
	long startTime = 0;
	long endTime = 0;
	long totTime = 0;
	int data_length = 0;
	int send_count = 0;
	boolean flush = false;
	int video_frame = 28;
	int audio_frame = 25;
	Audio_PCM AUDIO;
	Audio_decode audio_decode;
	Decode_h264 H264;
	// Decode_mp4 MP4;
	String IP;
	int PORT;
	String ACCOUNT;
	String PWD;
	Boolean show = false;
	Boolean run = true;
	TextView Time, Frame;
	Button disconnecttext;
	// int camera_id = 0;
	final int camera1 = 0xa1;
	final int camera2 = 0xa2;
	final int camera3 = 0xa3;
	final int camera4 = 0xa4;
	int Camera_v2 = 0x00;
	int Camera_v3 = 0x01;
	int SES_class = 0x02;
	int SES_group = 0x03;
	int camera_type = 0;
	boolean isFirstIFrame = true;
	final char[] hexArray = "0123456789ABCDEF".toCharArray();
	long counttime = 0;
	String hr, min, sec;
	int now_time = 0;
	int audio_nowtime = 0;
	int now_time_tem = 0;
	int group_calss_num = 0;
	boolean no_connect = false;
	boolean videodata_get = false;
	String demarcation = "--BlueEyes\r\n";
	ImageView image;
	ImageView disconnect;
	boolean dis_connect = true;
	int count_live = 0;
	int count_frame = 0;
	int count_aframe = 0;
	int count_frame_s = 0;
	int count_aframe_s = 0;
	int error_count = 0;
	int ID;
	boolean first = true;
	int countloss = 0;
	String video_time;
	String audio_time;

	boolean nextboundary = false;
	char temp_char = 0, temp_char2 = 0, temp_char3 = 0, temp_char1 = 0;
	String data_len[] = null;
	String tem_time[] = null;
	String video_frame_a[] = null;
	String audio_frame_a[] = null;
	long temtime = 0;
	Boolean audio_send = true;
	Boolean video_send = true;
	Boolean audio_pre = false;
	Boolean video_pre = false;
	byte audio[] = null;
	byte video[] = null;
	// boolean first_vframe = false;
	// boolean first_aframe = false;
	BlockingQueue<byte[]> videoQunue;
	// BlockingQueue<byte[]> _put_videoQunue1;
	// BlockingQueue<byte[]> _put_videoQunue2;
	BlockingQueue<byte[]> audioQunue;
	// BlockingQueue<byte[]> _put_audioQunue1;
	// BlockingQueue<byte[]> _put_audioQunue2;
	// BlockingQueue<byte[]> _get_videoQunue;
	// BlockingQueue<byte[]> _get_audioQunue;
	// BlockingQueue<Long> count_vtimeQunue;
	// BlockingQueue<Long> count_atimeQunue;
	long start_aTime;
	long decode_aTime;
	long start_gettime = 0;
	long cost_gettime = 1000;
	int count_vsec = 0;
	int count_asec = 0;
	int avg_vframe = 26;
	int avg_aframe = 26;
	int total_frame = 0;
	int total_aframe = 0;
	boolean audio_continue = false;
	// boolean frame30 = false;
	int samplerate = 0;
	int channelcount = 0;
	int bitrate = 0;
	Context Context;
	long get_vframe_time = 0;
	long put_vframe_time = 0;
	long get_aframe_time = 0;
	long put_aframe_time = 0;
	boolean video_bug = false;
	boolean audio_bug = false;
	int id_v = 0;
	int id_a = 0;
	int count_sec = 0;
	boolean iframe = false;
	int allow_v = 3;
	int allow_a = 3;
	boolean video_fast = false;
	boolean audio_fast = false;
	boolean av_same = false;
	float timecheck;
	boolean videoclean = false;
	boolean audioclean = false;
	int allow_d = 2;
	boolean firstvframe = true;
	int countoframe = 0;
	InputStream input;
	Bitmap bitmap;
	WeakReference weak_image;
	int real_time = 0x00;
	final int realtime_close = 0x0a;
	final int realtime_general = 0x0b;
	final int realtime_smooth = 0x0c;
	DefaultHttpClient mHttpClient;

	// WeakReference<byte[]> weak_data;
	public Getdata(Context context, int id, Button disabletext, ImageView disconnect, ImageView image, TextView frame,
			TextView timetext, Audio_decode g711, Audio_PCM pcm, Decode_h264 h264, String ip, int port, String account,
			String pwd, int camera, int cameratype, int corg_num) {
		Camera_data = PreferenceManager.getDefaultSharedPreferences(context);
		real_time = Camera_data.getInt("real_time", realtime_smooth);
		AUDIO = pcm;
		audio_decode = g711;
		H264 = h264;
		// MP4 = mp4;
		IP = ip;
		PORT = port;
		ACCOUNT = account;
		PWD = pwd;
		Time = timetext;
		Context = context;
		counttime = System.currentTimeMillis();
		// camera_id = camera;
		camera_type = cameratype;
		group_calss_num = corg_num;
		this.image = image;
		this.disconnect = disconnect;
		ID = id;
		disconnecttext = disabletext;
		Frame = frame;
		MyLog.i("Thread", "camera" + id + " " + ip + " connect");
		// weak_data = new WeakReference<byte[]>(data_send);
		weak_image = new WeakReference<Bitmap>(bitmap);
		audioQunue = new LinkedBlockingQueue<byte[]>(300);
		videoQunue = new LinkedBlockingQueue<byte[]>(300);
		// count_vtimeQunue = new LinkedBlockingQueue<Long>(7);
		// count_atimeQunue = new LinkedBlockingQueue<Long>(7);
		// _put_videoQunue1 = new LinkedBlockingQueue<byte[]>(100);
		// _put_videoQunue2 = new LinkedBlockingQueue<byte[]>(100);
		// _put_audioQunue1 = new LinkedBlockingQueue<byte[]>(100);
		// _put_audioQunue2 = new LinkedBlockingQueue<byte[]>(100);
		send_audio.start();
		send_video.start();
	}

	void getHttp() {
		Log.v("test", "start");
		Log.v("test", "ip" + IP);
		if (mHttpClient != null) {
			mHttpClient.getConnectionManager().shutdown();
			mHttpClient = null;
		}
		try {
			if (input != null) {
				input.close();
				input = null;
			}

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			Log.v("test", "inputstream close bug" + e1.toString());
		} catch (Exception e1) {
			Log.v("test", "inputstream close bug" + e1.toString());
		}

		System.gc();
		try {
			no_connect = false;
			count_live++;
			if (count_live >= 1000) {
				count_live = 0;
			}

			// Log.i("Thread", "camera " + IP);
			// Log.v("camera_type", "" + camera_type);
			if (camera_type == Camera_v3 || camera_type == Camera_v2) {
				icam();
			} else if (camera_type == SES_class) {
				ses("Class", group_calss_num);
			} else if (camera_type == SES_group) {
				ses("Group", group_calss_num);
			}
		} catch (ConnectTimeoutException e) {
			// Log.v("error", e.toString() + IP);
			disconnect();
			if (error_count < 3) {
				error_count++;
				MyLog.i("Thread", "camera " + ID + " " + IP + " disconnect");
			}
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (run) {
				new Thread() {
					public void run() {
						getHttp();
					}
				}.start();
			} else {
				try {
					if (mHttpClient != null) {
						mHttpClient.getConnectionManager().shutdown();
						mHttpClient = null;
					}
					if (input != null) {
						input.close();
						input = null;
					}

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					Log.v("test", "inputstream close bug" + e1.toString());
				} catch (Exception e1) {
					Log.v("test", "inputstream close bug" + e1.toString());
				}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			// Log.v("error", e.toString() + IP);
			disconnect();
			MyLog.i("Thread", "camera " + ID + " " + IP + " ip data maybe not correct");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			disconnect();
			Log.v("error", e.toString() + IP);
			MyLog.i("Thread", "camera " + ID + " " + IP + " ip data maybe not correct");
		} catch (IOException e) {
			MyLog.i("Thread", "camera " + ID + " " + IP + " disconnect");
			Log.v("error", e.toString() + IP);
			disconnect();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (run) {
				new Thread() {
					public void run() {
						getHttp();
					}
				}.start();
			} else {
				try {
					if (mHttpClient != null) {
						mHttpClient.getConnectionManager().shutdown();
						mHttpClient = null;
					}
					if (input != null) {
						input.close();
						input = null;
					}

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					Log.v("test", "inputstream close bug" + e1.toString());
				} catch (Exception e1) {
					Log.v("test", "inputstream close bug" + e1.toString());
				}
			}
		} catch (Exception e) {
			MyLog.i("Thread", "camera" + ID + " " + IP + " disconnect");
			Log.v("error", e.toString() + IP);
			disconnect();

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (run) {
				new Thread() {
					public void run() {
						getHttp();
					}
				}.start();
			} else {
				try {
					if (mHttpClient != null) {
						mHttpClient.getConnectionManager().shutdown();
						mHttpClient = null;
					}
					if (input != null) {
						input.close();
						input = null;
					}

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					Log.v("test", "inputstream close bug" + e1.toString());
				} catch (Exception e1) {
					Log.v("test", "inputstream close bug" + e1.toString());
				}
			}
		}
		first = false;
	}

	private void disconnect() {
		dis_connect = true;
		if (first) {
			if (show) {
				if (disconnect.getVisibility() == View.GONE) {
					Message msg1 = new Message();
					msg1.what = 0x1a;
					handler.sendMessage(msg1);
				}
			}
		} else {
			if (show) {
				if (disconnect.getVisibility() == View.GONE) {
					Message msg1 = new Message();
					msg1.what = 0x0d;
					handler.sendMessage(msg1);
				}
			}
		}

	}

	public void ses(String GorC, int num) throws Exception {
		String urlstr = "http://" + IP + ":" + PORT + "/sesstream.cgi?Audio=2&" + GorC + "=" + num;
		String username = ACCOUNT;
		String password = PWD;
		HttpGet mGet = new HttpGet(urlstr);
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 1500;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 1500;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpConnectionParams.setTcpNoDelay(httpParameters, true);

		mHttpClient = new DefaultHttpClient(httpParameters);
		mGet.setHeader("Authorization",
				"Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP));
		HttpResponse hr = mHttpClient.execute(mGet);
		int res = hr.getStatusLine().getStatusCode();
		if (res == 200) {
			System.out.println("get request send and receive 200");
			Header[] headers = hr.getAllHeaders();
			for (int i = 0; i < headers.length; i++) {
				Header h = headers[i];
				// Log.i("http getdata", "Header names: " + h.getName());
				// Log.i("http getdata", "Header Value: " + h.getValue());
				if (h.getName().equalsIgnoreCase("Content-Type") || h.getName().equalsIgnoreCase("ContentType")) {
					int position = h.getValue().indexOf("boundary=");
					boundary = h.getValue().substring(position + 9, h.getValue().length());
					// Log.v("http getdata", "boundary " + boundary);
					boundary_C = boundary.toCharArray();
					// Log.v("http getdata", "boundary_C " +
					// boundary_C.length);
				}
			}
			input = hr.getEntity().getContent();
			readStream(input);
		} else {
			disconnect();
			if (error_count < 3) {
				error_count++;
				if (res == 401) {
					MyLog.i("Thread", "camera" + ID + " " + IP + " account or password not correct.");
				} else {
					MyLog.i("Thread", "camera" + ID + " " + IP + " can not connect.");
				}
			}
			System.out.println("get request send and receive " + res);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (run) {
				new Thread() {
					public void run() {
						getHttp();
					}
				}.start();
			} else {
				try {
					if (input != null) {
						input.close();
						input = null;
					}

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					Log.v("test", "inputstream close bug" + e1.toString());
				} catch (Exception e1) {
					Log.v("test", "inputstream close bug" + e1.toString());
				}
			}
		}

		// String line = null;
		// StringBuilder builder = new StringBuilder();
		// BufferedReader reader = new BufferedReader(new
		// InputStreamReader(hr.getEntity().getContent()));
		// while ((line = reader.readLine()) != null) {
		// Log.v("http getdata", line);
		// }
		// Log.v("test" , "data end");

	}

	public void icam() throws Exception {
		// 1. 獲取並設置url地址，一個字符串變量，一個URL對象
		String urlStr;
		if (camera_type == Camera_v2) {
			urlStr = "http://" + IP + ":" + PORT + "/ipcam/stream.cgi?nowprofileid=2&audiostream=1";
		} else {
			// Log.v("thread", "icam3");
			urlStr = "http://" + IP + ":" + PORT + "/ipcam/stream.cgi";
		}
		URL url = new URL(urlStr);
		// 2. 創建一個密碼證書，(UsernamePasswordCredentials類)
		String username = ACCOUNT;
		String password = PWD;
		UsernamePasswordCredentials upc = new UsernamePasswordCredentials(username, password);
		// 3. 設置認證範圍 (AuthScore類)
		String strRealm = "iCam1080";
		String strHost = url.getHost();
		int iPort = url.getPort();
		AuthScope as = new AuthScope(strHost, iPort, strRealm);
		// 4. 創建認證提供者(BasicCredentials類) ，基於as和upc
		BasicCredentialsProvider bcp = new BasicCredentialsProvider();
		bcp.setCredentials(as, upc);
		// 5. 創建Http客戶端(DefaultHttpClient類)
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 1500;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 1500;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		mHttpClient = new DefaultHttpClient(httpParameters);
		// 6. 為此客戶端設置認證提供者
		mHttpClient.setCredentialsProvider(bcp);
		// 7. 創建一個get 方法(HttpGet類)，基於所訪問的url地址
		HttpGet hg = new HttpGet(urlStr);
		// 8. 執行get方法並返回 response
		HttpResponse hr = mHttpClient.execute(hg);
		int res = hr.getStatusLine().getStatusCode();
		// 9. 從response中取回數據，使用InputStreamReader讀取Response的Entity：
		if (res == 200) {
			Header[] headers = hr.getAllHeaders();
			for (int i = 0; i < headers.length; i++) {
				Header h = headers[i];
				// Log.i("http getdata", "Header names: " + h.getName());
				// Log.i("http getdata", "Header Value: " + h.getValue());
				if (h.getName().equalsIgnoreCase("Content-Type") || h.getName().equalsIgnoreCase("ContentType")) {
					int position = h.getValue().indexOf("boundary=");
					boundary = h.getValue().substring(position + 9, h.getValue().length());
					// Log.v("http getdata", "boundary " + boundary);
					boundary_C = boundary.toCharArray();
					// Log.v("http getdata", "boundary_C " + boundary_C.length);
				}
			}
			input = hr.getEntity().getContent();
			readStream(input);
		} else {
			disconnect();
			if (error_count < 3) {
				error_count++;
				if (res == 401) {
					MyLog.i("Thread", "camera" + ID + " " + IP + " account or password not correct.");
				} else {
					MyLog.i("Thread", "camera" + ID + " " + IP + " can not connect.");
				}
			}
			System.out.println("get request send and receive " + res);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (run) {
				new Thread() {
					public void run() {
						getHttp();
					}
				}.start();
			} else {
				try {
					if (input != null) {
						input.close();
						input = null;
					}

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					Log.v("test", "inputstream close bug" + e1.toString());
				} catch (Exception e1) {
					Log.v("test", "inputstream close bug" + e1.toString());
				}
			}
		}

		// String line = null;
		// StringBuilder builder = new StringBuilder();
		// BufferedReader reader = new BufferedReader(new
		// InputStreamReader(hr.getEntity().getContent()));
		// while ((line = reader.readLine()) != null) {
		// Log.v("http getdata", line);
		// }

		// readStream(hr.getEntity().getContent());

	}

	public void readStream(InputStream in) {
		audio_continue = false;
		first = false;
		error_count = 0;
		dis_connect = false;
		int buffer_count = 128 * 1024;
		byte[] buffer = new byte[buffer_count];
		int videotag = 0;
		int audiotag = 0;
		List<byte[]> bytes = new ArrayList<byte[]>();

		boolean first = true;
		boolean pass = false;
		int x = 0;
		// String header = "";
		nextboundary = false;
		boolean boundary_flag = false;
		boolean fulldata = false;
		int data_start = 0;
		int i = 0;
		int IFrame = 65;
		boolean nodata = false;
		int no_data_count = 0;
		byte[] getdata_tem1 = null;
		byte[] getdata_tem2 = null;

		try {
			while (!Thread.interrupted() || run) {
				if (show) {
					if (disconnect.getVisibility() == View.VISIBLE) {
						Message msg1 = new Message();
						msg1.what = 0x0e;
						handler.sendMessage(msg1);
					}
				}
				int readCount = 0;
				int readtem = 0;
				try {
					while (readCount < buffer_count && run) { // 資料流組合
						readtem += in.read(buffer, readCount, buffer_count - readCount);
						if (readtem == -1) {
							Log.i("no data1", "no data");
							nodata = true;
							break;
						}
						readCount = readtem;
						// Log.i("test", "get data " + readCount);
					}

				} catch (Exception e) {
					// Log.v("http getdata", e.toString());
					// MyLog.e("http getdata", e.toString());

					if (readCount <= 0) {
						Log.i("http getdata", "exception readCount " + readCount);
						nodata = true;
						break;
					}
				}

				count_live++;
				if (count_live >= 1000) {
					count_live = 0;
				}

				if (nodata) {
					Log.i("http getdata", "no data");
					break;
				}

				if (show) {
					// inputstream
					i = 0;
					data_start = 0;
					while (i < readCount) {

						// ****first boundary*****
						while (first) {
							for (; x < boundary_C.length; i++, x++) {
								if ((char) buffer[i] != boundary_C[x]) {
									x = 0;
									break;
								}

								if (x == boundary_C.length - 1) {
									// if (group_calss_num > 0 && new
									// String(buffer,
									// "UTF-8").indexOf("image/jpeg") != -1) {
									// fulldata = true;
									// data_tem1 = Arrays.copyOfRange(buffer, 0,
									// readCount);
									// } else {
									x = 0;
									nextboundary = true;
									first = false;
									data_start = i + 1;
									break;
									// }
								}
							}

							if (nextboundary) {
								break;
							} else {
								i++;
							}
						}

						// Log.v("test", "thread live " + run);
						// ****boundary*****
						while (nextboundary) {

							for (; x < boundary_C.length && i < readCount; i++, x++) {
								if ((char) buffer[i] != boundary_C[x]) {
									if (((char) buffer[i] == '-') && (x > 0)) {
										x--; // 可能會有多的--
									} else {
										boundary_flag = false;
										x = 0;
										break;
									}

								}

								if (x == boundary_C.length - 1) {
									// if(show) {
									// Log.w("data", "get boundary");
									// }

									x = 0;
									int data_tem = data_start;
									data_start = i + 1;
									nextboundary = false;
									fulldata = true;
									if (pass) {
										bytes.clear();
										pass = false;
										getdata_tem2 = Arrays.copyOfRange(buffer, 0, data_start);
										bytes.add(getdata_tem1);
										bytes.add(getdata_tem2);
										getdata_tem1 = streamCopy(bytes);
									} else {
										getdata_tem1 = Arrays.copyOfRange(buffer, data_tem, data_start);
									}
								}

								if (i >= readCount - 1) { // 重要,不然超過index
									boundary_flag = true;
									// Log.e("bug", "x " + x);
									break;
								}
							}
							if (fulldata) {
								break;
							} else {

								if (i >= readCount - 1) {
									// 跳出前先把目前的data記錄
									// Log.d("test", "test4 ");
									// if (boundary_cut) {
									// Log.e("bug", "boundary_cut " + x + " " +
									// (char) buffer[i]);
									// // 可能是boundary被切斷 +1 判斷
									// x++;
									// }
									if (i > readCount - 1) {
										Log.e("bug", "i " + i);
									}

									if (pass) {
										// Log.e("bug", "pass2 " +
										// data_tem1.length);
										// pass2 = true;
										getdata_tem2 = Arrays.copyOfRange(buffer, 0, readCount);
										bytes.add(getdata_tem1);
										bytes.add(getdata_tem2);
										getdata_tem1 = streamCopy(bytes);

										// pass = false;
										// first = true;
									} else {
										getdata_tem1 = Arrays.copyOfRange(buffer, data_start, readCount);
									}

									if (boundary_flag) {
										x++;
									}

									pass = true;
									// Log.d("bug", "pass");
									break;
								} else {
									i++;
								}
							}
						}

						if (fulldata) {
							String header = "";
							for (int s = 0; s + 3 < getdata_tem1.length; s++) {

								temp_char = (char) getdata_tem1[s];
								temp_char1 = (char) getdata_tem1[s + 1];
								temp_char2 = (char) getdata_tem1[s + 2];
								temp_char3 = (char) getdata_tem1[s + 3];

								if (temp_char == '\r' && temp_char1 == '\n' && temp_char2 == '\r' && temp_char3 == '\n') {
									for (int l = 0; l < s; l++) {
										header += "" + (char) getdata_tem1[l];
									}

									// if (show) {
									// Log.d("http getdata", header);
									// }
									channelcount = 0;
									bitrate = 0;
									samplerate = 0;
									String[] data_header = header.split("\r\n");
									for (int z = 0; z < data_header.length; z++) {
										if (data_header[z].indexOf("Content-Type") != -1) {
											if (data_header[z].indexOf("video/h264") != -1
													|| data_header[z].indexOf("image/avc") != -1
													|| data_header[z].indexOf("video/x-h264") != -1) {
												Data_Type = video_data;
												Video_Type = H264_Type;

											} else if (data_header[z].indexOf("audio/x-wav") != -1) {
												Data_Type = audio_data;
												Audio_Type = PCM_Type;
												if (Audio_Type != AAC_Type) {
													Audio_Type = PCM_Type;
												}
											} else if (data_header[z].indexOf("audio/x-mulaw") != -1) {
												Data_Type = audio_data;
												Audio_Type = ULAW_Type;

											} else if (data_header[z].indexOf("image/jpeg") != -1) {
												Data_Type = image_data;
												no_connect = true;
											} else if (data_header[z].indexOf("video/") != -1) {
												MyLog.i("Deocde", "camera" + ID + " " + IP
														+ " video formate not correct.");
											}
										}

										if (data_header[z].indexOf("X-Codec") != -1) {
											if (data_header[z].indexOf("AAC") != -1) {
												Audio_Type = AAC_Type;
											}
										}

										if (data_header[z].indexOf("X-SampleRate") != -1) {
											samplerate = Integer.valueOf(
													data_header[z].replace("X-SampleRate", "").replace(":", "").trim())
													.intValue();
											// Log.v("test" , "sample rate" +
											// samplerate);
										}

										if (data_header[z].indexOf("X-AChannels") != -1) {
											channelcount = Integer.valueOf(
													data_header[z].replace("X-AChannels", "").replace(":", "").trim())
													.intValue();
											// Log.v("test" , "sample rate" +
											// samplerate);
										}

										if (data_header[z].indexOf("X-Bitrate") != -1) {
											bitrate = Integer.valueOf(
													data_header[z].replace("X-Bitrate", "").replace(":", "").trim())
													.intValue();
										}

										if (show) {
											if (data_header[z].indexOf("X-Resolution") != -1) {
												if (data_header[z].indexOf("1920*1080") != -1) {
													if (LiveBox_Activity.width != 1920
															|| LiveBox_Activity.height != 1080) {
														LiveBox_Activity.width = 1920;
														LiveBox_Activity.height = 1080;
														H264.setformat(Ondemand_LiveBox_Activity.width,
																Ondemand_LiveBox_Activity.height);
													}
												} else if (data_header[z].indexOf("1280*720") != -1) {
													if (LiveBox_Activity.width != 1280
															|| LiveBox_Activity.height != 720) {
														LiveBox_Activity.width = 1280;
														LiveBox_Activity.height = 720;
														H264.setformat(Ondemand_LiveBox_Activity.width,
																Ondemand_LiveBox_Activity.height);
													}
												}
											}
										}

										if (data_header[z].indexOf("Content-Length") != -1) {
											data_len = data_header[z].split(":");
											data_length = Integer.valueOf(data_len[1].trim()).intValue();
										}

										if (data_header[z].indexOf("X-Framerate") != -1) {
											if (Data_Type == video_data) {
												video_frame_a = data_header[z].split(":");
												int v_frame = (int) Float.valueOf(video_frame_a[1].trim()).floatValue();
												// Log.v("frame",
												// "video frame "
												// + video_frame);
												if (v_frame == 0) {
													video_frame = 28;
												} else {
													video_frame = v_frame;
												}
											}
										}

										// else {
										// video_frame = avg_vframe;
										// }

										if (data_header[z].indexOf("X-Time") != -1) {
											tem_time = data_header[z].split(":");
											temtime = Long.valueOf(tem_time[1].trim()).longValue();
											// Log.v("test", "" + temtime);
											if (Data_Type == video_data) {
												now_time = (int) temtime;
											} else if (Data_Type == audio_data) {
												audio_nowtime = (int) temtime;
											}
										}

										if (data_header[z].indexOf("X-Timestamp") != -1) {
											tem_time = data_header[z].split(":");
											temtime = Long.valueOf(tem_time[1].trim()).longValue();
											// Log.v("test", "" + temtime);
											if (Data_Type == video_data) {
												now_time = (int) (temtime / 1000);
											} else if (Data_Type == audio_data) {
												audio_nowtime = (int) (temtime / 1000);
											}
										}
									}

									data_send = Arrays.copyOfRange(getdata_tem1, s + 4, s + 4 + data_length);

									if (data_send.length == data_length) {
										if (show) {
											count_live++;
											if (count_live >= 1000) {
												count_live = 0;
											}

											if (Data_Type == audio_data) {
												if ((!isFirstIFrame) || audio_continue) {
													count_aframe++;
													if (!audio_continue) {
														id_a = 0;
														if (Audio_Type == PCM_Type) {
															if (channelcount == 0) {
																AUDIO.clsoe();
																AUDIO.initAudioTrack();
															} else {
																if (channelcount == 1) {
																	AUDIO.clsoe();
																	AUDIO.initAudioTrack(samplerate,
																			AudioFormat.CHANNEL_OUT_MONO);
																} else if (channelcount == 2) {
																	AUDIO.clsoe();
																	AUDIO.initAudioTrack(samplerate,
																			AudioFormat.CHANNEL_OUT_STEREO);
																}
															}

														} else if (Audio_Type == ULAW_Type) {
															audio_decode.G711();
															AUDIO.clsoe();
															AUDIO.initAudioTrack();
														} else if (Audio_Type == AAC_Type) {
															audio_decode.AAC(samplerate, channelcount, bitrate);
															AUDIO.clsoe();
															if (channelcount == 1) {
																AUDIO.initAudioTrack(samplerate,
																		AudioFormat.CHANNEL_OUT_MONO);
															} else if (channelcount == 2) {
																AUDIO.initAudioTrack(samplerate,
																		AudioFormat.CHANNEL_OUT_STEREO);
															}
														}
														audio_continue = true;
													}

													if (!isFirstIFrame) {
														count_aframe_s++;
														audioQunue.put(data_send);
													}
												}
											} else if (Data_Type == video_data) {
												if (image.getVisibility() == View.VISIBLE) {
													Message msg1 = new Message();
													msg1.what = 0x0c;
													handler.sendMessage(msg1);
												}

												if (Video_Type == H264_Type) {
													count_frame++;
													if (isFirstIFrame) {
														if (checkiframe(data_send)) {
															Log.d("http getdata", header);

															isFirstIFrame = false;

															if (videoQunue != null) {
																videoQunue.put(data_send);
															}
														}
													} else {
														videoQunue.put(data_send);
														// checkiframe(data_send);
													}
													if (now_time != now_time_tem && audioQunue != null) {
														Message msg = new Message();
														msg.what = 0x0a;
														msg.arg1 = now_time;
														handler.sendMessage(msg);
														now_time_tem = now_time;
													}
												} else {
													Message msg = new Message();
													msg.what = 0x0f;
													handler.sendMessage(msg);
												}

											} else if (Data_Type == image_data) {
												Message msg = new Message();
												msg.what = 0x0b;
												handler.sendMessage(msg);
											}
											// data_send = null;
										}
									} else {
										Log.d("test", "data_length " + data_length + "get data " + data_send.length);
									}

									break;
								}
							}

							getdata_tem1 = null;
							nextboundary = true;
							fulldata = false;
						} else {
							Thread.sleep(2);
						}

						if (i >= readCount - 1 || i < 0) {
							break;
						}

						if (!run) {
							break;
						}

						count_live++;
						if (count_live >= 1000) {
							count_live = 0;
						}

						if (!run || nodata || no_connect) {
							break;
						}
					}

					if (!run || nodata || no_connect) {
						break;
					}
				}
			}
		} catch (Exception e) {
			// MyLog.e("bug", "exception " + e.toString());
			// MyLog.i("Thread" ,"camera " + IP + " handler image data");
			if (!run || Thread.currentThread().isInterrupted() || no_connect) {
				// MyLog.e("bug", "run or interupt or no_connect");
				try {
					if (mHttpClient != null) {
						mHttpClient.getConnectionManager().shutdown();
						mHttpClient = null;
					}
					if (in != null) {
						in.close();
						in = null;
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					Log.v("test", "inputstream close bug" + e1.toString());
				}
			}
		}

		try {
			if (mHttpClient != null) {
				mHttpClient.getConnectionManager().shutdown();
				mHttpClient = null;
			}
			if (in != null) {
				in.close();
				in = null;
			}
			Log.v("bug", "over");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			Log.v("bug", "inputstream close bug" + e1.toString());
		}

		if ((no_connect && run) || nodata) {
			count_live++;
			if (count_live >= 1000) {
				count_live = 0;
			}

			try {
				if (nodata) {
					nodata = false;
					disconnect();
					Thread.sleep(500);
					MyLog.i("Thread", "camera" + ID + " " + IP + " data timeout.");
				} else {
					Thread.sleep(500);
				}
				if (run) {
					new Thread() {
						public void run() {
							getHttp();
						}
					}.start();
				} else {
					try {
						if (mHttpClient != null) {
							mHttpClient.getConnectionManager().shutdown();
							mHttpClient = null;
						}
						if (in != null) {
							in.close();
							in = null;
						}

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						Log.v("test", "inputstream close bug" + e1.toString());
					} catch (Exception e1) {
						Log.v("test", "inputstream close bug" + e1.toString());
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	Thread send_audio = new Thread() {
		public void run() {
			while (LiveBox_Activity.run) {
				try {
					if (show) {
						if (audioQunue != null) {
							if (audioQunue.size() > 0) {
								// Log.v("test", "1 audiosize" +
								// audioQunue.size());
								start_aTime = System.currentTimeMillis();
								audio = audioQunue.take();

								if (Audio_Type == PCM_Type) {
									AUDIO.write(audio);
								} else {
									audio_decode.write(audio, AUDIO);
								}
								decode_aTime = System.currentTimeMillis() - start_aTime;
								if (avg_aframe == 0) {
									avg_aframe = 26;
								}

								if (real_time == realtime_smooth) {
									if (audioQunue.size() <= avg_aframe / 2) {
										Thread.sleep((1100) / avg_aframe);
									} else if (audioQunue.size() <= avg_aframe) {
										Thread.sleep(1000 / avg_aframe);
									}
								} else if (real_time == realtime_general) {
									if (audioQunue.size() <= avg_aframe / 4) {
										Thread.sleep((1100) / avg_aframe);
									} else if (audioQunue.size() <= avg_aframe / 2) {
										Thread.sleep(1000 / avg_aframe);
									}
								} else if (real_time == realtime_close) {
									if (audioQunue.size() <= avg_aframe / 4) {
										Thread.sleep((1100) / avg_aframe);
									}
								}

								if (real_time == realtime_smooth) {
									if (audioQunue.size() >= avg_aframe * 1.3) {
										while (audioQunue.size() > (avg_aframe * 0.5)) {
											audio = audioQunue.take();
											audio = null;
										}
									}
								} else if (real_time == realtime_general) {
									if (audioQunue.size() >= avg_aframe * 0.8) {
										while (audioQunue.size() > (avg_aframe * 0.35)) {
											audio = audioQunue.take();
											audio = null;
										}
									}
								} else if (real_time == realtime_close) {
									if (audioQunue.size() >= avg_aframe * 0.6) {
										audioQunue.clear();
									}
								}

								// if (audioQunue.size() > avg_aframe * 2) {
								// audioQunue.clear();
								// }
							}
							audio = null;
						}
					} else {
						Thread.sleep(1000 / 30);
						while (audioQunue.size() > avg_aframe) {
							audio = audioQunue.take();
							audio = null;
						}
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				} catch (Exception e) {
					MyLog.i("Thread", "camera" + ID + " " + IP + " audio stop decode.");
					e.printStackTrace();
					// break;
				}
			}
		}
	};

	Thread send_video = new Thread() {
		public void run() {
			while (LiveBox_Activity.run) {
				try {
					if (show) {
						if (videoQunue != null) {
							if (videoQunue.size() > 0) {
								// Log.v("test", "1 videosize" +
								// videoQunue.size());
								video = videoQunue.take();
								H264.get_h264(video, 1000000 / video_frame);

								if (avg_vframe == 0) {
									avg_vframe = 26;
								}
								if (real_time == realtime_smooth) {
									if (videoQunue.size() <= avg_vframe / 2) {
										Thread.sleep(1100 / avg_vframe);
									} else if (videoQunue.size() <= avg_vframe) {
										Thread.sleep(1000 / avg_vframe);
									}
								} else if (real_time == realtime_general) {
									if (videoQunue.size() <= avg_vframe / 4) {
										Thread.sleep(1100 / avg_vframe);
									} else if (videoQunue.size() <= avg_vframe / 2) {
										Thread.sleep(1000 / avg_vframe);
									}
								} else if (real_time == realtime_close) {
									if (videoQunue.size() <= avg_vframe / 4) {
										Thread.sleep(1100 / avg_vframe);
									}
								}

								if (videoQunue.size() > avg_vframe * 2) {
									videoQunue.clear();
								}
							} else {
								// Log.v("test" , "2 videosize"+
								// videoQunue.size() );
								Thread.sleep(100);
							}
							video = null;
						}
					} else {
						Thread.sleep(1000 / 30);
						while (videoQunue.size() > avg_vframe) {
							video = videoQunue.take();
							video = null;
						}
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					Log.e("bug", "video stop decode, interupt");
					e.printStackTrace();
					break;
				} catch (Exception e) {
					MyLog.i("Thread", "camera" + ID + " " + IP + " video stop decode.");
					e.printStackTrace();
					// break;
				}
			}
		}
	};

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0x0a:
				if (show) {
					if (group_calss_num == 0) {
						if (((((msg.arg1 % 86400) / 3600) + 8) % 24) < 10) {
							hr = "0" + ((((msg.arg1 % 86400) / 3600) + 8) % 24);
						} else {
							hr = "" + ((((msg.arg1 % 86400) / 3600) + 8) % 24);
						}
					} else {
						if (((msg.arg1 % 86400) / 3600) < 10) {
							hr = "0" + ((msg.arg1 % 86400) / 3600);
						} else {
							hr = "" + ((msg.arg1 % 86400) / 3600);
						}
					}

					if (((msg.arg1 % 86400) % 3600) / 60 < 10) {
						min = "0" + ((msg.arg1 % 86400) % 3600) / 60;
					} else {
						min = "" + ((msg.arg1 % 86400) % 3600) / 60;
					}

					if (((msg.arg1 % 86400) % 60) < 10) {
						sec = "0" + (msg.arg1 % 86400) % 60;
					} else {
						sec = "" + (msg.arg1 % 86400) % 60;
					}

					if ((count_frame < 20)) {
						if (Frame.getCurrentTextColor() != Color.RED) {
							Frame.setTextColor(Color.RED);
						}
					} else {
						if (Frame.getCurrentTextColor() != 0xE4F1FE) {
							Frame.setTextColor(Color.parseColor("#E4F1FE"));
						}
					}
					Frame.setText("V" + count_frame + "/" + H264.count_frame + "/A" + count_aframe + "/"
							+ AUDIO.count_frame);

					if (Remote_Service.debug) {
						Time.setText(video_time + " " + videoQunue.size()
						/* + num_video.size() */+ " " + audioQunue.size()
						/* + num_audio.size()) */);
					} else {
						Time.setText(video_time);
					}
					video_time = hr + ":" + min + ":" + sec;
					// Time.setText(video_time + " " + audio_time);

					count_sec++;
					// if ((count_aframe >= 13 && count_aframe <= 17) ||
					// (count_aframe >= 23 && count_aframe <= 27)
					// || (count_aframe >= 40 && count_aframe <= 50)) {
					total_aframe = total_aframe + count_aframe;

					if (count_sec >= 10) {
						avg_aframe = total_aframe / count_sec;
						// Log.v("test", "avg_aframe " + avg_aframe);
						total_aframe = 0;
						count_sec = 0;
					}
					// }

					count_vsec++;
					// if (count_frame >= 26 || count_frame <= 35) {
					total_frame = total_frame + count_frame;

					if (count_vsec >= 10) {
						avg_vframe = total_frame / count_vsec;
						total_frame = 0;
						count_vsec = 0;

						if (avg_vframe < 20) {
							avg_vframe = 26;
						} else if (avg_vframe > 50) {
							avg_vframe = 26;
						}
						// Log.v("test", "avg_vframe" + avg_vframe);
					}
					// }

					H264.count_frame = 0;
					AUDIO.count_frame = 0;
				}
				count_frame = 0;
				count_aframe = 0;
				System.gc();
				break;
			case 0x0b:
				if (show) {
					if (image.getVisibility() == View.GONE) {
						Log.v("test", "showimage");
						image.setVisibility(View.VISIBLE);
					}
					bitmap = BitmapFactory.decodeByteArray(data_send, 0, data_send.length);
					image.setImageBitmap(bitmap);
					Time.setText("");
					weak_image.clear();
					bitmap = null;
				}
				break;
			case 0x0c:
				image.setVisibility(View.GONE);
				break;
			case 0x0d:
				disconnect.setVisibility(View.VISIBLE);
				Frame.setText("V" + 0 + "/" + 0 + "/A" + 0 + "/" + 0);
				break;
			case 0x0e:
				disconnecttext.setVisibility(View.GONE);
				disconnect.setVisibility(View.GONE);
				break;
			case 0x0f:
				Time.setTextColor(Color.parseColor("#FF0000"));
				Time.setText("Only Support H.264");
				break;
			case 0x1a:
				disconnecttext.setVisibility(View.VISIBLE);
				disconnect.setVisibility(View.VISIBLE);
				disconnecttext.setText("Disconnected");
				break;
			case 0x1b:
				if (group_calss_num == 0) {
					if (((((msg.arg1 % 86400) / 3600) + 8) % 24) < 10) {
						hr = "0" + ((((msg.arg1 % 86400) / 3600) + 8) % 24);
					} else {
						hr = "" + ((((msg.arg1 % 86400) / 3600) + 8) % 24);
					}
				} else {
					if (((msg.arg1 % 86400) / 3600) < 10) {
						hr = "0" + ((msg.arg1 % 86400) / 3600);
					} else {
						hr = "" + ((msg.arg1 % 86400) / 3600);
					}
				}

				if (((msg.arg1 % 86400) % 3600) / 60 < 10) {
					min = "0" + ((msg.arg1 % 86400) % 3600) / 60;
				} else {
					min = "" + ((msg.arg1 % 86400) % 3600) / 60;
				}

				if (((msg.arg1 % 86400) % 60) < 10) {
					sec = "0" + (msg.arg1 % 86400) % 60;
				} else {
					sec = "" + (msg.arg1 % 86400) % 60;
				}

				audio_time = hr + ":" + min + ":" + sec;
				Time.setText(video_time + " " + audio_time);
			}
		}
	};

	public void clearbuffer() {
		try {
			while (audioQunue.size() > 0) {
				audio = audioQunue.take();
				audio = null;
			}
			while (videoQunue.size() > 0) {
				video = videoQunue.take();
				video = null;
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// audioQunue.clear();
		// videoQunue.clear();
		isFirstIFrame = true;
		audio_continue = false;
	}

	public String getEncodeBASE64String(String s) {
		if (s == null)
			return null;
		return Base64.encodeToString(s.getBytes(), Base64.DEFAULT);
	}

	public boolean checkiframe(byte[] data) {
		String x = bytesToHex(data);
		// Log.v("test", x);
		if (x.indexOf("00000167") != -1 || x.indexOf("00000165") != -1) {
			// count_vsec++;
			// count_asec++;
			// if (start_gettime == 0) {
			// cost_gettime = 1000;
			// } else {
			// cost_gettime = System.currentTimeMillis() - start_gettime;
			// if (cost_gettime < 1000) {
			// cost_gettime = 1100;
			// } else if (cost_gettime > 1700) {
			// cost_gettime = 1100;
			// }
			// // Log.v("test" , "cost_gettime" + cost_gettime);
			// }

			// if (count_frame_s >= 26 || count_frame_s <= 35) {
			// total_frame = total_frame + count_frame_s;
			//
			// if (count_vsec >= 10) {
			// avg_vframe = total_frame / count_vsec;
			// total_frame = 0;
			// count_vsec = 0;
			//
			// if(avg_vframe < 20) {
			// avg_vframe = 26;
			// } else if(avg_vframe > 50) {
			// avg_vframe = 26;
			// }
			// // Log.v("test", "avg_vframe" + avg_vframe);
			// }
			// }

			// if (count_aframe_s >= 15 || count_aframe_s <= 50) {
			// total_aframe = total_aframe + count_aframe_s;
			//
			// if (count_asec >= 10) {
			// avg_aframe = total_aframe / count_asec;
			// total_aframe = 0;
			// count_asec = 0;
			//
			// if(avg_aframe < 20) {
			// avg_aframe = 25;
			// } else if(avg_aframe > 50) {
			// avg_aframe = 25;
			// }
			// Log.v("test", "avg_aframe" + avg_aframe);
			// }
			// }

			// Log.v("test" , "avg_aframe " + avg_aframe + " avg_vframe " +
			// avg_vframe);

			// Log.v("test", ID + "i frame " + count_frame_s + " " +
			// count_aframe_s);

			// count_frame_s = 0;
			// count_aframe_s = 0;
			// count_frame_s++;
			// start_gettime = System.currentTimeMillis();
			return true;
		} else {
			// count_frame_s++;
			return false;
		}
	}

	public boolean checkiframe(ByteBuffer data) {
		int marker = 0xffffffff;
		while (data.hasRemaining()) {
			int b = data.get() & 0xff;
			if (marker == 1) {
				if ((b & 0x1f) == 5)
					return true;
			}
			marker = (marker << 8) | b;
		}
		return false;
	}

	public String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static byte[] streamCopy(List<byte[]> srcArrays) {
		byte[] destAray = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			for (byte[] srcArray : srcArrays) {
				bos.write(srcArray);
			}
			bos.flush();
			destAray = bos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bos.close();
			} catch (IOException e) {
			}
		}
		return destAray;
	}

	public static byte[] sysCopy(List<byte[]> srcArrays) {
		int len = 0;
		for (byte[] srcArray : srcArrays) {
			len += srcArray.length;
		}
		byte[] destArray = new byte[len];
		int destLen = 0;
		for (byte[] srcArray : srcArrays) {
			System.arraycopy(srcArray, 0, destArray, destLen, srcArray.length);
			destLen += srcArray.length;
		}
		return destArray;
	}
}
