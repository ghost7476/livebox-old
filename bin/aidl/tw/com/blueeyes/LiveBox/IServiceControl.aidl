package tw.com.blueeyes.LiveBox;

interface IServiceControl{
	void sendOK();
	void sendFail();
	void sendTYPE(String cmd);
	void sendSetting();
	void send_OndemandSetting();
	void sendvolumetype();
	void setondemand();
	void restoredefault();
	void check_ondemand();
	void sendloop_type(boolean loop);
}