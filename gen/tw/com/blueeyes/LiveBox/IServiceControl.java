/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: D:\\Blueeyes_android eclipse workspace\\LiveBox\\src\\tw\\com\\blueeyes\\LiveBox\\IServiceControl.aidl
 */
package tw.com.blueeyes.LiveBox;
public interface IServiceControl extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements tw.com.blueeyes.LiveBox.IServiceControl
{
private static final java.lang.String DESCRIPTOR = "tw.com.blueeyes.LiveBox.IServiceControl";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an tw.com.blueeyes.LiveBox.IServiceControl interface,
 * generating a proxy if needed.
 */
public static tw.com.blueeyes.LiveBox.IServiceControl asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof tw.com.blueeyes.LiveBox.IServiceControl))) {
return ((tw.com.blueeyes.LiveBox.IServiceControl)iin);
}
return new tw.com.blueeyes.LiveBox.IServiceControl.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_sendOK:
{
data.enforceInterface(DESCRIPTOR);
this.sendOK();
reply.writeNoException();
return true;
}
case TRANSACTION_sendFail:
{
data.enforceInterface(DESCRIPTOR);
this.sendFail();
reply.writeNoException();
return true;
}
case TRANSACTION_sendTYPE:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.sendTYPE(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_sendSetting:
{
data.enforceInterface(DESCRIPTOR);
this.sendSetting();
reply.writeNoException();
return true;
}
case TRANSACTION_send_OndemandSetting:
{
data.enforceInterface(DESCRIPTOR);
this.send_OndemandSetting();
reply.writeNoException();
return true;
}
case TRANSACTION_sendvolumetype:
{
data.enforceInterface(DESCRIPTOR);
this.sendvolumetype();
reply.writeNoException();
return true;
}
case TRANSACTION_setondemand:
{
data.enforceInterface(DESCRIPTOR);
this.setondemand();
reply.writeNoException();
return true;
}
case TRANSACTION_restoredefault:
{
data.enforceInterface(DESCRIPTOR);
this.restoredefault();
reply.writeNoException();
return true;
}
case TRANSACTION_check_ondemand:
{
data.enforceInterface(DESCRIPTOR);
this.check_ondemand();
reply.writeNoException();
return true;
}
case TRANSACTION_sendloop_type:
{
data.enforceInterface(DESCRIPTOR);
boolean _arg0;
_arg0 = (0!=data.readInt());
this.sendloop_type(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements tw.com.blueeyes.LiveBox.IServiceControl
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void sendOK() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_sendOK, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void sendFail() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_sendFail, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void sendTYPE(java.lang.String cmd) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(cmd);
mRemote.transact(Stub.TRANSACTION_sendTYPE, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void sendSetting() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_sendSetting, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void send_OndemandSetting() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_send_OndemandSetting, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void sendvolumetype() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_sendvolumetype, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void setondemand() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_setondemand, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void restoredefault() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_restoredefault, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void check_ondemand() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_check_ondemand, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void sendloop_type(boolean loop) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(((loop)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_sendloop_type, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_sendOK = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_sendFail = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_sendTYPE = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_sendSetting = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_send_OndemandSetting = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_sendvolumetype = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_setondemand = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_restoredefault = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_check_ondemand = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_sendloop_type = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
}
public void sendOK() throws android.os.RemoteException;
public void sendFail() throws android.os.RemoteException;
public void sendTYPE(java.lang.String cmd) throws android.os.RemoteException;
public void sendSetting() throws android.os.RemoteException;
public void send_OndemandSetting() throws android.os.RemoteException;
public void sendvolumetype() throws android.os.RemoteException;
public void setondemand() throws android.os.RemoteException;
public void restoredefault() throws android.os.RemoteException;
public void check_ondemand() throws android.os.RemoteException;
public void sendloop_type(boolean loop) throws android.os.RemoteException;
}
